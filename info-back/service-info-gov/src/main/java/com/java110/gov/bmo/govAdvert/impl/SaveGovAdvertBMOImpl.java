package com.java110.gov.bmo.govAdvert.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.context.DataFlowContext;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.gov.bmo.govAdvert.ISaveGovAdvertBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovAdvertInnerServiceSMO;
import com.java110.intf.gov.IGovAdvertItemInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govAdvert.GovAdvertPo;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service("saveGovAdvertBMOImpl")
public class SaveGovAdvertBMOImpl implements ISaveGovAdvertBMO {

    @Autowired
    private IGovAdvertInnerServiceSMO govAdvertInnerServiceSMOImpl;

    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    @Autowired
    private IGovAdvertItemInnerServiceSMO govAdvertItemInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param reqJson
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(JSONObject reqJson) {
        GovAdvertPo govAdvertPo = BeanConvertUtil.covertBean(reqJson, GovAdvertPo.class);
        govAdvertPo.setAdvertId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_advertId));
        int flag = govAdvertInnerServiceSMOImpl.saveGovAdvert(govAdvertPo);

        if (hasKeyAndValue(reqJson, "photos") && reqJson.getJSONArray("photos").size() > 0) {
            JSONArray photos = reqJson.getJSONArray("photos");
            for (int _photoIndex = 0; _photoIndex < photos.size(); _photoIndex++) {
                addAdvertFileRel(photos.getString(_photoIndex),govAdvertPo.getAdvertId(),"40000");
                addAdvertItem(govAdvertPo.getAdvertId(), photos.getString(_photoIndex), govAdvertPo.getCaId(), _photoIndex+"", "8888");//8888 图片 9999 视频
            }
        } else {
            addAdvertFileRel(reqJson.getString("vedioName"),govAdvertPo.getAdvertId(),"50000");
            addAdvertItem(govAdvertPo.getAdvertId(), reqJson.getString("vedioName"), govAdvertPo.getCaId(), "0", "9999");//8888 图片 9999 视频
        }
        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    private boolean hasKeyAndValue(JSONObject paramIn, String key) {
        if (!paramIn.containsKey(key)) {
            return false;
        }
        if (StringUtil.isEmpty(paramIn.getString(key))) {
            return false;
        }
        return true;
    }

    public void addAdvertFileRel(String fileName,String objId,String relType) {
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(fileName);
        fileRelPo.setObjId(objId);
        fileRelPo.setRelType(relType);
        fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
    }
    public void addAdvertItem(String advertId, String url, String caId, String seq, String itemTypeCd) {
        GovAdvertItemPo govAdvertItemPo = new GovAdvertItemPo();
        govAdvertItemPo.setAdvertItemId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_advertItemId));
        govAdvertItemPo.setAdvertId(advertId);
        govAdvertItemPo.setItemTypeCd(itemTypeCd);
        govAdvertItemPo.setSeq(seq);
        govAdvertItemPo.setUrl(url);
        govAdvertItemPo.setCaId(caId);
        govAdvertItemInnerServiceSMOImpl.saveGovAdvertItem(govAdvertItemPo);
    }
}
