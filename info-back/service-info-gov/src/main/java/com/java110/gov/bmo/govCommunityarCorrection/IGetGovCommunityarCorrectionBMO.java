package com.java110.gov.bmo.govCommunityarCorrection;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
public interface IGetGovCommunityarCorrectionBMO {


    /**
     * 查询矫正者
     * add by wuxw
     * @param  govCommunityarCorrectionDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityarCorrectionDto govCommunityarCorrectionDto);


}
