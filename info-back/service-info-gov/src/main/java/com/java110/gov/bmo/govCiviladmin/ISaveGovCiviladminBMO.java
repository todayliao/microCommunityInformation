package com.java110.gov.bmo.govCiviladmin;

import com.java110.po.govCiviladmin.GovCiviladminPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCiviladminBMO {


    /**
     * 添加民政服务宣传
     * add by wuxw
     * @param govCiviladminPo
     * @return
     */
    ResponseEntity<String> save(GovCiviladminPo govCiviladminPo);


}
