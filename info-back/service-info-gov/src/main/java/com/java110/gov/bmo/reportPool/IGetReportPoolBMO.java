package com.java110.gov.bmo.reportPool;
import com.java110.dto.reportPool.ReportPoolDto;
import org.springframework.http.ResponseEntity;
public interface IGetReportPoolBMO {


    /**
     * 查询报事管理
     * add by wuxw
     * @param  reportPoolDto
     * @return
     */
    ResponseEntity<String> get(ReportPoolDto reportPoolDto);

    /**
     * 查询报事管理
     * add by wuxw
     * @param  reportPoolDto
     * @return
     */
    ResponseEntity<String> quseryReportStaff(ReportPoolDto reportPoolDto);
    /**
     * 查询报事管理
     * add by wuxw
     * @param  reportPoolDto
     * @return
     */
    ResponseEntity<String> queryStaffFinishReport(ReportPoolDto reportPoolDto);

    /**
     * 查询报事管理
     * add by wuxw
     * @param  reportPoolDto
     * @return
     */
    ResponseEntity<String> quseryReportFinishStaffs(ReportPoolDto reportPoolDto);
    /**
     * 查询报事管理
     * add by wuxw
     * @param  reportPoolDto
     * @return
     */
    ResponseEntity<String> quseryLargeReportCount(ReportPoolDto reportPoolDto);


}
