package com.java110.gov.bmo.govPartyOrgContext;
import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPartyOrgContextBMO {


    /**
     * 查询党组织简介
     * add by wuxw
     * @param  govPartyOrgContextDto
     * @return
     */
    ResponseEntity<String> get(GovPartyOrgContextDto govPartyOrgContextDto);


}
