package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovAdvertServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 广告服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govAdvertServiceDaoImpl")
//@Transactional
public class GovAdvertServiceDaoImpl extends BaseServiceDao implements IGovAdvertServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovAdvertServiceDaoImpl.class);





    /**
     * 保存广告信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovAdvertInfo(Map info) throws DAOException {
        logger.debug("保存广告信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govAdvertServiceDaoImpl.saveGovAdvertInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存广告信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询广告信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovAdvertInfo(Map info) throws DAOException {
        logger.debug("查询广告信息 入参 info : {}",info);

        List<Map> businessGovAdvertInfos = sqlSessionTemplate.selectList("govAdvertServiceDaoImpl.getGovAdvertInfo",info);

        return businessGovAdvertInfos;
    }

    /**
     * 查询广告信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovAdvertItmesInfo(Map info) throws DAOException {
        logger.debug("查询广告信息 入参 info : {}",info);

        List<Map> businessGovAdvertInfos = sqlSessionTemplate.selectList("govAdvertServiceDaoImpl.getGovAdvertItems",info);

        return businessGovAdvertInfos;
    }

    /**
     * 修改广告信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovAdvertInfo(Map info) throws DAOException {
        logger.debug("修改广告信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govAdvertServiceDaoImpl.updateGovAdvertInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改广告信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询广告数量
     * @param info 广告信息
     * @return 广告数量
     */
    @Override
    public int queryGovAdvertsCount(Map info) {
        logger.debug("查询广告数据 入参 info : {}",info);

        List<Map> businessGovAdvertInfos = sqlSessionTemplate.selectList("govAdvertServiceDaoImpl.queryGovAdvertsCount", info);
        if (businessGovAdvertInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovAdvertInfos.get(0).get("count").toString());
    }


}
