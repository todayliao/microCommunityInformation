package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovProtectionCaseServiceDao;
import com.java110.intf.gov.IGovProtectionCaseInnerServiceSMO;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
import com.java110.po.govProtectionCase.GovProtectionCasePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 周边重点人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovProtectionCaseInnerServiceSMOImpl extends BaseServiceSMO implements IGovProtectionCaseInnerServiceSMO {

    @Autowired
    private IGovProtectionCaseServiceDao govProtectionCaseServiceDaoImpl;


    @Override
    public int saveGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo) {
        int saveFlag = 1;
        govProtectionCaseServiceDaoImpl.saveGovProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govProtectionCasePo));
        return saveFlag;
    }

     @Override
    public int updateGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo) {
        int saveFlag = 1;
         govProtectionCaseServiceDaoImpl.updateGovProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govProtectionCasePo));
        return saveFlag;
    }

     @Override
    public int deleteGovProtectionCase(@RequestBody  GovProtectionCasePo govProtectionCasePo) {
        int saveFlag = 1;
        govProtectionCasePo.setStatusCd("1");
        govProtectionCaseServiceDaoImpl.updateGovProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govProtectionCasePo));
        return saveFlag;
    }

    @Override
    public List<GovProtectionCaseDto> queryGovProtectionCases(@RequestBody  GovProtectionCaseDto govProtectionCaseDto) {

        //校验是否传了 分页信息

        int page = govProtectionCaseDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govProtectionCaseDto.setPage((page - 1) * govProtectionCaseDto.getRow());
        }

        List<GovProtectionCaseDto> govProtectionCases = BeanConvertUtil.covertBeanList(govProtectionCaseServiceDaoImpl.getGovProtectionCaseInfo(BeanConvertUtil.beanCovertMap(govProtectionCaseDto)), GovProtectionCaseDto.class);

        return govProtectionCases;
    }


    @Override
    public int queryGovProtectionCasesCount(@RequestBody GovProtectionCaseDto govProtectionCaseDto) {
        return govProtectionCaseServiceDaoImpl.queryGovProtectionCasesCount(BeanConvertUtil.beanCovertMap(govProtectionCaseDto));    }

    public IGovProtectionCaseServiceDao getGovProtectionCaseServiceDaoImpl() {
        return govProtectionCaseServiceDaoImpl;
    }

    public void setGovProtectionCaseServiceDaoImpl(IGovProtectionCaseServiceDao govProtectionCaseServiceDaoImpl) {
        this.govProtectionCaseServiceDaoImpl = govProtectionCaseServiceDaoImpl;
    }
}
