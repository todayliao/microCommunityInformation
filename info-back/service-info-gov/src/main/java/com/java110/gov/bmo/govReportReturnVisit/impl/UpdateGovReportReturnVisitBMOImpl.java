package com.java110.gov.bmo.govReportReturnVisit.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportReturnVisit.IUpdateGovReportReturnVisitBMO;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovReportReturnVisitBMOImpl")
public class UpdateGovReportReturnVisitBMOImpl implements IUpdateGovReportReturnVisitBMO {

    @Autowired
    private IGovReportReturnVisitInnerServiceSMO govReportReturnVisitInnerServiceSMOImpl;

    /**
     *
     *
     * @param govReportReturnVisitPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovReportReturnVisitPo govReportReturnVisitPo) {

        int flag = govReportReturnVisitInnerServiceSMOImpl.updateGovReportReturnVisit(govReportReturnVisitPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
