package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPartyOrgContextServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 党组织简介服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPartyOrgContextServiceDaoImpl")
//@Transactional
public class GovPartyOrgContextServiceDaoImpl extends BaseServiceDao implements IGovPartyOrgContextServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPartyOrgContextServiceDaoImpl.class);





    /**
     * 保存党组织简介信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPartyOrgContextInfo(Map info) throws DAOException {
        logger.debug("保存党组织简介信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPartyOrgContextServiceDaoImpl.saveGovPartyOrgContextInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存党组织简介信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询党组织简介信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPartyOrgContextInfo(Map info) throws DAOException {
        logger.debug("查询党组织简介信息 入参 info : {}",info);

        List<Map> businessGovPartyOrgContextInfos = sqlSessionTemplate.selectList("govPartyOrgContextServiceDaoImpl.getGovPartyOrgContextInfo",info);

        return businessGovPartyOrgContextInfos;
    }


    /**
     * 修改党组织简介信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPartyOrgContextInfo(Map info) throws DAOException {
        logger.debug("修改党组织简介信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPartyOrgContextServiceDaoImpl.updateGovPartyOrgContextInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改党组织简介信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询党组织简介数量
     * @param info 党组织简介信息
     * @return 党组织简介数量
     */
    @Override
    public int queryGovPartyOrgContextsCount(Map info) {
        logger.debug("查询党组织简介数据 入参 info : {}",info);

        List<Map> businessGovPartyOrgContextInfos = sqlSessionTemplate.selectList("govPartyOrgContextServiceDaoImpl.queryGovPartyOrgContextsCount", info);
        if (businessGovPartyOrgContextInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPartyOrgContextInfos.get(0).get("count").toString());
    }


}
