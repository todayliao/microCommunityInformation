package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCarInout.GovCarInoutDto;
import com.java110.gov.bmo.govCarInout.IDeleteGovCarInoutBMO;
import com.java110.gov.bmo.govCarInout.IGetGovCarInoutBMO;
import com.java110.gov.bmo.govCarInout.ISaveGovCarInoutBMO;
import com.java110.gov.bmo.govCarInout.IUpdateGovCarInoutBMO;
import com.java110.po.govCarInout.GovCarInoutPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCarInout")
public class GovCarInoutApi {

    @Autowired
    private ISaveGovCarInoutBMO saveGovCarInoutBMOImpl;
    @Autowired
    private IUpdateGovCarInoutBMO updateGovCarInoutBMOImpl;
    @Autowired
    private IDeleteGovCarInoutBMO deleteGovCarInoutBMOImpl;

    @Autowired
    private IGetGovCarInoutBMO getGovCarInoutBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCarInout/saveGovCarInout
     * @path /app/govCarInout/saveGovCarInout
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCarInout", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCarInout(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "inoutId", "请求报文中未包含inoutId");


        GovCarInoutPo govCarInoutPo = BeanConvertUtil.covertBean(reqJson, GovCarInoutPo.class);
        return saveGovCarInoutBMOImpl.save(govCarInoutPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCarInout/updateGovCarInout
     * @path /app/govCarInout/updateGovCarInout
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCarInout", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCarInout(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "inoutId", "请求报文中未包含inoutId");
        Assert.hasKeyAndValue(reqJson, "inoutId", "inoutId不能为空");


        GovCarInoutPo govCarInoutPo = BeanConvertUtil.covertBean(reqJson, GovCarInoutPo.class);
        return updateGovCarInoutBMOImpl.update(govCarInoutPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCarInout/deleteGovCarInout
     * @path /app/govCarInout/deleteGovCarInout
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCarInout", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCarInout(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "inoutId", "inoutId不能为空");


        GovCarInoutPo govCarInoutPo = BeanConvertUtil.covertBean(reqJson, GovCarInoutPo.class);
        return deleteGovCarInoutBMOImpl.delete(govCarInoutPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCarInout/queryGovCarInout
     * @path /app/govCarInout/queryGovCarInout
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCarInout", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCarInout(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "govCommunityId" , required = false) String govCommunityId,
                                                   @RequestParam(value = "carNum" , required = false) String carNum,
                                                   @RequestParam(value = "personName" , required = false) String personName,
                                                   @RequestParam(value = "personLink" , required = false) String personLink,
                                                   @RequestParam(value = "idCard" , required = false) String idCard,
                                                   @RequestParam(value = "state" , required = false) String state,
                                                   @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCarInoutDto govCarInoutDto = new GovCarInoutDto();
        govCarInoutDto.setPage(page);
        govCarInoutDto.setRow(row);
        govCarInoutDto.setCaId(caId);
        govCarInoutDto.setGovCommunityId(govCommunityId);
        govCarInoutDto.setCarNum(carNum);
        govCarInoutDto.setPersonName(personName);
        govCarInoutDto.setPersonLink(personLink);
        govCarInoutDto.setIdCard(idCard);
        govCarInoutDto.setState(state);
        return getGovCarInoutBMOImpl.get(govCarInoutDto);
    }
}
