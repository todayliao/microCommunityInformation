package com.java110.gov.bmo.govPersonInoutRecord.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govPersonInoutRecord.IDeleteGovPersonInoutRecordBMO;
import com.java110.intf.gov.IGovPersonInoutRecordInnerServiceSMO;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovPersonInoutRecordBMOImpl")
public class DeleteGovPersonInoutRecordBMOImpl implements IDeleteGovPersonInoutRecordBMO {

    @Autowired
    private IGovPersonInoutRecordInnerServiceSMO govPersonInoutRecordInnerServiceSMOImpl;

    /**
     * @param govPersonInoutRecordPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovPersonInoutRecordPo govPersonInoutRecordPo) {

        int flag = govPersonInoutRecordInnerServiceSMOImpl.deleteGovPersonInoutRecord(govPersonInoutRecordPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
