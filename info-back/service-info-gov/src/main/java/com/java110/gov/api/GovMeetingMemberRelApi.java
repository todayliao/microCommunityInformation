package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.gov.bmo.govMeetingMemberRel.IDeleteGovMeetingMemberRelBMO;
import com.java110.gov.bmo.govMeetingMemberRel.IGetGovMeetingMemberRelBMO;
import com.java110.gov.bmo.govMeetingMemberRel.ISaveGovMeetingMemberRelBMO;
import com.java110.gov.bmo.govMeetingMemberRel.IUpdateGovMeetingMemberRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMeetingMemberRel")
public class GovMeetingMemberRelApi {

    @Autowired
    private ISaveGovMeetingMemberRelBMO saveGovMeetingMemberRelBMOImpl;
    @Autowired
    private IUpdateGovMeetingMemberRelBMO updateGovMeetingMemberRelBMOImpl;
    @Autowired
    private IDeleteGovMeetingMemberRelBMO deleteGovMeetingMemberRelBMOImpl;

    @Autowired
    private IGetGovMeetingMemberRelBMO getGovMeetingMemberRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingMemberRel/saveGovMeetingMemberRel
     * @path /app/govMeetingMemberRel/saveGovMeetingMemberRel
     */
    @RequestMapping(value = "/saveGovMeetingMemberRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMeetingMemberRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");


        GovMeetingMemberRelPo govMeetingMemberRelPo = BeanConvertUtil.covertBean(reqJson, GovMeetingMemberRelPo.class);
        return saveGovMeetingMemberRelBMOImpl.save(govMeetingMemberRelPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingMemberRel/updateGovMeetingMemberRel
     * @path /app/govMeetingMemberRel/updateGovMeetingMemberRel
     */
    @RequestMapping(value = "/updateGovMeetingMemberRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMeetingMemberRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "memberRelId", "请求报文中未包含memberRelId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "memberRelId", "memberRelId不能为空");


        GovMeetingMemberRelPo govMeetingMemberRelPo = BeanConvertUtil.covertBean(reqJson, GovMeetingMemberRelPo.class);
        return updateGovMeetingMemberRelBMOImpl.update(govMeetingMemberRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingMemberRel/deleteGovMeetingMemberRel
     * @path /app/govMeetingMemberRel/deleteGovMeetingMemberRel
     */
    @RequestMapping(value = "/deleteGovMeetingMemberRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMeetingMemberRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "memberRelId", "memberRelId不能为空");


        GovMeetingMemberRelPo govMeetingMemberRelPo = BeanConvertUtil.covertBean(reqJson, GovMeetingMemberRelPo.class);
        return deleteGovMeetingMemberRelBMOImpl.delete(govMeetingMemberRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMeetingMemberRel/queryGovMeetingMemberRel
     * @path /app/govMeetingMemberRel/queryGovMeetingMemberRel
     */
    @RequestMapping(value = "/queryGovMeetingMemberRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMeetingMemberRel(@RequestParam(value = "caId") String caId,
                                                           @RequestParam(value = "page") int page,
                                                           @RequestParam(value = "row") int row) {
        GovMeetingMemberRelDto govMeetingMemberRelDto = new GovMeetingMemberRelDto();
        govMeetingMemberRelDto.setPage(page);
        govMeetingMemberRelDto.setRow(row);
        govMeetingMemberRelDto.setCaId(caId);
        return getGovMeetingMemberRelBMOImpl.get(govMeetingMemberRelDto);
    }
}
