package com.java110.gov.bmo.govActivity.impl;

import com.java110.dto.govActivity.GovActivityDto;
import com.java110.gov.bmo.govActivity.IGetGovActivityBMO;
import com.java110.intf.gov.IGovActivityInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivityBMOImpl")
public class GetGovActivityBMOImpl implements IGetGovActivityBMO {

    @Autowired
    private IGovActivityInnerServiceSMO govActivityInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govActivityDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivityDto govActivityDto) {


        int count = govActivityInnerServiceSMOImpl.queryGovActivitysCount(govActivityDto);

        List<GovActivityDto> govActivityDtos = null;
        if (count > 0) {
            govActivityDtos = govActivityInnerServiceSMOImpl.queryGovActivitys(govActivityDto);
        } else {
            govActivityDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivityDto.getRow()), count, govActivityDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
