package com.java110.gov.bmo.govMeetingList;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingList.GovMeetingListPo;
public interface ISaveGovMeetingListBMO {


    /**
     * 添加会议列表
     * add by wuxw
     * @param govMeetingListPo
     * @return
     */
    ResponseEntity<String> save(GovMeetingListPo govMeetingListPo, JSONArray govMembers);


}
