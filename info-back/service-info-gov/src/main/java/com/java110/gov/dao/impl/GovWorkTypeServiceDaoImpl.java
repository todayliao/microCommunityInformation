package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovWorkTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 党内职务服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govWorkTypeServiceDaoImpl")
//@Transactional
public class GovWorkTypeServiceDaoImpl extends BaseServiceDao implements IGovWorkTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovWorkTypeServiceDaoImpl.class);





    /**
     * 保存党内职务信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovWorkTypeInfo(Map info) throws DAOException {
        logger.debug("保存党内职务信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govWorkTypeServiceDaoImpl.saveGovWorkTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存党内职务信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询党内职务信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovWorkTypeInfo(Map info) throws DAOException {
        logger.debug("查询党内职务信息 入参 info : {}",info);

        List<Map> businessGovWorkTypeInfos = sqlSessionTemplate.selectList("govWorkTypeServiceDaoImpl.getGovWorkTypeInfo",info);

        return businessGovWorkTypeInfos;
    }


    /**
     * 修改党内职务信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovWorkTypeInfo(Map info) throws DAOException {
        logger.debug("修改党内职务信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govWorkTypeServiceDaoImpl.updateGovWorkTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改党内职务信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询党内职务数量
     * @param info 党内职务信息
     * @return 党内职务数量
     */
    @Override
    public int queryGovWorkTypesCount(Map info) {
        logger.debug("查询党内职务数据 入参 info : {}",info);

        List<Map> businessGovWorkTypeInfos = sqlSessionTemplate.selectList("govWorkTypeServiceDaoImpl.queryGovWorkTypesCount", info);
        if (businessGovWorkTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovWorkTypeInfos.get(0).get("count").toString());
    }


}
