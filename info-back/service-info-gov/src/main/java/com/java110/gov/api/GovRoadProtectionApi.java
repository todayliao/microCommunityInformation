package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govRoadProtection.GovRoadProtectionDto;
import com.java110.po.govRoadProtection.GovRoadProtectionPo;
import com.java110.gov.bmo.govRoadProtection.IDeleteGovRoadProtectionBMO;
import com.java110.gov.bmo.govRoadProtection.IGetGovRoadProtectionBMO;
import com.java110.gov.bmo.govRoadProtection.ISaveGovRoadProtectionBMO;
import com.java110.gov.bmo.govRoadProtection.IUpdateGovRoadProtectionBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govRoadProtection")
public class GovRoadProtectionApi {

    @Autowired
    private ISaveGovRoadProtectionBMO saveGovRoadProtectionBMOImpl;
    @Autowired
    private IUpdateGovRoadProtectionBMO updateGovRoadProtectionBMOImpl;
    @Autowired
    private IDeleteGovRoadProtectionBMO deleteGovRoadProtectionBMOImpl;

    @Autowired
    private IGetGovRoadProtectionBMO getGovRoadProtectionBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtection/saveGovRoadProtection
     * @path /app/govRoadProtection/saveGovRoadProtection
     */
    @RequestMapping(value = "/saveGovRoadProtection", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovRoadProtection(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "roadType", "请求报文中未包含roadType");
        Assert.hasKeyAndValue(reqJson, "roadName", "请求报文中未包含roadName");
        Assert.hasKeyAndValue(reqJson, "belongToDepartment", "请求报文中未包含belongToDepartment");
        Assert.hasKeyAndValue(reqJson, "departmentTel", "请求报文中未包含departmentTel");
        Assert.hasKeyAndValue(reqJson, "departmentAddress", "请求报文中未包含departmentAddress");
        Assert.hasKeyAndValue(reqJson, "leadingCadreName", "请求报文中未包含leadingCadreName");
        Assert.hasKeyAndValue(reqJson, "leaderLink", "请求报文中未包含leaderLink");
        Assert.hasKeyAndValue(reqJson, "manageDepartment", "请求报文中未包含manageDepartment");
        Assert.hasKeyAndValue(reqJson, "manageDepTel", "请求报文中未包含manageDepTel");
        Assert.hasKeyAndValue(reqJson, "manageDepAddress", "请求报文中未包含manageDepAddress");
        Assert.hasKeyAndValue(reqJson, "branchLeaders", "请求报文中未包含branchLeaders");
        Assert.hasKeyAndValue(reqJson, "branchLeadersTel", "请求报文中未包含branchLeadersTel");
        Assert.hasKeyAndValue(reqJson, "safeHiddenGrade", "请求报文中未包含safeHiddenGrade");
        Assert.hasKeyAndValue(reqJson, "safeTrouble", "请求报文中未包含safeTrouble");


        GovRoadProtectionPo govRoadProtectionPo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionPo.class);
        return saveGovRoadProtectionBMOImpl.save(govRoadProtectionPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtection/updateGovRoadProtection
     * @path /app/govRoadProtection/updateGovRoadProtection
     */
    @RequestMapping(value = "/updateGovRoadProtection", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovRoadProtection(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "roadProtectionId", "请求报文中未包含roadProtectionId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "roadType", "请求报文中未包含roadType");
        Assert.hasKeyAndValue(reqJson, "roadName", "请求报文中未包含roadName");
        Assert.hasKeyAndValue(reqJson, "belongToDepartment", "请求报文中未包含belongToDepartment");
        Assert.hasKeyAndValue(reqJson, "departmentTel", "请求报文中未包含departmentTel");
        Assert.hasKeyAndValue(reqJson, "departmentAddress", "请求报文中未包含departmentAddress");
        Assert.hasKeyAndValue(reqJson, "leadingCadreName", "请求报文中未包含leadingCadreName");
        Assert.hasKeyAndValue(reqJson, "leaderLink", "请求报文中未包含leaderLink");
        Assert.hasKeyAndValue(reqJson, "manageDepartment", "请求报文中未包含manageDepartment");
        Assert.hasKeyAndValue(reqJson, "manageDepTel", "请求报文中未包含manageDepTel");
        Assert.hasKeyAndValue(reqJson, "manageDepAddress", "请求报文中未包含manageDepAddress");
        Assert.hasKeyAndValue(reqJson, "branchLeaders", "请求报文中未包含branchLeaders");
        Assert.hasKeyAndValue(reqJson, "branchLeadersTel", "请求报文中未包含branchLeadersTel");
        Assert.hasKeyAndValue(reqJson, "safeHiddenGrade", "请求报文中未包含safeHiddenGrade");
        Assert.hasKeyAndValue(reqJson, "safeTrouble", "请求报文中未包含safeTrouble");


        GovRoadProtectionPo govRoadProtectionPo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionPo.class);
        return updateGovRoadProtectionBMOImpl.update(govRoadProtectionPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtection/deleteGovRoadProtection
     * @path /app/govRoadProtection/deleteGovRoadProtection
     */
    @RequestMapping(value = "/deleteGovRoadProtection", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovRoadProtection(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "roadProtectionId", "roadProtectionId不能为空");

        GovRoadProtectionPo govRoadProtectionPo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionPo.class);
        return deleteGovRoadProtectionBMOImpl.delete(govRoadProtectionPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govRoadProtection/queryGovRoadProtection
     * @path /app/govRoadProtection/queryGovRoadProtection
     */
    @RequestMapping(value = "/queryGovRoadProtection", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovRoadProtection(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "roadName",required = false) String roadName,
                                                         @RequestParam(value = "belongToDepartment",required = false) String belongToDepartment,
                                                         @RequestParam(value = "manageDepartment",required = false) String manageDepartment,
                                                         @RequestParam(value = "branchLeaders",required = false) String branchLeaders,
                                                         @RequestParam(value = "manageDepTel",required = false) String manageDepTel,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovRoadProtectionDto govRoadProtectionDto = new GovRoadProtectionDto();
        govRoadProtectionDto.setPage(page);
        govRoadProtectionDto.setRow(row);
        govRoadProtectionDto.setCaId(caId);
        govRoadProtectionDto.setRoadName(roadName);
        govRoadProtectionDto.setBelongToDepartment(belongToDepartment);
        govRoadProtectionDto.setManageDepartment(manageDepartment);
        govRoadProtectionDto.setBranchLeaders(branchLeaders);
        govRoadProtectionDto.setManageDepTel(manageDepTel);
        return getGovRoadProtectionBMOImpl.get(govRoadProtectionDto);
    }
}
