package com.java110.gov.bmo.govHelpPolicy;
import org.springframework.http.ResponseEntity;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;

public interface IUpdateGovHelpPolicyBMO {


    /**
     * 修改帮扶政策
     * add by wuxw
     * @param govHelpPolicyPo
     * @return
     */
    ResponseEntity<String> update(GovHelpPolicyPo govHelpPolicyPo);


}
