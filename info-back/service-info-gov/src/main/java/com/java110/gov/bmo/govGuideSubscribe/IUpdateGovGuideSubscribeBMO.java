package com.java110.gov.bmo.govGuideSubscribe;
import com.java110.po.govGuideSubscribe.GovGuideSubscribePo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovGuideSubscribeBMO {


    /**
     * 修改办事预约
     * add by wuxw
     * @param govGuideSubscribePo
     * @return
     */
    ResponseEntity<String> update(GovGuideSubscribePo govGuideSubscribePo);


}
