package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovHelpPolicyListServiceDao;
import com.java110.intf.gov.IGovHelpPolicyListInnerServiceSMO;
import com.java110.dto.govHelpPolicyList.GovHelpPolicyListDto;
import com.java110.po.govHelpPolicyList.GovHelpPolicyListPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 帮扶记录内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHelpPolicyListInnerServiceSMOImpl extends BaseServiceSMO implements IGovHelpPolicyListInnerServiceSMO {

    @Autowired
    private IGovHelpPolicyListServiceDao govHelpPolicyListServiceDaoImpl;


    @Override
    public int saveGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo) {
        int saveFlag = 1;
        govHelpPolicyListServiceDaoImpl.saveGovHelpPolicyListInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyListPo));
        return saveFlag;
    }

     @Override
    public int updateGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo) {
        int saveFlag = 1;
         govHelpPolicyListServiceDaoImpl.updateGovHelpPolicyListInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyListPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHelpPolicyList(@RequestBody  GovHelpPolicyListPo govHelpPolicyListPo) {
        int saveFlag = 1;
        govHelpPolicyListPo.setStatusCd("1");
        govHelpPolicyListServiceDaoImpl.updateGovHelpPolicyListInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyListPo));
        return saveFlag;
    }

    @Override
    public List<GovHelpPolicyListDto> queryGovHelpPolicyLists(@RequestBody  GovHelpPolicyListDto govHelpPolicyListDto) {

        //校验是否传了 分页信息

        int page = govHelpPolicyListDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHelpPolicyListDto.setPage((page - 1) * govHelpPolicyListDto.getRow());
        }

        List<GovHelpPolicyListDto> govHelpPolicyLists = BeanConvertUtil.covertBeanList(govHelpPolicyListServiceDaoImpl.getGovHelpPolicyListInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyListDto)), GovHelpPolicyListDto.class);

        return govHelpPolicyLists;
    }


    @Override
    public int queryGovHelpPolicyListsCount(@RequestBody GovHelpPolicyListDto govHelpPolicyListDto) {
        return govHelpPolicyListServiceDaoImpl.queryGovHelpPolicyListsCount(BeanConvertUtil.beanCovertMap(govHelpPolicyListDto));    }

    public IGovHelpPolicyListServiceDao getGovHelpPolicyListServiceDaoImpl() {
        return govHelpPolicyListServiceDaoImpl;
    }

    public void setGovHelpPolicyListServiceDaoImpl(IGovHelpPolicyListServiceDao govHelpPolicyListServiceDaoImpl) {
        this.govHelpPolicyListServiceDaoImpl = govHelpPolicyListServiceDaoImpl;
    }
}
