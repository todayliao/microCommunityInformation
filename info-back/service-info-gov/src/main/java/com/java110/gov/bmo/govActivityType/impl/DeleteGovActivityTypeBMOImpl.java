package com.java110.gov.bmo.govActivityType.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivityType.IDeleteGovActivityTypeBMO;
import com.java110.intf.gov.IGovActivityTypeInnerServiceSMO;
import com.java110.po.govActivityType.GovActivityTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovActivityTypeBMOImpl")
public class DeleteGovActivityTypeBMOImpl implements IDeleteGovActivityTypeBMO {

    @Autowired
    private IGovActivityTypeInnerServiceSMO govActivityTypeInnerServiceSMOImpl;

    /**
     * @param govActivityTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovActivityTypePo govActivityTypePo) {

        int flag = govActivityTypeInnerServiceSMOImpl.deleteGovActivityType(govActivityTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
