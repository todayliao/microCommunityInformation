package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovActivityPersonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 活动报名人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govActivityPersonServiceDaoImpl")
//@Transactional
public class GovActivityPersonServiceDaoImpl extends BaseServiceDao implements IGovActivityPersonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovActivityPersonServiceDaoImpl.class);





    /**
     * 保存活动报名人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovActivityPersonInfo(Map info) throws DAOException {
        logger.debug("保存活动报名人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govActivityPersonServiceDaoImpl.saveGovActivityPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存活动报名人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询活动报名人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovActivityPersonInfo(Map info) throws DAOException {
        logger.debug("查询活动报名人员信息 入参 info : {}",info);

        List<Map> businessGovActivityPersonInfos = sqlSessionTemplate.selectList("govActivityPersonServiceDaoImpl.getGovActivityPersonInfo",info);

        return businessGovActivityPersonInfos;
    }


    /**
     * 修改活动报名人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovActivityPersonInfo(Map info) throws DAOException {
        logger.debug("修改活动报名人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govActivityPersonServiceDaoImpl.updateGovActivityPersonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改活动报名人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询活动报名人员数量
     * @param info 活动报名人员信息
     * @return 活动报名人员数量
     */
    @Override
    public int queryGovActivityPersonsCount(Map info) {
        logger.debug("查询活动报名人员数据 入参 info : {}",info);

        List<Map> businessGovActivityPersonInfos = sqlSessionTemplate.selectList("govActivityPersonServiceDaoImpl.queryGovActivityPersonsCount", info);
        if (businessGovActivityPersonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovActivityPersonInfos.get(0).get("count").toString());
    }


}
