package com.java110.gov.bmo.govVolunteerServiceRecord;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govVolunteerServiceRecord.GovVolunteerServiceRecordDto;
public interface IGetGovVolunteerServiceRecordBMO {


    /**
     * 查询服务记录表
     * add by wuxw
     * @param  govVolunteerServiceRecordDto
     * @return
     */
    ResponseEntity<String> get(GovVolunteerServiceRecordDto govVolunteerServiceRecordDto);


}
