package com.java110.gov.bmo.govProtectionCase.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govProtectionCase.IUpdateGovProtectionCaseBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovProtectionCaseInnerServiceSMO;
import com.java110.dto.govProtectionCase.GovProtectionCaseDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govProtectionCase.GovProtectionCasePo;
import java.util.List;

@Service("updateGovProtectionCaseBMOImpl")
public class UpdateGovProtectionCaseBMOImpl implements IUpdateGovProtectionCaseBMO {

    @Autowired
    private IGovProtectionCaseInnerServiceSMO govProtectionCaseInnerServiceSMOImpl;

    /**
     *
     *
     * @param govProtectionCasePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovProtectionCasePo govProtectionCasePo) {

        int flag = govProtectionCaseInnerServiceSMOImpl.updateGovProtectionCase(govProtectionCasePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
