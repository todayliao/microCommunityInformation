package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovAidsServiceDao;
import com.java110.intf.gov.IGovAidsInnerServiceSMO;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.po.govAids.GovAidsPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 艾滋病者内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovAidsInnerServiceSMOImpl extends BaseServiceSMO implements IGovAidsInnerServiceSMO {

    @Autowired
    private IGovAidsServiceDao govAidsServiceDaoImpl;


    @Override
    public int saveGovAids(@RequestBody  GovAidsPo govAidsPo) {
        int saveFlag = 1;
        govAidsServiceDaoImpl.saveGovAidsInfo(BeanConvertUtil.beanCovertMap(govAidsPo));
        return saveFlag;
    }

     @Override
    public int updateGovAids(@RequestBody  GovAidsPo govAidsPo) {
        int saveFlag = 1;
         govAidsServiceDaoImpl.updateGovAidsInfo(BeanConvertUtil.beanCovertMap(govAidsPo));
        return saveFlag;
    }

     @Override
    public int deleteGovAids(@RequestBody  GovAidsPo govAidsPo) {
        int saveFlag = 1;
        govAidsPo.setStatusCd("1");
        govAidsServiceDaoImpl.updateGovAidsInfo(BeanConvertUtil.beanCovertMap(govAidsPo));
        return saveFlag;
    }

    @Override
    public List<GovAidsDto> queryGovAidss(@RequestBody  GovAidsDto govAidsDto) {

        //校验是否传了 分页信息

        int page = govAidsDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAidsDto.setPage((page - 1) * govAidsDto.getRow());
        }

        List<GovAidsDto> govAidss = BeanConvertUtil.covertBeanList(govAidsServiceDaoImpl.getGovAidsInfo(BeanConvertUtil.beanCovertMap(govAidsDto)), GovAidsDto.class);

        return govAidss;
    }


    @Override
    public int queryGovAidssCount(@RequestBody GovAidsDto govAidsDto) {
        return govAidsServiceDaoImpl.queryGovAidssCount(BeanConvertUtil.beanCovertMap(govAidsDto));    }

    public IGovAidsServiceDao getGovAidsServiceDaoImpl() {
        return govAidsServiceDaoImpl;
    }

    public void setGovAidsServiceDaoImpl(IGovAidsServiceDao govAidsServiceDaoImpl) {
        this.govAidsServiceDaoImpl = govAidsServiceDaoImpl;
    }
}
