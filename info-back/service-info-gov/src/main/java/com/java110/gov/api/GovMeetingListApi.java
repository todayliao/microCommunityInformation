package com.java110.gov.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.po.govMeetingList.GovMeetingListPo;
import com.java110.gov.bmo.govMeetingList.IDeleteGovMeetingListBMO;
import com.java110.gov.bmo.govMeetingList.IGetGovMeetingListBMO;
import com.java110.gov.bmo.govMeetingList.ISaveGovMeetingListBMO;
import com.java110.gov.bmo.govMeetingList.IUpdateGovMeetingListBMO;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMeetingList")
public class GovMeetingListApi {

    @Autowired
    private ISaveGovMeetingListBMO saveGovMeetingListBMOImpl;
    @Autowired
    private IUpdateGovMeetingListBMO updateGovMeetingListBMOImpl;
    @Autowired
    private IDeleteGovMeetingListBMO deleteGovMeetingListBMOImpl;

    @Autowired
    private IGetGovMeetingListBMO getGovMeetingListBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingList/saveGovMeetingList
     * @path /app/govMeetingList/saveGovMeetingList
     */
    @RequestMapping(value = "/saveGovMeetingList", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMeetingList(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "meetingName", "请求报文中未包含meetingName");
        Assert.hasKeyAndValue(reqJson, "meetingTime", "请求报文中未包含meetingTime");
        Assert.hasKeyAndValue(reqJson, "meetingAddress", "请求报文中未包含meetingAddress");

        JSONArray govMembers = reqJson.getJSONArray("govMembers");
        if(govMembers == null || govMembers.size() < 1){
            throw new IllegalArgumentException("未选择参会人员，请添加参会人员");
        }

        GovMeetingListPo govMeetingListPo = BeanConvertUtil.covertBean(reqJson, GovMeetingListPo.class);
        return saveGovMeetingListBMOImpl.save(govMeetingListPo,govMembers);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingList/updateGovMeetingList
     * @path /app/govMeetingList/updateGovMeetingList
     */
    @RequestMapping(value = "/updateGovMeetingList", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMeetingList(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govMeetingId", "请求报文中未包含govMeetingId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "govMemberId", "请求报文中未包含govMemberId");
        Assert.hasKeyAndValue(reqJson, "meetingName", "请求报文中未包含meetingName");
        Assert.hasKeyAndValue(reqJson, "meetingTime", "请求报文中未包含meetingTime");
        Assert.hasKeyAndValue(reqJson, "meetingAddress", "请求报文中未包含meetingAddress");
        JSONArray govMembers = reqJson.getJSONArray("govMembers");
        if(govMembers == null || govMembers.size() < 1){
            throw new IllegalArgumentException("未选择参会人员，请添加参会人员");
        }

        GovMeetingListPo govMeetingListPo = BeanConvertUtil.covertBean(reqJson, GovMeetingListPo.class);
        return updateGovMeetingListBMOImpl.update(govMeetingListPo,govMembers);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingList/recordGovMeetingList
     * @path /app/govMeetingList/recordGovMeetingList
     */
    @RequestMapping(value = "/recordGovMeetingList", method = RequestMethod.POST)
    public ResponseEntity<String> recordGovMeetingList(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govMeetingId", "请求报文中未包含govMeetingId");
        Assert.hasKeyAndValue(reqJson, "meetingPoints", "请求报文中未包含meetingPoints");
        Assert.hasKeyAndValue(reqJson, "meetingContent", "请求报文中未包含meetingContent");

        JSONArray meetingImgs = reqJson.getJSONArray("meetingImgs");
        if(meetingImgs == null || meetingImgs.size() < 1){
            throw new IllegalArgumentException("未上传会议照片");
        }
        GovMeetingListPo govMeetingListPo = BeanConvertUtil.covertBean(reqJson, GovMeetingListPo.class);
        return updateGovMeetingListBMOImpl.recordGovMeetingList(govMeetingListPo,meetingImgs);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMeetingList/deleteGovMeetingList
     * @path /app/govMeetingList/deleteGovMeetingList
     */
    @RequestMapping(value = "/deleteGovMeetingList", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMeetingList(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "govMeetingId", "govMeetingId不能为空");


        GovMeetingListPo govMeetingListPo = BeanConvertUtil.covertBean(reqJson, GovMeetingListPo.class);
        return deleteGovMeetingListBMOImpl.delete(govMeetingListPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMeetingList/queryGovMeetingList
     * @path /app/govMeetingList/queryGovMeetingList
     */
    @RequestMapping(value = "/queryGovMeetingList", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMeetingList(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "meetingName",required = false) String meetingName,
                                                      @RequestParam(value = "meetingTime",required = false) String meetingTime,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovMeetingListDto govMeetingListDto = new GovMeetingListDto();
        govMeetingListDto.setPage(page);
        govMeetingListDto.setRow(row);
        govMeetingListDto.setCaId(caId);
        govMeetingListDto.setMeetingName(meetingName);
        govMeetingListDto.setMeetingTime(meetingTime);
        return getGovMeetingListBMOImpl.get(govMeetingListDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMeetingList/queryPersonIdMeetingList
     * @path /app/govMeetingList/queryPersonIdMeetingList
     */
    @RequestMapping(value = "/queryPersonIdMeetingList", method = RequestMethod.GET)
    public ResponseEntity<String> queryPersonIdMeetingList(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "govMemberId") String govMemberId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovMeetingListDto govMeetingListDto = new GovMeetingListDto();
        govMeetingListDto.setPage(page);
        govMeetingListDto.setRow(row);
        govMeetingListDto.setCaId(caId);
        return getGovMeetingListBMOImpl.getPersonIdMeetingList(govMeetingListDto,govMemberId);
    }
}
