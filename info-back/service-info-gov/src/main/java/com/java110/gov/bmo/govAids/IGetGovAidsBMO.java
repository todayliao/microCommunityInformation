package com.java110.gov.bmo.govAids;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govAids.GovAidsDto;
public interface IGetGovAidsBMO {


    /**
     * 查询艾滋病者
     * add by wuxw
     * @param  govAidsDto
     * @return
     */
    ResponseEntity<String> get(GovAidsDto govAidsDto);


}
