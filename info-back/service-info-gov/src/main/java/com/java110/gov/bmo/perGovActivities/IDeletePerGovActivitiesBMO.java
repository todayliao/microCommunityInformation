package com.java110.gov.bmo.perGovActivities;
import org.springframework.http.ResponseEntity;
import com.java110.po.perGovActivities.PerGovActivitiesPo;

public interface IDeletePerGovActivitiesBMO {


    /**
     * 修改生日记录
     * add by wuxw
     * @param perGovActivitiesPo
     * @return
     */
    ResponseEntity<String> delete(PerGovActivitiesPo perGovActivitiesPo);


}
