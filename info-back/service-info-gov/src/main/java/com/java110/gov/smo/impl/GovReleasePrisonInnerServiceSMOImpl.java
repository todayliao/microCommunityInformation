package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovReleasePrisonServiceDao;
import com.java110.intf.gov.IGovReleasePrisonInnerServiceSMO;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.po.govReleasePrison.GovReleasePrisonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 刑满释放人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovReleasePrisonInnerServiceSMOImpl extends BaseServiceSMO implements IGovReleasePrisonInnerServiceSMO {

    @Autowired
    private IGovReleasePrisonServiceDao govReleasePrisonServiceDaoImpl;


    @Override
    public int saveGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo) {
        int saveFlag = 1;
        govReleasePrisonServiceDaoImpl.saveGovReleasePrisonInfo(BeanConvertUtil.beanCovertMap(govReleasePrisonPo));
        return saveFlag;
    }

     @Override
    public int updateGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo) {
        int saveFlag = 1;
         govReleasePrisonServiceDaoImpl.updateGovReleasePrisonInfo(BeanConvertUtil.beanCovertMap(govReleasePrisonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo) {
        int saveFlag = 1;
        govReleasePrisonPo.setStatusCd("1");
        govReleasePrisonServiceDaoImpl.updateGovReleasePrisonInfo(BeanConvertUtil.beanCovertMap(govReleasePrisonPo));
        return saveFlag;
    }

    @Override
    public List<GovReleasePrisonDto> queryGovReleasePrisons(@RequestBody  GovReleasePrisonDto govReleasePrisonDto) {

        //校验是否传了 分页信息

        int page = govReleasePrisonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govReleasePrisonDto.setPage((page - 1) * govReleasePrisonDto.getRow());
        }

        List<GovReleasePrisonDto> govReleasePrisons = BeanConvertUtil.covertBeanList(govReleasePrisonServiceDaoImpl.getGovReleasePrisonInfo(BeanConvertUtil.beanCovertMap(govReleasePrisonDto)), GovReleasePrisonDto.class);

        return govReleasePrisons;
    }


    @Override
    public int queryGovReleasePrisonsCount(@RequestBody GovReleasePrisonDto govReleasePrisonDto) {
        return govReleasePrisonServiceDaoImpl.queryGovReleasePrisonsCount(BeanConvertUtil.beanCovertMap(govReleasePrisonDto));    }

    public IGovReleasePrisonServiceDao getGovReleasePrisonServiceDaoImpl() {
        return govReleasePrisonServiceDaoImpl;
    }

    public void setGovReleasePrisonServiceDaoImpl(IGovReleasePrisonServiceDao govReleasePrisonServiceDaoImpl) {
        this.govReleasePrisonServiceDaoImpl = govReleasePrisonServiceDaoImpl;
    }
}
