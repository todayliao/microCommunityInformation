package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.po.govReleasePrison.GovReleasePrisonPo;
import com.java110.gov.bmo.govReleasePrison.IDeleteGovReleasePrisonBMO;
import com.java110.gov.bmo.govReleasePrison.IGetGovReleasePrisonBMO;
import com.java110.gov.bmo.govReleasePrison.ISaveGovReleasePrisonBMO;
import com.java110.gov.bmo.govReleasePrison.IUpdateGovReleasePrisonBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govReleasePrison")
public class GovReleasePrisonApi {

    @Autowired
    private ISaveGovReleasePrisonBMO saveGovReleasePrisonBMOImpl;
    @Autowired
    private IUpdateGovReleasePrisonBMO updateGovReleasePrisonBMOImpl;
    @Autowired
    private IDeleteGovReleasePrisonBMO deleteGovReleasePrisonBMOImpl;

    @Autowired
    private IGetGovReleasePrisonBMO getGovReleasePrisonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govReleasePrison/saveGovReleasePrison
     * @path /app/govReleasePrison/saveGovReleasePrison
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovReleasePrison", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovReleasePrison(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "releaseTime", "请求报文中未包含releaseTime");
        Assert.hasKeyAndValue(reqJson, "sentenceStartTime", "请求报文中未包含sentenceStartTime");
        Assert.hasKeyAndValue(reqJson, "sentenceEndTime", "请求报文中未包含sentenceEndTime");


        GovReleasePrisonPo govReleasePrisonPo = BeanConvertUtil.covertBean(reqJson, GovReleasePrisonPo.class);
        return saveGovReleasePrisonBMOImpl.save(govReleasePrisonPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govReleasePrison/updateGovReleasePrison
     * @path /app/govReleasePrison/updateGovReleasePrison
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovReleasePrison", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovReleasePrison(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "releaseTime", "请求报文中未包含releaseTime");
        Assert.hasKeyAndValue(reqJson, "sentenceStartTime", "请求报文中未包含sentenceStartTime");
        Assert.hasKeyAndValue(reqJson, "sentenceEndTime", "请求报文中未包含sentenceEndTime");
        Assert.hasKeyAndValue(reqJson, "sentenceReason", "请求报文中未包含sentenceReason");
        Assert.hasKeyAndValue(reqJson, "releasePrisonId", "releasePrisonId不能为空");


        GovReleasePrisonPo govReleasePrisonPo = BeanConvertUtil.covertBean(reqJson, GovReleasePrisonPo.class);
        return updateGovReleasePrisonBMOImpl.update(govReleasePrisonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReleasePrison/deleteGovReleasePrison
     * @path /app/govReleasePrison/deleteGovReleasePrison
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovReleasePrison", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovReleasePrison(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "releasePrisonId", "releasePrisonId不能为空");


        GovReleasePrisonPo govReleasePrisonPo = BeanConvertUtil.covertBean(reqJson, GovReleasePrisonPo.class);
        return deleteGovReleasePrisonBMOImpl.delete(govReleasePrisonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReleasePrison/queryGovReleasePrison
     * @path /app/govReleasePrison/queryGovReleasePrison
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovReleasePrison", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovReleasePrison(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "releasePrisonId" , required = false) String releasePrisonId,
                                                        @RequestParam(value = "name" , required = false) String name,
                                                        @RequestParam(value = "address" , required = false) String address,
                                                        @RequestParam(value = "tel" , required = false) String tel,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovReleasePrisonDto govReleasePrisonDto = new GovReleasePrisonDto();
        govReleasePrisonDto.setPage(page);
        govReleasePrisonDto.setRow(row);
        govReleasePrisonDto.setReleasePrisonId(releasePrisonId);
        govReleasePrisonDto.setCaId(caId);
        govReleasePrisonDto.setName(name);
        govReleasePrisonDto.setAddress(address);
        govReleasePrisonDto.setTel(tel);
        return getGovReleasePrisonBMOImpl.get(govReleasePrisonDto);
    }
}
