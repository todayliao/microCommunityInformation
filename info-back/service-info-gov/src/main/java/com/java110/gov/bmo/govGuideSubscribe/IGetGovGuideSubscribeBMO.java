package com.java110.gov.bmo.govGuideSubscribe;
import com.java110.dto.govGuideSubscribe.GovGuideSubscribeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovGuideSubscribeBMO {


    /**
     * 查询办事预约
     * add by wuxw
     * @param  govGuideSubscribeDto
     * @return
     */
    ResponseEntity<String> get(GovGuideSubscribeDto govGuideSubscribeDto);


}
