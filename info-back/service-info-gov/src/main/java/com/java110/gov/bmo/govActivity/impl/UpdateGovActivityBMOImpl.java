package com.java110.gov.bmo.govActivity.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govActivity.IUpdateGovActivityBMO;
import com.java110.intf.gov.IGovActivityInnerServiceSMO;
import com.java110.po.govActivity.GovActivityPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovActivityBMOImpl")
public class UpdateGovActivityBMOImpl implements IUpdateGovActivityBMO {

    @Autowired
    private IGovActivityInnerServiceSMO govActivityInnerServiceSMOImpl;

    /**
     *
     *
     * @param govActivityPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovActivityPo govActivityPo) {

        int flag = govActivityInnerServiceSMOImpl.updateGovActivity(govActivityPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
