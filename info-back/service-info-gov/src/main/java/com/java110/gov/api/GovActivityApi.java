package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivity.GovActivityDto;
import com.java110.gov.bmo.govActivity.IDeleteGovActivityBMO;
import com.java110.gov.bmo.govActivity.IGetGovActivityBMO;
import com.java110.gov.bmo.govActivity.ISaveGovActivityBMO;
import com.java110.gov.bmo.govActivity.IUpdateGovActivityBMO;
import com.java110.po.govActivity.GovActivityPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/govActivity")
public class GovActivityApi {

    @Autowired
    private ISaveGovActivityBMO saveGovActivityBMOImpl;
    @Autowired
    private IUpdateGovActivityBMO updateGovActivityBMOImpl;
    @Autowired
    private IDeleteGovActivityBMO deleteGovActivityBMOImpl;

    @Autowired
    private IGetGovActivityBMO getGovActivityBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivity/saveGovActivity
     * @path /app/govActivity/saveGovActivity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivity", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivity(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");
        Assert.hasKeyAndValue(reqJson, "actName", "请求报文中未包含actName");
        Assert.hasKeyAndValue(reqJson, "actTime", "请求报文中未包含actTime");
        Assert.hasKeyAndValue(reqJson, "personCount", "请求报文中未包含personCount");
        Assert.hasKeyAndValue(reqJson, "actTime", "请求报文中未包含actTime");
        Assert.hasKeyAndValue(reqJson, "contactName", "请求报文中未包含contactName");
        Assert.hasKeyAndValue(reqJson, "contactLink", "请求报文中未包含contactLink");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");


        GovActivityPo govActivityPo = BeanConvertUtil.covertBean(reqJson, GovActivityPo.class);
        return saveGovActivityBMOImpl.save(govActivityPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivity/updateGovActivity
     * @path /app/govActivity/updateGovActivity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivity", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivity(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");
        Assert.hasKeyAndValue(reqJson, "actName", "请求报文中未包含actName");
        Assert.hasKeyAndValue(reqJson, "actTime", "请求报文中未包含actTime");
        Assert.hasKeyAndValue(reqJson, "personCount", "请求报文中未包含personCount");
        Assert.hasKeyAndValue(reqJson, "actTime", "请求报文中未包含actTime");
        Assert.hasKeyAndValue(reqJson, "contactName", "请求报文中未包含contactName");
        Assert.hasKeyAndValue(reqJson, "contactLink", "请求报文中未包含contactLink");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "actId", "actId不能为空");


        GovActivityPo govActivityPo = BeanConvertUtil.covertBean(reqJson, GovActivityPo.class);
        return updateGovActivityBMOImpl.update(govActivityPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivity/deleteGovActivity
     * @path /app/govActivity/deleteGovActivity
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivity", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivity(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "actId", "actId不能为空");


        GovActivityPo govActivityPo = BeanConvertUtil.covertBean(reqJson, GovActivityPo.class);
        return deleteGovActivityBMOImpl.delete(govActivityPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivity/queryGovActivity
     * @path /app/govActivity/queryGovActivity
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovActivity", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivity(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "actName" , required = false) String actName,
                                                   @RequestParam(value = "actId" , required = false) String actId,
                                                   @RequestParam(value = "contactName" , required = false) String contactName,
                                                   @RequestParam(value = "contactLink" , required = false) String contactLink,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        GovActivityDto govActivityDto = new GovActivityDto();
        govActivityDto.setPage(page);
        govActivityDto.setRow(row);
        govActivityDto.setActId(actId);
        govActivityDto.setActName(actName);
        govActivityDto.setContactName(contactName);
        govActivityDto.setContactLink(contactLink);
        return getGovActivityBMOImpl.get(govActivityDto);
    }
}
