package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IReportPoolServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 报事管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("reportPoolServiceDaoImpl")
//@Transactional
public class ReportPoolServiceDaoImpl extends BaseServiceDao implements IReportPoolServiceDao {

    private static Logger logger = LoggerFactory.getLogger(ReportPoolServiceDaoImpl.class);





    /**
     * 保存报事管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveReportPoolInfo(Map info) throws DAOException {
        logger.debug("保存报事管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("reportPoolServiceDaoImpl.saveReportPoolInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存报事管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询报事管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getReportPoolInfo(Map info) throws DAOException {
        logger.debug("查询报事管理信息 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.getReportPoolInfo",info);

        return businessReportPoolInfos;
    }

    /**
     * 查询报事管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> quseryReportStaffs(Map info) throws DAOException {
        logger.debug("查询报事管理信息 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.quseryReportStaffs",info);

        return businessReportPoolInfos;
    }
    /**
     * 查询报事管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> queryStaffFinishReports(Map info) throws DAOException {
        logger.debug("查询报事管理信息 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.queryStaffFinishReports",info);

        return businessReportPoolInfos;
    }
    /**
     * 查询报事管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> quseryReportFinishStaffs(Map info) throws DAOException {
        logger.debug("查询报事管理信息 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.quseryReportFinishStaffs",info);

        return businessReportPoolInfos;
    }


    /**
     * 修改报事管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateReportPoolInfo(Map info) throws DAOException {
        logger.debug("修改报事管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("reportPoolServiceDaoImpl.updateReportPoolInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改报事管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询报事管理数量
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    @Override
    public int queryReportPoolsCount(Map info) {
        logger.debug("查询报事管理数据 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.queryReportPoolsCount", info);
        if (businessReportPoolInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessReportPoolInfos.get(0).get("count").toString());
    }
     /**
     * 查询报事管理数量
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    @Override
    public int quseryReportStaffCount(Map info) {
        logger.debug("查询报事管理数据 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.quseryReportStaffCount", info);
        if (businessReportPoolInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessReportPoolInfos.get(0).get("count").toString());
    }
     /**
     * 查询报事管理数量
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    @Override
    public int queryStaffFinishReportCount(Map info) {
        logger.debug("查询报事管理数据 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.queryStaffFinishReportCount", info);
        if (businessReportPoolInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessReportPoolInfos.get(0).get("count").toString());
    }
     /**
     * 查询报事管理数量
     * @param info 报事管理信息
     * @return 报事管理数量
     */
    @Override
    public int quseryReportFinishStaffCount(Map info) {
        logger.debug("查询报事管理数据 入参 info : {}",info);

        List<Map> businessReportPoolInfos = sqlSessionTemplate.selectList("reportPoolServiceDaoImpl.quseryReportFinishStaffCount", info);
        if (businessReportPoolInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessReportPoolInfos.get(0).get("count").toString());
    }


}
