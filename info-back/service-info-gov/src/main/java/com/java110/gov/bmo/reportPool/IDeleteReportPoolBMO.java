package com.java110.gov.bmo.reportPool;
import com.java110.po.reportPool.ReportPoolPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteReportPoolBMO {


    /**
     * 修改报事管理
     * add by wuxw
     * @param reportPoolPo
     * @return
     */
    ResponseEntity<String> delete(ReportPoolPo reportPoolPo);


}
