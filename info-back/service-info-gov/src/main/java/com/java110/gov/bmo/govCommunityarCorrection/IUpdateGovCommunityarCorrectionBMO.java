package com.java110.gov.bmo.govCommunityarCorrection;
import org.springframework.http.ResponseEntity;
import com.java110.po.govCommunityarCorrection.GovCommunityarCorrectionPo;

public interface IUpdateGovCommunityarCorrectionBMO {


    /**
     * 修改矫正者
     * add by wuxw
     * @param govCommunityarCorrectionPo
     * @return
     */
    ResponseEntity<String> update(GovCommunityarCorrectionPo govCommunityarCorrectionPo);


}
