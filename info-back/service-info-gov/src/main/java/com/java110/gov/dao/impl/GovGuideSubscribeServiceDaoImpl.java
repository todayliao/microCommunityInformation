package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovGuideSubscribeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 办事预约服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govGuideSubscribeServiceDaoImpl")
//@Transactional
public class GovGuideSubscribeServiceDaoImpl extends BaseServiceDao implements IGovGuideSubscribeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovGuideSubscribeServiceDaoImpl.class);





    /**
     * 保存办事预约信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovGuideSubscribeInfo(Map info) throws DAOException {
        logger.debug("保存办事预约信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govGuideSubscribeServiceDaoImpl.saveGovGuideSubscribeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存办事预约信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询办事预约信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovGuideSubscribeInfo(Map info) throws DAOException {
        logger.debug("查询办事预约信息 入参 info : {}",info);

        List<Map> businessGovGuideSubscribeInfos = sqlSessionTemplate.selectList("govGuideSubscribeServiceDaoImpl.getGovGuideSubscribeInfo",info);

        return businessGovGuideSubscribeInfos;
    }


    /**
     * 修改办事预约信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovGuideSubscribeInfo(Map info) throws DAOException {
        logger.debug("修改办事预约信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govGuideSubscribeServiceDaoImpl.updateGovGuideSubscribeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改办事预约信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询办事预约数量
     * @param info 办事预约信息
     * @return 办事预约数量
     */
    @Override
    public int queryGovGuideSubscribesCount(Map info) {
        logger.debug("查询办事预约数据 入参 info : {}",info);

        List<Map> businessGovGuideSubscribeInfos = sqlSessionTemplate.selectList("govGuideSubscribeServiceDaoImpl.queryGovGuideSubscribesCount", info);
        if (businessGovGuideSubscribeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovGuideSubscribeInfos.get(0).get("count").toString());
    }


}
