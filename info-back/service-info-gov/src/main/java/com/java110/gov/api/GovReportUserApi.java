package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govReportUser.GovReportUserDto;
import com.java110.gov.bmo.govReportUser.IDeleteGovReportUserBMO;
import com.java110.gov.bmo.govReportUser.IGetGovReportUserBMO;
import com.java110.gov.bmo.govReportUser.ISaveGovReportUserBMO;
import com.java110.gov.bmo.govReportUser.IUpdateGovReportUserBMO;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govReportUser")
public class GovReportUserApi {

    @Autowired
    private ISaveGovReportUserBMO saveGovReportUserBMOImpl;
    @Autowired
    private IUpdateGovReportUserBMO updateGovReportUserBMOImpl;
    @Autowired
    private IDeleteGovReportUserBMO deleteGovReportUserBMOImpl;

    @Autowired
    private IGetGovReportUserBMO getGovReportUserBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govReportUser/saveGovReportUser
     * @path /app/govReportUser/saveGovReportUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovReportUser", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovReportUser(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "reportId", "请求报文中未包含reportId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "staffId", "请求报文中未包含staffId");
        Assert.hasKeyAndValue(reqJson, "staffName", "请求报文中未包含staffName");
        Assert.hasKeyAndValue(reqJson, "preStaffId", "请求报文中未包含preStaffId");
        Assert.hasKeyAndValue(reqJson, "preStaffName", "请求报文中未包含preStaffName");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "reportEvent", "请求报文中未包含reportEvent");
        Assert.hasKeyAndValue(reqJson, "preRuId", "请求报文中未包含preRuId");


        GovReportUserPo govReportUserPo = BeanConvertUtil.covertBean(reqJson, GovReportUserPo.class);
        return saveGovReportUserBMOImpl.save(govReportUserPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govReportUser/updateGovReportUser
     * @path /app/govReportUser/updateGovReportUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovReportUser", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovReportUser(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "ruId", "请求报文中未包含ruId");
        Assert.hasKeyAndValue(reqJson, "reportId", "请求报文中未包含reportId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");


        GovReportUserPo govReportUserPo = BeanConvertUtil.covertBean(reqJson, GovReportUserPo.class);
        return updateGovReportUserBMOImpl.update(govReportUserPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportUser/deleteGovReportUser
     * @path /app/govReportUser/deleteGovReportUser
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovReportUser", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovReportUser(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "ruId", "ruId不能为空");


        GovReportUserPo govReportUserPo = BeanConvertUtil.covertBean(reqJson, GovReportUserPo.class);
        return deleteGovReportUserBMOImpl.delete(govReportUserPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govReportUser/queryGovReportUser
     * @path /app/govReportUser/queryGovReportUser
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovReportUser", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovReportUser(@RequestParam(value = "ruId",required = false) String ruId,
                                                     @RequestParam(value = "caId") String caId,
                                                     @RequestParam(value = "reportId",required = false) String reportId,
                                                     @RequestParam(value = "reportEvent",required = false) String reportEvent,
                                                     @RequestParam(value = "preRuId",required = false) String preRuId,
                                                     @RequestParam(value = "state",required = false) String state,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovReportUserDto govReportUserDto = new GovReportUserDto();
        govReportUserDto.setPage(page);
        govReportUserDto.setRow(row);
        govReportUserDto.setRuId(ruId);
        govReportUserDto.setCaId(caId);
        govReportUserDto.setReportId(reportId);
        govReportUserDto.setReportEvent(reportEvent);
        govReportUserDto.setPreRuId(preRuId);
        govReportUserDto.setState(state);
        return getGovReportUserBMOImpl.get(govReportUserDto);
    }

    /**
     * 工单派单处理方法
     *
     * @param reqJson
     * @return
     * @serviceCode /govReportUser/poolDispatch
     * @path /app/govReportUser/poolDispatch
     */
    @RequestMapping(value = "/poolDispatch", method = RequestMethod.POST)
    public ResponseEntity<String> poolDispatch(@RequestHeader("user-id") String userId,
                                               @RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "区域不能为空");
        Assert.hasKeyAndValue(reqJson, "staffId", "未包含员工ID信息");
        Assert.hasKeyAndValue(reqJson, "staffName", "未包含员工名称信息");
        Assert.hasKeyAndValue(reqJson, "reportId", "未包含工单信息");
        Assert.hasKeyAndValue(reqJson, "context", "未包含派单内容");
        Assert.hasKeyAndValue(reqJson, "action", "未包含处理动作");
        reqJson.put("userId", userId);
        return saveGovReportUserBMOImpl.poolDispatch(reqJson);
    }

    /**
     * 办结
     *
     * @param reqJson
     * @return
     * @serviceCode /govReportUser/poolFinish
     * @path /app/housekeepingServPoolUser/poolDispatch
     */
    @RequestMapping(value = "/poolFinish", method = RequestMethod.POST)
    public ResponseEntity<String> poolFinish(@RequestHeader("user-id") String userId,
                                             @RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "区域不能为空");
        Assert.hasKeyAndValue(reqJson, "reportId", "未包含工单信息");
        Assert.hasKeyAndValue(reqJson, "context", "未包含派单内容");
        reqJson.put("staffId", userId);
        return saveGovReportUserBMOImpl.poolFinish(reqJson);
    }
}
