package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovWorkGuideServiceDao;
import com.java110.intf.gov.IGovWorkGuideInnerServiceSMO;
import com.java110.dto.govWorkGuide.GovWorkGuideDto;
import com.java110.po.govWorkGuide.GovWorkGuidePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 办事指南内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovWorkGuideInnerServiceSMOImpl extends BaseServiceSMO implements IGovWorkGuideInnerServiceSMO {

    @Autowired
    private IGovWorkGuideServiceDao govWorkGuideServiceDaoImpl;


    @Override
    public int saveGovWorkGuide(@RequestBody GovWorkGuidePo govWorkGuidePo) {
        int saveFlag = 1;
        govWorkGuideServiceDaoImpl.saveGovWorkGuideInfo(BeanConvertUtil.beanCovertMap(govWorkGuidePo));
        return saveFlag;
    }

     @Override
    public int updateGovWorkGuide(@RequestBody  GovWorkGuidePo govWorkGuidePo) {
        int saveFlag = 1;
         govWorkGuideServiceDaoImpl.updateGovWorkGuideInfo(BeanConvertUtil.beanCovertMap(govWorkGuidePo));
        return saveFlag;
    }

     @Override
    public int deleteGovWorkGuide(@RequestBody  GovWorkGuidePo govWorkGuidePo) {
        int saveFlag = 1;
        govWorkGuidePo.setStatusCd("1");
        govWorkGuideServiceDaoImpl.updateGovWorkGuideInfo(BeanConvertUtil.beanCovertMap(govWorkGuidePo));
        return saveFlag;
    }

    @Override
    public List<GovWorkGuideDto> queryGovWorkGuides(@RequestBody  GovWorkGuideDto govWorkGuideDto) {

        //校验是否传了 分页信息

        int page = govWorkGuideDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govWorkGuideDto.setPage((page - 1) * govWorkGuideDto.getRow());
        }

        List<GovWorkGuideDto> govWorkGuides = BeanConvertUtil.covertBeanList(govWorkGuideServiceDaoImpl.getGovWorkGuideInfo(BeanConvertUtil.beanCovertMap(govWorkGuideDto)), GovWorkGuideDto.class);

        return govWorkGuides;
    }


    @Override
    public int queryGovWorkGuidesCount(@RequestBody GovWorkGuideDto govWorkGuideDto) {
        return govWorkGuideServiceDaoImpl.queryGovWorkGuidesCount(BeanConvertUtil.beanCovertMap(govWorkGuideDto));    }

    public IGovWorkGuideServiceDao getGovWorkGuideServiceDaoImpl() {
        return govWorkGuideServiceDaoImpl;
    }

    public void setGovWorkGuideServiceDaoImpl(IGovWorkGuideServiceDao govWorkGuideServiceDaoImpl) {
        this.govWorkGuideServiceDaoImpl = govWorkGuideServiceDaoImpl;
    }
}
