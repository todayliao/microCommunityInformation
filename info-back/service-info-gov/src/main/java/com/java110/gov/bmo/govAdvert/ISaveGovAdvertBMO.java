package com.java110.gov.bmo.govAdvert;

import com.alibaba.fastjson.JSONObject;
import org.springframework.http.ResponseEntity;
public interface ISaveGovAdvertBMO {


    /**
     * 添加广告
     * add by wuxw
     * @param reqJson
     * @return
     */
    ResponseEntity<String> save(JSONObject reqJson);


}
