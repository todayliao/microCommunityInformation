package com.java110.gov.bmo.govActivityType;

import com.java110.po.govActivityType.GovActivityTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovActivityTypeBMO {


    /**
     * 添加活动类型
     * add by wuxw
     * @param govActivityTypePo
     * @return
     */
    ResponseEntity<String> save(GovActivityTypePo govActivityTypePo);


}
