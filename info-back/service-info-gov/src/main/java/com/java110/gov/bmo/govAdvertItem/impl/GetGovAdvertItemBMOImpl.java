package com.java110.gov.bmo.govAdvertItem.impl;

import com.java110.dto.govAdvertItem.GovAdvertItemDto;
import com.java110.gov.bmo.govAdvertItem.IGetGovAdvertItemBMO;
import com.java110.intf.gov.IGovAdvertItemInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovAdvertItemBMOImpl")
public class GetGovAdvertItemBMOImpl implements IGetGovAdvertItemBMO {

    @Autowired
    private IGovAdvertItemInnerServiceSMO govAdvertItemInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govAdvertItemDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovAdvertItemDto govAdvertItemDto) {


        int count = govAdvertItemInnerServiceSMOImpl.queryGovAdvertItemsCount(govAdvertItemDto);

        List<GovAdvertItemDto> govAdvertItemDtos = null;
        if (count > 0) {
            govAdvertItemDtos = govAdvertItemInnerServiceSMOImpl.queryGovAdvertItems(govAdvertItemDto);
        } else {
            govAdvertItemDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govAdvertItemDto.getRow()), count, govAdvertItemDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
