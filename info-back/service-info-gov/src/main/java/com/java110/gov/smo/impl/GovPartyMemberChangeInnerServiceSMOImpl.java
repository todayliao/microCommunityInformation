package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovPartyMemberChangeServiceDao;
import com.java110.dto.govPartyMemberChange.GovPartyMemberChangeDto;
import com.java110.intf.gov.IGovPartyMemberChangeInnerServiceSMO;
import com.java110.po.govPartyMemberChange.GovPartyMemberChangePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 党关系管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPartyMemberChangeInnerServiceSMOImpl extends BaseServiceSMO implements IGovPartyMemberChangeInnerServiceSMO {

    @Autowired
    private IGovPartyMemberChangeServiceDao govPartyMemberChangeServiceDaoImpl;


    @Override
    public int saveGovPartyMemberChange(@RequestBody GovPartyMemberChangePo govPartyMemberChangePo) {
        int saveFlag = 1;
        govPartyMemberChangeServiceDaoImpl.saveGovPartyMemberChangeInfo(BeanConvertUtil.beanCovertMap(govPartyMemberChangePo));
        return saveFlag;
    }

     @Override
    public int updateGovPartyMemberChange(@RequestBody  GovPartyMemberChangePo govPartyMemberChangePo) {
        int saveFlag = 1;
         govPartyMemberChangeServiceDaoImpl.updateGovPartyMemberChangeInfo(BeanConvertUtil.beanCovertMap(govPartyMemberChangePo));
        return saveFlag;
    }

     @Override
    public int deleteGovPartyMemberChange(@RequestBody  GovPartyMemberChangePo govPartyMemberChangePo) {
        int saveFlag = 1;
        govPartyMemberChangePo.setStatusCd("1");
        govPartyMemberChangeServiceDaoImpl.updateGovPartyMemberChangeInfo(BeanConvertUtil.beanCovertMap(govPartyMemberChangePo));
        return saveFlag;
    }

    @Override
    public List<GovPartyMemberChangeDto> queryGovPartyMemberChanges(@RequestBody  GovPartyMemberChangeDto govPartyMemberChangeDto) {

        //校验是否传了 分页信息

        int page = govPartyMemberChangeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPartyMemberChangeDto.setPage((page - 1) * govPartyMemberChangeDto.getRow());
        }

        List<GovPartyMemberChangeDto> govPartyMemberChanges = BeanConvertUtil.covertBeanList(govPartyMemberChangeServiceDaoImpl.getGovPartyMemberChangeInfo(BeanConvertUtil.beanCovertMap(govPartyMemberChangeDto)), GovPartyMemberChangeDto.class);

        return govPartyMemberChanges;
    }


    @Override
    public int queryGovPartyMemberChangesCount(@RequestBody GovPartyMemberChangeDto govPartyMemberChangeDto) {
        return govPartyMemberChangeServiceDaoImpl.queryGovPartyMemberChangesCount(BeanConvertUtil.beanCovertMap(govPartyMemberChangeDto));    }

    public IGovPartyMemberChangeServiceDao getGovPartyMemberChangeServiceDaoImpl() {
        return govPartyMemberChangeServiceDaoImpl;
    }

    public void setGovPartyMemberChangeServiceDaoImpl(IGovPartyMemberChangeServiceDao govPartyMemberChangeServiceDaoImpl) {
        this.govPartyMemberChangeServiceDaoImpl = govPartyMemberChangeServiceDaoImpl;
    }
}
