package com.java110.gov.bmo.govVolunteerPersonRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govVolunteerPersonRel.IGetGovVolunteerPersonRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovVolunteerPersonRelInnerServiceSMO;
import com.java110.dto.govVolunteerPersonRel.GovVolunteerPersonRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovVolunteerPersonRelBMOImpl")
public class GetGovVolunteerPersonRelBMOImpl implements IGetGovVolunteerPersonRelBMO {

    @Autowired
    private IGovVolunteerPersonRelInnerServiceSMO govVolunteerPersonRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govVolunteerPersonRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovVolunteerPersonRelDto govVolunteerPersonRelDto) {


        int count = govVolunteerPersonRelInnerServiceSMOImpl.queryGovVolunteerPersonRelsCount(govVolunteerPersonRelDto);

        List<GovVolunteerPersonRelDto> govVolunteerPersonRelDtos = null;
        if (count > 0) {
            govVolunteerPersonRelDtos = govVolunteerPersonRelInnerServiceSMOImpl.queryGovVolunteerPersonRels(govVolunteerPersonRelDto);
        } else {
            govVolunteerPersonRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govVolunteerPersonRelDto.getRow()), count, govVolunteerPersonRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
