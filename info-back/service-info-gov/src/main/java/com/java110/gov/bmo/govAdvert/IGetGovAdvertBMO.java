package com.java110.gov.bmo.govAdvert;
import com.java110.dto.govAdvert.GovAdvertDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovAdvertBMO {


    /**
     * 查询广告
     * add by wuxw
     * @param  govAdvertDto
     * @return
     */
    ResponseEntity<String> get(GovAdvertDto govAdvertDto);

    /**
     * 查询广告图片
     * add by wuxw
     * @param  govAdvertDto
     * @return
     */
    ResponseEntity<String> getAdvertItmes(GovAdvertDto govAdvertDto);
}
