package com.java110.gov.bmo.govSchool;

import org.springframework.http.ResponseEntity;
import com.java110.po.govSchool.GovSchoolPo;
public interface ISaveGovSchoolBMO {


    /**
     * 添加学校
     * add by wuxw
     * @param govSchoolPo
     * @return
     */
    ResponseEntity<String> save(GovSchoolPo govSchoolPo);


}
