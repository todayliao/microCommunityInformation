package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovRoadProtectionServiceDao;
import com.java110.intf.gov.IGovRoadProtectionInnerServiceSMO;
import com.java110.dto.govRoadProtection.GovRoadProtectionDto;
import com.java110.po.govRoadProtection.GovRoadProtectionPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 护路护线内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovRoadProtectionInnerServiceSMOImpl extends BaseServiceSMO implements IGovRoadProtectionInnerServiceSMO {

    @Autowired
    private IGovRoadProtectionServiceDao govRoadProtectionServiceDaoImpl;


    @Override
    public int saveGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo) {
        int saveFlag = 1;
        govRoadProtectionServiceDaoImpl.saveGovRoadProtectionInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionPo));
        return saveFlag;
    }

     @Override
    public int updateGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo) {
        int saveFlag = 1;
         govRoadProtectionServiceDaoImpl.updateGovRoadProtectionInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionPo));
        return saveFlag;
    }

     @Override
    public int deleteGovRoadProtection(@RequestBody  GovRoadProtectionPo govRoadProtectionPo) {
        int saveFlag = 1;
        govRoadProtectionPo.setStatusCd("1");
        govRoadProtectionServiceDaoImpl.updateGovRoadProtectionInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionPo));
        return saveFlag;
    }

    @Override
    public List<GovRoadProtectionDto> queryGovRoadProtections(@RequestBody  GovRoadProtectionDto govRoadProtectionDto) {

        //校验是否传了 分页信息

        int page = govRoadProtectionDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govRoadProtectionDto.setPage((page - 1) * govRoadProtectionDto.getRow());
        }

        List<GovRoadProtectionDto> govRoadProtections = BeanConvertUtil.covertBeanList(govRoadProtectionServiceDaoImpl.getGovRoadProtectionInfo(BeanConvertUtil.beanCovertMap(govRoadProtectionDto)), GovRoadProtectionDto.class);

        return govRoadProtections;
    }


    @Override
    public int queryGovRoadProtectionsCount(@RequestBody GovRoadProtectionDto govRoadProtectionDto) {
        return govRoadProtectionServiceDaoImpl.queryGovRoadProtectionsCount(BeanConvertUtil.beanCovertMap(govRoadProtectionDto));    }

    public IGovRoadProtectionServiceDao getGovRoadProtectionServiceDaoImpl() {
        return govRoadProtectionServiceDaoImpl;
    }

    public void setGovRoadProtectionServiceDaoImpl(IGovRoadProtectionServiceDao govRoadProtectionServiceDaoImpl) {
        this.govRoadProtectionServiceDaoImpl = govRoadProtectionServiceDaoImpl;
    }
}
