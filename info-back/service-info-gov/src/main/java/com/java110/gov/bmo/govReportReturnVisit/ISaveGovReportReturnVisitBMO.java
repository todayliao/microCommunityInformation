package com.java110.gov.bmo.govReportReturnVisit;

import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovReportReturnVisitBMO {


    /**
     * 添加回访管理
     * add by wuxw
     * @param govReportReturnVisitPo
     * @return
     */
    ResponseEntity<String> save(GovReportReturnVisitPo govReportReturnVisitPo);


}
