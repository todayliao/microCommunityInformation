package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovVolunteerPersonRelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 服务记录人员关系表服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govVolunteerPersonRelServiceDaoImpl")
//@Transactional
public class GovVolunteerPersonRelServiceDaoImpl extends BaseServiceDao implements IGovVolunteerPersonRelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovVolunteerPersonRelServiceDaoImpl.class);





    /**
     * 保存服务记录人员关系表信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovVolunteerPersonRelInfo(Map info) throws DAOException {
        logger.debug("保存服务记录人员关系表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govVolunteerPersonRelServiceDaoImpl.saveGovVolunteerPersonRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存服务记录人员关系表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询服务记录人员关系表信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovVolunteerPersonRelInfo(Map info) throws DAOException {
        logger.debug("查询服务记录人员关系表信息 入参 info : {}",info);

        List<Map> businessGovVolunteerPersonRelInfos = sqlSessionTemplate.selectList("govVolunteerPersonRelServiceDaoImpl.getGovVolunteerPersonRelInfo",info);

        return businessGovVolunteerPersonRelInfos;
    }


    /**
     * 修改服务记录人员关系表信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovVolunteerPersonRelInfo(Map info) throws DAOException {
        logger.debug("修改服务记录人员关系表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govVolunteerPersonRelServiceDaoImpl.updateGovVolunteerPersonRelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改服务记录人员关系表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询服务记录人员关系表数量
     * @param info 服务记录人员关系表信息
     * @return 服务记录人员关系表数量
     */
    @Override
    public int queryGovVolunteerPersonRelsCount(Map info) {
        logger.debug("查询服务记录人员关系表数据 入参 info : {}",info);

        List<Map> businessGovVolunteerPersonRelInfos = sqlSessionTemplate.selectList("govVolunteerPersonRelServiceDaoImpl.queryGovVolunteerPersonRelsCount", info);
        if (businessGovVolunteerPersonRelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovVolunteerPersonRelInfos.get(0).get("count").toString());
    }


}
