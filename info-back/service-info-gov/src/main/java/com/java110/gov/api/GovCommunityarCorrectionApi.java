package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.po.govCommunityarCorrection.GovCommunityarCorrectionPo;
import com.java110.gov.bmo.govCommunityarCorrection.IDeleteGovCommunityarCorrectionBMO;
import com.java110.gov.bmo.govCommunityarCorrection.IGetGovCommunityarCorrectionBMO;
import com.java110.gov.bmo.govCommunityarCorrection.ISaveGovCommunityarCorrectionBMO;
import com.java110.gov.bmo.govCommunityarCorrection.IUpdateGovCommunityarCorrectionBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCommunityarCorrection")
public class GovCommunityarCorrectionApi {

    @Autowired
    private ISaveGovCommunityarCorrectionBMO saveGovCommunityarCorrectionBMOImpl;
    @Autowired
    private IUpdateGovCommunityarCorrectionBMO updateGovCommunityarCorrectionBMOImpl;
    @Autowired
    private IDeleteGovCommunityarCorrectionBMO deleteGovCommunityarCorrectionBMOImpl;

    @Autowired
    private IGetGovCommunityarCorrectionBMO getGovCommunityarCorrectionBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCommunityarCorrection/saveGovCommunityarCorrection
     * @path /app/govCommunityarCorrection/saveGovCommunityarCorrection
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCommunityarCorrection", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCommunityarCorrection(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "correctionStartTime", "请求报文中未包含correctionStartTime");
        Assert.hasKeyAndValue(reqJson, "correctionEndTime", "请求报文中未包含correctionEndTime");
        Assert.hasKeyAndValue(reqJson, "correctionReason", "请求报文中未包含correctionReason");

        GovCommunityarCorrectionPo govCommunityarCorrectionPo = BeanConvertUtil.covertBean(reqJson, GovCommunityarCorrectionPo.class);
        return saveGovCommunityarCorrectionBMOImpl.save(govCommunityarCorrectionPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCommunityarCorrection/updateGovCommunityarCorrection
     * @path /app/govCommunityarCorrection/updateGovCommunityarCorrection
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCommunityarCorrection", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCommunityarCorrection(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "correctionId", "请求报文中未包含correctionId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "correctionStartTime", "请求报文中未包含correctionStartTime");
        Assert.hasKeyAndValue(reqJson, "correctionEndTime", "请求报文中未包含correctionEndTime");
        Assert.hasKeyAndValue(reqJson, "correctionReason", "请求报文中未包含correctionReason");


        GovCommunityarCorrectionPo govCommunityarCorrectionPo = BeanConvertUtil.covertBean(reqJson, GovCommunityarCorrectionPo.class);
        return updateGovCommunityarCorrectionBMOImpl.update(govCommunityarCorrectionPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunityarCorrection/deleteGovCommunityarCorrection
     * @path /app/govCommunityarCorrection/deleteGovCommunityarCorrection
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCommunityarCorrection", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCommunityarCorrection(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "correctionId", "correctionId不能为空");
        GovCommunityarCorrectionPo govCommunityarCorrectionPo = BeanConvertUtil.covertBean(reqJson, GovCommunityarCorrectionPo.class);
        return deleteGovCommunityarCorrectionBMOImpl.delete(govCommunityarCorrectionPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCommunityarCorrection/queryGovCommunityarCorrection
     * @path /app/govCommunityarCorrection/queryGovCommunityarCorrection
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCommunityarCorrection", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCommunityarCorrection(@RequestParam(value = "caId") String caId,
                                                    @RequestParam(value = "correctionId" , required = false) String correctionId,
                                                    @RequestParam(value = "name" , required = false) String name,
                                                    @RequestParam(value = "tel" , required = false) String tel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCommunityarCorrectionDto govCommunityarCorrectionDto = new GovCommunityarCorrectionDto();
        govCommunityarCorrectionDto.setPage(page);
        govCommunityarCorrectionDto.setRow(row);
        govCommunityarCorrectionDto.setCaId(caId);
        govCommunityarCorrectionDto.setTel(tel);
        govCommunityarCorrectionDto.setName(name);
        govCommunityarCorrectionDto.setCorrectionId(correctionId);
        return getGovCommunityarCorrectionBMOImpl.get(govCommunityarCorrectionDto);
    }
}
