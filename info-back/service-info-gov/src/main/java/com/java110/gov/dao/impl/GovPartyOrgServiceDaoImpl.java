package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovPartyOrgServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 党组织服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPartyOrgServiceDaoImpl")
//@Transactional
public class GovPartyOrgServiceDaoImpl extends BaseServiceDao implements IGovPartyOrgServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPartyOrgServiceDaoImpl.class);





    /**
     * 保存党组织信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPartyOrgInfo(Map info) throws DAOException {
        logger.debug("保存党组织信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPartyOrgServiceDaoImpl.saveGovPartyOrgInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存党组织信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询党组织信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPartyOrgInfo(Map info) throws DAOException {
        logger.debug("查询党组织信息 入参 info : {}",info);

        List<Map> businessGovPartyOrgInfos = sqlSessionTemplate.selectList("govPartyOrgServiceDaoImpl.getGovPartyOrgInfo",info);

        return businessGovPartyOrgInfos;
    }
    /**
     * 查询党组织信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getNotGovPartyOrgInfo(Map info) throws DAOException {
        logger.debug("查询党组织信息 入参 info : {}",info);

        List<Map> businessGovPartyOrgInfos = sqlSessionTemplate.selectList("govPartyOrgServiceDaoImpl.getNotGovPartyOrgInfo",info);

        return businessGovPartyOrgInfos;
    }


    /**
     * 修改党组织信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPartyOrgInfo(Map info) throws DAOException {
        logger.debug("修改党组织信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPartyOrgServiceDaoImpl.updateGovPartyOrgInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改党组织信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询党组织数量
     * @param info 党组织信息
     * @return 党组织数量
     */
    @Override
    public int queryGovPartyOrgsCount(Map info) {
        logger.debug("查询党组织数据 入参 info : {}",info);

        List<Map> businessGovPartyOrgInfos = sqlSessionTemplate.selectList("govPartyOrgServiceDaoImpl.queryGovPartyOrgsCount", info);
        if (businessGovPartyOrgInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPartyOrgInfos.get(0).get("count").toString());
    }


}
