package com.java110.gov.bmo.govReportUser;
import com.java110.dto.govReportUser.GovReportUserDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovReportUserBMO {


    /**
     * 查询报事类型人员
     * add by wuxw
     * @param  govReportUserDto
     * @return
     */
    ResponseEntity<String> get(GovReportUserDto govReportUserDto);


}
