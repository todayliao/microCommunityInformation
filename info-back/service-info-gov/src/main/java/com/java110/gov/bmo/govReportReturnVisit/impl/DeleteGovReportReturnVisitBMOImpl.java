package com.java110.gov.bmo.govReportReturnVisit.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportReturnVisit.IDeleteGovReportReturnVisitBMO;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("deleteGovReportReturnVisitBMOImpl")
public class DeleteGovReportReturnVisitBMOImpl implements IDeleteGovReportReturnVisitBMO {

    @Autowired
    private IGovReportReturnVisitInnerServiceSMO govReportReturnVisitInnerServiceSMOImpl;

    /**
     * @param govReportReturnVisitPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovReportReturnVisitPo govReportReturnVisitPo) {

        int flag = govReportReturnVisitInnerServiceSMOImpl.deleteGovReportReturnVisit(govReportReturnVisitPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
