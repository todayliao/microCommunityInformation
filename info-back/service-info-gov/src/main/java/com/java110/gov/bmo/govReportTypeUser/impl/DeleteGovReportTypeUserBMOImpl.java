package com.java110.gov.bmo.govReportTypeUser.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govReportTypeUser.IDeleteGovReportTypeUserBMO;
import com.java110.intf.gov.IGovReportTypeUserInnerServiceSMO;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovReportTypeUserBMOImpl")
public class DeleteGovReportTypeUserBMOImpl implements IDeleteGovReportTypeUserBMO {

    @Autowired
    private IGovReportTypeUserInnerServiceSMO govReportTypeUserInnerServiceSMOImpl;

    /**
     * @param govReportTypeUserPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovReportTypeUserPo govReportTypeUserPo) {

        int flag = govReportTypeUserInnerServiceSMOImpl.deleteGovReportTypeUser(govReportTypeUserPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
