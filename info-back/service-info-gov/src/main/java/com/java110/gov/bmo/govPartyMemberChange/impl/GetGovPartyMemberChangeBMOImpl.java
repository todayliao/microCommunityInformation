package com.java110.gov.bmo.govPartyMemberChange.impl;

import com.java110.gov.bmo.govPartyMemberChange.IGetGovPartyMemberChangeBMO;
import com.java110.intf.gov.IGovPartyMemberChangeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govPartyMemberChange.GovPartyMemberChangeDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovPartyMemberChangeBMOImpl")
public class GetGovPartyMemberChangeBMOImpl implements IGetGovPartyMemberChangeBMO {

    @Autowired
    private IGovPartyMemberChangeInnerServiceSMO govPartyMemberChangeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPartyMemberChangeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPartyMemberChangeDto govPartyMemberChangeDto) {


        int count = govPartyMemberChangeInnerServiceSMOImpl.queryGovPartyMemberChangesCount(govPartyMemberChangeDto);

        List<GovPartyMemberChangeDto> govPartyMemberChangeDtos = null;
        if (count > 0) {
            govPartyMemberChangeDtos = govPartyMemberChangeInnerServiceSMOImpl.queryGovPartyMemberChanges(govPartyMemberChangeDto);
        } else {
            govPartyMemberChangeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPartyMemberChangeDto.getRow()), count, govPartyMemberChangeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
