package com.java110.gov.bmo.govAdvertItem.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govAdvertItem.ISaveGovAdvertItemBMO;
import com.java110.intf.gov.IGovAdvertItemInnerServiceSMO;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovAdvertItemBMOImpl")
public class SaveGovAdvertItemBMOImpl implements ISaveGovAdvertItemBMO {

    @Autowired
    private IGovAdvertItemInnerServiceSMO govAdvertItemInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govAdvertItemPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovAdvertItemPo govAdvertItemPo) {

        govAdvertItemPo.setAdvertItemId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_advertItemId));
        int flag = govAdvertItemInnerServiceSMOImpl.saveGovAdvertItem(govAdvertItemPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
