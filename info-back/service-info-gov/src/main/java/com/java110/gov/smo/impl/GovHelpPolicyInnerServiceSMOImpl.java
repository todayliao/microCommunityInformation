package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovHelpPolicyServiceDao;
import com.java110.intf.gov.IGovHelpPolicyInnerServiceSMO;
import com.java110.dto.govHelpPolicy.GovHelpPolicyDto;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 帮扶政策内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHelpPolicyInnerServiceSMOImpl extends BaseServiceSMO implements IGovHelpPolicyInnerServiceSMO {

    @Autowired
    private IGovHelpPolicyServiceDao govHelpPolicyServiceDaoImpl;


    @Override
    public int saveGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo) {
        int saveFlag = 1;
        govHelpPolicyServiceDaoImpl.saveGovHelpPolicyInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyPo));
        return saveFlag;
    }

     @Override
    public int updateGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo) {
        int saveFlag = 1;
         govHelpPolicyServiceDaoImpl.updateGovHelpPolicyInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHelpPolicy(@RequestBody  GovHelpPolicyPo govHelpPolicyPo) {
        int saveFlag = 1;
        govHelpPolicyPo.setStatusCd("1");
        govHelpPolicyServiceDaoImpl.updateGovHelpPolicyInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyPo));
        return saveFlag;
    }

    @Override
    public List<GovHelpPolicyDto> queryGovHelpPolicys(@RequestBody  GovHelpPolicyDto govHelpPolicyDto) {

        //校验是否传了 分页信息

        int page = govHelpPolicyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHelpPolicyDto.setPage((page - 1) * govHelpPolicyDto.getRow());
        }

        List<GovHelpPolicyDto> govHelpPolicys = BeanConvertUtil.covertBeanList(govHelpPolicyServiceDaoImpl.getGovHelpPolicyInfo(BeanConvertUtil.beanCovertMap(govHelpPolicyDto)), GovHelpPolicyDto.class);

        return govHelpPolicys;
    }


    @Override
    public int queryGovHelpPolicysCount(@RequestBody GovHelpPolicyDto govHelpPolicyDto) {
        return govHelpPolicyServiceDaoImpl.queryGovHelpPolicysCount(BeanConvertUtil.beanCovertMap(govHelpPolicyDto));    }

    public IGovHelpPolicyServiceDao getGovHelpPolicyServiceDaoImpl() {
        return govHelpPolicyServiceDaoImpl;
    }

    public void setGovHelpPolicyServiceDaoImpl(IGovHelpPolicyServiceDao govHelpPolicyServiceDaoImpl) {
        this.govHelpPolicyServiceDaoImpl = govHelpPolicyServiceDaoImpl;
    }
}
