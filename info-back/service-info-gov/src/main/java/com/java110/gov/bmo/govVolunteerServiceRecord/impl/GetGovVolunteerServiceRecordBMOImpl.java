package com.java110.gov.bmo.govVolunteerServiceRecord.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govVolunteerServiceRecord.IGetGovVolunteerServiceRecordBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovVolunteerServiceRecordInnerServiceSMO;
import com.java110.dto.govVolunteerServiceRecord.GovVolunteerServiceRecordDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovVolunteerServiceRecordBMOImpl")
public class GetGovVolunteerServiceRecordBMOImpl implements IGetGovVolunteerServiceRecordBMO {

    @Autowired
    private IGovVolunteerServiceRecordInnerServiceSMO govVolunteerServiceRecordInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govVolunteerServiceRecordDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovVolunteerServiceRecordDto govVolunteerServiceRecordDto) {


        int count = govVolunteerServiceRecordInnerServiceSMOImpl.queryGovVolunteerServiceRecordsCount(govVolunteerServiceRecordDto);

        List<GovVolunteerServiceRecordDto> govVolunteerServiceRecordDtos = null;
        if (count > 0) {
            govVolunteerServiceRecordDtos = govVolunteerServiceRecordInnerServiceSMOImpl.queryGovVolunteerServiceRecords(govVolunteerServiceRecordDto);
        } else {
            govVolunteerServiceRecordDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govVolunteerServiceRecordDto.getRow()), count, govVolunteerServiceRecordDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
