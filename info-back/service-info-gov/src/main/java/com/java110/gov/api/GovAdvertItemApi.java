package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govAdvertItem.GovAdvertItemDto;
import com.java110.gov.bmo.govAdvertItem.IDeleteGovAdvertItemBMO;
import com.java110.gov.bmo.govAdvertItem.IGetGovAdvertItemBMO;
import com.java110.gov.bmo.govAdvertItem.ISaveGovAdvertItemBMO;
import com.java110.gov.bmo.govAdvertItem.IUpdateGovAdvertItemBMO;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/govAdvertItem")
public class GovAdvertItemApi {

    @Autowired
    private ISaveGovAdvertItemBMO saveGovAdvertItemBMOImpl;
    @Autowired
    private IUpdateGovAdvertItemBMO updateGovAdvertItemBMOImpl;
    @Autowired
    private IDeleteGovAdvertItemBMO deleteGovAdvertItemBMOImpl;

    @Autowired
    private IGetGovAdvertItemBMO getGovAdvertItemBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govAdvertItem/saveGovAdvertItem
     * @path /app/govAdvertItem/saveGovAdvertItem
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovAdvertItem", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovAdvertItem(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "advertId", "请求报文中未包含advertId");
        Assert.hasKeyAndValue(reqJson, "itemTypeCd", "请求报文中未包含itemTypeCd");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");


        GovAdvertItemPo govAdvertItemPo = BeanConvertUtil.covertBean(reqJson, GovAdvertItemPo.class);
        return saveGovAdvertItemBMOImpl.save(govAdvertItemPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govAdvertItem/updateGovAdvertItem
     * @path /app/govAdvertItem/updateGovAdvertItem
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovAdvertItem", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovAdvertItem(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "advertId", "请求报文中未包含advertId");
        Assert.hasKeyAndValue(reqJson, "itemTypeCd", "请求报文中未包含itemTypeCd");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "advertItemId", "advertItemId不能为空");


        GovAdvertItemPo govAdvertItemPo = BeanConvertUtil.covertBean(reqJson, GovAdvertItemPo.class);
        return updateGovAdvertItemBMOImpl.update(govAdvertItemPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAdvertItem/deleteGovAdvertItem
     * @path /app/govAdvertItem/deleteGovAdvertItem
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovAdvertItem", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovAdvertItem(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "advertItemId", "advertItemId不能为空");


        GovAdvertItemPo govAdvertItemPo = BeanConvertUtil.covertBean(reqJson, GovAdvertItemPo.class);
        return deleteGovAdvertItemBMOImpl.delete(govAdvertItemPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govAdvertItem/queryGovAdvertItem
     * @path /app/govAdvertItem/queryGovAdvertItem
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovAdvertItem", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAdvertItem(@RequestParam(value = "caId") String caId,
                                                     @RequestParam(value = "advertId" , required = false) String advertId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovAdvertItemDto govAdvertItemDto = new GovAdvertItemDto();
        govAdvertItemDto.setAdvertId(advertId);
        govAdvertItemDto.setPage(page);
        govAdvertItemDto.setRow(row);
        return getGovAdvertItemBMOImpl.get(govAdvertItemDto);
    }
}
