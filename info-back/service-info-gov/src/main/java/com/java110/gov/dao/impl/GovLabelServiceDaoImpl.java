package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovLabelServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 标签管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govLabelServiceDaoImpl")
//@Transactional
public class GovLabelServiceDaoImpl extends BaseServiceDao implements IGovLabelServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovLabelServiceDaoImpl.class);





    /**
     * 保存标签管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovLabelInfo(Map info) throws DAOException {
        logger.debug("保存标签管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govLabelServiceDaoImpl.saveGovLabelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存标签管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询标签管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovLabelInfo(Map info) throws DAOException {
        logger.debug("查询标签管理信息 入参 info : {}",info);

        List<Map> businessGovLabelInfos = sqlSessionTemplate.selectList("govLabelServiceDaoImpl.getGovLabelInfo",info);

        return businessGovLabelInfos;
    }


    /**
     * 修改标签管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovLabelInfo(Map info) throws DAOException {
        logger.debug("修改标签管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govLabelServiceDaoImpl.updateGovLabelInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改标签管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询标签管理数量
     * @param info 标签管理信息
     * @return 标签管理数量
     */
    @Override
    public int queryGovLabelsCount(Map info) {
        logger.debug("查询标签管理数据 入参 info : {}",info);

        List<Map> businessGovLabelInfos = sqlSessionTemplate.selectList("govLabelServiceDaoImpl.queryGovLabelsCount", info);
        if (businessGovLabelInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovLabelInfos.get(0).get("count").toString());
    }


}
