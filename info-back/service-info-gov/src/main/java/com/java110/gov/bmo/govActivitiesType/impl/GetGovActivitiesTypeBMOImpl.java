package com.java110.gov.bmo.govActivitiesType.impl;

import com.java110.gov.bmo.govActivitiesType.IGetGovActivitiesTypeBMO;
import com.java110.intf.gov.IGovActivitiesTypeInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govActivitiesType.GovActivitiesTypeDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovActivitiesTypeBMOImpl")
public class GetGovActivitiesTypeBMOImpl implements IGetGovActivitiesTypeBMO {

    @Autowired
    private IGovActivitiesTypeInnerServiceSMO govActivitiesTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govActivitiesTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovActivitiesTypeDto govActivitiesTypeDto) {


        int count = govActivitiesTypeInnerServiceSMOImpl.queryGovActivitiesTypesCount(govActivitiesTypeDto);

        List<GovActivitiesTypeDto> govActivitiesTypeDtos = null;
        if (count > 0) {
            govActivitiesTypeDtos = govActivitiesTypeInnerServiceSMOImpl.queryGovActivitiesTypes(govActivitiesTypeDto);
        } else {
            govActivitiesTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govActivitiesTypeDto.getRow()), count, govActivitiesTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
