package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govSchool.GovSchoolDto;
import com.java110.po.govSchool.GovSchoolPo;
import com.java110.gov.bmo.govSchool.IDeleteGovSchoolBMO;
import com.java110.gov.bmo.govSchool.IGetGovSchoolBMO;
import com.java110.gov.bmo.govSchool.ISaveGovSchoolBMO;
import com.java110.gov.bmo.govSchool.IUpdateGovSchoolBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govSchool")
public class GovSchoolApi {

    @Autowired
    private ISaveGovSchoolBMO saveGovSchoolBMOImpl;
    @Autowired
    private IUpdateGovSchoolBMO updateGovSchoolBMOImpl;
    @Autowired
    private IDeleteGovSchoolBMO deleteGovSchoolBMOImpl;

    @Autowired
    private IGetGovSchoolBMO getGovSchoolBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchool/saveGovSchool
     * @path /app/govSchool/saveGovSchool
     */
    @RequestMapping(value = "/saveGovSchool", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovSchool(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "schoolName", "请求报文中未包含schoolName");
        Assert.hasKeyAndValue(reqJson, "schoolAddress", "请求报文中未包含schoolAddress");
        Assert.hasKeyAndValue(reqJson, "schoolType", "请求报文中未包含schoolType");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "studentNum", "请求报文中未包含studentNum");
        Assert.hasKeyAndValue(reqJson, "schoolmaster", "请求报文中未包含schoolmaster");
        Assert.hasKeyAndValue(reqJson, "masterTel", "请求报文中未包含masterTel");
        Assert.hasKeyAndValue(reqJson, "partSafetyLeader", "请求报文中未包含partSafetyLeader");
        Assert.hasKeyAndValue(reqJson, "partLeaderTel", "请求报文中未包含partLeaderTel");
        Assert.hasKeyAndValue(reqJson, "safetyLeader", "请求报文中未包含safetyLeader");
        Assert.hasKeyAndValue(reqJson, "leaderTel", "请求报文中未包含leaderTel");
        Assert.hasKeyAndValue(reqJson, "securityLeader", "请求报文中未包含securityLeader");
        Assert.hasKeyAndValue(reqJson, "securityLeaderTel", "请求报文中未包含securityLeaderTel");
        Assert.hasKeyAndValue(reqJson, "securityPersonnelNum", "请求报文中未包含securityPersonnelNum");


        GovSchoolPo govSchoolPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPo.class);
        return saveGovSchoolBMOImpl.save(govSchoolPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchool/updateGovSchool
     * @path /app/govSchool/updateGovSchool
     */
    @RequestMapping(value = "/updateGovSchool", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovSchool(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "schoolId", "请求报文中未包含schoolId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "schoolName", "请求报文中未包含schoolName");
        Assert.hasKeyAndValue(reqJson, "schoolAddress", "请求报文中未包含schoolAddress");
        Assert.hasKeyAndValue(reqJson, "schoolType", "请求报文中未包含schoolType");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "studentNum", "请求报文中未包含studentNum");
        Assert.hasKeyAndValue(reqJson, "schoolmaster", "请求报文中未包含schoolmaster");
        Assert.hasKeyAndValue(reqJson, "masterTel", "请求报文中未包含masterTel");
        Assert.hasKeyAndValue(reqJson, "partSafetyLeader", "请求报文中未包含partSafetyLeader");
        Assert.hasKeyAndValue(reqJson, "partLeaderTel", "请求报文中未包含partLeaderTel");
        Assert.hasKeyAndValue(reqJson, "safetyLeader", "请求报文中未包含safetyLeader");
        Assert.hasKeyAndValue(reqJson, "leaderTel", "请求报文中未包含leaderTel");
        Assert.hasKeyAndValue(reqJson, "securityLeader", "请求报文中未包含securityLeader");
        Assert.hasKeyAndValue(reqJson, "securityLeaderTel", "请求报文中未包含securityLeaderTel");
        Assert.hasKeyAndValue(reqJson, "securityPersonnelNum", "请求报文中未包含securityPersonnelNum");


        GovSchoolPo govSchoolPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPo.class);
        return updateGovSchoolBMOImpl.update(govSchoolPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govSchool/deleteGovSchool
     * @path /app/govSchool/deleteGovSchool
     */
    @RequestMapping(value = "/deleteGovSchool", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovSchool(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "schoolId", "schoolId不能为空");


        GovSchoolPo govSchoolPo = BeanConvertUtil.covertBean(reqJson, GovSchoolPo.class);
        return deleteGovSchoolBMOImpl.delete(govSchoolPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govSchool/queryGovSchool
     * @path /app/govSchool/queryGovSchool
     */
    @RequestMapping(value = "/queryGovSchool", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovSchool(@RequestParam(value = "caId") String caId,
                                                 @RequestParam(value = "schoolName" , required = false) String schoolName,
                                                 @RequestParam(value = "schoolType" , required = false) String schoolType,
                                                 @RequestParam(value = "securityLeaderTel" , required = false) String securityLeaderTel,
                                                 @RequestParam(value = "schoolmaster" , required = false) String schoolmaster,
                                                 @RequestParam(value = "masterTel" , required = false) String masterTel,
                                                 @RequestParam(value = "page") int page,
                                                 @RequestParam(value = "row") int row) {
        GovSchoolDto govSchoolDto = new GovSchoolDto();
        govSchoolDto.setPage(page);
        govSchoolDto.setRow(row);
        govSchoolDto.setSchoolName(schoolName);
        govSchoolDto.setSchoolType(schoolType);
        govSchoolDto.setSecurityLeaderTel(securityLeaderTel);
        govSchoolDto.setSchoolmaster(schoolmaster);
        govSchoolDto.setMasterTel(masterTel);
        govSchoolDto.setCaId(caId);
        return getGovSchoolBMOImpl.get(govSchoolDto);
    }
}
