package com.java110.gov.bmo.govPersonLabelRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govPersonLabelRel.IGetGovPersonLabelRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonLabelRelBMOImpl")
public class GetGovPersonLabelRelBMOImpl implements IGetGovPersonLabelRelBMO {

    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govPersonLabelRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonLabelRelDto govPersonLabelRelDto) {


        int count = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRelsCount(govPersonLabelRelDto);

        List<GovPersonLabelRelDto> govPersonLabelRelDtos = null;
        if (count > 0) {
            govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels(govPersonLabelRelDto);
        } else {
            govPersonLabelRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonLabelRelDto.getRow()), count, govPersonLabelRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
