package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovDrugServiceDao;
import com.java110.intf.gov.IGovDrugInnerServiceSMO;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.po.govDrug.GovDrugPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 吸毒者内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovDrugInnerServiceSMOImpl extends BaseServiceSMO implements IGovDrugInnerServiceSMO {

    @Autowired
    private IGovDrugServiceDao govDrugServiceDaoImpl;


    @Override
    public int saveGovDrug(@RequestBody  GovDrugPo govDrugPo) {
        int saveFlag = 1;
        govDrugServiceDaoImpl.saveGovDrugInfo(BeanConvertUtil.beanCovertMap(govDrugPo));
        return saveFlag;
    }

     @Override
    public int updateGovDrug(@RequestBody  GovDrugPo govDrugPo) {
        int saveFlag = 1;
         govDrugServiceDaoImpl.updateGovDrugInfo(BeanConvertUtil.beanCovertMap(govDrugPo));
        return saveFlag;
    }

     @Override
    public int deleteGovDrug(@RequestBody  GovDrugPo govDrugPo) {
        int saveFlag = 1;
        govDrugPo.setStatusCd("1");
        govDrugServiceDaoImpl.updateGovDrugInfo(BeanConvertUtil.beanCovertMap(govDrugPo));
        return saveFlag;
    }

    @Override
    public List<GovDrugDto> queryGovDrugs(@RequestBody  GovDrugDto govDrugDto) {

        //校验是否传了 分页信息

        int page = govDrugDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govDrugDto.setPage((page - 1) * govDrugDto.getRow());
        }

        List<GovDrugDto> govDrugs = BeanConvertUtil.covertBeanList(govDrugServiceDaoImpl.getGovDrugInfo(BeanConvertUtil.beanCovertMap(govDrugDto)), GovDrugDto.class);

        return govDrugs;
    }


    @Override
    public int queryGovDrugsCount(@RequestBody GovDrugDto govDrugDto) {
        return govDrugServiceDaoImpl.queryGovDrugsCount(BeanConvertUtil.beanCovertMap(govDrugDto));    }

    public IGovDrugServiceDao getGovDrugServiceDaoImpl() {
        return govDrugServiceDaoImpl;
    }

    public void setGovDrugServiceDaoImpl(IGovDrugServiceDao govDrugServiceDaoImpl) {
        this.govDrugServiceDaoImpl = govDrugServiceDaoImpl;
    }
}
