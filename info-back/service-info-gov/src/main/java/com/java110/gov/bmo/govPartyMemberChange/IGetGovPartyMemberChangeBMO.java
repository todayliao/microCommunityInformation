package com.java110.gov.bmo.govPartyMemberChange;
import com.java110.dto.govPartyMemberChange.GovPartyMemberChangeDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovPartyMemberChangeBMO {


    /**
     * 查询党关系管理
     * add by wuxw
     * @param  govPartyMemberChangeDto
     * @return
     */
    ResponseEntity<String> get(GovPartyMemberChangeDto govPartyMemberChangeDto);


}
