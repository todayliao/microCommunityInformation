package com.java110.gov.bmo.govRenovationCheck.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govRenovationCheck.IGetGovRenovationCheckBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovRenovationCheckInnerServiceSMO;
import com.java110.dto.govRenovationCheck.GovRenovationCheckDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovRenovationCheckBMOImpl")
public class GetGovRenovationCheckBMOImpl implements IGetGovRenovationCheckBMO {

    @Autowired
    private IGovRenovationCheckInnerServiceSMO govRenovationCheckInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govRenovationCheckDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovRenovationCheckDto govRenovationCheckDto) {


        int count = govRenovationCheckInnerServiceSMOImpl.queryGovRenovationChecksCount(govRenovationCheckDto);

        List<GovRenovationCheckDto> govRenovationCheckDtos = null;
        if (count > 0) {
            govRenovationCheckDtos = govRenovationCheckInnerServiceSMOImpl.queryGovRenovationChecks(govRenovationCheckDto);
        } else {
            govRenovationCheckDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govRenovationCheckDto.getRow()), count, govRenovationCheckDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
