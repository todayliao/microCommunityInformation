package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovReportTypeUserServiceDao;
import com.java110.dto.govReportTypeUser.GovReportTypeUserDto;
import com.java110.intf.gov.IGovReportTypeUserInnerServiceSMO;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 报事类型人员内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovReportTypeUserInnerServiceSMOImpl extends BaseServiceSMO implements IGovReportTypeUserInnerServiceSMO {

    @Autowired
    private IGovReportTypeUserServiceDao govReportTypeUserServiceDaoImpl;


    @Override
    public int saveGovReportTypeUser(@RequestBody GovReportTypeUserPo govReportTypeUserPo) {
        int saveFlag = 1;
        govReportTypeUserServiceDaoImpl.saveGovReportTypeUserInfo(BeanConvertUtil.beanCovertMap(govReportTypeUserPo));
        return saveFlag;
    }

     @Override
    public int updateGovReportTypeUser(@RequestBody  GovReportTypeUserPo govReportTypeUserPo) {
        int saveFlag = 1;
         govReportTypeUserServiceDaoImpl.updateGovReportTypeUserInfo(BeanConvertUtil.beanCovertMap(govReportTypeUserPo));
        return saveFlag;
    }

     @Override
    public int deleteGovReportTypeUser(@RequestBody  GovReportTypeUserPo govReportTypeUserPo) {
        int saveFlag = 1;
        govReportTypeUserPo.setStatusCd("1");
        govReportTypeUserServiceDaoImpl.updateGovReportTypeUserInfo(BeanConvertUtil.beanCovertMap(govReportTypeUserPo));
        return saveFlag;
    }

    @Override
    public List<GovReportTypeUserDto> queryGovReportTypeUsers(@RequestBody  GovReportTypeUserDto govReportTypeUserDto) {

        //校验是否传了 分页信息

        int page = govReportTypeUserDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govReportTypeUserDto.setPage((page - 1) * govReportTypeUserDto.getRow());
        }

        List<GovReportTypeUserDto> govReportTypeUsers = BeanConvertUtil.covertBeanList(govReportTypeUserServiceDaoImpl.getGovReportTypeUserInfo(BeanConvertUtil.beanCovertMap(govReportTypeUserDto)), GovReportTypeUserDto.class);

        return govReportTypeUsers;
    }


    @Override
    public int queryGovReportTypeUsersCount(@RequestBody GovReportTypeUserDto govReportTypeUserDto) {
        return govReportTypeUserServiceDaoImpl.queryGovReportTypeUsersCount(BeanConvertUtil.beanCovertMap(govReportTypeUserDto));    }

    public IGovReportTypeUserServiceDao getGovReportTypeUserServiceDaoImpl() {
        return govReportTypeUserServiceDaoImpl;
    }

    public void setGovReportTypeUserServiceDaoImpl(IGovReportTypeUserServiceDao govReportTypeUserServiceDaoImpl) {
        this.govReportTypeUserServiceDaoImpl = govReportTypeUserServiceDaoImpl;
    }
}
