package com.java110.gov.bmo.govPartyOrgContext.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govPartyOrgContext.IUpdateGovPartyOrgContextBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("updateGovPartyOrgContextBMOImpl")
public class UpdateGovPartyOrgContextBMOImpl implements IUpdateGovPartyOrgContextBMO {

    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;

    /**
     *
     *
     * @param govPartyOrgContextPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovPartyOrgContextPo govPartyOrgContextPo) {

        int flag = govPartyOrgContextInnerServiceSMOImpl.updateGovPartyOrgContext(govPartyOrgContextPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
