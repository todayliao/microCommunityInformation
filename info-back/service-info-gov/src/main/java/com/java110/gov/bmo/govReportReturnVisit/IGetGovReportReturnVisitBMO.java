package com.java110.gov.bmo.govReportReturnVisit;
import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovReportReturnVisitBMO {


    /**
     * 查询回访管理
     * add by wuxw
     * @param  govReportReturnVisitDto
     * @return
     */
    ResponseEntity<String> get(GovReportReturnVisitDto govReportReturnVisitDto);


}
