package com.java110.gov.bmo.govHelpPolicyList;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govHelpPolicyList.GovHelpPolicyListDto;
public interface IGetGovHelpPolicyListBMO {


    /**
     * 查询帮扶记录
     * add by wuxw
     * @param  govHelpPolicyListDto
     * @return
     */
    ResponseEntity<String> get(GovHelpPolicyListDto govHelpPolicyListDto);


}
