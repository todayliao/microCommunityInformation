package com.java110.gov.bmo.govMeetingList.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.gov.bmo.govMeetingList.IUpdateGovMeetingListBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovMeetingListInnerServiceSMO;
import com.java110.dto.govMeetingList.GovMeetingListDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govMeetingList.GovMeetingListPo;

import java.util.ArrayList;
import java.util.List;

@Service("updateGovMeetingListBMOImpl")
public class UpdateGovMeetingListBMOImpl implements IUpdateGovMeetingListBMO {

    @Autowired
    private IGovMeetingListInnerServiceSMO govMeetingListInnerServiceSMOImpl;
    @Autowired
    private IGovMeetingMemberRelInnerServiceSMO govMeetingMemberRelInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     * @param govMeetingListPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovMeetingListPo govMeetingListPo, JSONArray govMembers) {


        int flag = govMeetingListInnerServiceSMOImpl.updateGovMeetingList(govMeetingListPo);

        if (flag < 1) {
            throw new IllegalArgumentException("保存会议信息失败");
        }
        if (govMembers != null && govMembers.size() > 0) {

            GovMeetingMemberRelDto govMeetingMemberRelDto = new GovMeetingMemberRelDto();
            govMeetingMemberRelDto.setGovMeetingId(govMeetingListPo.getGovMeetingId());
            govMeetingMemberRelDto.setCaId(govMeetingListPo.getCaId());
            List<GovMeetingMemberRelDto> govMeetingMemberRelDtos = govMeetingMemberRelInnerServiceSMOImpl.queryGovMeetingMemberRels(govMeetingMemberRelDto);

            if (govMeetingMemberRelDtos != null && govMeetingMemberRelDtos.size() > 0) {

                List<String> memberRelIds = new ArrayList<>();
                for (GovMeetingMemberRelDto GovMeetingMemberRel : govMeetingMemberRelDtos) {
                    memberRelIds.add(GovMeetingMemberRel.getMemberRelId());
                }
                GovMeetingMemberRelPo govMeetingMemberRelPo = new GovMeetingMemberRelPo();
                govMeetingMemberRelPo.setMemberRelIds(memberRelIds.toArray(new String[memberRelIds.size()]));
                flag = govMeetingMemberRelInnerServiceSMOImpl.deleteGovMeetingMemberRel(govMeetingMemberRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("删除参会人员失败");
                }
            }

            for (int GovMeetingMemberRelIndex = 0; GovMeetingMemberRelIndex < govMembers.size(); GovMeetingMemberRelIndex++) {
                GovMeetingMemberRelPo govMeetingMemberRelPo = new GovMeetingMemberRelPo();
                govMeetingMemberRelPo = BeanConvertUtil.covertBean(govMembers.getJSONObject(GovMeetingMemberRelIndex), GovMeetingMemberRelPo.class);
                govMeetingMemberRelPo.setGovMeetingId(govMeetingListPo.getGovMeetingId());
                govMeetingMemberRelPo.setMemberRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_memberRelId));
                flag = govMeetingMemberRelInnerServiceSMOImpl.saveGovMeetingMemberRel(govMeetingMemberRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存会议参会人员信息失败");
                }
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    /**
     * @param govMeetingListPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> recordGovMeetingList(GovMeetingListPo govMeetingListPo, JSONArray meetingImgs) {

        int flag = govMeetingListInnerServiceSMOImpl.updateGovMeetingList(govMeetingListPo);
        if (flag < 1) {
            throw new IllegalArgumentException("修改会议信息失败");
        }
        if (meetingImgs != null && meetingImgs.size() > 0) {

            FileRelDto fileRelDto = new FileRelDto();
            fileRelDto.setObjId(govMeetingListPo.getGovMeetingId());
            fileRelDto.setRelType(FileRelDto.REL_MEETING_TYPE);
            List<FileRelDto> fileRelDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
            if (fileRelDtos != null && fileRelDtos.size() > 0) {
                //删除原文件关系
                FileRelPo fileRelPo = new FileRelPo();
                fileRelPo.setObjId(govMeetingListPo.getGovMeetingId());
                fileRelPo.setRelType(FileRelDto.REL_MEETING_TYPE);
                flag = fileRelInnerServiceSMOImpl.deleteFileRel(fileRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("删除文件关系失败");
                }
            }
            for (int meetingImgIndex = 0; meetingImgIndex < meetingImgs.size(); meetingImgIndex++) {
                getFileRelPo(govMeetingListPo.getGovMeetingId(), FileRelDto.REL_MEETING_TYPE, meetingImgs.getString(meetingImgIndex));
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private FileRelPo getFileRelPo(String objId, String relType, String objImg) {

        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(objImg);
        fileRelPo.setRelType(relType);
        fileRelPo.setObjId(objId);
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存文件关系失败");
        }
        return fileRelPo;
    }
}
