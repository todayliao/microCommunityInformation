package com.java110.gov.bmo.govActivity.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govActivity.ISaveGovActivityBMO;
import com.java110.intf.gov.IGovActivityInnerServiceSMO;
import com.java110.po.govActivity.GovActivityPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovActivityBMOImpl")
public class SaveGovActivityBMOImpl implements ISaveGovActivityBMO {

    @Autowired
    private IGovActivityInnerServiceSMO govActivityInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govActivityPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovActivityPo govActivityPo) {

        govActivityPo.setActId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_actId));
        int flag = govActivityInnerServiceSMOImpl.saveGovActivity(govActivityPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
