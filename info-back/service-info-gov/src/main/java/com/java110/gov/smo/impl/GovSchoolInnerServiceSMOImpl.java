package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovSchoolServiceDao;
import com.java110.intf.gov.IGovSchoolInnerServiceSMO;
import com.java110.dto.govSchool.GovSchoolDto;
import com.java110.po.govSchool.GovSchoolPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 学校内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovSchoolInnerServiceSMOImpl extends BaseServiceSMO implements IGovSchoolInnerServiceSMO {

    @Autowired
    private IGovSchoolServiceDao govSchoolServiceDaoImpl;


    @Override
    public int saveGovSchool(@RequestBody  GovSchoolPo govSchoolPo) {
        int saveFlag = 1;
        govSchoolServiceDaoImpl.saveGovSchoolInfo(BeanConvertUtil.beanCovertMap(govSchoolPo));
        return saveFlag;
    }

     @Override
    public int updateGovSchool(@RequestBody  GovSchoolPo govSchoolPo) {
        int saveFlag = 1;
         govSchoolServiceDaoImpl.updateGovSchoolInfo(BeanConvertUtil.beanCovertMap(govSchoolPo));
        return saveFlag;
    }

     @Override
    public int deleteGovSchool(@RequestBody  GovSchoolPo govSchoolPo) {
        int saveFlag = 1;
        govSchoolPo.setStatusCd("1");
        govSchoolServiceDaoImpl.updateGovSchoolInfo(BeanConvertUtil.beanCovertMap(govSchoolPo));
        return saveFlag;
    }

    @Override
    public List<GovSchoolDto> queryGovSchools(@RequestBody  GovSchoolDto govSchoolDto) {

        //校验是否传了 分页信息

        int page = govSchoolDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govSchoolDto.setPage((page - 1) * govSchoolDto.getRow());
        }

        List<GovSchoolDto> govSchools = BeanConvertUtil.covertBeanList(govSchoolServiceDaoImpl.getGovSchoolInfo(BeanConvertUtil.beanCovertMap(govSchoolDto)), GovSchoolDto.class);

        return govSchools;
    }


    @Override
    public int queryGovSchoolsCount(@RequestBody GovSchoolDto govSchoolDto) {
        return govSchoolServiceDaoImpl.queryGovSchoolsCount(BeanConvertUtil.beanCovertMap(govSchoolDto));    }

    public IGovSchoolServiceDao getGovSchoolServiceDaoImpl() {
        return govSchoolServiceDaoImpl;
    }

    public void setGovSchoolServiceDaoImpl(IGovSchoolServiceDao govSchoolServiceDaoImpl) {
        this.govSchoolServiceDaoImpl = govSchoolServiceDaoImpl;
    }
}
