package com.java110.gov.bmo.govWorkGuide.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.gov.bmo.govWorkGuide.ISaveGovWorkGuideBMO;
import com.java110.intf.gov.IGovWorkGuideInnerServiceSMO;
import com.java110.po.govWorkGuide.GovWorkGuidePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovWorkGuideBMOImpl")
public class SaveGovWorkGuideBMOImpl implements ISaveGovWorkGuideBMO {

    @Autowired
    private IGovWorkGuideInnerServiceSMO govWorkGuideInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govWorkGuidePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovWorkGuidePo govWorkGuidePo) {

        govWorkGuidePo.setWgId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_wgId));
        int flag = govWorkGuideInnerServiceSMOImpl.saveGovWorkGuide(govWorkGuidePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
