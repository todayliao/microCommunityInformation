package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovCarInoutServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 车辆进出记录服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCarInoutServiceDaoImpl")
//@Transactional
public class GovCarInoutServiceDaoImpl extends BaseServiceDao implements IGovCarInoutServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCarInoutServiceDaoImpl.class);





    /**
     * 保存车辆进出记录信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCarInoutInfo(Map info) throws DAOException {
        logger.debug("保存车辆进出记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCarInoutServiceDaoImpl.saveGovCarInoutInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存车辆进出记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询车辆进出记录信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCarInoutInfo(Map info) throws DAOException {
        logger.debug("查询车辆进出记录信息 入参 info : {}",info);

        List<Map> businessGovCarInoutInfos = sqlSessionTemplate.selectList("govCarInoutServiceDaoImpl.getGovCarInoutInfo",info);

        return businessGovCarInoutInfos;
    }


    /**
     * 修改车辆进出记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCarInoutInfo(Map info) throws DAOException {
        logger.debug("修改车辆进出记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCarInoutServiceDaoImpl.updateGovCarInoutInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改车辆进出记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询车辆进出记录数量
     * @param info 车辆进出记录信息
     * @return 车辆进出记录数量
     */
    @Override
    public int queryGovCarInoutsCount(Map info) {
        logger.debug("查询车辆进出记录数据 入参 info : {}",info);

        List<Map> businessGovCarInoutInfos = sqlSessionTemplate.selectList("govCarInoutServiceDaoImpl.queryGovCarInoutsCount", info);
        if (businessGovCarInoutInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCarInoutInfos.get(0).get("count").toString());
    }


}
