package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govRoadProtectionCase.GovRoadProtectionCaseDto;
import com.java110.po.govRoadProtectionCase.GovRoadProtectionCasePo;
import com.java110.gov.bmo.govRoadProtectionCase.IDeleteGovRoadProtectionCaseBMO;
import com.java110.gov.bmo.govRoadProtectionCase.IGetGovRoadProtectionCaseBMO;
import com.java110.gov.bmo.govRoadProtectionCase.ISaveGovRoadProtectionCaseBMO;
import com.java110.gov.bmo.govRoadProtectionCase.IUpdateGovRoadProtectionCaseBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govRoadProtectionCase")
public class GovRoadProtectionCaseApi {

    @Autowired
    private ISaveGovRoadProtectionCaseBMO saveGovRoadProtectionCaseBMOImpl;
    @Autowired
    private IUpdateGovRoadProtectionCaseBMO updateGovRoadProtectionCaseBMOImpl;
    @Autowired
    private IDeleteGovRoadProtectionCaseBMO deleteGovRoadProtectionCaseBMOImpl;

    @Autowired
    private IGetGovRoadProtectionCaseBMO getGovRoadProtectionCaseBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtectionCase/saveGovRoadProtectionCase
     * @path /app/govRoadProtectionCase/saveGovRoadProtectionCase
     */
    @RequestMapping(value = "/saveGovRoadProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovRoadProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "roadProtectionId", "请求报文中未包含roadProtectionId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "caseName", "请求报文中未包含caseName");
        Assert.hasKeyAndValue(reqJson, "caseCode", "请求报文中未包含caseCode");
        Assert.hasKeyAndValue(reqJson, "happenedTime", "请求报文中未包含happenedTime");
        Assert.hasKeyAndValue(reqJson, "happenedPlace", "请求报文中未包含happenedPlace");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "isSolved", "请求报文中未包含isSolved");
        Assert.hasKeyAndValue(reqJson, "personNum", "请求报文中未包含personNum");
        Assert.hasKeyAndValue(reqJson, "fleeingNum", "请求报文中未包含fleeingNum");
        Assert.hasKeyAndValue(reqJson, "arrestsNum", "请求报文中未包含arrestsNum");
        Assert.hasKeyAndValue(reqJson, "detectionContent", "请求报文中未包含detectionContent");
        Assert.hasKeyAndValue(reqJson, "caseContent", "请求报文中未包含caseContent");

        GovRoadProtectionCasePo govRoadProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionCasePo.class);
        return saveGovRoadProtectionCaseBMOImpl.save(govRoadProtectionCasePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtectionCase/updateGovRoadProtectionCase
     * @path /app/govRoadProtectionCase/updateGovRoadProtectionCase
     */
    @RequestMapping(value = "/updateGovRoadProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovRoadProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "roadCaseId", "请求报文中未包含roadCaseId");
        Assert.hasKeyAndValue(reqJson, "roadProtectionId", "请求报文中未包含roadProtectionId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "caseName", "请求报文中未包含caseName");
        Assert.hasKeyAndValue(reqJson, "caseCode", "请求报文中未包含caseCode");
        Assert.hasKeyAndValue(reqJson, "happenedTime", "请求报文中未包含happenedTime");
        Assert.hasKeyAndValue(reqJson, "happenedPlace", "请求报文中未包含happenedPlace");
        Assert.hasKeyAndValue(reqJson, "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "isSolved", "请求报文中未包含isSolved");
        Assert.hasKeyAndValue(reqJson, "personNum", "请求报文中未包含personNum");
        Assert.hasKeyAndValue(reqJson, "fleeingNum", "请求报文中未包含fleeingNum");
        Assert.hasKeyAndValue(reqJson, "arrestsNum", "请求报文中未包含arrestsNum");
        Assert.hasKeyAndValue(reqJson, "detectionContent", "请求报文中未包含detectionContent");
        Assert.hasKeyAndValue(reqJson, "caseContent", "请求报文中未包含caseContent");


        GovRoadProtectionCasePo govRoadProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionCasePo.class);
        return updateGovRoadProtectionCaseBMOImpl.update(govRoadProtectionCasePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govRoadProtectionCase/deleteGovRoadProtectionCase
     * @path /app/govRoadProtectionCase/deleteGovRoadProtectionCase
     */
    @RequestMapping(value = "/deleteGovRoadProtectionCase", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovRoadProtectionCase(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "roadCaseId", "roadCaseId不能为空");


        GovRoadProtectionCasePo govRoadProtectionCasePo = BeanConvertUtil.covertBean(reqJson, GovRoadProtectionCasePo.class);
        return deleteGovRoadProtectionCaseBMOImpl.delete(govRoadProtectionCasePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govRoadProtectionCase/queryGovRoadProtectionCase
     * @path /app/govRoadProtectionCase/queryGovRoadProtectionCase
     */
    @RequestMapping(value = "/queryGovRoadProtectionCase", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovRoadProtectionCase(@RequestParam(value = "caId") String caId,
                                                             @RequestParam(value = "happenedPlace",required = false) String happenedPlace,
                                                             @RequestParam(value = "happenedTime",required = false) String happenedTime,
                                                             @RequestParam(value = "personName",required = false) String personName,
                                                             @RequestParam(value = "isSolved",required = false) String isSolved,
                                                             @RequestParam(value = "idCard",required = false) String idCard,
                                                             @RequestParam(value = "caseName",required = false) String caseName,
                                                             @RequestParam(value = "caseCode",required = false) String caseCode,
                                                             @RequestParam(value = "page") int page,
                                                             @RequestParam(value = "row") int row) {
        GovRoadProtectionCaseDto govRoadProtectionCaseDto = new GovRoadProtectionCaseDto();
        govRoadProtectionCaseDto.setPage(page);
        govRoadProtectionCaseDto.setRow(row);
        govRoadProtectionCaseDto.setCaId(caId);
        govRoadProtectionCaseDto.setHappenedPlace(happenedPlace);
        govRoadProtectionCaseDto.setHappenedTime(happenedTime);
        govRoadProtectionCaseDto.setPersonName(personName);
        govRoadProtectionCaseDto.setIsSolved(isSolved);
        govRoadProtectionCaseDto.setIdCard(idCard);
        govRoadProtectionCaseDto.setCaseName(caseName);
        govRoadProtectionCaseDto.setCaseCode(caseCode);
        return getGovRoadProtectionCaseBMOImpl.get(govRoadProtectionCaseDto);
    }
}
