package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovHelpPolicyServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 帮扶政策服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHelpPolicyServiceDaoImpl")
//@Transactional
public class GovHelpPolicyServiceDaoImpl extends BaseServiceDao implements IGovHelpPolicyServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHelpPolicyServiceDaoImpl.class);





    /**
     * 保存帮扶政策信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHelpPolicyInfo(Map info) throws DAOException {
        logger.debug("保存帮扶政策信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHelpPolicyServiceDaoImpl.saveGovHelpPolicyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存帮扶政策信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询帮扶政策信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHelpPolicyInfo(Map info) throws DAOException {
        logger.debug("查询帮扶政策信息 入参 info : {}",info);

        List<Map> businessGovHelpPolicyInfos = sqlSessionTemplate.selectList("govHelpPolicyServiceDaoImpl.getGovHelpPolicyInfo",info);

        return businessGovHelpPolicyInfos;
    }


    /**
     * 修改帮扶政策信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHelpPolicyInfo(Map info) throws DAOException {
        logger.debug("修改帮扶政策信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHelpPolicyServiceDaoImpl.updateGovHelpPolicyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改帮扶政策信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询帮扶政策数量
     * @param info 帮扶政策信息
     * @return 帮扶政策数量
     */
    @Override
    public int queryGovHelpPolicysCount(Map info) {
        logger.debug("查询帮扶政策数据 入参 info : {}",info);

        List<Map> businessGovHelpPolicyInfos = sqlSessionTemplate.selectList("govHelpPolicyServiceDaoImpl.queryGovHelpPolicysCount", info);
        if (businessGovHelpPolicyInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHelpPolicyInfos.get(0).get("count").toString());
    }


}
