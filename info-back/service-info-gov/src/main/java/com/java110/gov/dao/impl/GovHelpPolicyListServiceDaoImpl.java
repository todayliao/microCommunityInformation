package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovHelpPolicyListServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 帮扶记录服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHelpPolicyListServiceDaoImpl")
//@Transactional
public class GovHelpPolicyListServiceDaoImpl extends BaseServiceDao implements IGovHelpPolicyListServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHelpPolicyListServiceDaoImpl.class);





    /**
     * 保存帮扶记录信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHelpPolicyListInfo(Map info) throws DAOException {
        logger.debug("保存帮扶记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHelpPolicyListServiceDaoImpl.saveGovHelpPolicyListInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存帮扶记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询帮扶记录信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHelpPolicyListInfo(Map info) throws DAOException {
        logger.debug("查询帮扶记录信息 入参 info : {}",info);

        List<Map> businessGovHelpPolicyListInfos = sqlSessionTemplate.selectList("govHelpPolicyListServiceDaoImpl.getGovHelpPolicyListInfo",info);

        return businessGovHelpPolicyListInfos;
    }


    /**
     * 修改帮扶记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHelpPolicyListInfo(Map info) throws DAOException {
        logger.debug("修改帮扶记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHelpPolicyListServiceDaoImpl.updateGovHelpPolicyListInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改帮扶记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询帮扶记录数量
     * @param info 帮扶记录信息
     * @return 帮扶记录数量
     */
    @Override
    public int queryGovHelpPolicyListsCount(Map info) {
        logger.debug("查询帮扶记录数据 入参 info : {}",info);

        List<Map> businessGovHelpPolicyListInfos = sqlSessionTemplate.selectList("govHelpPolicyListServiceDaoImpl.queryGovHelpPolicyListsCount", info);
        if (businessGovHelpPolicyListInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHelpPolicyListInfos.get(0).get("count").toString());
    }


}
