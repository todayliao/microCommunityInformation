package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovRoadProtectionServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 护路护线服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govRoadProtectionServiceDaoImpl")
//@Transactional
public class GovRoadProtectionServiceDaoImpl extends BaseServiceDao implements IGovRoadProtectionServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovRoadProtectionServiceDaoImpl.class);





    /**
     * 保存护路护线信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovRoadProtectionInfo(Map info) throws DAOException {
        logger.debug("保存护路护线信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govRoadProtectionServiceDaoImpl.saveGovRoadProtectionInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存护路护线信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询护路护线信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovRoadProtectionInfo(Map info) throws DAOException {
        logger.debug("查询护路护线信息 入参 info : {}",info);

        List<Map> businessGovRoadProtectionInfos = sqlSessionTemplate.selectList("govRoadProtectionServiceDaoImpl.getGovRoadProtectionInfo",info);

        return businessGovRoadProtectionInfos;
    }


    /**
     * 修改护路护线信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovRoadProtectionInfo(Map info) throws DAOException {
        logger.debug("修改护路护线信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govRoadProtectionServiceDaoImpl.updateGovRoadProtectionInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改护路护线信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询护路护线数量
     * @param info 护路护线信息
     * @return 护路护线数量
     */
    @Override
    public int queryGovRoadProtectionsCount(Map info) {
        logger.debug("查询护路护线数据 入参 info : {}",info);

        List<Map> businessGovRoadProtectionInfos = sqlSessionTemplate.selectList("govRoadProtectionServiceDaoImpl.queryGovRoadProtectionsCount", info);
        if (businessGovRoadProtectionInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovRoadProtectionInfos.get(0).get("count").toString());
    }


}
