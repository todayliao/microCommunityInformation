package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govPetitionLetter.GovPetitionLetterDto;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;
import com.java110.gov.bmo.govPetitionLetter.IDeleteGovPetitionLetterBMO;
import com.java110.gov.bmo.govPetitionLetter.IGetGovPetitionLetterBMO;
import com.java110.gov.bmo.govPetitionLetter.ISaveGovPetitionLetterBMO;
import com.java110.gov.bmo.govPetitionLetter.IUpdateGovPetitionLetterBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govPetitionLetter")
public class GovPetitionLetterApi {

    @Autowired
    private ISaveGovPetitionLetterBMO saveGovPetitionLetterBMOImpl;
    @Autowired
    private IUpdateGovPetitionLetterBMO updateGovPetitionLetterBMOImpl;
    @Autowired
    private IDeleteGovPetitionLetterBMO deleteGovPetitionLetterBMOImpl;

    @Autowired
    private IGetGovPetitionLetterBMO getGovPetitionLetterBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionLetter/saveGovPetitionLetter
     * @path /app/govPetitionLetter/saveGovPetitionLetter
     */
    @RequestMapping(value = "/saveGovPetitionLetter", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovPetitionLetter(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "letterCode", "请求报文中未包含letterCode");
        Assert.hasKeyAndValue(reqJson, "queryCode", "请求报文中未包含queryCode");
        Assert.hasKeyAndValue(reqJson, "petitionTime", "请求报文中未包含petitionTime");
        Assert.hasKeyAndValue(reqJson, "petitionPersonName", "请求报文中未包含petitionPersonName");
        Assert.hasKeyAndValue(reqJson, "petitionType", "请求报文中未包含petitionType");
        Assert.hasKeyAndValue(reqJson, "petitionPurpose", "请求报文中未包含petitionPurpose");
        Assert.hasKeyAndValue(reqJson, "context", "请求报文中未包含context");
        Assert.hasKeyAndValue(reqJson, "receiveUserName", "请求报文中未包含receiveUserName");
        Assert.hasKeyAndValue(reqJson, "receiveDepartmentName", "请求报文中未包含receiveDepartmentName");

        GovPetitionLetterPo govPetitionLetterPo = BeanConvertUtil.covertBean(reqJson, GovPetitionLetterPo.class);
        return saveGovPetitionLetterBMOImpl.save(govPetitionLetterPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionLetter/updateGovPetitionLetter
     * @path /app/govPetitionLetter/updateGovPetitionLetter
     */
    @RequestMapping(value = "/updateGovPetitionLetter", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovPetitionLetter(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "petitionLetterId", "请求报文中未包含petitionLetterId");
        GovPetitionLetterPo govPetitionLetterPo = BeanConvertUtil.covertBean(reqJson, GovPetitionLetterPo.class);
        return updateGovPetitionLetterBMOImpl.update(govPetitionLetterPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govPetitionLetter/deleteGovPetitionLetter
     * @path /app/govPetitionLetter/deleteGovPetitionLetter
     */
    @RequestMapping(value = "/deleteGovPetitionLetter", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovPetitionLetter(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "petitionLetterId", "petitionLetterId不能为空");

        GovPetitionLetterPo govPetitionLetterPo = BeanConvertUtil.covertBean(reqJson, GovPetitionLetterPo.class);
        return deleteGovPetitionLetterBMOImpl.delete(govPetitionLetterPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govPetitionLetter/queryGovPetitionLetter
     * @path /app/govPetitionLetter/queryGovPetitionLetter
     */
    @RequestMapping(value = "/queryGovPetitionLetter", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovPetitionLetter(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "queryCode", required = false) String queryCode,
                                                         @RequestParam(value = "letterCode", required = false) String letterCode,
                                                         @RequestParam(value = "petitionPersonId", required = false) String petitionPersonId,
                                                         @RequestParam(value = "petitionType", required = false) String petitionType,
                                                         @RequestParam(value = "state", required = false) String state,
                                                         @RequestParam(value = "petitionPersonName", required = false) String petitionPersonName,
                                                         @RequestParam(value = "petitionTime", required = false) String petitionTime,
                                                         @RequestParam(value = "dealUserName", required = false) String dealUserName,
                                                         @RequestParam(value = "receiveUserName", required = false) String receiveUserName,

                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovPetitionLetterDto govPetitionLetterDto = new GovPetitionLetterDto();
        govPetitionLetterDto.setPage(page);
        govPetitionLetterDto.setRow(row);
        govPetitionLetterDto.setCaId(caId);
        govPetitionLetterDto.setQueryCode(queryCode);
        govPetitionLetterDto.setLetterCode(letterCode);
        govPetitionLetterDto.setPetitionPersonId(petitionPersonId);
        govPetitionLetterDto.setPetitionType(petitionType);
        govPetitionLetterDto.setState(state);
        govPetitionLetterDto.setPetitionPersonName(petitionPersonName);
        govPetitionLetterDto.setPetitionTime(petitionTime);
        govPetitionLetterDto.setDealUserName(dealUserName);
        govPetitionLetterDto.setReceiveUserName(receiveUserName);
        return getGovPetitionLetterBMOImpl.get(govPetitionLetterDto);
    }

}
