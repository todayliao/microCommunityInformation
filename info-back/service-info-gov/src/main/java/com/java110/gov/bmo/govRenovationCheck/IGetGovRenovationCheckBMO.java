package com.java110.gov.bmo.govRenovationCheck;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govRenovationCheck.GovRenovationCheckDto;
public interface IGetGovRenovationCheckBMO {


    /**
     * 查询重点地区整治情况
     * add by wuxw
     * @param  govRenovationCheckDto
     * @return
     */
    ResponseEntity<String> get(GovRenovationCheckDto govRenovationCheckDto);


}
