package com.java110.gov.bmo.govReportTypeUser;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovReportTypeUserBMO {


    /**
     * 修改报事类型人员
     * add by wuxw
     * @param govReportTypeUserPo
     * @return
     */
    ResponseEntity<String> delete(GovReportTypeUserPo govReportTypeUserPo);


}
