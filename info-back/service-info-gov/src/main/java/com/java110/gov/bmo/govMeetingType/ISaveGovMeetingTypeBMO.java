package com.java110.gov.bmo.govMeetingType;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMeetingType.GovMeetingTypePo;
public interface ISaveGovMeetingTypeBMO {


    /**
     * 添加会议类型
     * add by wuxw
     * @param govMeetingTypePo
     * @return
     */
    ResponseEntity<String> save(GovMeetingTypePo govMeetingTypePo);


}
