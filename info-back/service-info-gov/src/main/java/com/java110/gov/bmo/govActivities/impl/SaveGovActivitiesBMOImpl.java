package com.java110.gov.bmo.govActivities.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.govActivities.ISaveGovActivitiesBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.gov.IGovActivitiesInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govActivities.GovActivitiesPo;
import com.java110.utils.util.Assert;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovActivitiesBMOImpl")
public class SaveGovActivitiesBMOImpl implements ISaveGovActivitiesBMO {

    @Autowired
    private IGovActivitiesInnerServiceSMO govActivitiesInnerServiceSMOImpl;
    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govActivitiesPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovActivitiesPo govActivitiesPo) {

        UserDto userDto = new UserDto();
        userDto.setUserId(govActivitiesPo.getUserId());
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        govActivitiesPo.setUserName( userDtos.get( 0 ).getUserName() );
        govActivitiesPo.setActivitiesId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_activitiesId));


        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govActivitiesPo.getHeaderImg());
        fileRelPo.setRelType(FileRelDto.REL_TYPE);
        fileRelPo.setObjId(govActivitiesPo.getActivitiesId());
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }
        govActivitiesPo.setHeaderImg(fileRelPo.getFileRelId());
        flag = govActivitiesInnerServiceSMOImpl.saveGovActivities(govActivitiesPo);
        if (flag < 1) {
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
