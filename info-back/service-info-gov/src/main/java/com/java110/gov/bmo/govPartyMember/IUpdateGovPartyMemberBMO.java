package com.java110.gov.bmo.govPartyMember;
import com.java110.po.govPartyMember.GovPartyMemberPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovPartyMemberBMO {


    /**
     * 修改党员管理
     * add by wuxw
     * @param govPartyMemberPo
     * @return
     */
    ResponseEntity<String> update(GovPartyMemberPo govPartyMemberPo);


}
