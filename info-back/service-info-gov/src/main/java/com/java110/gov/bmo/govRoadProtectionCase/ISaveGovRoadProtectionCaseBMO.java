package com.java110.gov.bmo.govRoadProtectionCase;

import org.springframework.http.ResponseEntity;
import com.java110.po.govRoadProtectionCase.GovRoadProtectionCasePo;
public interface ISaveGovRoadProtectionCaseBMO {


    /**
     * 添加路案事件
     * add by wuxw
     * @param govRoadProtectionCasePo
     * @return
     */
    ResponseEntity<String> save(GovRoadProtectionCasePo govRoadProtectionCasePo);


}
