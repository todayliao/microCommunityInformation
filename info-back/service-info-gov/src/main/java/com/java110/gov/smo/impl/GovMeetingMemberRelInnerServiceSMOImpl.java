package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovMeetingMemberRelServiceDao;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 会议与参会人关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMeetingMemberRelInnerServiceSMOImpl extends BaseServiceSMO implements IGovMeetingMemberRelInnerServiceSMO {

    @Autowired
    private IGovMeetingMemberRelServiceDao govMeetingMemberRelServiceDaoImpl;


    @Override
    public int saveGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo) {
        int saveFlag = 1;
        govMeetingMemberRelServiceDaoImpl.saveGovMeetingMemberRelInfo(BeanConvertUtil.beanCovertMap(govMeetingMemberRelPo));
        return saveFlag;
    }

     @Override
    public int updateGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo) {
        int saveFlag = 1;
         govMeetingMemberRelServiceDaoImpl.updateGovMeetingMemberRelInfo(BeanConvertUtil.beanCovertMap(govMeetingMemberRelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMeetingMemberRel(@RequestBody  GovMeetingMemberRelPo govMeetingMemberRelPo) {
        int saveFlag = 1;
        govMeetingMemberRelPo.setStatusCd("1");
        govMeetingMemberRelServiceDaoImpl.updateGovMeetingMemberRelInfo(BeanConvertUtil.beanCovertMap(govMeetingMemberRelPo));
        return saveFlag;
    }

    @Override
    public List<GovMeetingMemberRelDto> queryGovMeetingMemberRels(@RequestBody  GovMeetingMemberRelDto govMeetingMemberRelDto) {

        //校验是否传了 分页信息

        int page = govMeetingMemberRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMeetingMemberRelDto.setPage((page - 1) * govMeetingMemberRelDto.getRow());
        }

        List<GovMeetingMemberRelDto> govMeetingMemberRels = BeanConvertUtil.covertBeanList(govMeetingMemberRelServiceDaoImpl.getGovMeetingMemberRelInfo(BeanConvertUtil.beanCovertMap(govMeetingMemberRelDto)), GovMeetingMemberRelDto.class);

        return govMeetingMemberRels;
    }


    @Override
    public int queryGovMeetingMemberRelsCount(@RequestBody GovMeetingMemberRelDto govMeetingMemberRelDto) {
        return govMeetingMemberRelServiceDaoImpl.queryGovMeetingMemberRelsCount(BeanConvertUtil.beanCovertMap(govMeetingMemberRelDto));    }

    public IGovMeetingMemberRelServiceDao getGovMeetingMemberRelServiceDaoImpl() {
        return govMeetingMemberRelServiceDaoImpl;
    }

    public void setGovMeetingMemberRelServiceDaoImpl(IGovMeetingMemberRelServiceDao govMeetingMemberRelServiceDaoImpl) {
        this.govMeetingMemberRelServiceDaoImpl = govMeetingMemberRelServiceDaoImpl;
    }
}
