package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 公告管理组件内部之间使用，没有给外围系统提供服务能力
 * 公告管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovActivitiesServiceDao {


    /**
     * 保存 公告管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovActivitiesInfo(Map info) throws DAOException;




    /**
     * 查询公告管理信息（instance过程）
     * 根据bId 查询公告管理信息
     * @param info bId 信息
     * @return 公告管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovActivitiesInfo(Map info) throws DAOException;



    /**
     * 修改公告管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovActivitiesInfo(Map info) throws DAOException;


    /**
     * 查询公告管理总数
     *
     * @param info 公告管理信息
     * @return 公告管理数量
     */
    int queryGovActivitiessCount(Map info);

}
