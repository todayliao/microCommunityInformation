package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govAreaType.GovAreaTypeDto;
import com.java110.po.govAreaType.GovAreaTypePo;
import com.java110.gov.bmo.govAreaType.IDeleteGovAreaTypeBMO;
import com.java110.gov.bmo.govAreaType.IGetGovAreaTypeBMO;
import com.java110.gov.bmo.govAreaType.ISaveGovAreaTypeBMO;
import com.java110.gov.bmo.govAreaType.IUpdateGovAreaTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govAreaType")
public class GovAreaTypeApi {

    @Autowired
    private ISaveGovAreaTypeBMO saveGovAreaTypeBMOImpl;
    @Autowired
    private IUpdateGovAreaTypeBMO updateGovAreaTypeBMOImpl;
    @Autowired
    private IDeleteGovAreaTypeBMO deleteGovAreaTypeBMOImpl;

    @Autowired
    private IGetGovAreaTypeBMO getGovAreaTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaType/saveGovAreaType
     * @path /app/govAreaType/saveGovAreaType
     */
    @RequestMapping(value = "/saveGovAreaType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovAreaType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "isShow", "请求报文中未包含isShow" );


        GovAreaTypePo govAreaTypePo = BeanConvertUtil.covertBean( reqJson, GovAreaTypePo.class );
        return saveGovAreaTypeBMOImpl.save( govAreaTypePo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaType/updateGovAreaType
     * @path /app/govAreaType/updateGovAreaType
     */
    @RequestMapping(value = "/updateGovAreaType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovAreaType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "typeId", "请求报文中未包含typeId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "isShow", "请求报文中未包含isShow" );


        GovAreaTypePo govAreaTypePo = BeanConvertUtil.covertBean( reqJson, GovAreaTypePo.class );
        return updateGovAreaTypeBMOImpl.update( govAreaTypePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govAreaType/deleteGovAreaType
     * @path /app/govAreaType/deleteGovAreaType
     */
    @RequestMapping(value = "/deleteGovAreaType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovAreaType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "typeId", "typeId不能为空" );


        GovAreaTypePo govAreaTypePo = BeanConvertUtil.covertBean( reqJson, GovAreaTypePo.class );
        return deleteGovAreaTypeBMOImpl.delete( govAreaTypePo );
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govAreaType/queryGovAreaType
     * @path /app/govAreaType/queryGovAreaType
     */
    @RequestMapping(value = "/queryGovAreaType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovAreaType(@RequestParam(value = "caId") String caId,
                                                   @RequestParam(value = "page") int page,
                                                   @RequestParam(value = "row") int row) {
        GovAreaTypeDto govAreaTypeDto = new GovAreaTypeDto();
        govAreaTypeDto.setPage( page );
        govAreaTypeDto.setRow( row );
        govAreaTypeDto.setCaId( caId );
        return getGovAreaTypeBMOImpl.get( govAreaTypeDto );
    }
}
