package com.java110.gov.bmo.govPartyMember;

import com.java110.po.govPartyMember.GovPartyMemberPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovPartyMemberBMO {


    /**
     * 添加党员管理
     * add by wuxw
     * @param govPartyMemberPo
     * @return
     */
    ResponseEntity<String> save(GovPartyMemberPo govPartyMemberPo);


}
