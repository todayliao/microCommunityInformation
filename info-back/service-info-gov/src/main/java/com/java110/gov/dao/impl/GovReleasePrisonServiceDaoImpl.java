package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovReleasePrisonServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 刑满释放人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govReleasePrisonServiceDaoImpl")
//@Transactional
public class GovReleasePrisonServiceDaoImpl extends BaseServiceDao implements IGovReleasePrisonServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovReleasePrisonServiceDaoImpl.class);





    /**
     * 保存刑满释放人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovReleasePrisonInfo(Map info) throws DAOException {
        logger.debug("保存刑满释放人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govReleasePrisonServiceDaoImpl.saveGovReleasePrisonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存刑满释放人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询刑满释放人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovReleasePrisonInfo(Map info) throws DAOException {
        logger.debug("查询刑满释放人员信息 入参 info : {}",info);

        List<Map> businessGovReleasePrisonInfos = sqlSessionTemplate.selectList("govReleasePrisonServiceDaoImpl.getGovReleasePrisonInfo",info);

        return businessGovReleasePrisonInfos;
    }


    /**
     * 修改刑满释放人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovReleasePrisonInfo(Map info) throws DAOException {
        logger.debug("修改刑满释放人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govReleasePrisonServiceDaoImpl.updateGovReleasePrisonInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改刑满释放人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询刑满释放人员数量
     * @param info 刑满释放人员信息
     * @return 刑满释放人员数量
     */
    @Override
    public int queryGovReleasePrisonsCount(Map info) {
        logger.debug("查询刑满释放人员数据 入参 info : {}",info);

        List<Map> businessGovReleasePrisonInfos = sqlSessionTemplate.selectList("govReleasePrisonServiceDaoImpl.queryGovReleasePrisonsCount", info);
        if (businessGovReleasePrisonInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovReleasePrisonInfos.get(0).get("count").toString());
    }


}
