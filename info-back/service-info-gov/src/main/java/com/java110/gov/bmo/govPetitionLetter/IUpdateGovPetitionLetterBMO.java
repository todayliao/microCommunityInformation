package com.java110.gov.bmo.govPetitionLetter;
import org.springframework.http.ResponseEntity;
import com.java110.po.govPetitionLetter.GovPetitionLetterPo;

public interface IUpdateGovPetitionLetterBMO {


    /**
     * 修改信访管理
     * add by wuxw
     * @param govPetitionLetterPo
     * @return
     */
    ResponseEntity<String> update(GovPetitionLetterPo govPetitionLetterPo);


}
