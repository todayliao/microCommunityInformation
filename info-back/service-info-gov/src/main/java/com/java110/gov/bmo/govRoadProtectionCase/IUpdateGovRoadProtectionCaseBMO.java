package com.java110.gov.bmo.govRoadProtectionCase;
import org.springframework.http.ResponseEntity;
import com.java110.po.govRoadProtectionCase.GovRoadProtectionCasePo;

public interface IUpdateGovRoadProtectionCaseBMO {


    /**
     * 修改路案事件
     * add by wuxw
     * @param govRoadProtectionCasePo
     * @return
     */
    ResponseEntity<String> update(GovRoadProtectionCasePo govRoadProtectionCasePo);


}
