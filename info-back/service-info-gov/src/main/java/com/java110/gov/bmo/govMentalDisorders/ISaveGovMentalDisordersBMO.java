package com.java110.gov.bmo.govMentalDisorders;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMentalDisorders.GovMentalDisordersPo;
public interface ISaveGovMentalDisordersBMO {


    /**
     * 添加障碍者
     * add by wuxw
     * @param govMentalDisordersPo
     * @return
     */
    ResponseEntity<String> save(GovMentalDisordersPo govMentalDisordersPo);


}
