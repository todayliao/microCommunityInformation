package com.java110.gov.bmo.govReleasePrison;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
public interface IGetGovReleasePrisonBMO {


    /**
     * 查询刑满释放人员
     * add by wuxw
     * @param  govReleasePrisonDto
     * @return
     */
    ResponseEntity<String> get(GovReleasePrisonDto govReleasePrisonDto);


}
