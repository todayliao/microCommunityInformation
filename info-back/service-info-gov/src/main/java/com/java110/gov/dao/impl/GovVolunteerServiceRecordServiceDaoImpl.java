package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovVolunteerServiceRecordServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 服务记录表服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govVolunteerServiceRecordServiceDaoImpl")
//@Transactional
public class GovVolunteerServiceRecordServiceDaoImpl extends BaseServiceDao implements IGovVolunteerServiceRecordServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovVolunteerServiceRecordServiceDaoImpl.class);





    /**
     * 保存服务记录表信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovVolunteerServiceRecordInfo(Map info) throws DAOException {
        logger.debug("保存服务记录表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govVolunteerServiceRecordServiceDaoImpl.saveGovVolunteerServiceRecordInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存服务记录表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询服务记录表信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovVolunteerServiceRecordInfo(Map info) throws DAOException {
        logger.debug("查询服务记录表信息 入参 info : {}",info);

        List<Map> businessGovVolunteerServiceRecordInfos = sqlSessionTemplate.selectList("govVolunteerServiceRecordServiceDaoImpl.getGovVolunteerServiceRecordInfo",info);

        return businessGovVolunteerServiceRecordInfos;
    }


    /**
     * 修改服务记录表信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovVolunteerServiceRecordInfo(Map info) throws DAOException {
        logger.debug("修改服务记录表信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govVolunteerServiceRecordServiceDaoImpl.updateGovVolunteerServiceRecordInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改服务记录表信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询服务记录表数量
     * @param info 服务记录表信息
     * @return 服务记录表数量
     */
    @Override
    public int queryGovVolunteerServiceRecordsCount(Map info) {
        logger.debug("查询服务记录表数据 入参 info : {}",info);

        List<Map> businessGovVolunteerServiceRecordInfos = sqlSessionTemplate.selectList("govVolunteerServiceRecordServiceDaoImpl.queryGovVolunteerServiceRecordsCount", info);
        if (businessGovVolunteerServiceRecordInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovVolunteerServiceRecordInfos.get(0).get("count").toString());
    }


}
