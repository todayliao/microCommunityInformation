package com.java110.gov.bmo.perGovActivities.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.perGovActivities.ISavePerGovActivitiesBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.gov.IPerGovActivityRelInnerServiceSMO;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import com.java110.intf.gov.IPerGovActivitiesInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("savePerGovActivitiesBMOImpl")
public class SavePerGovActivitiesBMOImpl implements ISavePerGovActivitiesBMO {
    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;

    @Autowired
    private IPerGovActivitiesInnerServiceSMO perGovActivitiesInnerServiceSMOImpl;

    @Autowired
    private IPerGovActivityRelInnerServiceSMO perGovActivityRelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param perGovActivitiesPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(PerGovActivitiesPo perGovActivitiesPo,JSONArray oldPersons) {
        UserDto userDto = new UserDto();
        userDto.setUserId(perGovActivitiesPo.getUserId());
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        perGovActivitiesPo.setUserName( userDtos.get(0).getUserName() );
        perGovActivitiesPo.setPerActivitiesId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_perActivitiesId));
        int flag = perGovActivitiesInnerServiceSMOImpl.savePerGovActivities(perGovActivitiesPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "记录保存失败");
        }
        if (oldPersons != null && oldPersons.size() > 0) {
            for (int oldIndex = 0; oldIndex < oldPersons.size(); oldIndex++) {
                PerGovActivityRelPo perGovActivityRelPo = BeanConvertUtil.covertBean(oldPersons.get(oldIndex), PerGovActivityRelPo.class);
                perGovActivityRelPo.setPerActivitiesId(perGovActivitiesPo.getPerActivitiesId());
                perGovActivityRelPo.setCaId(perGovActivitiesPo.getCaId());
                perGovActivityRelPo.setActRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_actRelId));
                flag = perGovActivityRelInnerServiceSMOImpl.savePerGovActivityRel(perGovActivityRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存老人信息失败");
                }
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "记录保存成功");
    }

}
