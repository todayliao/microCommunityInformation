package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovMentalDisordersServiceDao;
import com.java110.intf.gov.IGovMentalDisordersInnerServiceSMO;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.po.govMentalDisorders.GovMentalDisordersPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 障碍者内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovMentalDisordersInnerServiceSMOImpl extends BaseServiceSMO implements IGovMentalDisordersInnerServiceSMO {

    @Autowired
    private IGovMentalDisordersServiceDao govMentalDisordersServiceDaoImpl;


    @Override
    public int saveGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo) {
        int saveFlag = 1;
        govMentalDisordersServiceDaoImpl.saveGovMentalDisordersInfo(BeanConvertUtil.beanCovertMap(govMentalDisordersPo));
        return saveFlag;
    }

     @Override
    public int updateGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo) {
        int saveFlag = 1;
         govMentalDisordersServiceDaoImpl.updateGovMentalDisordersInfo(BeanConvertUtil.beanCovertMap(govMentalDisordersPo));
        return saveFlag;
    }

     @Override
    public int deleteGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo) {
        int saveFlag = 1;
        govMentalDisordersPo.setStatusCd("1");
        govMentalDisordersServiceDaoImpl.updateGovMentalDisordersInfo(BeanConvertUtil.beanCovertMap(govMentalDisordersPo));
        return saveFlag;
    }

    @Override
    public List<GovMentalDisordersDto> queryGovMentalDisorderss(@RequestBody  GovMentalDisordersDto govMentalDisordersDto) {

        //校验是否传了 分页信息

        int page = govMentalDisordersDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govMentalDisordersDto.setPage((page - 1) * govMentalDisordersDto.getRow());
        }

        List<GovMentalDisordersDto> govMentalDisorderss = BeanConvertUtil.covertBeanList(govMentalDisordersServiceDaoImpl.getGovMentalDisordersInfo(BeanConvertUtil.beanCovertMap(govMentalDisordersDto)), GovMentalDisordersDto.class);

        return govMentalDisorderss;
    }


    @Override
    public int queryGovMentalDisorderssCount(@RequestBody GovMentalDisordersDto govMentalDisordersDto) {
        return govMentalDisordersServiceDaoImpl.queryGovMentalDisorderssCount(BeanConvertUtil.beanCovertMap(govMentalDisordersDto));    }

    public IGovMentalDisordersServiceDao getGovMentalDisordersServiceDaoImpl() {
        return govMentalDisordersServiceDaoImpl;
    }

    public void setGovMentalDisordersServiceDaoImpl(IGovMentalDisordersServiceDao govMentalDisordersServiceDaoImpl) {
        this.govMentalDisordersServiceDaoImpl = govMentalDisordersServiceDaoImpl;
    }
}
