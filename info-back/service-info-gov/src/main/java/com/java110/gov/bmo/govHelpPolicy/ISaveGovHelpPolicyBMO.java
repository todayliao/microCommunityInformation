package com.java110.gov.bmo.govHelpPolicy;

import org.springframework.http.ResponseEntity;
import com.java110.po.govHelpPolicy.GovHelpPolicyPo;
public interface ISaveGovHelpPolicyBMO {


    /**
     * 添加帮扶政策
     * add by wuxw
     * @param govHelpPolicyPo
     * @return
     */
    ResponseEntity<String> save(GovHelpPolicyPo govHelpPolicyPo);


}
