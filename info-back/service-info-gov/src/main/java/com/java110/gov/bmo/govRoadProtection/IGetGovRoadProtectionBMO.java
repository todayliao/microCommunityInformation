package com.java110.gov.bmo.govRoadProtection;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govRoadProtection.GovRoadProtectionDto;
public interface IGetGovRoadProtectionBMO {


    /**
     * 查询护路护线
     * add by wuxw
     * @param  govRoadProtectionDto
     * @return
     */
    ResponseEntity<String> get(GovRoadProtectionDto govRoadProtectionDto);


}
