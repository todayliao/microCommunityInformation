package com.java110.gov.bmo.govVolunteerPersonRel;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerPersonRel.GovVolunteerPersonRelPo;

public interface IDeleteGovVolunteerPersonRelBMO {


    /**
     * 修改服务记录人员关系表
     * add by wuxw
     * @param govVolunteerPersonRelPo
     * @return
     */
    ResponseEntity<String> delete(GovVolunteerPersonRelPo govVolunteerPersonRelPo);


}
