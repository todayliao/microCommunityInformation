package com.java110.gov.bmo.perGovActivityRel;

import org.springframework.http.ResponseEntity;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
public interface ISavePerGovActivityRelBMO {


    /**
     * 添加生日记录关系
     * add by wuxw
     * @param perGovActivityRelPo
     * @return
     */
    ResponseEntity<String> save(PerGovActivityRelPo perGovActivityRelPo);


}
