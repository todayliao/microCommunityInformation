package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 路案事件组件内部之间使用，没有给外围系统提供服务能力
 * 路案事件服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovRoadProtectionCaseServiceDao {


    /**
     * 保存 路案事件信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovRoadProtectionCaseInfo(Map info) throws DAOException;




    /**
     * 查询路案事件信息（instance过程）
     * 根据bId 查询路案事件信息
     * @param info bId 信息
     * @return 路案事件信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovRoadProtectionCaseInfo(Map info) throws DAOException;



    /**
     * 修改路案事件信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovRoadProtectionCaseInfo(Map info) throws DAOException;


    /**
     * 查询路案事件总数
     *
     * @param info 路案事件信息
     * @return 路案事件数量
     */
    int queryGovRoadProtectionCasesCount(Map info);

}
