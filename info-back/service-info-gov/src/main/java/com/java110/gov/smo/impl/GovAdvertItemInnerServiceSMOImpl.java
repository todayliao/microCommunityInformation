package com.java110.gov.smo.impl;


import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.govAdvertItem.GovAdvertItemDto;
import com.java110.gov.dao.IGovAdvertItemServiceDao;
import com.java110.intf.gov.IGovAdvertItemInnerServiceSMO;
import com.java110.po.govAdvertItem.GovAdvertItemPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 广告明细内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovAdvertItemInnerServiceSMOImpl extends BaseServiceSMO implements IGovAdvertItemInnerServiceSMO {

    @Autowired
    private IGovAdvertItemServiceDao govAdvertItemServiceDaoImpl;


    @Override
    public int saveGovAdvertItem(@RequestBody GovAdvertItemPo govAdvertItemPo) {
        int saveFlag = 1;
        govAdvertItemServiceDaoImpl.saveGovAdvertItemInfo(BeanConvertUtil.beanCovertMap(govAdvertItemPo));
        return saveFlag;
    }

     @Override
    public int updateGovAdvertItem(@RequestBody  GovAdvertItemPo govAdvertItemPo) {
        int saveFlag = 1;
         govAdvertItemServiceDaoImpl.updateGovAdvertItemInfo(BeanConvertUtil.beanCovertMap(govAdvertItemPo));
        return saveFlag;
    }

     @Override
    public int deleteGovAdvertItem(@RequestBody  GovAdvertItemPo govAdvertItemPo) {
        int saveFlag = 1;
        govAdvertItemPo.setStatusCd("1");
        govAdvertItemServiceDaoImpl.updateGovAdvertItemInfo(BeanConvertUtil.beanCovertMap(govAdvertItemPo));
        return saveFlag;
    }

    @Override
    public List<GovAdvertItemDto> queryGovAdvertItems(@RequestBody  GovAdvertItemDto govAdvertItemDto) {

        //校验是否传了 分页信息

        int page = govAdvertItemDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAdvertItemDto.setPage((page - 1) * govAdvertItemDto.getRow());
        }

        List<GovAdvertItemDto> govAdvertItems = BeanConvertUtil.covertBeanList(govAdvertItemServiceDaoImpl.getGovAdvertItemInfo(BeanConvertUtil.beanCovertMap(govAdvertItemDto)), GovAdvertItemDto.class);

        return govAdvertItems;
    }


    @Override
    public int queryGovAdvertItemsCount(@RequestBody GovAdvertItemDto govAdvertItemDto) {
        return govAdvertItemServiceDaoImpl.queryGovAdvertItemsCount(BeanConvertUtil.beanCovertMap(govAdvertItemDto));    }

    public IGovAdvertItemServiceDao getGovAdvertItemServiceDaoImpl() {
        return govAdvertItemServiceDaoImpl;
    }

    public void setGovAdvertItemServiceDaoImpl(IGovAdvertItemServiceDao govAdvertItemServiceDaoImpl) {
        this.govAdvertItemServiceDaoImpl = govAdvertItemServiceDaoImpl;
    }
}
