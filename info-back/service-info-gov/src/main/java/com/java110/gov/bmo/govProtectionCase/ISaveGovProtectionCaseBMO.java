package com.java110.gov.bmo.govProtectionCase;

import com.alibaba.fastjson.JSONArray;
import org.springframework.http.ResponseEntity;
import com.java110.po.govProtectionCase.GovProtectionCasePo;
public interface ISaveGovProtectionCaseBMO {


    /**
     * 添加周边重点人员
     * add by wuxw
     * @param govProtectionCasePo
     * @return
     */
    ResponseEntity<String> save(GovProtectionCasePo govProtectionCasePo);


    ResponseEntity<String> savePhone(GovProtectionCasePo govProtectionCasePo,JSONArray photos);
}
