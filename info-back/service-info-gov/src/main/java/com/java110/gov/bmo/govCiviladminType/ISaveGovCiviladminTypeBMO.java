package com.java110.gov.bmo.govCiviladminType;

import com.java110.po.govCiviladminType.GovCiviladminTypePo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCiviladminTypeBMO {


    /**
     * 添加民政服务宣传类型
     * add by wuxw
     * @param govCiviladminTypePo
     * @return
     */
    ResponseEntity<String> save(GovCiviladminTypePo govCiviladminTypePo);


}
