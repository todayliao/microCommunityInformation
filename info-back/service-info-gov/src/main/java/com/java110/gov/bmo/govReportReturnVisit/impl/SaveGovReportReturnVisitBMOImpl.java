package com.java110.gov.bmo.govReportReturnVisit.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.govReportReturnVisit.ISaveGovReportReturnVisitBMO;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service("saveGovReportReturnVisitBMOImpl")
public class SaveGovReportReturnVisitBMOImpl implements ISaveGovReportReturnVisitBMO {

    @Autowired
    private IGovReportReturnVisitInnerServiceSMO govReportReturnVisitInnerServiceSMOImpl;
    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;

    @Autowired
    private IGovReportUserInnerServiceSMO govReportUserInnerServiceSMOImpl;
    @Autowired
    private IReportPoolInnerServiceSMO reportPoolInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govReportReturnVisitPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovReportReturnVisitPo govReportReturnVisitPo) {

        UserDto userDto = new UserDto();
        userDto.setUserId(govReportReturnVisitPo.getVisitPersonId());
        List<UserDto> userDtos = userInnerServiceSMOImpl.getUsers(userDto);
        Assert.listOnlyOne(userDtos, "用户不存在");
        govReportReturnVisitPo.setVisitPersonName(userDtos.get(0).getUserName());

        govReportReturnVisitPo.setVisitId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_visitId));
        int flag = govReportReturnVisitInnerServiceSMOImpl.saveGovReportReturnVisit(govReportReturnVisitPo);

        if (flag < 1) {
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存回访信息失败");
        }

       /* //查询报修派单状态
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setReportId(govReportReturnVisitPo.getReportId());
        govReportUserPo.setCaId(govReportReturnVisitPo.getCaId());
        govReportUserPo.set
        govReportUserInnerServiceSMOImpl.updateGovReportUser(govReportUserPo);*/

        ReportPoolPo reportPoolPo = new ReportPoolPo();
        reportPoolPo.setReportId(govReportReturnVisitPo.getReportId());
        reportPoolPo.setState(ReportPoolDto.STATE_COMPLATE);
        reportPoolPo.setCaId(govReportReturnVisitPo.getCaId());

        flag = reportPoolInnerServiceSMOImpl.updateReportPool(reportPoolPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "工单状态修改失败");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
