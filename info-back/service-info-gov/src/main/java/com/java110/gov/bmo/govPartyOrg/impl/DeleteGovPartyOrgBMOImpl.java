package com.java110.gov.bmo.govPartyOrg.impl;


import com.java110.core.annotation.Java110Transactional;

import com.java110.dto.govPartyOrgContext.GovPartyOrgContextDto;
import com.java110.gov.bmo.govPartyOrg.IDeleteGovPartyOrgBMO;
import com.java110.intf.gov.IGovPartyOrgContextInnerServiceSMO;
import com.java110.intf.gov.IGovPartyOrgInnerServiceSMO;
import com.java110.po.govPartyOrg.GovPartyOrgPo;
import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovPartyOrgBMOImpl")
public class DeleteGovPartyOrgBMOImpl implements IDeleteGovPartyOrgBMO {

    @Autowired
    private IGovPartyOrgInnerServiceSMO govPartyOrgInnerServiceSMOImpl;
    @Autowired
    private IGovPartyOrgContextInnerServiceSMO govPartyOrgContextInnerServiceSMOImpl;
    /**
     * @param govPartyOrgPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovPartyOrgPo govPartyOrgPo) {

        int flag = govPartyOrgInnerServiceSMOImpl.deleteGovPartyOrg(govPartyOrgPo);

        if (flag > 0) {

            GovPartyOrgContextDto govPartyOrgContextDto = new GovPartyOrgContextDto();
            govPartyOrgContextDto.setOrgId( govPartyOrgPo.getOrgId() );
           int govdto = govPartyOrgContextInnerServiceSMOImpl.queryGovPartyOrgContextsCount( govPartyOrgContextDto );
           if(govdto > 0){
               GovPartyOrgContextPo govPartyOrgContextPo = new GovPartyOrgContextPo();
               govPartyOrgContextPo.setOrgId( govPartyOrgPo.getOrgId() );
               govPartyOrgContextInnerServiceSMOImpl.deleteGovPartyOrgContext( govPartyOrgContextPo );
           }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
