package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.po.govDrug.GovDrugPo;
import com.java110.gov.bmo.govDrug.IDeleteGovDrugBMO;
import com.java110.gov.bmo.govDrug.IGetGovDrugBMO;
import com.java110.gov.bmo.govDrug.ISaveGovDrugBMO;
import com.java110.gov.bmo.govDrug.IUpdateGovDrugBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govDrug")
public class GovDrugApi {

    @Autowired
    private ISaveGovDrugBMO saveGovDrugBMOImpl;
    @Autowired
    private IUpdateGovDrugBMO updateGovDrugBMOImpl;
    @Autowired
    private IDeleteGovDrugBMO deleteGovDrugBMOImpl;

    @Autowired
    private IGetGovDrugBMO getGovDrugBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govDrug/saveGovDrug
     * @path /app/govDrug/saveGovDrug
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovDrug", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovDrug(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "drugStartTime", "请求报文中未包含drugStartTime");
        Assert.hasKeyAndValue(reqJson, "drugEndTime", "请求报文中未包含drugEndTime");
        Assert.hasKeyAndValue(reqJson, "drugReason", "请求报文中未包含drugReason");
        Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
        Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");

        GovDrugPo govDrugPo = BeanConvertUtil.covertBean(reqJson, GovDrugPo.class);
        return saveGovDrugBMOImpl.save(govDrugPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govDrug/updateGovDrug
     * @path /app/govDrug/updateGovDrug
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovDrug", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovDrug(@RequestBody JSONObject reqJson) {

    Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
    Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
    Assert.hasKeyAndValue(reqJson, "address", "请求报文中未包含address");
    Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
    Assert.hasKeyAndValue(reqJson, "drugStartTime", "请求报文中未包含drugStartTime");
    Assert.hasKeyAndValue(reqJson, "drugEndTime", "请求报文中未包含drugEndTime");
    Assert.hasKeyAndValue(reqJson, "drugReason", "请求报文中未包含drugReason");
    Assert.hasKeyAndValue(reqJson, "emergencyPerson", "请求报文中未包含emergencyPerson");
    Assert.hasKeyAndValue(reqJson, "emergencyTel", "请求报文中未包含emergencyTel");
    Assert.hasKeyAndValue(reqJson, "drugId", "drugId不能为空");


        GovDrugPo govDrugPo = BeanConvertUtil.covertBean(reqJson, GovDrugPo.class);
        return updateGovDrugBMOImpl.update(govDrugPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govDrug/deleteGovDrug
     * @path /app/govDrug/deleteGovDrug
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovDrug", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovDrug(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "drugId", "drugId不能为空");
        GovDrugPo govDrugPo = BeanConvertUtil.covertBean(reqJson, GovDrugPo.class);
        return deleteGovDrugBMOImpl.delete(govDrugPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govDrug/queryGovDrug
     * @path /app/govDrug/queryGovDrug
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovDrug", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovDrug(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "name" , required = false) String name,
                                               @RequestParam(value = "tel" , required = false) String tel,
                                               @RequestParam(value = "emergencyPerson" , required = false) String emergencyPerson,
                                               @RequestParam(value = "emergencyTel" , required = false) String emergencyTel,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovDrugDto govDrugDto = new GovDrugDto();
        govDrugDto.setPage(page);
        govDrugDto.setRow(row);
        govDrugDto.setTel(tel);
        govDrugDto.setName(name);
        govDrugDto.setEmergencyPerson(emergencyPerson);
        govDrugDto.setEmergencyTel(emergencyTel);
        govDrugDto.setCaId(caId);
        return getGovDrugBMOImpl.get(govDrugDto);
    }
}
