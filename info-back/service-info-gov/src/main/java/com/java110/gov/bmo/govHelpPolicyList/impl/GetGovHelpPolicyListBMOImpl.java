package com.java110.gov.bmo.govHelpPolicyList.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.gov.bmo.govHelpPolicyList.IGetGovHelpPolicyListBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.gov.IGovHelpPolicyListInnerServiceSMO;
import com.java110.dto.govHelpPolicyList.GovHelpPolicyListDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHelpPolicyListBMOImpl")
public class GetGovHelpPolicyListBMOImpl implements IGetGovHelpPolicyListBMO {

    @Autowired
    private IGovHelpPolicyListInnerServiceSMO govHelpPolicyListInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHelpPolicyListDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHelpPolicyListDto govHelpPolicyListDto) {


        int count = govHelpPolicyListInnerServiceSMOImpl.queryGovHelpPolicyListsCount(govHelpPolicyListDto);

        List<GovHelpPolicyListDto> govHelpPolicyListDtos = null;
        if (count > 0) {
            govHelpPolicyListDtos = govHelpPolicyListInnerServiceSMOImpl.queryGovHelpPolicyLists(govHelpPolicyListDto);
        } else {
            govHelpPolicyListDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHelpPolicyListDto.getRow()), count, govHelpPolicyListDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
