package com.java110.gov.bmo.reportPool.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.file.FileDto;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govReportUser.GovReportUserDto;
import com.java110.dto.reportPool.ReportPoolDto;
import com.java110.dto.user.UserDto;
import com.java110.gov.bmo.reportPool.ISaveReportPoolBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.cust.IUserInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.intf.gov.IGovReportUserInnerServiceSMO;
import com.java110.intf.gov.IReportPoolInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govReportUser.GovReportUserPo;
import com.java110.po.reportPool.ReportPoolPo;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveReportPoolBMOImpl")
public class SaveReportPoolBMOImpl implements ISaveReportPoolBMO {

    @Autowired
    private IReportPoolInnerServiceSMO reportPoolInnerServiceSMOImpl;
    @Autowired
    private IGovReportUserInnerServiceSMO govReportUserInnerServiceSMOImpl;
    @Autowired
    private IUserInnerServiceSMO userInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param reportPoolPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(ReportPoolPo reportPoolPo) {

        reportPoolPo.setReportId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_reportId));
        int flag = reportPoolInnerServiceSMOImpl.saveReportPool(reportPoolPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }


    /**
     * 添加小区信息
     *
     * @param reportPoolPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> saveTel(ReportPoolPo reportPoolPo, String userId) {

        reportPoolPo.setReportId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_reportId));
        reportPoolPo.setState(ReportPoolDto.STATE_WAIT);
        reportPoolPo.setRepairChannel(ReportPoolDto.REPAIR_T_CHANNEL);
        reportPoolPo.setCreateUserId(userId);
        int flag = reportPoolInnerServiceSMOImpl.saveReportPool(reportPoolPo);
        if (flag < 1) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存失败");
        }
        //查询用户信息
        UserDto userDto = new UserDto();
        userDto.setUserId( userId );
        List<UserDto> userDtoList = userInnerServiceSMOImpl.getUsers( userDto );
        if(null == userDtoList || userDtoList.size() < 1){
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "用户信息查询失败！");
        }
        //写入派单流程表
        /**
         *  `ru_id` varchar(30) NOT NULL COMMENT '报修派单ID',
         *   `report_id` varchar(30) NOT NULL COMMENT '报事ID',
         *   `ca_id` varchar(30) NOT NULL COMMENT '区域ID',
         *   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
         *   `state` varchar(8) NOT NULL COMMENT '员工处理状态，请查看t_dict 表',
         *   `context` longtext COMMENT '报事内容',
         *   `status_cd` varchar(2) NOT NULL DEFAULT '0' COMMENT '数据状态，详细参考c_status表，S 保存，0, 在用 1失效',
         *   `staff_id` varchar(30) NOT NULL COMMENT '当前处理员工',
         *   `staff_name` varchar(64) NOT NULL COMMENT '当前处理员工名称',
         *   `pre_staff_id` varchar(30) NOT NULL COMMENT '上一节点处理员工',
         *   `pre_staff_name` varchar(64) NOT NULL COMMENT '上一节点处理员工名称',
         *   `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
         *   `end_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
         *   `report_event` varchar(12) NOT NULL COMMENT '事件，startUser 开始用户 auditUser 审核处理员工',
         *   `pre_ru_id` varchar(30) NOT NULL COMMENT '上一节点处理ID'
         */
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        govReportUserPo.setReportId(reportPoolPo.getReportId());
        govReportUserPo.setCaId(reportPoolPo.getCaId());
        govReportUserPo.setContext(reportPoolPo.getContext()+"提交电话报事订单");
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_START_USER);
        govReportUserPo.setPreRuId("-1");
        govReportUserPo.setPreStaffId("-1");
        govReportUserPo.setPreStaffName("无");
        govReportUserPo.setStaffId(userId);
        govReportUserPo.setStaffName(userDtoList.get( 0 ).getUserName());
        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_SUBMIT);
        int userFlag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
        if (userFlag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    /**
     * 添加小区信息
     *
     * @param reportPoolPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> savePhone(ReportPoolPo reportPoolPo, String userId, JSONArray photos) {
        reportPoolPo.setReportId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_reportId));
        reportPoolPo.setState(ReportPoolDto.STATE_WAIT);
        reportPoolPo.setRepairChannel(ReportPoolDto.REPAIR_T_CHANNEL);
        reportPoolPo.setCreateUserId(userId);
        int flag = reportPoolInnerServiceSMOImpl.saveReportPool(reportPoolPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存失败");
        }
        for (int _photoIndex = 0; _photoIndex < photos.size(); _photoIndex++) {
                FileDto fileDto = new FileDto();
            fileDto.setFileId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_file_id));
            fileDto.setFileName(fileDto.getFileId());
            fileDto.setContext(photos.getJSONObject(_photoIndex).getString("photo"));
            fileDto.setSuffix("jpeg");
            String fileName = fileInnerServiceSMOImpl.saveFile(fileDto);
            FileRelPo fileRelPo = new FileRelPo();
            fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
            fileRelPo.setFileName(fileName);
            fileRelPo.setRelType("90300");
                fileRelPo.setObjId(reportPoolPo.getReportId());
                flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
                if (flag < 1) {
                    return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
                }
            }
        //查询用户信息
        UserDto userDto = new UserDto();
        userDto.setUserId( userId );
        List<UserDto> userDtoList = userInnerServiceSMOImpl.getUsers( userDto );
        if(null == userDtoList || userDtoList.size() < 1){
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "用户信息查询失败！");
        }
        //写入派单流程表
        /**
         *  `ru_id` varchar(30) NOT NULL COMMENT '报修派单ID',
         *   `report_id` varchar(30) NOT NULL COMMENT '报事ID',
         *   `ca_id` varchar(30) NOT NULL COMMENT '区域ID',
         *   `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
         *   `state` varchar(8) NOT NULL COMMENT '员工处理状态，请查看t_dict 表',
         *   `context` longtext COMMENT '报事内容',
         *   `status_cd` varchar(2) NOT NULL DEFAULT '0' COMMENT '数据状态，详细参考c_status表，S 保存，0, 在用 1失效',
         *   `staff_id` varchar(30) NOT NULL COMMENT '当前处理员工',
         *   `staff_name` varchar(64) NOT NULL COMMENT '当前处理员工名称',
         *   `pre_staff_id` varchar(30) NOT NULL COMMENT '上一节点处理员工',
         *   `pre_staff_name` varchar(64) NOT NULL COMMENT '上一节点处理员工名称',
         *   `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '开始时间',
         *   `end_time` timestamp NULL DEFAULT NULL COMMENT '结束时间',
         *   `report_event` varchar(12) NOT NULL COMMENT '事件，startUser 开始用户 auditUser 审核处理员工',
         *   `pre_ru_id` varchar(30) NOT NULL COMMENT '上一节点处理ID'
         */
        GovReportUserPo govReportUserPo = new GovReportUserPo();
        govReportUserPo.setRuId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_ruId));
        govReportUserPo.setReportId(reportPoolPo.getReportId());
        govReportUserPo.setCaId(reportPoolPo.getCaId());
        govReportUserPo.setContext(reportPoolPo.getContext()+"提交移动端报事订单");
        govReportUserPo.setEndTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setReportEvent(GovReportUserDto.POOL_EVENT_START_USER);
        govReportUserPo.setPreRuId("-1");
        govReportUserPo.setPreStaffId("-1");
        govReportUserPo.setPreStaffName("无");
        govReportUserPo.setStaffId(userId);
        govReportUserPo.setStaffName(userDtoList.get( 0 ).getUserName());
        govReportUserPo.setStartTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_A));
        govReportUserPo.setState(GovReportUserDto.STATE_SUBMIT);
        int userFlag = govReportUserInnerServiceSMOImpl.saveGovReportUser(govReportUserPo);
        if (userFlag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
