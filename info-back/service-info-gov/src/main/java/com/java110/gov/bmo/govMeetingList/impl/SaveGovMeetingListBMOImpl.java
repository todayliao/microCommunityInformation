package com.java110.gov.bmo.govMeetingList.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.gov.bmo.govMeetingList.ISaveGovMeetingListBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovMeetingMemberRelInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govMeetingMemberRel.GovMeetingMemberRelPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govMeetingList.GovMeetingListPo;
import com.java110.intf.gov.IGovMeetingListInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovMeetingListBMOImpl")
public class SaveGovMeetingListBMOImpl implements ISaveGovMeetingListBMO {

    @Autowired
    private IGovMeetingListInnerServiceSMO govMeetingListInnerServiceSMOImpl;
    @Autowired
    private IGovMeetingMemberRelInnerServiceSMO govMeetingMemberRelInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govMeetingListPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovMeetingListPo govMeetingListPo, JSONArray govMembers) {

        govMeetingListPo.setGovMeetingId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govMeetingId));
//        if (!StringUtil.isEmpty(govMeetingListPo.getMeetingImg())) {
//            FileRelPo fileRelPo = getFileRelPo(govMeetingListPo.getGovMeetingId(), FileRelDto.REL_MEETING_TYPE, govMeetingListPo.getMeetingImg());
//            govMeetingListPo.setMeetingImg(fileRelPo.getFileRelId());
//        }
        int flag = govMeetingListInnerServiceSMOImpl.saveGovMeetingList(govMeetingListPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存会议信息失败");
        }
        if (govMembers != null && govMembers.size() > 0) {
            for (int GovMeetingMemberRelIndex = 0; GovMeetingMemberRelIndex < govMembers.size(); GovMeetingMemberRelIndex++) {
                GovMeetingMemberRelPo govMeetingMemberRelPo = BeanConvertUtil.covertBean(govMembers.get(GovMeetingMemberRelIndex), GovMeetingMemberRelPo.class);
                govMeetingMemberRelPo.setGovMeetingId(govMeetingListPo.getGovMeetingId());
                govMeetingMemberRelPo.setCaId(govMeetingListPo.getCaId());
                govMeetingMemberRelPo.setMemberRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_memberRelId));
                flag = govMeetingMemberRelInnerServiceSMOImpl.saveGovMeetingMemberRel(govMeetingMemberRelPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存会议信息失败");
                }
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }


    private FileRelPo getFileRelPo(String objId, String relType, String objImg) {
        FileRelDto fileRelDto = new FileRelDto();
        fileRelDto.setObjId(objId);
        fileRelDto.setRelType(relType);
        List<FileRelDto> fileRelDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
        FileRelPo fileRelPo = null;
        if (fileRelDtos != null && fileRelDtos.size() > 0) {
            //删除原文件关系
            fileRelPo = new FileRelPo();
            fileRelPo.setObjId(objId);
            fileRelPo.setRelType(relType);
            int flag = fileRelInnerServiceSMOImpl.deleteFileRel(fileRelPo);
            if (flag < 1) {
                throw new IllegalArgumentException("删除文件关系失败");
            }
        }
        fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(objImg);
        fileRelPo.setRelType(relType);
        fileRelPo.setObjId(objId);
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存文件关系失败");
        }
        return fileRelPo;
    }


}
