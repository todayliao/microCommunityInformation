package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovReportReturnVisitServiceDao;
import com.java110.dto.govReportReturnVisit.GovReportReturnVisitDto;
import com.java110.intf.gov.IGovReportReturnVisitInnerServiceSMO;
import com.java110.po.govReportReturnVisit.GovReportReturnVisitPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 回访管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovReportReturnVisitInnerServiceSMOImpl extends BaseServiceSMO implements IGovReportReturnVisitInnerServiceSMO {

    @Autowired
    private IGovReportReturnVisitServiceDao govReportReturnVisitServiceDaoImpl;


    @Override
    public int saveGovReportReturnVisit(@RequestBody GovReportReturnVisitPo govReportReturnVisitPo) {
        int saveFlag = 1;
        govReportReturnVisitServiceDaoImpl.saveGovReportReturnVisitInfo(BeanConvertUtil.beanCovertMap(govReportReturnVisitPo));
        return saveFlag;
    }

     @Override
    public int updateGovReportReturnVisit(@RequestBody  GovReportReturnVisitPo govReportReturnVisitPo) {
        int saveFlag = 1;
         govReportReturnVisitServiceDaoImpl.updateGovReportReturnVisitInfo(BeanConvertUtil.beanCovertMap(govReportReturnVisitPo));
        return saveFlag;
    }

     @Override
    public int deleteGovReportReturnVisit(@RequestBody  GovReportReturnVisitPo govReportReturnVisitPo) {
        int saveFlag = 1;
        govReportReturnVisitPo.setStatusCd("1");
        govReportReturnVisitServiceDaoImpl.updateGovReportReturnVisitInfo(BeanConvertUtil.beanCovertMap(govReportReturnVisitPo));
        return saveFlag;
    }

    @Override
    public List<GovReportReturnVisitDto> queryGovReportReturnVisits(@RequestBody  GovReportReturnVisitDto govReportReturnVisitDto) {

        //校验是否传了 分页信息

        int page = govReportReturnVisitDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govReportReturnVisitDto.setPage((page - 1) * govReportReturnVisitDto.getRow());
        }

        List<GovReportReturnVisitDto> govReportReturnVisits = BeanConvertUtil.covertBeanList(govReportReturnVisitServiceDaoImpl.getGovReportReturnVisitInfo(BeanConvertUtil.beanCovertMap(govReportReturnVisitDto)), GovReportReturnVisitDto.class);

        return govReportReturnVisits;
    }


    @Override
    public int queryGovReportReturnVisitsCount(@RequestBody GovReportReturnVisitDto govReportReturnVisitDto) {
        return govReportReturnVisitServiceDaoImpl.queryGovReportReturnVisitsCount(BeanConvertUtil.beanCovertMap(govReportReturnVisitDto));    }

    public IGovReportReturnVisitServiceDao getGovReportReturnVisitServiceDaoImpl() {
        return govReportReturnVisitServiceDaoImpl;
    }

    public void setGovReportReturnVisitServiceDaoImpl(IGovReportReturnVisitServiceDao govReportReturnVisitServiceDaoImpl) {
        this.govReportReturnVisitServiceDaoImpl = govReportReturnVisitServiceDaoImpl;
    }
}
