package com.java110.gov.bmo.govPartyOrgContext;

import com.java110.po.govPartyOrgContext.GovPartyOrgContextPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovPartyOrgContextBMO {


    /**
     * 添加党组织简介
     * add by wuxw
     * @param govPartyOrgContextPo
     * @return
     */
    ResponseEntity<String> save(GovPartyOrgContextPo govPartyOrgContextPo);


}
