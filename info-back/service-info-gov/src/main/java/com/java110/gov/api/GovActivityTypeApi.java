package com.java110.gov.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govActivityType.GovActivityTypeDto;
import com.java110.gov.bmo.govActivityType.IDeleteGovActivityTypeBMO;
import com.java110.gov.bmo.govActivityType.IGetGovActivityTypeBMO;
import com.java110.gov.bmo.govActivityType.ISaveGovActivityTypeBMO;
import com.java110.gov.bmo.govActivityType.IUpdateGovActivityTypeBMO;
import com.java110.po.govActivityType.GovActivityTypePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/govActivityType")
public class GovActivityTypeApi {

    @Autowired
    private ISaveGovActivityTypeBMO saveGovActivityTypeBMOImpl;
    @Autowired
    private IUpdateGovActivityTypeBMO updateGovActivityTypeBMOImpl;
    @Autowired
    private IDeleteGovActivityTypeBMO deleteGovActivityTypeBMOImpl;

    @Autowired
    private IGetGovActivityTypeBMO getGovActivityTypeBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govActivityType/saveGovActivityType
     * @path /app/govActivityType/saveGovActivityType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovActivityType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovActivityType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");


        GovActivityTypePo govActivityTypePo = BeanConvertUtil.covertBean(reqJson, GovActivityTypePo.class);
        return saveGovActivityTypeBMOImpl.save(govActivityTypePo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govActivityType/updateGovActivityType
     * @path /app/govActivityType/updateGovActivityType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovActivityType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovActivityType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");
        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovActivityTypePo govActivityTypePo = BeanConvertUtil.covertBean(reqJson, GovActivityTypePo.class);
        return updateGovActivityTypeBMOImpl.update(govActivityTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityType/deleteGovActivityType
     * @path /app/govActivityType/deleteGovActivityType
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovActivityType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovActivityType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovActivityTypePo govActivityTypePo = BeanConvertUtil.covertBean(reqJson, GovActivityTypePo.class);
        return deleteGovActivityTypeBMOImpl.delete(govActivityTypePo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govActivityType/queryGovActivityType
     * @path /app/govActivityType/queryGovActivityType
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovActivityType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovActivityType(@RequestParam(value = "caId") String caId,
                                                       @RequestParam(value = "typeName" , required = false) String typeName,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {
        GovActivityTypeDto govActivityTypeDto = new GovActivityTypeDto();
        govActivityTypeDto.setPage(page);
        govActivityTypeDto.setRow(row);
        govActivityTypeDto.setTypeName(typeName);
        return getGovActivityTypeBMOImpl.get(govActivityTypeDto);
    }
}
