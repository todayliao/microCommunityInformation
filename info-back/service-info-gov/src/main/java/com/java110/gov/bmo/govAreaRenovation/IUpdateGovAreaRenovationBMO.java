package com.java110.gov.bmo.govAreaRenovation;
import org.springframework.http.ResponseEntity;
import com.java110.po.govAreaRenovation.GovAreaRenovationPo;

public interface IUpdateGovAreaRenovationBMO {


    /**
     * 修改重点地区排查整治
     * add by wuxw
     * @param govAreaRenovationPo
     * @return
     */
    ResponseEntity<String> update(GovAreaRenovationPo govAreaRenovationPo);


}
