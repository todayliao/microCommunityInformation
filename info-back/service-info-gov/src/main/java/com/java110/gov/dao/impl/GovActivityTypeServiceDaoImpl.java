package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovActivityTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 活动类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govActivityTypeServiceDaoImpl")
//@Transactional
public class GovActivityTypeServiceDaoImpl extends BaseServiceDao implements IGovActivityTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovActivityTypeServiceDaoImpl.class);





    /**
     * 保存活动类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovActivityTypeInfo(Map info) throws DAOException {
        logger.debug("保存活动类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govActivityTypeServiceDaoImpl.saveGovActivityTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存活动类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询活动类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovActivityTypeInfo(Map info) throws DAOException {
        logger.debug("查询活动类型信息 入参 info : {}",info);

        List<Map> businessGovActivityTypeInfos = sqlSessionTemplate.selectList("govActivityTypeServiceDaoImpl.getGovActivityTypeInfo",info);

        return businessGovActivityTypeInfos;
    }


    /**
     * 修改活动类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovActivityTypeInfo(Map info) throws DAOException {
        logger.debug("修改活动类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govActivityTypeServiceDaoImpl.updateGovActivityTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改活动类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询活动类型数量
     * @param info 活动类型信息
     * @return 活动类型数量
     */
    @Override
    public int queryGovActivityTypesCount(Map info) {
        logger.debug("查询活动类型数据 入参 info : {}",info);

        List<Map> businessGovActivityTypeInfos = sqlSessionTemplate.selectList("govActivityTypeServiceDaoImpl.queryGovActivityTypesCount", info);
        if (businessGovActivityTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovActivityTypeInfos.get(0).get("count").toString());
    }


}
