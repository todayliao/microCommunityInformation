package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovSpecialFollowServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 特殊人员跟进记录服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govSpecialFollowServiceDaoImpl")
//@Transactional
public class GovSpecialFollowServiceDaoImpl extends BaseServiceDao implements IGovSpecialFollowServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovSpecialFollowServiceDaoImpl.class);





    /**
     * 保存特殊人员跟进记录信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovSpecialFollowInfo(Map info) throws DAOException {
        logger.debug("保存特殊人员跟进记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govSpecialFollowServiceDaoImpl.saveGovSpecialFollowInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存特殊人员跟进记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询特殊人员跟进记录信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovSpecialFollowInfo(Map info) throws DAOException {
        logger.debug("查询特殊人员跟进记录信息 入参 info : {}",info);

        List<Map> businessGovSpecialFollowInfos = sqlSessionTemplate.selectList("govSpecialFollowServiceDaoImpl.getGovSpecialFollowInfo",info);

        return businessGovSpecialFollowInfos;
    }


    /**
     * 修改特殊人员跟进记录信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovSpecialFollowInfo(Map info) throws DAOException {
        logger.debug("修改特殊人员跟进记录信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govSpecialFollowServiceDaoImpl.updateGovSpecialFollowInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改特殊人员跟进记录信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询特殊人员跟进记录数量
     * @param info 特殊人员跟进记录信息
     * @return 特殊人员跟进记录数量
     */
    @Override
    public int queryGovSpecialFollowsCount(Map info) {
        logger.debug("查询特殊人员跟进记录数据 入参 info : {}",info);

        List<Map> businessGovSpecialFollowInfos = sqlSessionTemplate.selectList("govSpecialFollowServiceDaoImpl.queryGovSpecialFollowsCount", info);
        if (businessGovSpecialFollowInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovSpecialFollowInfos.get(0).get("count").toString());
    }


}
