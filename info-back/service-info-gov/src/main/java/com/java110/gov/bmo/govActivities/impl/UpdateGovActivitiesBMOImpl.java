package com.java110.gov.bmo.govActivities.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.gov.bmo.govActivities.IUpdateGovActivitiesBMO;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.gov.IGovActivitiesInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govActivities.GovActivitiesPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovActivitiesBMOImpl")
public class UpdateGovActivitiesBMOImpl implements IUpdateGovActivitiesBMO {

    @Autowired
    private IGovActivitiesInnerServiceSMO govActivitiesInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    /**
     *
     *
     * @param govActivitiesPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovActivitiesPo govActivitiesPo) {


        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId( govActivitiesPo.getActivitiesId()  );
        fileRelPo.setFileName( govActivitiesPo.getHeaderImg()  );
        fileRelPo.setRelType(FileRelDto.REL_TYPE);
        fileRelInnerServiceSMOImpl.deleteFileRel( fileRelPo );

        fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govActivitiesPo.getHeaderImg());
        fileRelPo.setRelType(FileRelDto.REL_TYPE);
        fileRelPo.setObjId(govActivitiesPo.getActivitiesId());
        int flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }
        govActivitiesPo.setHeaderImg(fileRelPo.getFileRelId());
        flag = govActivitiesInnerServiceSMOImpl.updateGovActivities(govActivitiesPo);

        if (flag < 1) {
            return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
