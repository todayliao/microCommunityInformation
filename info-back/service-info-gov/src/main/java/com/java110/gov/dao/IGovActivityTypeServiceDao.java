package com.java110.gov.dao;


import com.java110.utils.exception.DAOException;

import java.util.List;
import java.util.Map;

/**
 * 活动类型组件内部之间使用，没有给外围系统提供服务能力
 * 活动类型服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovActivityTypeServiceDao {


    /**
     * 保存 活动类型信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovActivityTypeInfo(Map info) throws DAOException;




    /**
     * 查询活动类型信息（instance过程）
     * 根据bId 查询活动类型信息
     * @param info bId 信息
     * @return 活动类型信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovActivityTypeInfo(Map info) throws DAOException;



    /**
     * 修改活动类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovActivityTypeInfo(Map info) throws DAOException;


    /**
     * 查询活动类型总数
     *
     * @param info 活动类型信息
     * @return 活动类型数量
     */
    int queryGovActivityTypesCount(Map info);

}
