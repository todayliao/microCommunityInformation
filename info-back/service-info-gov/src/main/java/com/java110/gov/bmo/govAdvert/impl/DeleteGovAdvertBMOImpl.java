package com.java110.gov.bmo.govAdvert.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.gov.bmo.govAdvert.IDeleteGovAdvertBMO;
import com.java110.intf.gov.IGovAdvertInnerServiceSMO;
import com.java110.po.govAdvert.GovAdvertPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovAdvertBMOImpl")
public class DeleteGovAdvertBMOImpl implements IDeleteGovAdvertBMO {

    @Autowired
    private IGovAdvertInnerServiceSMO govAdvertInnerServiceSMOImpl;

    /**
     * @param govAdvertPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovAdvertPo govAdvertPo) {

        int flag = govAdvertInnerServiceSMOImpl.deleteGovAdvert(govAdvertPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
