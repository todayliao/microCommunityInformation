package com.java110.gov.smo.impl;


import com.java110.gov.dao.IGovAreaTypeServiceDao;
import com.java110.intf.gov.IGovAreaTypeInnerServiceSMO;
import com.java110.dto.govAreaType.GovAreaTypeDto;
import com.java110.po.govAreaType.GovAreaTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 涉及区域类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovAreaTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovAreaTypeInnerServiceSMO {

    @Autowired
    private IGovAreaTypeServiceDao govAreaTypeServiceDaoImpl;


    @Override
    public int saveGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo) {
        int saveFlag = 1;
        govAreaTypeServiceDaoImpl.saveGovAreaTypeInfo(BeanConvertUtil.beanCovertMap(govAreaTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo) {
        int saveFlag = 1;
         govAreaTypeServiceDaoImpl.updateGovAreaTypeInfo(BeanConvertUtil.beanCovertMap(govAreaTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovAreaType(@RequestBody  GovAreaTypePo govAreaTypePo) {
        int saveFlag = 1;
        govAreaTypePo.setStatusCd("1");
        govAreaTypeServiceDaoImpl.updateGovAreaTypeInfo(BeanConvertUtil.beanCovertMap(govAreaTypePo));
        return saveFlag;
    }

    @Override
    public List<GovAreaTypeDto> queryGovAreaTypes(@RequestBody  GovAreaTypeDto govAreaTypeDto) {

        //校验是否传了 分页信息

        int page = govAreaTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govAreaTypeDto.setPage((page - 1) * govAreaTypeDto.getRow());
        }

        List<GovAreaTypeDto> govAreaTypes = BeanConvertUtil.covertBeanList(govAreaTypeServiceDaoImpl.getGovAreaTypeInfo(BeanConvertUtil.beanCovertMap(govAreaTypeDto)), GovAreaTypeDto.class);

        return govAreaTypes;
    }


    @Override
    public int queryGovAreaTypesCount(@RequestBody GovAreaTypeDto govAreaTypeDto) {
        return govAreaTypeServiceDaoImpl.queryGovAreaTypesCount(BeanConvertUtil.beanCovertMap(govAreaTypeDto));    }

    public IGovAreaTypeServiceDao getGovAreaTypeServiceDaoImpl() {
        return govAreaTypeServiceDaoImpl;
    }

    public void setGovAreaTypeServiceDaoImpl(IGovAreaTypeServiceDao govAreaTypeServiceDaoImpl) {
        this.govAreaTypeServiceDaoImpl = govAreaTypeServiceDaoImpl;
    }
}
