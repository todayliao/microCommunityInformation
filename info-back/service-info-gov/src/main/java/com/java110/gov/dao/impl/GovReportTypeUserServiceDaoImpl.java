package com.java110.gov.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.gov.dao.IGovReportTypeUserServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 报事类型人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govReportTypeUserServiceDaoImpl")
//@Transactional
public class GovReportTypeUserServiceDaoImpl extends BaseServiceDao implements IGovReportTypeUserServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovReportTypeUserServiceDaoImpl.class);





    /**
     * 保存报事类型人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovReportTypeUserInfo(Map info) throws DAOException {
        logger.debug("保存报事类型人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govReportTypeUserServiceDaoImpl.saveGovReportTypeUserInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存报事类型人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询报事类型人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovReportTypeUserInfo(Map info) throws DAOException {
        logger.debug("查询报事类型人员信息 入参 info : {}",info);

        List<Map> businessGovReportTypeUserInfos = sqlSessionTemplate.selectList("govReportTypeUserServiceDaoImpl.getGovReportTypeUserInfo",info);

        return businessGovReportTypeUserInfos;
    }


    /**
     * 修改报事类型人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovReportTypeUserInfo(Map info) throws DAOException {
        logger.debug("修改报事类型人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govReportTypeUserServiceDaoImpl.updateGovReportTypeUserInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改报事类型人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询报事类型人员数量
     * @param info 报事类型人员信息
     * @return 报事类型人员数量
     */
    @Override
    public int queryGovReportTypeUsersCount(Map info) {
        logger.debug("查询报事类型人员数据 入参 info : {}",info);

        List<Map> businessGovReportTypeUserInfos = sqlSessionTemplate.selectList("govReportTypeUserServiceDaoImpl.queryGovReportTypeUsersCount", info);
        if (businessGovReportTypeUserInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovReportTypeUserInfos.get(0).get("count").toString());
    }


}
