package com.java110.gov.bmo.govWorkGuide;
import com.java110.dto.govWorkGuide.GovWorkGuideDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovWorkGuideBMO {


    /**
     * 查询办事指南
     * add by wuxw
     * @param  govWorkGuideDto
     * @return
     */
    ResponseEntity<String> get(GovWorkGuideDto govWorkGuideDto);


}
