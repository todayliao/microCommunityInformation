package com.java110.api.front.busi.file;

import com.java110.core.context.IPageData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 添加沃文件接口
 *
 * add by wuxw 2019-06-30
 */
public interface IAddFileSMO {


    /**
     * 政务添加文件
     * @param
     * @return ResponseEntity 对象
     */
    String saveFileImage(MultipartFile uploadFile) throws IOException;
}
