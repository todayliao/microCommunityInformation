package com.java110.api.front.busi.file.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.api.front.busi.AppAbstractComponentSMO;
import com.java110.api.front.busi.file.IAddFileSMO;
import com.java110.config.properties.code.Java110Properties;
import com.java110.core.client.FtpUploadTemplate;
import com.java110.core.client.OssUploadTemplate;
import com.java110.core.context.IPageData;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.constant.DomainContant;
import com.java110.utils.util.OSSUtil;
import com.java110.utils.util.StringUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 添加小区服务实现类
 * add by wuxw 2019-06-30
 */
@Service("addFileSMOImpl")
public class AddFileSMOImpl extends AppAbstractComponentSMO implements IAddFileSMO {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private Java110Properties java110Properties;

    @Autowired
    private FtpUploadTemplate ftpUploadTemplate;

    @Autowired
    private OssUploadTemplate ossUploadTemplate;


    @Override
    public String saveFileImage(MultipartFile uploadFile) throws IOException {

        if (uploadFile.getSize() > 2 * 1024 * 1024) {
            throw new IllegalArgumentException("上传文件超过两兆");
        }

        String fileName = "";
        String ossSwitch = MappingCache.getValue(OSSUtil.DOMAIN, OSSUtil.OSS_SWITCH);
        String ftpServer = MappingCache.getValue(FtpUploadTemplate.FTP_DOMAIN, FtpUploadTemplate.FTP_SERVER);
        int ftpPort = Integer.parseInt(MappingCache.getValue(FtpUploadTemplate.FTP_DOMAIN, FtpUploadTemplate.FTP_PORT));
        String ftpUserName = MappingCache.getValue(FtpUploadTemplate.FTP_DOMAIN, FtpUploadTemplate.FTP_USERNAME);
        String ftpUserPassword = MappingCache.getValue(FtpUploadTemplate.FTP_DOMAIN, FtpUploadTemplate.FTP_USERPASSWORD);
        String ftpPath = MappingCache.getValue(FtpUploadTemplate.FTP_DOMAIN, FtpUploadTemplate.FTP_PATH);

        String imageUrl = MappingCache.getValue(DomainContant.COMMON_DOMAIN, "IMG_PATH");
        if (StringUtil.isEmpty(ossSwitch) || !OSSUtil.OSS_SWITCH_OSS.equals(ossSwitch)) {
            fileName = ftpUploadTemplate.upload(uploadFile, ftpServer, ftpPort, ftpUserName,
                    ftpUserPassword, ftpPath);
        } else {
            fileName = ossUploadTemplate.upload(uploadFile, ftpPath);
        }
        return imageUrl + fileName;


    }


    public RestTemplate getRestTemplate() {
        return restTemplate;
    }

    public void setRestTemplate(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @Override
    protected void validate(IPageData pd, JSONObject paramIn) {

    }

    @Override
    protected ResponseEntity<String> doBusinessProcess(IPageData pd, JSONObject paramIn) throws Exception {
        return null;
    }
}
