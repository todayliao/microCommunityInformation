package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.cust.IGovCompanyInnerServiceSMO;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import com.java110.utils.util.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 保存房屋
 */
@Service(value = "EDIT_STAFF")
public class ReportEditStaffDataAdapt implements IReportDataAdapt {

    private final static Logger logger = LoggerFactory.getLogger(ReportEditStaffDataAdapt.class);
    @Autowired
    private IGovCompanyInnerServiceSMO govCompanyInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "staffInfo", "请求报文中未包含staffInfo");

        //处理员工信息
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "personName", "请求报文staffInfo中未包含personName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "personTel", "请求报文staffInfo中未包含personTel");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "personSex", "请求报文staffInfo中未包含personSex");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "govOrgName", "请求报文staffInfo中未包含govOrgName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "relCd", "请求报文staffInfo中未包含relCd");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), "extCompanyId", "请求报文staffInfo中未包含extCompanyId");
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo"), GovPersonPo.class);
        String extCompanyId = reportDataDto.getReportDataBodyDto().getJSONObject("staffInfo").getString("extCompanyId");
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        govPersonPo.setCaId(govCommunity.getCaId());
        govPersonPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);

        if (StringUtil.isEmpty(govPersonPo.getBirthday())) {
            govPersonPo.setBirthday(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_B));
        }
        if (StringUtil.isEmpty(govPersonPo.getIdType())) {
            govPersonPo.setIdType("无");
        }
        if (StringUtil.isEmpty(govPersonPo.getIdCard())) {
            govPersonPo.setIdCard("无");
        }
        if (StringUtil.isEmpty(govPersonPo.getNation())) {
            govPersonPo.setNation("无");
        }
        if (StringUtil.isEmpty(govPersonPo.getRamark())) {
            govPersonPo.setRamark("无");
        }
        if (StringUtil.isEmpty(govPersonPo.getNativePlace())) {
            govPersonPo.setNativePlace("无");
        }
        if (StringUtil.isEmpty(govPersonPo.getPoliticalOutlook())) {
            govPersonPo.setPoliticalOutlook("5012");
        }
        govPersonPo.setPersonType("8008");

        if (StringUtil.isEmpty(govPersonPo.getReligiousBelief())) {
            govPersonPo.setReligiousBelief("无");
        }

        if (StringUtil.isEmpty(govPersonPo.getMaritalStatus())) {
            govPersonPo.setMaritalStatus("无");
        }


        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存员工信息失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
            int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改员工信息失败");
            }
        }
        //处理员工企业关系信息
        GovCompanyPersonPo govCompanyPersonPo = BeanConvertUtil.covertBean(reportDataDto.getReportDataBodyDto().getString("staffInfo"), GovCompanyPersonPo.class);
        govCompanyPersonPo.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_relId));
        govCompanyPersonPo.setGovCompanyId(extCompanyId);
        govCompanyPersonPo.setGovPersonId(govPersonPo.getGovPersonId());
        govCompanyPersonPo.setCaId(govCommunity.getCaId());
        //状态 1001 在职 2002 出差 3003 休假 4004 离职 5005 其他
        govCompanyPersonPo.setState("1001");
        govCompanyPersonPo.setGovOrgName(govPersonPo.getGovOrgName());
        govCompanyPersonPo.setRelCd(govPersonPo.getRelCd());

        GovCompanyPersonDto govCompanyPersonDto = new GovCompanyPersonDto();
        govCompanyPersonDto.setCaId(govCommunity.getCaId());
        govCompanyPersonDto.setGovCompanyId(extCompanyId);
        govCompanyPersonDto.setGovPersonId(govPersonPo.getGovPersonId());
        List<GovCompanyPersonDto> govCompanyPersonDtos = govCompanyPersonInnerServiceSMOImpl.queryGovCompanyPersons(govCompanyPersonDto);

        if (govCompanyPersonDtos == null || govCompanyPersonDtos.size() < 1) {
            int flag = govCompanyPersonInnerServiceSMOImpl.saveGovCompanyPerson(govCompanyPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存员工企业关系信息失败");
            }
        } else {
            govCompanyPersonPo.setRelId(govCompanyPersonDtos.get(0).getRelId());
            int flag = govCompanyPersonInnerServiceSMOImpl.updateGovCompanyPerson(govCompanyPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改员工企业关系信息失败");
            }
        }


        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extPersonId", govPersonPo.getGovPersonId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
