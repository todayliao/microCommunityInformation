package com.java110.databus.reportData.parkingArea;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.databus.reportData.IReportDataAdapt;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.parkingArea.ParkingAreaDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.po.parkingArea.ParkingAreaPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 修改停车场同步信息
 * add by 吴学文 2021-09-17
 */
@Service(value = "EDIT_PARKING_AREA")
public class ReportEditParkingAreaDataAdapt implements IReportDataAdapt {

    private final static Logger logger = LoggerFactory.getLogger(ReportEditParkingAreaDataAdapt.class);
    /**
     * 停车场处理类
     */
    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "num", "请求报文中未包含num");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "parkingCount", "请求报文中未包含parkingCount");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "typeCd", "请求报文中未包含typeCd");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "extPaId", "请求报文中未包含extPaId");

        ParkingAreaPo parkingAreaPo = new ParkingAreaPo();
        //parkingAreaPo.setPaId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_paId));
        parkingAreaPo.setGovCommunityId(govCommunity.getGovCommunityId());
        parkingAreaPo.setNum(reportDataDto.getReportDataBodyDto().getString("num"));
        parkingAreaPo.setParkingCount(reportDataDto.getReportDataBodyDto().getString("parkingCount"));
        parkingAreaPo.setTypeCd(reportDataDto.getReportDataBodyDto().getString("typeCd"));
        parkingAreaPo.setCaId(govCommunity.getCaId());

        ParkingAreaDto parkingAreaDto = new ParkingAreaDto();
        parkingAreaDto.setPaId(reportDataDto.getReportDataBodyDto().getString("extPaId"));
        parkingAreaDto.setCaId(govCommunity.getCaId());
        parkingAreaDto.setGovCommunityId(govCommunity.getGovCommunityId());
        List<ParkingAreaDto> parkingAreaDtos = parkingAreaInnerServiceSMOImpl.queryParkingAreas(parkingAreaDto);
        if (parkingAreaDtos == null || parkingAreaDtos.size() < 1) {
//            int flag = parkingAreaInnerServiceSMOImpl.saveParkingArea(parkingAreaPo);
//            if (flag < 1) {
//                throw new IllegalArgumentException("保存停车场信息失败");
//            }
        } else {
            parkingAreaPo.setPaId(parkingAreaDtos.get(0).getPaId());
            int flag = parkingAreaInnerServiceSMOImpl.updateParkingArea(parkingAreaPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改停车场信息失败");
            }
        }

        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extPaId", parkingAreaPo.getPaId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
