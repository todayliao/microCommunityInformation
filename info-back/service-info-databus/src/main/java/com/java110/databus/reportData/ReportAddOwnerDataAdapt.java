package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govOwner.GovOwnerDto;
import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.govRoom.GovRoomDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerInnerServiceSMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.po.govOwner.GovOwnerPo;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 保存房屋
 */
@Service(value = "ADD_OWNER")
public class ReportAddOwnerDataAdapt implements IReportDataAdapt {

    private final static Logger logger = LoggerFactory.getLogger(ReportAddOwnerDataAdapt.class);
    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerInnerServiceSMO govOwnerInnerServiceSMOImpl;
    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));
        //校验报文
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "idType", "请求报文中未包含idType");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "birthday", "请求报文中未包含birthday");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "nativePlace", "请求报文中未包含nativePlace");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "politicalOutlook", "请求报文中未包含politicalOutlook");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "maritalStatus", "请求报文中未包含maritalStatus");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "ownerType", "请求报文中未包含ownerType");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "ownerTypeCd", "请求报文中未包含ownerTypeCd");

        GovOwnerPo govOwnerPo = BeanConvertUtil.covertBean(reportDataDto.getReportDataBodyDto(), GovOwnerPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reportDataDto.getReportDataBodyDto(), GovPersonPo.class);
        if ("1001".equals(reportDataDto.getReportDataBodyDto().getString("ownerTypeCd"))) {
            //处理户籍信息
            govOwnerPo.setGovOwnerId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOwnerId));
            govOwnerPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
            govOwnerPo.setCaId(govCommunity.getCaId());
            govOwnerPo.setOwnerNum(govOwnerPo.getGovOwnerId());
            govOwnerPo.setOwnerName(govPersonPo.getPersonName());
            govOwnerPo.setOwnerTel(govPersonPo.getPersonTel());
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("ownerAddress"))) {
                govOwnerPo.setOwnerAddress("无");
            }
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("ramark"))) {
                govOwnerPo.setRamark("政务系统自动推送，添加户籍人口，请完善相关信息");
            }
            GovOwnerDto govOwnerDto = new GovOwnerDto();
            govOwnerDto.setCaId(govCommunity.getCaId());
            govOwnerDto.setIdCard(govOwnerPo.getIdCard());
            List<GovOwnerDto> govOwnerDtos = govOwnerInnerServiceSMOImpl.queryGovOwners(govOwnerDto);
            if (govOwnerDtos == null || govOwnerDtos.size() < 1) {
                int flag = govOwnerInnerServiceSMOImpl.saveGovOwner(govOwnerPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息失败");
                }
            } else {
                govOwnerPo.setGovOwnerId(govOwnerDtos.get(0).getGovOwnerId());
                int flag = govOwnerInnerServiceSMOImpl.updateGovOwner(govOwnerPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("修改户籍信息失败");
                }
            }

            //保存户籍人口信息到人口表
            govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
            govPersonPo.setCaId(govOwnerPo.getCaId());
            govPersonPo.setPersonType("1001");
            govPersonPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("prePersonName"))) {
                govPersonPo.setPrePersonName(govPersonPo.getPersonName());
            }
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("religiousBelief"))) {
                govPersonPo.setReligiousBelief("无");
            }
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("ramark"))) {
                govPersonPo.setRamark("政务系统自动推送，添加户籍人口时，系统自动写入户籍人口信息，请完善相关信息");
            }
            GovPersonDto govPersonDto = new GovPersonDto();
            govPersonDto.setCaId(govPersonPo.getCaId());
            govPersonDto.setIdCard(govPersonPo.getIdCard());
            List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
            if (govPersonDtos == null || govPersonDtos.size() < 1) {
                int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息失败");
                }
            } else {
                govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
                int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息失败");
                }
            }

            //保存户籍信息和人口信息关系表
            GovOwnerPersonPo govOwnerPersonPo = new GovOwnerPersonPo();
            govOwnerPersonPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
            govOwnerPersonPo.setGovOwnerId(govOwnerPo.getGovOwnerId());
            govOwnerPersonPo.setCaId(govOwnerPo.getCaId());
            govOwnerPersonPo.setGovPersonId(govPersonPo.getGovPersonId());
            govOwnerPersonPo.setRelCd("1001");
            govOwnerPersonPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);

            GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
            govOwnerPersonDto.setGovOwnerId(govOwnerPo.getGovOwnerId());
            govOwnerPersonDto.setGovPersonId(govPersonPo.getGovPersonId());
            govOwnerPersonDto.setCaId(govOwnerPo.getCaId());
            govOwnerPersonDto.setRelCd("1001");
            List<GovOwnerPersonDto> govOwnerPersonDtos = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons(govOwnerPersonDto);
            if (govOwnerPersonDtos == null || govOwnerPersonDtos.size() < 1) {
                int flag = govOwnerPersonInnerServiceSMOImpl.saveGovOwnerPerson(govOwnerPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息和人口信息关系表失败");
                }
            } else {
                govOwnerPersonPo.setGovOpId(govOwnerPersonDtos.get(0).getGovOpId());
                int flag = govOwnerPersonInnerServiceSMOImpl.updateGovOwnerPerson(govOwnerPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("修改户籍信息和人口信息关系表");
                }
            }

            //修改房屋关联房主信息
            JSONArray extRoomIds = JSONObject.parseArray(reportDataDto.getReportDataBodyDto().getString("extRoomId"));
            GovRoomDto govRoomDto = new GovRoomDto();
            govRoomDto.setGovRoomIds(extRoomIds.toArray(new String[extRoomIds.size()]));
            govRoomDto.setCaId(govCommunity.getCaId());
            govRoomDto.setGovCommunityId(govCommunity.getGovCommunityId());
            List<GovRoomDto> govRoomDtoList = govRoomInnerServiceSMOImpl.queryGovRooms(govRoomDto);
            if (govRoomDtoList != null && govRoomDtoList.size() > 0) {
                for (GovRoomDto govRoom : govRoomDtoList) {

                    GovRoomPo govRoomPo = new GovRoomPo();
                    govRoomPo.setGovRoomId(govRoom.getGovRoomId());
                    govRoomPo.setOwnerId(govPersonPo.getGovPersonId());
                    govRoomPo.setOwnerName(govPersonPo.getPersonName());
                    govRoomPo.setOwnerTel(govPersonPo.getPersonTel());
                    int flag = govRoomInnerServiceSMOImpl.updateGovRoom(govRoomPo);
                    if (flag < 1) {
                        throw new IllegalArgumentException("修改房屋房主信息失败");
                    }
                }
            }
        }
        if ("1002".equals(reportDataDto.getReportDataBodyDto().getString("ownerTypeCd"))) {
            //保存户籍人口信息到人口表
            govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
            govPersonPo.setCaId(govOwnerPo.getCaId());
            govPersonPo.setPersonType("1001");
            govPersonPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("prePersonName"))) {
                govPersonPo.setPrePersonName(govPersonPo.getPersonName());
            }
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("religiousBelief"))) {
                govPersonPo.setReligiousBelief("无");
            }
            if (!StringUtil.isNullOrNone(reportDataDto.getReportDataBodyDto().getString("ramark"))) {
                govPersonPo.setRamark("政务系统自动推送，添加户籍人口时，系统自动写入户籍人口信息，请完善相关信息");
            }
            GovPersonDto govPersonDto = new GovPersonDto();
            govPersonDto.setCaId(govPersonPo.getCaId());
            govPersonDto.setIdCard(govPersonPo.getIdCard());
            List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
            if (govPersonDtos == null || govPersonDtos.size() < 1) {
                int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息失败");
                }
            } else {
                govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
                int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息失败");
                }
            }
            String extOwnerId = reportDataDto.getReportDataBodyDto().getString("extOwnerId");
            //保存户籍信息和人口信息关系表
            GovOwnerPersonPo govOwnerPersonPo = new GovOwnerPersonPo();
            govOwnerPersonPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
            govOwnerPersonPo.setGovOwnerId(extOwnerId);
            govOwnerPersonPo.setCaId(govOwnerPo.getCaId());
            govOwnerPersonPo.setGovPersonId(govPersonPo.getGovPersonId());
            govOwnerPersonPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
            //暂时以父子关系
            govOwnerPersonPo.setRelCd("2002");
            GovOwnerPersonDto govOwnerPersonDto = new GovOwnerPersonDto();
            govOwnerPersonDto.setGovOwnerId(extOwnerId);
            govOwnerPersonDto.setGovPersonId(govPersonPo.getGovPersonId());
            govOwnerPersonDto.setCaId(govOwnerPo.getCaId());
            govOwnerPersonDto.setRelCd("2002");
            List<GovOwnerPersonDto> govOwnerPersonDtos = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons(govOwnerPersonDto);
            if (govOwnerPersonDtos == null || govOwnerPersonDtos.size() < 1) {
                int flag = govOwnerPersonInnerServiceSMOImpl.saveGovOwnerPerson(govOwnerPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存户籍信息和人口信息关系表失败");
                }
            } else {
                govOwnerPersonPo.setGovOpId(govOwnerPersonDtos.get(0).getGovOpId());
                int flag = govOwnerPersonInnerServiceSMOImpl.updateGovOwnerPerson(govOwnerPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("修改户籍信息和人口信息关系表");
                }
            }
        }


        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extOwnerId", govPersonPo.getGovPersonId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
