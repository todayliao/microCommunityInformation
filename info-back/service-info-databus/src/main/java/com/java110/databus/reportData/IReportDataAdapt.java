package com.java110.databus.reportData;

import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.reportData.ReportDataDto;

/**
 * 数据上报 适配器
 */
public interface IReportDataAdapt {

    /**
     * 数据上报接口
     *
     * @param reportDataDto
     * @return
     */
    ReportDataDto report(ReportDataDto reportDataDto,GovCommunityDto govCommunity);
}
