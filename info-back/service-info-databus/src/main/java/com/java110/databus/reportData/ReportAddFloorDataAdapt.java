package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.databus.kafka.DatabusServiceKafka;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.govFloor.GovFloorDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 保存建筑物
 * reportAddFloorDataAdapt
 */
@Service(value = "ADD_FLOOR")
public class ReportAddFloorDataAdapt implements IReportDataAdapt {
    private final static Logger logger = LoggerFactory.getLogger(ReportAddFloorDataAdapt.class);
    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;
    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "floorName", "请求报文中未包含floorName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "floorNum", "请求报文中未包含floorNum");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "floorArea", "请求报文中未包含floorArea");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "layerCount", "请求报文中未包含layerCount");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "unitCount", "请求报文中未包含unitCount");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "floorUse", "请求报文中未包含floorUse");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reportDataDto.getReportDataBodyDto(), "personLink", "请求报文中未包含personLink");

        GovFloorPo govFloorPo =new GovFloorPo();
        govFloorPo.setGovFloorId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govFloorId));
        govFloorPo.setGovCommunityId(govCommunity.getGovCommunityId());
        govFloorPo.setFloorNum(reportDataDto.getReportDataBodyDto().getString("floorNum"));
        govFloorPo.setFloorName(reportDataDto.getReportDataBodyDto().getString("floorName"));
        govFloorPo.setFloorType(reportDataDto.getReportDataBodyDto().getString("floorType"));
        govFloorPo.setDatasourceType(ReportDataHeaderDto.SYSTEM_COMMUNITY_TYPE);
        if(StringUtil.isEmpty(govFloorPo.getFloorType())){
            govFloorPo.setFloorType(ReportDataHeaderDto.SYSTEM_FLOOR_TYPE);
        }
        govFloorPo.setCaId(govCommunity.getCaId());
        govFloorPo.setFloorArea(reportDataDto.getReportDataBodyDto().getString("floorArea"));
        govFloorPo.setLayerCount(reportDataDto.getReportDataBodyDto().getString("layerCount"));
        govFloorPo.setUnitCount(reportDataDto.getReportDataBodyDto().getString("unitCount"));
        govFloorPo.setFloorUse(reportDataDto.getReportDataBodyDto().getString("floorUse"));
        govFloorPo.setPersonName(reportDataDto.getReportDataBodyDto().getString("personName"));
        govFloorPo.setPersonLink(reportDataDto.getReportDataBodyDto().getString("personLink"));


        GovFloorDto govFloorDto = new GovFloorDto();
        govFloorDto.setFloorNum(reportDataDto.getReportDataBodyDto().getString("floorNum"));
        govFloorDto.setCaId(govCommunity.getCaId());
        govFloorDto.setGovCommunityId(govCommunity.getGovCommunityId());
        List<GovFloorDto> govFloorDtoList = govFloorInnerServiceSMOImpl.queryGovFloors(govFloorDto);
        if(govFloorDtoList == null || govFloorDtoList.size() < 1){
            int flag = govFloorInnerServiceSMOImpl.saveGovFloor(govFloorPo);
            if(flag < 1){
                throw  new IllegalArgumentException("保存楼栋信息失败");
            }
        }else{
            govFloorPo.setGovFloorId(govFloorDtoList.get(0).getGovFloorId());
            int flag = govFloorInnerServiceSMOImpl.updateGovFloor(govFloorPo);
            if(flag < 1){
                throw  new IllegalArgumentException("修改楼栋信息失败");
            }
        }

        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extFloorId",govFloorPo.getGovFloorId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
