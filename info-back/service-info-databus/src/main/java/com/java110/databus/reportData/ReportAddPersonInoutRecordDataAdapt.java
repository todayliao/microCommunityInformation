package com.java110.databus.reportData;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.dto.reportData.ReportDataDto;
import com.java110.dto.reportData.ReportDataHeaderDto;
import com.java110.intf.gov.IGovPersonInoutRecordInnerServiceSMO;
import com.java110.po.govPersonInoutRecord.GovPersonInoutRecordPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.utils.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 人脸记录上报 小区位置
 * <p>
 * add by  wuxw 2021-09-05
 * <p>
 * ReportAddLocationDataAdapt
 */
@Service(value = "ADD_PERSON_INOUT_RECORD")
public class ReportAddPersonInoutRecordDataAdapt implements IReportDataAdapt {
    private final static Logger logger = LoggerFactory.getLogger(ReportAddPersonInoutRecordDataAdapt.class);
    @Autowired
    private IGovPersonInoutRecordInnerServiceSMO govPersonInoutRecordInnerServiceSMOImpl;

    @Override
    public ReportDataDto report(ReportDataDto reportDataDto, GovCommunityDto govCommunity) {
        logger.info(JSONObject.toJSONString(reportDataDto));

        JSONObject reqJson = reportDataDto.getReportDataBodyDto();

        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");
        Assert.hasKeyAndValue(reqJson, "extLocationId", "请求报文中未包含extLocationId");
        Assert.hasKeyAndValue(reqJson, "openTypeCd", "请求报文中未包含openTypeCd");
        Assert.hasKeyAndValue(reqJson, "tel", "请求报文中未包含tel");
        Assert.hasKeyAndValue(reqJson, "recordTypeCd", "请求报文中未包含recordTypeCd");
        Assert.hasKeyAndValue(reqJson, "faceUrl", "请求报文中未包含faceUrl");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");

        GovPersonInoutRecordPo govPersonInoutRecordPo = BeanConvertUtil.covertBean(reqJson, GovPersonInoutRecordPo.class);
        govPersonInoutRecordPo.setRecordId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govFloorId));
        govPersonInoutRecordPo.setGovCommunityId(govCommunity.getGovCommunityId());
        govPersonInoutRecordPo.setCaId(govCommunity.getCaId());

        int flag = govPersonInoutRecordInnerServiceSMOImpl.saveGovPersonInoutRecord(govPersonInoutRecordPo);
        if (flag < 1) {
            throw new IllegalArgumentException("人员记录信息保存失败");
        }

        ReportDataDto returnReportData = new ReportDataDto();
        ReportDataHeaderDto returnReportHeader = reportDataDto.getReportDataHeaderDto();
        returnReportHeader.setCode(ReportDataHeaderDto.CODE_SUCCESS);
        returnReportHeader.setMsg(ReportDataHeaderDto.MSG_SUCCESS);
        returnReportHeader.setResTime(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_DEFAULT));
        returnReportHeader.setReqTime(null);
        JSONObject body = new JSONObject();
        body.put("extRecordId", govPersonInoutRecordPo.getRecordId());
        returnReportData.setReportDataHeaderDto(returnReportHeader);
        returnReportData.setReportDataBodyDto(body);
        return returnReportData;
    }
}
