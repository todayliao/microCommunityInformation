package com.java110.assets.bmo.govCommunityArea;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCommunityAreaBMO {


    /**
     * 查询区域管理
     * add by wuxw
     * @param  govCommunityAreaDto
     * @return
     */
    ResponseEntity<String> get(GovCommunityAreaDto govCommunityAreaDto);


}
