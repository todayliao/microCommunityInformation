package com.java110.assets.bmo.govCommunity.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunity.IUpdateGovCommunityBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.core.client.RestTemplate;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.dto.file.FileDto;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityAttrInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.intf.engine.IFileInnerServiceSMO;
import com.java110.po.fileRel.FileRelPo;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.po.govCommunityAttr.GovCommunityAttrPo;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("updateGovCommunityBMOImpl")
public class UpdateGovCommunityBMOImpl implements IUpdateGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private IFileInnerServiceSMO fileInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    @Autowired
    private IGovCommunityAttrInnerServiceSMO govCommunityAttrInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param govCommunityPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovCommunityPo govCommunityPo, JSONObject reqJson) {


        //删除原文件关系
        FileRelPo fileRelPo = new FileRelPo();
        fileRelPo.setObjId(govCommunityPo.getGovCommunityId());
        fileRelPo.setRelType("10000");
        int flag = fileRelInnerServiceSMOImpl.deleteFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除文件关系失败");
        }
        fileRelPo = new FileRelPo();
        fileRelPo.setFileRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_fileRelId));
        fileRelPo.setFileName(govCommunityPo.getCommunityIcon());
        fileRelPo.setRelType("10000");
        fileRelPo.setObjId(govCommunityPo.getGovCommunityId());
        flag = fileRelInnerServiceSMOImpl.saveFileRel(fileRelPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存文件关系失败");
        }
        govCommunityPo.setCommunityIcon(fileRelPo.getFileRelId());

        flag = govCommunityInnerServiceSMOImpl.updateGovCommunity(govCommunityPo);
        if (flag < 1) {
            throw new IllegalArgumentException("修改小区失败");
        }

        saveCommunityAttrs(govCommunityPo, GovCommunityAttrPo.COMMUNITY_ATTR_MAPX, govCommunityPo.getMapX());
        saveCommunityAttrs(govCommunityPo, GovCommunityAttrPo.COMMUNITY_ATTR_MAPY, govCommunityPo.getMapY());
        reqJson.put("govCommunityId", govCommunityPo.getGovCommunityId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.EDIT_COMMUNITY_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private void saveCommunityAttrs(GovCommunityPo govCommunityPo, String attrCdType, String attrValue) {
        int flag;
        GovCommunityAttrPo govCommunityAttrPo = new GovCommunityAttrPo();
        govCommunityAttrPo.setGovCommunityId(govCommunityPo.getGovCommunityId());
        govCommunityAttrPo.setCaId(govCommunityPo.getCaId());
        govCommunityAttrPo.setSpecCd(attrCdType);
        govCommunityAttrPo.setValue(attrValue);

        GovCommunityAttrDto govCommunityAttrDto = new GovCommunityAttrDto();
        govCommunityAttrDto.setGovCommunityId(govCommunityPo.getGovCommunityId());
        govCommunityAttrDto.setCaId(govCommunityPo.getCaId());
        govCommunityAttrDto.setSpecCd(attrCdType);
        List<GovCommunityAttrDto> govCommunityAttrDtos = govCommunityAttrInnerServiceSMOImpl.queryGovCommunityAttrs(govCommunityAttrDto);
        if (govCommunityAttrDtos != null && govCommunityAttrDtos.size() > 0) {
            govCommunityAttrPo.setAttrId(govCommunityAttrDtos.get(0).getAttrId());
            flag = govCommunityAttrInnerServiceSMOImpl.deleteGovCommunityAttr(govCommunityAttrPo);
            if (flag < 1) {
                throw new IllegalArgumentException("清除小区坐标属性失败");
            }
        }
        govCommunityAttrPo.setAttrId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_attrId));
        flag = govCommunityAttrInnerServiceSMOImpl.saveGovCommunityAttr(govCommunityAttrPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存小区坐标属性失败");
        }
    }
}
