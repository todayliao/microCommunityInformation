package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govFloor.IDeleteGovFloorBMO;
import com.java110.assets.bmo.govFloor.IGetGovFloorBMO;
import com.java110.assets.bmo.govFloor.ISaveGovFloorBMO;
import com.java110.assets.bmo.govFloor.IUpdateGovFloorBMO;
import com.java110.dto.govFloor.GovFloorDto;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govFloor")
public class GovFloorApi {

    @Autowired
    private ISaveGovFloorBMO saveGovFloorBMOImpl;
    @Autowired
    private IUpdateGovFloorBMO updateGovFloorBMOImpl;
    @Autowired
    private IDeleteGovFloorBMO deleteGovFloorBMOImpl;

    @Autowired
    private IGetGovFloorBMO getGovFloorBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govFloor/saveGovFloor
     * @path /app/govFloor/saveGovFloor
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovFloor", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovFloor(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "floorNum", "请求报文中未包含floorNum");
        Assert.hasKeyAndValue(reqJson, "floorName", "请求报文中未包含floorName");
        Assert.hasKeyAndValue(reqJson, "floorType", "请求报文中未包含floorType");
        Assert.hasKeyAndValue(reqJson, "floorArea", "请求报文中未包含floorArea");
        Assert.hasKeyAndValue(reqJson, "layerCount", "请求报文中未包含layerCount");
        Assert.hasKeyAndValue(reqJson, "unitCount", "请求报文中未包含unitCount");
        Assert.hasKeyAndValue(reqJson, "floorUse", "请求报文中未包含floorUse");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "floorIcon", "请求报文中未包含floorIcon");


        GovFloorPo govFloorPo = BeanConvertUtil.covertBean(reqJson, GovFloorPo.class);
        return saveGovFloorBMOImpl.save(govFloorPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govFloor/updateGovFloor
     * @path /app/govFloor/updateGovFloor
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovFloor", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovFloor(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "floorNum", "请求报文中未包含floorNum");
        Assert.hasKeyAndValue(reqJson, "floorName", "请求报文中未包含floorName");
        Assert.hasKeyAndValue(reqJson, "floorType", "请求报文中未包含floorType");
        Assert.hasKeyAndValue(reqJson, "floorArea", "请求报文中未包含floorArea");
        Assert.hasKeyAndValue(reqJson, "layerCount", "请求报文中未包含layerCount");
        Assert.hasKeyAndValue(reqJson, "unitCount", "请求报文中未包含unitCount");
        Assert.hasKeyAndValue(reqJson, "floorUse", "请求报文中未包含floorUse");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personLink", "请求报文中未包含personLink");
        Assert.hasKeyAndValue(reqJson, "floorIcon", "请求报文中未包含floorIcon");
        Assert.hasKeyAndValue(reqJson, "govFloorId", "govFloorId不能为空");


        GovFloorPo govFloorPo = BeanConvertUtil.covertBean(reqJson, GovFloorPo.class);
        return updateGovFloorBMOImpl.update(govFloorPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govFloor/deleteGovFloor
     * @path /app/govFloor/deleteGovFloor
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovFloor", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovFloor(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govFloorId", "govFloorId不能为空");


        GovFloorPo govFloorPo = BeanConvertUtil.covertBean(reqJson, GovFloorPo.class);
        return deleteGovFloorBMOImpl.delete(govFloorPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govFloor/queryGovFloor
     * @path /app/govFloor/queryGovFloor
     * @param
     * @return
     */
    @RequestMapping(value = "/queryGovFloor", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovFloor(@RequestParam(value = "caId", required = false) String caId,
                                                      @RequestParam(value = "govCommunityId", required = false) String govCommunityId,
                                                      @RequestParam(value = "floorName", required = false) String floorName,
                                                      @RequestParam(value = "floorType", required = false) String floorType,
                                                      @RequestParam(value = "govFloorId", required = false) String govFloorId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovFloorDto govFloorDto = new GovFloorDto();
        govFloorDto.setPage(page);
        govFloorDto.setRow(row);
        govFloorDto.setCaId(caId);
        govFloorDto.setGovCommunityId(govCommunityId);
        govFloorDto.setFloorName(floorName);
        govFloorDto.setFloorType(floorType);
        govFloorDto.setGovFloorId( govFloorId );
        return getGovFloorBMOImpl.get(govFloorDto);
    }
}
