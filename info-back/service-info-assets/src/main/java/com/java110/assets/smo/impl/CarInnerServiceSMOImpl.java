package com.java110.assets.smo.impl;


import com.java110.assets.dao.ICarServiceDao;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import com.java110.dto.car.CarDto;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.po.car.CarPo;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 车辆内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class CarInnerServiceSMOImpl extends BaseServiceSMO implements ICarInnerServiceSMO {

    @Autowired
    private ICarServiceDao carServiceDaoImpl;


    @Override
    public int saveCar(@RequestBody CarPo carPo) {
        int saveFlag = 1;
        carServiceDaoImpl.saveCarInfo(BeanConvertUtil.beanCovertMap(carPo));
        return saveFlag;
    }

    @Override
    public int updateCar(@RequestBody CarPo carPo) {
        int saveFlag = 1;
        carServiceDaoImpl.updateCarInfo(BeanConvertUtil.beanCovertMap(carPo));
        return saveFlag;
    }

    @Override
    public int deleteCar(@RequestBody CarPo carPo) {
        int saveFlag = 1;
        carPo.setStatusCd("1");
        carServiceDaoImpl.updateCarInfo(BeanConvertUtil.beanCovertMap(carPo));
        return saveFlag;
    }

    @Override
    public List<CarDto> queryCars(@RequestBody CarDto carDto) {

        //校验是否传了 分页信息

        int page = carDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            carDto.setPage((page - 1) * carDto.getRow());
        }

        List<CarDto> cars = BeanConvertUtil.covertBeanList(carServiceDaoImpl.getCarInfo(BeanConvertUtil.beanCovertMap(carDto)), CarDto.class);

        return cars;
    }


    @Override
    public int queryCarsCount(@RequestBody CarDto carDto) {
        return carServiceDaoImpl.queryCarsCount(BeanConvertUtil.beanCovertMap(carDto));
    }

    public ICarServiceDao getCarServiceDaoImpl() {
        return carServiceDaoImpl;
    }

    public void setCarServiceDaoImpl(ICarServiceDao carServiceDaoImpl) {
        this.carServiceDaoImpl = carServiceDaoImpl;
    }
}
