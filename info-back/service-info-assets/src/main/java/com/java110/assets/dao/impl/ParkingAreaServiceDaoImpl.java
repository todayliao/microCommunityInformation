package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IParkingAreaServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 停車場服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("parkingAreaServiceDaoImpl")
//@Transactional
public class ParkingAreaServiceDaoImpl extends BaseServiceDao implements IParkingAreaServiceDao {

    private static Logger logger = LoggerFactory.getLogger(ParkingAreaServiceDaoImpl.class);





    /**
     * 保存停車場信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveParkingAreaInfo(Map info) throws DAOException {
        logger.debug("保存停車場信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("parkingAreaServiceDaoImpl.saveParkingAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存停車場信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询停車場信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getParkingAreaInfo(Map info) throws DAOException {
        logger.debug("查询停車場信息 入参 info : {}",info);

        List<Map> businessParkingAreaInfos = sqlSessionTemplate.selectList("parkingAreaServiceDaoImpl.getParkingAreaInfo",info);

        return businessParkingAreaInfos;
    }


    /**
     * 修改停車場信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateParkingAreaInfo(Map info) throws DAOException {
        logger.debug("修改停車場信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("parkingAreaServiceDaoImpl.updateParkingAreaInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改停車場信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询停車場数量
     * @param info 停車場信息
     * @return 停車場数量
     */
    @Override
    public int queryParkingAreasCount(Map info) {
        logger.debug("查询停車場数据 入参 info : {}",info);

        List<Map> businessParkingAreaInfos = sqlSessionTemplate.selectList("parkingAreaServiceDaoImpl.queryParkingAreasCount", info);
        if (businessParkingAreaInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessParkingAreaInfos.get(0).get("count").toString());
    }


}
