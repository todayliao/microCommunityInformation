package com.java110.assets.bmo.govFloor.impl;

import com.java110.assets.bmo.govFloor.IGetGovFloorBMO;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govCommunity.GovCommunityDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.dto.govFloor.GovFloorDto;

import java.util.ArrayList;
import java.util.List;

@Service("getGovFloorBMOImpl")
public class GetGovFloorBMOImpl implements IGetGovFloorBMO {

    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;

    /**
     * @param govFloorDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovFloorDto govFloorDto) {


        int count = govFloorInnerServiceSMOImpl.queryGovFloorsCount(govFloorDto);

        List<GovFloorDto> govFloorDtos = null;
        if (count > 0) {
            govFloorDtos = govFloorInnerServiceSMOImpl.queryGovFloors(govFloorDto);
            reFreshHeaderImg(govFloorDtos);
        } else {
            govFloorDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govFloorDto.getRow()), count, govFloorDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    /**
     * @param govFloorDtos
     */
    private void reFreshHeaderImg(List<GovFloorDto> govFloorDtos) {
        for (GovFloorDto govFloorDto : govFloorDtos) {
            if (!StringUtil.isEmpty(govFloorDto.getFloorIcon())) {
                String imgId = govFloorDto.getFloorIcon();
                FileRelDto fileRelDto = new FileRelDto();
                fileRelDto.setFileRelId(imgId);
                fileRelDto.setRelType("90200");
                List<FileRelDto> relDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
                if (relDtos != null && relDtos.size() > 0) {
                    govFloorDto.setFloorIcon(relDtos.get(0).getFileName());
                }
            }

        }
    }

}
