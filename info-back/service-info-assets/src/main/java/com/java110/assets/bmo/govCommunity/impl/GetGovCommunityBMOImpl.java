package com.java110.assets.bmo.govCommunity.impl;


import com.java110.assets.bmo.govCommunity.IGetGovCommunityBMO;
import com.java110.dto.fileRel.FileRelDto;
import com.java110.dto.govActivities.GovActivitiesDto;
import com.java110.dto.govCommunityAttr.GovCommunityAttrDto;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.intf.assets.IFileRelInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityAttrInnerServiceSMO;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.java110.dto.govCommunity.GovCommunityDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityBMOImpl")
public class GetGovCommunityBMOImpl implements IGetGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private IFileRelInnerServiceSMO fileRelInnerServiceSMOImpl;
    @Autowired
    private IGovCommunityAttrInnerServiceSMO govCommunityAttrInnerServiceSMOImpl;

    /**
     * @param govCommunityDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityDto govCommunityDto) {


        int count = govCommunityInnerServiceSMOImpl.queryGovCommunitysCount(govCommunityDto);

        List<GovCommunityDto> govCommunityDtos = null;
        if (count > 0) {
            govCommunityDtos = govCommunityInnerServiceSMOImpl.queryGovCommunitys(govCommunityDto);
            reFreshHeaderImg(govCommunityDtos);
            reFreshCommunityAttr(govCommunityDtos);
        } else {
            govCommunityDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityDto.getRow()), count, govCommunityDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     * @param govCommunityDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovCommunityYm(GovCommunityDto govCommunityDto) {

        List<GovCommunityDto>  govCommunityDtos = govCommunityInnerServiceSMOImpl.getGovCommunityYm(govCommunityDto);

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) 20 / (double) govCommunityDto.getRow()), 20, govCommunityDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void reFreshCommunityAttr(List<GovCommunityDto> govCommunityDtos) {

        if (govCommunityDtos == null || govCommunityDtos.size() < 1) {
            return;
        }
        List<String> govCommunityIds = new ArrayList<>();
        for (GovCommunityDto govCommunityDto : govCommunityDtos) {
            govCommunityIds.add(govCommunityDto.getGovCommunityId());
        }

        GovCommunityAttrDto govCommunityAttrDto = new GovCommunityAttrDto();
        govCommunityAttrDto.setGovCommunityIds(govCommunityIds.toArray(new String[govCommunityIds.size()]));
        govCommunityAttrDto.setCaId(govCommunityDtos.get(0).getCaId());
        List<GovCommunityAttrDto> govCommunityAttrDtos
                = govCommunityAttrInnerServiceSMOImpl.queryGovCommunityAttrs(govCommunityAttrDto);

        List<GovCommunityAttrDto> tmpgovCommunityAttrDto = null;
        for (GovCommunityDto govCommunityDto : govCommunityDtos) {
            tmpgovCommunityAttrDto = new ArrayList<>();
            for (GovCommunityAttrDto govCommunityAttr : govCommunityAttrDtos) {
                if (govCommunityAttr.getGovCommunityId().equals(govCommunityDto.getGovCommunityId())) {
                    tmpgovCommunityAttrDto.add(govCommunityAttr);
                }
            }
            govCommunityDto.setGovCommunityAttrDtos(tmpgovCommunityAttrDto);
        }


    }

    /**
     * @param govCommunityDtos
     */
    private void reFreshHeaderImg(List<GovCommunityDto> govCommunityDtos) {
        for (GovCommunityDto govCommunityDto : govCommunityDtos) {
            if (!StringUtil.isEmpty(govCommunityDto.getCommunityIcon())) {
                String imgId = govCommunityDto.getCommunityIcon();
                FileRelDto fileRelDto = new FileRelDto();
                fileRelDto.setFileRelId(imgId);
                fileRelDto.setRelType("10000");
                List<FileRelDto> relDtos = fileRelInnerServiceSMOImpl.queryFileRels(fileRelDto);
                if (relDtos != null && relDtos.size() > 0) {
                    govCommunityDto.setCommunityIcon(relDtos.get(0).getFileName());
                }
            }
        }
    }

}
