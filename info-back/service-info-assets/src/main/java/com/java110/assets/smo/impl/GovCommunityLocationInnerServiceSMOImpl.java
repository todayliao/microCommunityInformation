package com.java110.assets.smo.impl;


import com.java110.assets.dao.IGovCommunityLocationServiceDao;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import com.java110.intf.assets.IGovCommunityLocationInnerServiceSMO;
import com.java110.po.govCommunityLocation.GovCommunityLocationPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 小区位置内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovCommunityLocationInnerServiceSMOImpl extends BaseServiceSMO implements IGovCommunityLocationInnerServiceSMO {

    @Autowired
    private IGovCommunityLocationServiceDao govCommunityLocationServiceDaoImpl;


    @Override
    public int saveGovCommunityLocation(@RequestBody GovCommunityLocationPo govCommunityLocationPo) {
        int saveFlag = 1;
        govCommunityLocationServiceDaoImpl.saveGovCommunityLocationInfo(BeanConvertUtil.beanCovertMap(govCommunityLocationPo));
        return saveFlag;
    }

     @Override
    public int updateGovCommunityLocation(@RequestBody  GovCommunityLocationPo govCommunityLocationPo) {
        int saveFlag = 1;
         govCommunityLocationServiceDaoImpl.updateGovCommunityLocationInfo(BeanConvertUtil.beanCovertMap(govCommunityLocationPo));
        return saveFlag;
    }

     @Override
    public int deleteGovCommunityLocation(@RequestBody  GovCommunityLocationPo govCommunityLocationPo) {
        int saveFlag = 1;
        govCommunityLocationPo.setStatusCd("1");
        govCommunityLocationServiceDaoImpl.updateGovCommunityLocationInfo(BeanConvertUtil.beanCovertMap(govCommunityLocationPo));
        return saveFlag;
    }

    @Override
    public List<GovCommunityLocationDto> queryGovCommunityLocations(@RequestBody  GovCommunityLocationDto govCommunityLocationDto) {

        //校验是否传了 分页信息

        int page = govCommunityLocationDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govCommunityLocationDto.setPage((page - 1) * govCommunityLocationDto.getRow());
        }

        List<GovCommunityLocationDto> govCommunityLocations = BeanConvertUtil.covertBeanList(govCommunityLocationServiceDaoImpl.getGovCommunityLocationInfo(BeanConvertUtil.beanCovertMap(govCommunityLocationDto)), GovCommunityLocationDto.class);

        return govCommunityLocations;
    }


    @Override
    public int queryGovCommunityLocationsCount(@RequestBody GovCommunityLocationDto govCommunityLocationDto) {
        return govCommunityLocationServiceDaoImpl.queryGovCommunityLocationsCount(BeanConvertUtil.beanCovertMap(govCommunityLocationDto));    }

    public IGovCommunityLocationServiceDao getGovCommunityLocationServiceDaoImpl() {
        return govCommunityLocationServiceDaoImpl;
    }

    public void setGovCommunityLocationServiceDaoImpl(IGovCommunityLocationServiceDao govCommunityLocationServiceDaoImpl) {
        this.govCommunityLocationServiceDaoImpl = govCommunityLocationServiceDaoImpl;
    }
}
