package com.java110.assets.bmo.govFloor.impl;


import com.java110.assets.bmo.govFloor.IDeleteGovFloorBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.IGovFloorInnerServiceSMO;
import com.java110.po.govFloor.GovFloorPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovFloorBMOImpl")
public class DeleteGovFloorBMOImpl implements IDeleteGovFloorBMO {

    @Autowired
    private IGovFloorInnerServiceSMO govFloorInnerServiceSMOImpl;

    /**
     * @param govFloorPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovFloorPo govFloorPo) {

        int flag = govFloorInnerServiceSMOImpl.deleteGovFloor(govFloorPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
