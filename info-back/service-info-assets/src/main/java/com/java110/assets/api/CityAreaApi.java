package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.cityArea.CityAreaDto;
import com.java110.po.cityArea.CityAreaPo;
import com.java110.assets.bmo.cityArea.IDeleteCityAreaBMO;
import com.java110.assets.bmo.cityArea.IGetCityAreaBMO;
import com.java110.assets.bmo.cityArea.ISaveCityAreaBMO;
import com.java110.assets.bmo.cityArea.IUpdateCityAreaBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/cityArea")
public class CityAreaApi {

    @Autowired
    private ISaveCityAreaBMO saveCityAreaBMOImpl;
    @Autowired
    private IUpdateCityAreaBMO updateCityAreaBMOImpl;
    @Autowired
    private IDeleteCityAreaBMO deleteCityAreaBMOImpl;

    @Autowired
    private IGetCityAreaBMO getCityAreaBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /cityArea/saveCityArea
     * @path /app/cityArea/saveCityArea
     */
    @RequestMapping(value = "/saveCityArea", method = RequestMethod.POST)
    public ResponseEntity<String> saveCityArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "id", "请求报文中未包含id");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "areaName", "请求报文中未包含areaName");
        Assert.hasKeyAndValue(reqJson, "areaLevel", "请求报文中未包含areaLevel");
        Assert.hasKeyAndValue(reqJson, "parentAreaCode", "请求报文中未包含parentAreaCode");
        Assert.hasKeyAndValue(reqJson, "parentAreaName", "请求报文中未包含parentAreaName");
        Assert.hasKeyAndValue(reqJson, "lon", "请求报文中未包含lon");
        Assert.hasKeyAndValue(reqJson, "lat", "请求报文中未包含lat");


        CityAreaPo cityAreaPo = BeanConvertUtil.covertBean(reqJson, CityAreaPo.class);
        return saveCityAreaBMOImpl.save(cityAreaPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /cityArea/updateCityArea
     * @path /app/cityArea/updateCityArea
     */
    @RequestMapping(value = "/updateCityArea", method = RequestMethod.POST)
    public ResponseEntity<String> updateCityArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "id", "请求报文中未包含id");
        Assert.hasKeyAndValue(reqJson, "areaCode", "请求报文中未包含areaCode");
        Assert.hasKeyAndValue(reqJson, "areaName", "请求报文中未包含areaName");
        Assert.hasKeyAndValue(reqJson, "areaLevel", "请求报文中未包含areaLevel");
        Assert.hasKeyAndValue(reqJson, "parentAreaCode", "请求报文中未包含parentAreaCode");
        Assert.hasKeyAndValue(reqJson, "parentAreaName", "请求报文中未包含parentAreaName");
        Assert.hasKeyAndValue(reqJson, "lon", "请求报文中未包含lon");
        Assert.hasKeyAndValue(reqJson, "lat", "请求报文中未包含lat");
        Assert.hasKeyAndValue(reqJson, "cityAreaId", "cityAreaId不能为空");


        CityAreaPo cityAreaPo = BeanConvertUtil.covertBean(reqJson, CityAreaPo.class);
        return updateCityAreaBMOImpl.update(cityAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /cityArea/deleteCityArea
     * @path /app/cityArea/deleteCityArea
     */
    @RequestMapping(value = "/deleteCityArea", method = RequestMethod.POST)
    public ResponseEntity<String> deleteCityArea(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "cityAreaId", "cityAreaId不能为空");


        CityAreaPo cityAreaPo = BeanConvertUtil.covertBean(reqJson, CityAreaPo.class);
        return deleteCityAreaBMOImpl.delete(cityAreaPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param areaCode 小区ID
     * @return
     * @serviceCode /cityArea/queryCityArea
     * @path /app/cityArea/queryCityArea
     */
    @RequestMapping(value = "/queryCityArea", method = RequestMethod.GET)
    public ResponseEntity<String> queryCityArea(@RequestParam(value = "areaCode", required = false) String areaCode,
                                                @RequestParam(value = "areaName", required = false) String areaName,
                                                @RequestParam(value = "page") int page,
                                                @RequestParam(value = "row") int row) {
        CityAreaDto cityAreaDto = new CityAreaDto();
        cityAreaDto.setPage(page);
        cityAreaDto.setRow(row);
        cityAreaDto.setAreaCode(areaCode);
        cityAreaDto.setAreaName(areaName);
        return getCityAreaBMOImpl.get(cityAreaDto);
    }

    /**
     * 获取
     *
     * @param areaCode 小区ID
     * @return
     * @serviceCode /cityArea/queryCityArea
     * @path /app/cityArea/queryCityArea
     */
    @RequestMapping(value = "/getAreas", method = RequestMethod.GET)
    public ResponseEntity<String> getAreas(@RequestParam(value = "areaCode", required = false) String areaCode,
                                           @RequestParam(value = "areaLevel", required = false) String areaLevel,
                                           @RequestParam(value = "parentAreaCode", required = false) String parentAreaCode,
                                           @RequestParam(value = "areaName", required = false) String areaName) {
        CityAreaDto cityAreaDto = new CityAreaDto();
        cityAreaDto.setAreaCode(areaCode);
        cityAreaDto.setAreaName(areaName);
        cityAreaDto.setAreaLevel(areaLevel);
        cityAreaDto.setParentAreaCode(parentAreaCode);
        return getCityAreaBMOImpl.getAreas(cityAreaDto);
    }
}
