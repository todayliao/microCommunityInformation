package com.java110.assets.bmo.govCommunity;
import com.java110.po.govCommunity.GovCommunityPo;
import org.springframework.http.ResponseEntity;

public interface IDeleteGovCommunityBMO {


    /**
     * 修改小区信息
     * add by wuxw
     * @param govCommunityPo
     * @return
     */
    ResponseEntity<String> delete(GovCommunityPo govCommunityPo);


}
