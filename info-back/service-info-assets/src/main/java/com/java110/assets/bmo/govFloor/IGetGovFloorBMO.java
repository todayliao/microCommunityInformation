package com.java110.assets.bmo.govFloor;
import com.java110.dto.govFloor.GovFloorDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovFloorBMO {


    /**
     * 查询建筑物管理
     * add by wuxw
     * @param  govFloorDto
     * @return
     */
    ResponseEntity<String> get(GovFloorDto govFloorDto);


}
