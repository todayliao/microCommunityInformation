package com.java110.assets.bmo.cityArea.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.cityArea.ISaveCityAreaBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.cityArea.CityAreaPo;
import com.java110.intf.assets.ICityAreaInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveCityAreaBMOImpl")
public class SaveCityAreaBMOImpl implements ISaveCityAreaBMO {

    @Autowired
    private ICityAreaInnerServiceSMO cityAreaInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param cityAreaPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(CityAreaPo cityAreaPo) {

        cityAreaPo.setId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_cityAreaId));
        int flag = cityAreaInnerServiceSMOImpl.saveCityArea(cityAreaPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
