package com.java110.assets.bmo.govCommunityLocation.impl;

import com.java110.assets.bmo.govCommunityLocation.IGetGovCommunityLocationBMO;
import com.java110.dto.govCommunityLocation.GovCommunityLocationDto;
import com.java110.intf.assets.IGovCommunityLocationInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovCommunityLocationBMOImpl")
public class GetGovCommunityLocationBMOImpl implements IGetGovCommunityLocationBMO {

    @Autowired
    private IGovCommunityLocationInnerServiceSMO govCommunityLocationInnerServiceSMOImpl;

    /**
     * @param govCommunityLocationDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovCommunityLocationDto govCommunityLocationDto) {


        int count = govCommunityLocationInnerServiceSMOImpl.queryGovCommunityLocationsCount(govCommunityLocationDto);

        List<GovCommunityLocationDto> govCommunityLocationDtos = null;
        if (count > 0) {
            govCommunityLocationDtos = govCommunityLocationInnerServiceSMOImpl.queryGovCommunityLocations(govCommunityLocationDto);
        } else {
            govCommunityLocationDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govCommunityLocationDto.getRow()), count, govCommunityLocationDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
