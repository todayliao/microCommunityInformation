package com.java110.assets.bmo.govCommunityCompany;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import org.springframework.http.ResponseEntity;

public interface IUpdateGovCommunityCompanyBMO {


    /**
     * 修改小区位置
     * add by wuxw
     * @param govCommunityCompanyPo
     * @return
     */
    ResponseEntity<String> update(GovCommunityCompanyPo govCommunityCompanyPo);


}
