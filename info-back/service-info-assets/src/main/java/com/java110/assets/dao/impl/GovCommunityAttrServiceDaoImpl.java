package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IGovCommunityAttrServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 小区属性服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govCommunityAttrServiceDaoImpl")
//@Transactional
public class GovCommunityAttrServiceDaoImpl extends BaseServiceDao implements IGovCommunityAttrServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovCommunityAttrServiceDaoImpl.class);





    /**
     * 保存小区属性信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovCommunityAttrInfo(Map info) throws DAOException {
        logger.debug("保存小区属性信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govCommunityAttrServiceDaoImpl.saveGovCommunityAttrInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存小区属性信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询小区属性信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovCommunityAttrInfo(Map info) throws DAOException {
        logger.debug("查询小区属性信息 入参 info : {}",info);

        List<Map> businessGovCommunityAttrInfos = sqlSessionTemplate.selectList("govCommunityAttrServiceDaoImpl.getGovCommunityAttrInfo",info);

        return businessGovCommunityAttrInfos;
    }


    /**
     * 修改小区属性信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovCommunityAttrInfo(Map info) throws DAOException {
        logger.debug("修改小区属性信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govCommunityAttrServiceDaoImpl.updateGovCommunityAttrInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改小区属性信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询小区属性数量
     * @param info 小区属性信息
     * @return 小区属性数量
     */
    @Override
    public int queryGovCommunityAttrsCount(Map info) {
        logger.debug("查询小区属性数据 入参 info : {}",info);

        List<Map> businessGovCommunityAttrInfos = sqlSessionTemplate.selectList("govCommunityAttrServiceDaoImpl.queryGovCommunityAttrsCount", info);
        if (businessGovCommunityAttrInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovCommunityAttrInfos.get(0).get("count").toString());
    }


}
