package com.java110.assets.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.car.IDeleteCarBMO;
import com.java110.assets.bmo.car.IGetCarBMO;
import com.java110.assets.bmo.car.ISaveCarBMO;
import com.java110.assets.bmo.car.IUpdateCarBMO;
import com.java110.dto.car.CarDto;
import com.java110.po.car.CarPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(value = "/car")
public class CarApi {

    @Autowired
    private ISaveCarBMO saveCarBMOImpl;
    @Autowired
    private IUpdateCarBMO updateCarBMOImpl;
    @Autowired
    private IDeleteCarBMO deleteCarBMOImpl;

    @Autowired
    private IGetCarBMO getCarBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /car/saveCar
     * @path /app/car/saveCar
     */
    @RequestMapping(value = "/saveCar", method = RequestMethod.POST)
    public ResponseEntity<String> saveCar(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "paId", "请求报文中未包含paId");
        Assert.hasKeyAndValue(reqJson, "carNum", "请求报文中未包含carNum");
        Assert.hasKeyAndValue(reqJson, "carBrand", "请求报文中未包含carBrand");
        Assert.hasKeyAndValue(reqJson, "carType", "请求报文中未包含carType");
        Assert.hasKeyAndValue(reqJson, "carColor", "请求报文中未包含carColor");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govCommunityId", "请求报文中未包含govCommunityId");
        Assert.hasKeyAndValue(reqJson, "startTime", "请求报文中未包含startTime");
        Assert.hasKeyAndValue(reqJson, "endTime", "请求报文中未包含endTime");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "carTypeCd", "请求报文中未包含carTypeCd");
        Assert.hasKeyAndValue(reqJson, "datasourceType", "请求报文中未包含datasourceType");


        CarPo carPo = BeanConvertUtil.covertBean(reqJson, CarPo.class);

        return saveCarBMOImpl.save(carPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /car/updateCar
     * @path /app/car/updateCar
     */
    @RequestMapping(value = "/updateCar", method = RequestMethod.POST)
    public ResponseEntity<String> updateCar(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "carId", "carId不能为空");


        CarPo carPo = BeanConvertUtil.covertBean(reqJson, CarPo.class);
        return updateCarBMOImpl.update(carPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /car/deleteCar
     * @path /app/car/deleteCar
     */
    @RequestMapping(value = "/deleteCar", method = RequestMethod.POST)
    public ResponseEntity<String> deleteCar(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "区域ID不能为空");

        Assert.hasKeyAndValue(reqJson, "carId", "carId不能为空");


        CarPo carPo = BeanConvertUtil.covertBean(reqJson, CarPo.class);
        return deleteCarBMOImpl.delete(carPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /car/queryCar
     * @path /app/car/queryCar
     */
    @RequestMapping(value = "/queryCar", method = RequestMethod.GET)
    public ResponseEntity<String> queryCar(@RequestParam(value = "caId") String caId,
                                           @RequestParam(value = "govPersonId",required = false) String govPersonId,
                                           @RequestParam(value = "page") int page,
                                           @RequestParam(value = "row") int row) {
        CarDto carDto = new CarDto();
        carDto.setPage(page);
        carDto.setRow(row);
        carDto.setCaId(caId);
        carDto.setGovPersonId( govPersonId );
        return getCarBMOImpl.get(carDto);
    }
}
