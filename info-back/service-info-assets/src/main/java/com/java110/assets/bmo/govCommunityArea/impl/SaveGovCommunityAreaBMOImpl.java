package com.java110.assets.bmo.govCommunityArea.impl;

import com.java110.assets.bmo.govCommunityArea.ISaveGovCommunityAreaBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;

import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("saveGovCommunityAreaBMOImpl")
public class SaveGovCommunityAreaBMOImpl implements ISaveGovCommunityAreaBMO {

    @Autowired
    private IGovCommunityAreaInnerServiceSMO govCommunityAreaInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govCommunityAreaPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCommunityAreaPo govCommunityAreaPo) {

        govCommunityAreaPo.setCaId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_caId));
        int flag = govCommunityAreaInnerServiceSMOImpl.saveGovCommunityArea(govCommunityAreaPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
