package com.java110.assets.bmo.govCommunity.impl;


import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.govCommunity.IDeleteGovCommunityBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.intf.assets.IGovCommunityInnerServiceSMO;
import com.java110.po.govCommunity.GovCommunityPo;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("deleteGovCommunityBMOImpl")
public class DeleteGovCommunityBMOImpl implements IDeleteGovCommunityBMO {

    @Autowired
    private IGovCommunityInnerServiceSMO govCommunityInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param govCommunityPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCommunityPo govCommunityPo) {

        int flag = govCommunityInnerServiceSMOImpl.deleteGovCommunity(govCommunityPo);

        if (flag > 0) {
            JSONObject reqJson = new JSONObject();
            reqJson.put("govCommunityId",govCommunityPo.getGovCommunityId());
            ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.DELETE_COMMUNITY_MALL, HttpMethod.POST);
            if (responseEntity != null) {
                if (responseEntity.getStatusCode() != HttpStatus.OK) {
                    throw new IllegalArgumentException(responseEntity.getBody());
                }
            }


            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
