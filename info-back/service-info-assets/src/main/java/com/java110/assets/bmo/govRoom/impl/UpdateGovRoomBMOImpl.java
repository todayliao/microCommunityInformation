package com.java110.assets.bmo.govRoom.impl;

import com.java110.assets.bmo.govRoom.IUpdateGovRoomBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.intf.assets.IGovRoomInnerServiceSMO;
import com.java110.po.govRoom.GovRoomPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("updateGovRoomBMOImpl")
public class UpdateGovRoomBMOImpl implements IUpdateGovRoomBMO {

    @Autowired
    private IGovRoomInnerServiceSMO govRoomInnerServiceSMOImpl;

    /**
     *
     *
     * @param govRoomPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovRoomPo govRoomPo) {

        int flag = govRoomInnerServiceSMOImpl.updateGovRoom(govRoomPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
