package com.java110.assets.bmo.govCommunityCompany.impl;

import com.java110.assets.bmo.govCommunityCompany.IDeleteGovCommunityCompanyBMO;
import com.java110.core.annotation.Java110Transactional;

import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("deleteGovCommunityCompanyBMOImpl")
public class DeleteGovCommunityCompanyBMOImpl implements IDeleteGovCommunityCompanyBMO {

    @Autowired
    private IGovCommunityCompanyInnerServiceSMO govCommunityCompanyInnerServiceSMOImpl;

    /**
     * @param govCommunityCompanyPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCommunityCompanyPo govCommunityCompanyPo) {

        int flag = govCommunityCompanyInnerServiceSMOImpl.deleteGovCommunityCompany(govCommunityCompanyPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
