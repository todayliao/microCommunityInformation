package com.java110.assets.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.assets.dao.IMachineServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 设备管理服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("machineServiceDaoImpl")
//@Transactional
public class MachineServiceDaoImpl extends BaseServiceDao implements IMachineServiceDao {

    private static Logger logger = LoggerFactory.getLogger(MachineServiceDaoImpl.class);





    /**
     * 保存设备管理信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveMachineInfo(Map info) throws DAOException {
        logger.debug("保存设备管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("machineServiceDaoImpl.saveMachineInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存设备管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询设备管理信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getMachineInfo(Map info) throws DAOException {
        logger.debug("查询设备管理信息 入参 info : {}",info);

        List<Map> businessMachineInfos = sqlSessionTemplate.selectList("machineServiceDaoImpl.getMachineInfo",info);

        return businessMachineInfos;
    }


    /**
     * 修改设备管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateMachineInfo(Map info) throws DAOException {
        logger.debug("修改设备管理信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("machineServiceDaoImpl.updateMachineInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改设备管理信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询设备管理数量
     * @param info 设备管理信息
     * @return 设备管理数量
     */
    @Override
    public int queryMachinesCount(Map info) {
        logger.debug("查询设备管理数据 入参 info : {}",info);

        List<Map> businessMachineInfos = sqlSessionTemplate.selectList("machineServiceDaoImpl.queryMachinesCount", info);
        if (businessMachineInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessMachineInfos.get(0).get("count").toString());
    }


}
