package com.java110.assets.bmo.machineType;

import org.springframework.http.ResponseEntity;
import com.java110.po.machineType.MachineTypePo;
public interface ISaveMachineTypeBMO {


    /**
     * 添加设备类型
     * add by wuxw
     * @param machineTypePo
     * @return
     */
    ResponseEntity<String> save(MachineTypePo machineTypePo);


}
