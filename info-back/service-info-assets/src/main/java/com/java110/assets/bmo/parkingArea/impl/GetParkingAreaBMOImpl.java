package com.java110.assets.bmo.parkingArea.impl;

import com.java110.assets.bmo.parkingArea.IGetParkingAreaBMO;
import com.java110.dto.parkingArea.ParkingAreaDto;
import com.java110.intf.assets.IParkingAreaInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getParkingAreaBMOImpl")
public class GetParkingAreaBMOImpl implements IGetParkingAreaBMO {

    @Autowired
    private IParkingAreaInnerServiceSMO parkingAreaInnerServiceSMOImpl;

    /**
     * @param parkingAreaDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(ParkingAreaDto parkingAreaDto) {


        int count = parkingAreaInnerServiceSMOImpl.queryParkingAreasCount(parkingAreaDto);

        List<ParkingAreaDto> parkingAreaDtos = null;
        if (count > 0) {
            parkingAreaDtos = parkingAreaInnerServiceSMOImpl.queryParkingAreas(parkingAreaDto);
        } else {
            parkingAreaDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) parkingAreaDto.getRow()), count, parkingAreaDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
