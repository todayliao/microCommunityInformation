package com.java110.assets.bmo.govFloor;

import com.java110.po.govFloor.GovFloorPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovFloorBMO {


    /**
     * 添加建筑物管理
     * add by wuxw
     * @param govFloorPo
     * @return
     */
    ResponseEntity<String> save(GovFloorPo govFloorPo);


}
