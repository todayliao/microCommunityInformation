package com.java110.assets.bmo.car.impl;

import com.java110.assets.bmo.car.IDeleteCarBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.intf.assets.ICarInnerServiceSMO;
import com.java110.po.car.CarPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteCarBMOImpl")
public class DeleteCarBMOImpl implements IDeleteCarBMO {

    @Autowired
    private ICarInnerServiceSMO carInnerServiceSMOImpl;

    /**
     * @param carPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(CarPo carPo) {

        int flag = carInnerServiceSMOImpl.deleteCar(carPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
