package com.java110.assets.bmo.machine.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.assets.bmo.machine.IDeleteMachineBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import com.java110.po.machine.MachinePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.assets.IMachineInnerServiceSMO;
import com.java110.dto.machine.MachineDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteMachineBMOImpl")
public class DeleteMachineBMOImpl implements IDeleteMachineBMO {

    @Autowired
    private IMachineInnerServiceSMO machineInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(JSONObject reqJson) {

        MachinePo machinePo = BeanConvertUtil.covertBean(reqJson, MachinePo.class);
        int flag = machineInnerServiceSMOImpl.deleteMachine( machinePo );

        if (flag > 0) {
//            reqJson.put( "extMachineId", machinePo.getMachineId() );
//            ResponseEntity<String> responseEntity = RestTemplateFactory.restOutTemplate( reqJson, outRestTemplate, MallConstant.DELETE_MACHINE_MALL, HttpMethod.POST );
//            if (responseEntity != null) {
//                if (responseEntity.getStatusCode() != HttpStatus.OK) {
//                    throw new IllegalArgumentException( responseEntity.getBody() );
//                }
//            }
            return ResultVo.createResponseEntity( ResultVo.CODE_OK, "保存成功" );
        }

        return ResultVo.createResponseEntity( ResultVo.CODE_ERROR, "保存失败" );
    }

}
