package com.java110.assets.smo.impl;


import com.java110.assets.dao.ICityAreaServiceDao;
import com.java110.intf.assets.ICityAreaInnerServiceSMO;
import com.java110.dto.cityArea.CityAreaDto;
import com.java110.po.cityArea.CityAreaPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 选择省市区内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class CityAreaInnerServiceSMOImpl extends BaseServiceSMO implements ICityAreaInnerServiceSMO {

    @Autowired
    private ICityAreaServiceDao cityAreaServiceDaoImpl;


    @Override
    public int saveCityArea(@RequestBody  CityAreaPo cityAreaPo) {
        int saveFlag = 1;
        cityAreaServiceDaoImpl.saveCityAreaInfo(BeanConvertUtil.beanCovertMap(cityAreaPo));
        return saveFlag;
    }

     @Override
    public int updateCityArea(@RequestBody  CityAreaPo cityAreaPo) {
        int saveFlag = 1;
         cityAreaServiceDaoImpl.updateCityAreaInfo(BeanConvertUtil.beanCovertMap(cityAreaPo));
        return saveFlag;
    }

     @Override
    public int deleteCityArea(@RequestBody  CityAreaPo cityAreaPo) {
        int saveFlag = 1;
        cityAreaPo.setStatusCd("1");
        cityAreaServiceDaoImpl.updateCityAreaInfo(BeanConvertUtil.beanCovertMap(cityAreaPo));
        return saveFlag;
    }

    @Override
    public List<CityAreaDto> queryCityAreas(@RequestBody  CityAreaDto cityAreaDto) {

        //校验是否传了 分页信息

        int page = cityAreaDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            cityAreaDto.setPage((page - 1) * cityAreaDto.getRow());
        }

        List<CityAreaDto> cityAreas = BeanConvertUtil.covertBeanList(cityAreaServiceDaoImpl.getCityAreaInfo(BeanConvertUtil.beanCovertMap(cityAreaDto)), CityAreaDto.class);

        return cityAreas;
    }

    @Override
    public List<CityAreaDto> getAreas(@RequestBody  CityAreaDto cityAreaDto) {

        //校验是否传了 分页信息

        int page = cityAreaDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            cityAreaDto.setPage((page - 1) * cityAreaDto.getRow());
        }

        List<CityAreaDto> cityAreas = BeanConvertUtil.covertBeanList(cityAreaServiceDaoImpl.getAreas(BeanConvertUtil.beanCovertMap(cityAreaDto)), CityAreaDto.class);

        return cityAreas;
    }


    @Override
    public int queryCityAreasCount(@RequestBody CityAreaDto cityAreaDto) {
        return cityAreaServiceDaoImpl.queryCityAreasCount(BeanConvertUtil.beanCovertMap(cityAreaDto));    }

    public ICityAreaServiceDao getCityAreaServiceDaoImpl() {
        return cityAreaServiceDaoImpl;
    }

    public void setCityAreaServiceDaoImpl(ICityAreaServiceDao cityAreaServiceDaoImpl) {
        this.cityAreaServiceDaoImpl = cityAreaServiceDaoImpl;
    }
}
