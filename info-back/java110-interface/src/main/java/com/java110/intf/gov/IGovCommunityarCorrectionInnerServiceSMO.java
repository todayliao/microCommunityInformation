package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.po.govCommunityarCorrection.GovCommunityarCorrectionPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityarCorrectionInnerServiceSMO
 * @Description 矫正者接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityarCorrectionApi")
public interface IGovCommunityarCorrectionInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunityarCorrection", method = RequestMethod.POST)
    public int saveGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo);

    @RequestMapping(value = "/updateGovCommunityarCorrection", method = RequestMethod.POST)
    public int updateGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo);

    @RequestMapping(value = "/deleteGovCommunityarCorrection", method = RequestMethod.POST)
    public int deleteGovCommunityarCorrection(@RequestBody  GovCommunityarCorrectionPo govCommunityarCorrectionPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityarCorrectionDto 数据对象分享
     * @return GovCommunityarCorrectionDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunityarCorrections", method = RequestMethod.POST)
    List<GovCommunityarCorrectionDto> queryGovCommunityarCorrections(@RequestBody GovCommunityarCorrectionDto govCommunityarCorrectionDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityarCorrectionDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunityarCorrectionsCount", method = RequestMethod.POST)
    int queryGovCommunityarCorrectionsCount(@RequestBody GovCommunityarCorrectionDto govCommunityarCorrectionDto);
}
