package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMedicalClassify.GovMedicalClassifyDto;
import com.java110.po.govMedicalClassify.GovMedicalClassifyPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMedicalClassifyInnerServiceSMO
 * @Description 医疗分类接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govMedicalClassifyApi")
public interface IGovMedicalClassifyInnerServiceSMO {


    @RequestMapping(value = "/saveGovMedicalClassify", method = RequestMethod.POST)
    public int saveGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo);

    @RequestMapping(value = "/updateGovMedicalClassify", method = RequestMethod.POST)
    public int updateGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo);

    @RequestMapping(value = "/deleteGovMedicalClassify", method = RequestMethod.POST)
    public int deleteGovMedicalClassify(@RequestBody  GovMedicalClassifyPo govMedicalClassifyPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMedicalClassifyDto 数据对象分享
     * @return GovMedicalClassifyDto 对象数据
     */
    @RequestMapping(value = "/queryGovMedicalClassifys", method = RequestMethod.POST)
    List<GovMedicalClassifyDto> queryGovMedicalClassifys(@RequestBody GovMedicalClassifyDto govMedicalClassifyDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMedicalClassifyDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMedicalClassifysCount", method = RequestMethod.POST)
    int queryGovMedicalClassifysCount(@RequestBody GovMedicalClassifyDto govMedicalClassifyDto);
}
