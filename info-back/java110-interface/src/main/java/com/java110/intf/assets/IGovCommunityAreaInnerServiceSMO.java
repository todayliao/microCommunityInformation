package com.java110.intf.assets;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;
import com.java110.po.govCommunityArea.GovCommunityAreaPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCommunityAreaInnerServiceSMO
 * @Description 区域管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-assets", configuration = {FeignConfiguration.class})
@RequestMapping("/govCommunityAreaApi")
public interface IGovCommunityAreaInnerServiceSMO {


    @RequestMapping(value = "/saveGovCommunityArea", method = RequestMethod.POST)
    public int saveGovCommunityArea(@RequestBody GovCommunityAreaPo govCommunityAreaPo);

    @RequestMapping(value = "/updateGovCommunityArea", method = RequestMethod.POST)
    public int updateGovCommunityArea(@RequestBody  GovCommunityAreaPo govCommunityAreaPo);

    @RequestMapping(value = "/deleteGovCommunityArea", method = RequestMethod.POST)
    public int deleteGovCommunityArea(@RequestBody  GovCommunityAreaPo govCommunityAreaPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCommunityAreaDto 数据对象分享
     * @return GovCommunityAreaDto 对象数据
     */
    @RequestMapping(value = "/queryGovCommunityAreas", method = RequestMethod.POST)
    List<GovCommunityAreaDto> queryGovCommunityAreas(@RequestBody GovCommunityAreaDto govCommunityAreaDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCommunityAreaDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCommunityAreasCount", method = RequestMethod.POST)
    int queryGovCommunityAreasCount(@RequestBody GovCommunityAreaDto govCommunityAreaDto);
}
