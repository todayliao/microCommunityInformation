package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCiviladmin.GovCiviladminDto;
import com.java110.po.govCiviladmin.GovCiviladminPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCiviladminInnerServiceSMO
 * @Description 民政服务宣传接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govCiviladminApi")
public interface IGovCiviladminInnerServiceSMO {


    @RequestMapping(value = "/saveGovCiviladmin", method = RequestMethod.POST)
    public int saveGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo);

    @RequestMapping(value = "/updateGovCiviladmin", method = RequestMethod.POST)
    public int updateGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo);

    @RequestMapping(value = "/deleteGovCiviladmin", method = RequestMethod.POST)
    public int deleteGovCiviladmin(@RequestBody  GovCiviladminPo govCiviladminPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCiviladminDto 数据对象分享
     * @return GovCiviladminDto 对象数据
     */
    @RequestMapping(value = "/queryGovCiviladmins", method = RequestMethod.POST)
    List<GovCiviladminDto> queryGovCiviladmins(@RequestBody GovCiviladminDto govCiviladminDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCiviladminDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCiviladminsCount", method = RequestMethod.POST)
    int queryGovCiviladminsCount(@RequestBody GovCiviladminDto govCiviladminDto);
}
