package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govReportTypeUser.GovReportTypeUserDto;
import com.java110.po.govReportTypeUser.GovReportTypeUserPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovReportTypeUserInnerServiceSMO
 * @Description 报事类型人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "gov-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govReportTypeUserApi")
public interface IGovReportTypeUserInnerServiceSMO {


    @RequestMapping(value = "/saveGovReportTypeUser", method = RequestMethod.POST)
    public int saveGovReportTypeUser(@RequestBody GovReportTypeUserPo govReportTypeUserPo);

    @RequestMapping(value = "/updateGovReportTypeUser", method = RequestMethod.POST)
    public int updateGovReportTypeUser(@RequestBody  GovReportTypeUserPo govReportTypeUserPo);

    @RequestMapping(value = "/deleteGovReportTypeUser", method = RequestMethod.POST)
    public int deleteGovReportTypeUser(@RequestBody  GovReportTypeUserPo govReportTypeUserPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govReportTypeUserDto 数据对象分享
     * @return GovReportTypeUserDto 对象数据
     */
    @RequestMapping(value = "/queryGovReportTypeUsers", method = RequestMethod.POST)
    List<GovReportTypeUserDto> queryGovReportTypeUsers(@RequestBody GovReportTypeUserDto govReportTypeUserDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govReportTypeUserDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovReportTypeUsersCount", method = RequestMethod.POST)
    int queryGovReportTypeUsersCount(@RequestBody GovReportTypeUserDto govReportTypeUserDto);
}
