package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCase.GovCaseDto;
import com.java110.po.govCase.GovCasePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCaseInnerServiceSMO
 * @Description 命案基本信息接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govCaseApi")
public interface IGovCaseInnerServiceSMO {


    @RequestMapping(value = "/saveGovCase", method = RequestMethod.POST)
    public int saveGovCase(@RequestBody  GovCasePo govCasePo);

    @RequestMapping(value = "/updateGovCase", method = RequestMethod.POST)
    public int updateGovCase(@RequestBody  GovCasePo govCasePo);

    @RequestMapping(value = "/deleteGovCase", method = RequestMethod.POST)
    public int deleteGovCase(@RequestBody  GovCasePo govCasePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCaseDto 数据对象分享
     * @return GovCaseDto 对象数据
     */
    @RequestMapping(value = "/queryGovCases", method = RequestMethod.POST)
    List<GovCaseDto> queryGovCases(@RequestBody GovCaseDto govCaseDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCaseDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCasesCount", method = RequestMethod.POST)
    int queryGovCasesCount(@RequestBody GovCaseDto govCaseDto);
}
