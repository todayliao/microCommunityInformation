package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCompanyPersonInnerServiceSMO
 * @Description 小区位置接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govCompanyPersonApi")
public interface IGovCompanyPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovCompanyPerson", method = RequestMethod.POST)
    public int saveGovCompanyPerson(@RequestBody GovCompanyPersonPo govCompanyPersonPo);

    @RequestMapping(value = "/updateGovCompanyPerson", method = RequestMethod.POST)
    public int updateGovCompanyPerson(@RequestBody  GovCompanyPersonPo govCompanyPersonPo);

    @RequestMapping(value = "/deleteGovCompanyPerson", method = RequestMethod.POST)
    public int deleteGovCompanyPerson(@RequestBody  GovCompanyPersonPo govCompanyPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCompanyPersonDto 数据对象分享
     * @return GovCompanyPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovCompanyPersons", method = RequestMethod.POST)
    List<GovCompanyPersonDto> queryGovCompanyPersons(@RequestBody GovCompanyPersonDto govCompanyPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCompanyPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCompanyPersonsCount", method = RequestMethod.POST)
    int queryGovCompanyPersonsCount(@RequestBody GovCompanyPersonDto govCompanyPersonDto);
}
