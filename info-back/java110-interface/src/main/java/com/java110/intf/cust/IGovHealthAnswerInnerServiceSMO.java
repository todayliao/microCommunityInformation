package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovHealthAnswerInnerServiceSMO
 * @Description 体检单提交者接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govHealthAnswerApi")
public interface IGovHealthAnswerInnerServiceSMO {


    @RequestMapping(value = "/saveGovHealthAnswer", method = RequestMethod.POST)
    public int saveGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo);

    @RequestMapping(value = "/updateGovHealthAnswer", method = RequestMethod.POST)
    public int updateGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo);

    @RequestMapping(value = "/deleteGovHealthAnswer", method = RequestMethod.POST)
    public int deleteGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govHealthAnswerDto 数据对象分享
     * @return GovHealthAnswerDto 对象数据
     */
    @RequestMapping(value = "/queryGovHealthAnswers", method = RequestMethod.POST)
    List<GovHealthAnswerDto> queryGovHealthAnswers(@RequestBody GovHealthAnswerDto govHealthAnswerDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govHealthAnswerDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovHealthAnswersCount", method = RequestMethod.POST)
    int queryGovHealthAnswersCount(@RequestBody GovHealthAnswerDto govHealthAnswerDto);
}
