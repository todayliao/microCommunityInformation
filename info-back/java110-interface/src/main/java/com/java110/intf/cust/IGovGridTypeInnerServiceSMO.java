package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govGridType.GovGridTypeDto;
import com.java110.po.govGridType.GovGridTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovGridTypeInnerServiceSMO
 * @Description 网格类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govGridTypeApi")
public interface IGovGridTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovGridType", method = RequestMethod.POST)
    public int saveGovGridType(@RequestBody GovGridTypePo govGridTypePo);

    @RequestMapping(value = "/updateGovGridType", method = RequestMethod.POST)
    public int updateGovGridType(@RequestBody  GovGridTypePo govGridTypePo);

    @RequestMapping(value = "/deleteGovGridType", method = RequestMethod.POST)
    public int deleteGovGridType(@RequestBody  GovGridTypePo govGridTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govGridTypeDto 数据对象分享
     * @return GovGridTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovGridTypes", method = RequestMethod.POST)
    List<GovGridTypeDto> queryGovGridTypes(@RequestBody GovGridTypeDto govGridTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govGridTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovGridTypesCount", method = RequestMethod.POST)
    int queryGovGridTypesCount(@RequestBody GovGridTypeDto govGridTypeDto);
}
