package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govActivities.GovActivitiesDto;
import com.java110.po.govActivities.GovActivitiesPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovActivitiesInnerServiceSMO
 * @Description 公告管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "community-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govActivitiesApi")
public interface IGovActivitiesInnerServiceSMO {


    @RequestMapping(value = "/saveGovActivities", method = RequestMethod.POST)
    public int saveGovActivities(@RequestBody GovActivitiesPo govActivitiesPo);

    @RequestMapping(value = "/updateGovActivities", method = RequestMethod.POST)
    public int updateGovActivities(@RequestBody  GovActivitiesPo govActivitiesPo);

    @RequestMapping(value = "/deleteGovActivities", method = RequestMethod.POST)
    public int deleteGovActivities(@RequestBody  GovActivitiesPo govActivitiesPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govActivitiesDto 数据对象分享
     * @return GovActivitiesDto 对象数据
     */
    @RequestMapping(value = "/queryGovActivitiess", method = RequestMethod.POST)
    List<GovActivitiesDto> queryGovActivitiess(@RequestBody GovActivitiesDto govActivitiesDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govActivitiesDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovActivitiessCount", method = RequestMethod.POST)
    int queryGovActivitiessCount(@RequestBody GovActivitiesDto govActivitiesDto);
}
