package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.po.govMentalDisorders.GovMentalDisordersPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMentalDisordersInnerServiceSMO
 * @Description 障碍者接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govMentalDisordersApi")
public interface IGovMentalDisordersInnerServiceSMO {


    @RequestMapping(value = "/saveGovMentalDisorders", method = RequestMethod.POST)
    public int saveGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo);

    @RequestMapping(value = "/updateGovMentalDisorders", method = RequestMethod.POST)
    public int updateGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo);

    @RequestMapping(value = "/deleteGovMentalDisorders", method = RequestMethod.POST)
    public int deleteGovMentalDisorders(@RequestBody  GovMentalDisordersPo govMentalDisordersPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMentalDisordersDto 数据对象分享
     * @return GovMentalDisordersDto 对象数据
     */
    @RequestMapping(value = "/queryGovMentalDisorderss", method = RequestMethod.POST)
    List<GovMentalDisordersDto> queryGovMentalDisorderss(@RequestBody GovMentalDisordersDto govMentalDisordersDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMentalDisordersDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMentalDisorderssCount", method = RequestMethod.POST)
    int queryGovMentalDisorderssCount(@RequestBody GovMentalDisordersDto govMentalDisordersDto);
}
