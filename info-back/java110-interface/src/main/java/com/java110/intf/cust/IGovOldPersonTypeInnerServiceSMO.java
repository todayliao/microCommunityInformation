package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
import com.java110.po.govOldPersonType.GovOldPersonTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovOldPersonTypeInnerServiceSMO
 * @Description 老人类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govOldPersonTypeApi")
public interface IGovOldPersonTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovOldPersonType", method = RequestMethod.POST)
    public int saveGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo);

    @RequestMapping(value = "/updateGovOldPersonType", method = RequestMethod.POST)
    public int updateGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo);

    @RequestMapping(value = "/deleteGovOldPersonType", method = RequestMethod.POST)
    public int deleteGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOldPersonTypeDto 数据对象分享
     * @return GovOldPersonTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovOldPersonTypes", method = RequestMethod.POST)
    List<GovOldPersonTypeDto> queryGovOldPersonTypes(@RequestBody GovOldPersonTypeDto govOldPersonTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOldPersonTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovOldPersonTypesCount", method = RequestMethod.POST)
    int queryGovOldPersonTypesCount(@RequestBody GovOldPersonTypeDto govOldPersonTypeDto);
}
