package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govRenovationCheck.GovRenovationCheckDto;
import com.java110.po.govRenovationCheck.GovRenovationCheckPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovRenovationCheckInnerServiceSMO
 * @Description 重点地区整治情况接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govRenovationCheckApi")
public interface IGovRenovationCheckInnerServiceSMO {


    @RequestMapping(value = "/saveGovRenovationCheck", method = RequestMethod.POST)
    public int saveGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo);

    @RequestMapping(value = "/updateGovRenovationCheck", method = RequestMethod.POST)
    public int updateGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo);

    @RequestMapping(value = "/deleteGovRenovationCheck", method = RequestMethod.POST)
    public int deleteGovRenovationCheck(@RequestBody  GovRenovationCheckPo govRenovationCheckPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govRenovationCheckDto 数据对象分享
     * @return GovRenovationCheckDto 对象数据
     */
    @RequestMapping(value = "/queryGovRenovationChecks", method = RequestMethod.POST)
    List<GovRenovationCheckDto> queryGovRenovationChecks(@RequestBody GovRenovationCheckDto govRenovationCheckDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govRenovationCheckDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovRenovationChecksCount", method = RequestMethod.POST)
    int queryGovRenovationChecksCount(@RequestBody GovRenovationCheckDto govRenovationCheckDto);
}
