package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govSpecialFollow.GovSpecialFollowDto;
import com.java110.po.govSpecialFollow.GovSpecialFollowPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovSpecialFollowInnerServiceSMO
 * @Description 特殊人员跟进记录接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govSpecialFollowApi")
public interface IGovSpecialFollowInnerServiceSMO {


    @RequestMapping(value = "/saveGovSpecialFollow", method = RequestMethod.POST)
    public int saveGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo);

    @RequestMapping(value = "/updateGovSpecialFollow", method = RequestMethod.POST)
    public int updateGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo);

    @RequestMapping(value = "/deleteGovSpecialFollow", method = RequestMethod.POST)
    public int deleteGovSpecialFollow(@RequestBody  GovSpecialFollowPo govSpecialFollowPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govSpecialFollowDto 数据对象分享
     * @return GovSpecialFollowDto 对象数据
     */
    @RequestMapping(value = "/queryGovSpecialFollows", method = RequestMethod.POST)
    List<GovSpecialFollowDto> queryGovSpecialFollows(@RequestBody GovSpecialFollowDto govSpecialFollowDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govSpecialFollowDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovSpecialFollowsCount", method = RequestMethod.POST)
    int queryGovSpecialFollowsCount(@RequestBody GovSpecialFollowDto govSpecialFollowDto);
}
