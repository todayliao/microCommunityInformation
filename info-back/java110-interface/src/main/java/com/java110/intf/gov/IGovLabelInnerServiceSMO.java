package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.po.govLabel.GovLabelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovLabelInnerServiceSMO
 * @Description 标签管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govLabelApi")
public interface IGovLabelInnerServiceSMO {


    @RequestMapping(value = "/saveGovLabel", method = RequestMethod.POST)
    public int saveGovLabel(@RequestBody  GovLabelPo govLabelPo);

    @RequestMapping(value = "/updateGovLabel", method = RequestMethod.POST)
    public int updateGovLabel(@RequestBody  GovLabelPo govLabelPo);

    @RequestMapping(value = "/deleteGovLabel", method = RequestMethod.POST)
    public int deleteGovLabel(@RequestBody  GovLabelPo govLabelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govLabelDto 数据对象分享
     * @return GovLabelDto 对象数据
     */
    @RequestMapping(value = "/queryGovLabels", method = RequestMethod.POST)
    List<GovLabelDto> queryGovLabels(@RequestBody GovLabelDto govLabelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govLabelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovLabelsCount", method = RequestMethod.POST)
    int queryGovLabelsCount(@RequestBody GovLabelDto govLabelDto);
}
