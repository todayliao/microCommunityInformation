package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.perGovActivityRel.PerGovActivityRelDto;
import com.java110.po.perGovActivityRel.PerGovActivityRelPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IPerGovActivityRelInnerServiceSMO
 * @Description 生日记录关系接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/perGovActivityRelApi")
public interface IPerGovActivityRelInnerServiceSMO {


    @RequestMapping(value = "/savePerGovActivityRel", method = RequestMethod.POST)
    public int savePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo);

    @RequestMapping(value = "/updatePerGovActivityRel", method = RequestMethod.POST)
    public int updatePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo);

    @RequestMapping(value = "/deletePerGovActivityRel", method = RequestMethod.POST)
    public int deletePerGovActivityRel(@RequestBody  PerGovActivityRelPo perGovActivityRelPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param perGovActivityRelDto 数据对象分享
     * @return PerGovActivityRelDto 对象数据
     */
    @RequestMapping(value = "/queryPerGovActivityRels", method = RequestMethod.POST)
    List<PerGovActivityRelDto> queryPerGovActivityRels(@RequestBody PerGovActivityRelDto perGovActivityRelDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param perGovActivityRelDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryPerGovActivityRelsCount", method = RequestMethod.POST)
    int queryPerGovActivityRelsCount(@RequestBody PerGovActivityRelDto perGovActivityRelDto);
}
