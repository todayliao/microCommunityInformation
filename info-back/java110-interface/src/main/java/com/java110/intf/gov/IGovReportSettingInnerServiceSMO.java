package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govReportSetting.GovReportSettingDto;
import com.java110.po.govReportSetting.GovReportSettingPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovReportSettingInnerServiceSMO
 * @Description 报事设置接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "gov-service", configuration = {FeignConfiguration.class})
@RequestMapping("/govReportSettingApi")
public interface IGovReportSettingInnerServiceSMO {


    @RequestMapping(value = "/saveGovReportSetting", method = RequestMethod.POST)
    public int saveGovReportSetting(@RequestBody GovReportSettingPo govReportSettingPo);

    @RequestMapping(value = "/updateGovReportSetting", method = RequestMethod.POST)
    public int updateGovReportSetting(@RequestBody  GovReportSettingPo govReportSettingPo);

    @RequestMapping(value = "/deleteGovReportSetting", method = RequestMethod.POST)
    public int deleteGovReportSetting(@RequestBody  GovReportSettingPo govReportSettingPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govReportSettingDto 数据对象分享
     * @return GovReportSettingDto 对象数据
     */
    @RequestMapping(value = "/queryGovReportSettings", method = RequestMethod.POST)
    List<GovReportSettingDto> queryGovReportSettings(@RequestBody GovReportSettingDto govReportSettingDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govReportSettingDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovReportSettingsCount", method = RequestMethod.POST)
    int queryGovReportSettingsCount(@RequestBody GovReportSettingDto govReportSettingDto);
}
