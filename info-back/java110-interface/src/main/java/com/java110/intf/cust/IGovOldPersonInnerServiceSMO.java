package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.po.govOldPerson.GovOldPersonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovOldPersonInnerServiceSMO
 * @Description 老人管理接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govOldPersonApi")
public interface IGovOldPersonInnerServiceSMO {


    @RequestMapping(value = "/saveGovOldPerson", method = RequestMethod.POST)
    public int saveGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo);

    @RequestMapping(value = "/updateGovOldPerson", method = RequestMethod.POST)
    public int updateGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo);

    @RequestMapping(value = "/deleteGovOldPerson", method = RequestMethod.POST)
    public int deleteGovOldPerson(@RequestBody  GovOldPersonPo govOldPersonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOldPersonDto 数据对象分享
     * @return GovOldPersonDto 对象数据
     */
    @RequestMapping(value = "/queryGovOldPersons", method = RequestMethod.POST)
    List<GovOldPersonDto> queryGovOldPersons(@RequestBody GovOldPersonDto govOldPersonDto);
    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govOldPersonDto 数据对象分享
     * @return GovOldPersonDto 对象数据
     */
    @RequestMapping(value = "/getGovOldTypePersonCount", method = RequestMethod.POST)
    List<GovOldPersonDto> getGovOldTypePersonCount(@RequestBody GovOldPersonDto govOldPersonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govOldPersonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovOldPersonsCount", method = RequestMethod.POST)
    int queryGovOldPersonsCount(@RequestBody GovOldPersonDto govOldPersonDto);
}
