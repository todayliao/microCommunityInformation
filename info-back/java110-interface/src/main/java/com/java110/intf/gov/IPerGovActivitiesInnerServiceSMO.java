package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.perGovActivities.PerGovActivitiesDto;
import com.java110.po.perGovActivities.PerGovActivitiesPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IPerGovActivitiesInnerServiceSMO
 * @Description 生日记录接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/perGovActivitiesApi")
public interface IPerGovActivitiesInnerServiceSMO {


    @RequestMapping(value = "/savePerGovActivities", method = RequestMethod.POST)
    public int savePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo);

    @RequestMapping(value = "/updatePerGovActivities", method = RequestMethod.POST)
    public int updatePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo);

    @RequestMapping(value = "/deletePerGovActivities", method = RequestMethod.POST)
    public int deletePerGovActivities(@RequestBody  PerGovActivitiesPo perGovActivitiesPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param perGovActivitiesDto 数据对象分享
     * @return PerGovActivitiesDto 对象数据
     */
    @RequestMapping(value = "/queryPerGovActivitiess", method = RequestMethod.POST)
    List<PerGovActivitiesDto> queryPerGovActivitiess(@RequestBody PerGovActivitiesDto perGovActivitiesDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param perGovActivitiesDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryPerGovActivitiessCount", method = RequestMethod.POST)
    int queryPerGovActivitiessCount(@RequestBody PerGovActivitiesDto perGovActivitiesDto);
}
