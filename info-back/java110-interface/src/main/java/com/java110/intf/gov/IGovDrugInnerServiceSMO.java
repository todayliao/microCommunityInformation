package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.po.govDrug.GovDrugPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovDrugInnerServiceSMO
 * @Description 吸毒者接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govDrugApi")
public interface IGovDrugInnerServiceSMO {


    @RequestMapping(value = "/saveGovDrug", method = RequestMethod.POST)
    public int saveGovDrug(@RequestBody  GovDrugPo govDrugPo);

    @RequestMapping(value = "/updateGovDrug", method = RequestMethod.POST)
    public int updateGovDrug(@RequestBody  GovDrugPo govDrugPo);

    @RequestMapping(value = "/deleteGovDrug", method = RequestMethod.POST)
    public int deleteGovDrug(@RequestBody  GovDrugPo govDrugPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govDrugDto 数据对象分享
     * @return GovDrugDto 对象数据
     */
    @RequestMapping(value = "/queryGovDrugs", method = RequestMethod.POST)
    List<GovDrugDto> queryGovDrugs(@RequestBody GovDrugDto govDrugDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govDrugDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovDrugsCount", method = RequestMethod.POST)
    int queryGovDrugsCount(@RequestBody GovDrugDto govDrugDto);
}
