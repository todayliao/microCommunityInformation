package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPersonDie.GovPersonDieDto;
import com.java110.po.govPersonDie.GovPersonDiePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPersonDieInnerServiceSMO
 * @Description 死亡登记接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govPersonDieApi")
public interface IGovPersonDieInnerServiceSMO {


    @RequestMapping(value = "/saveGovPersonDie", method = RequestMethod.POST)
    public int saveGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo);

    @RequestMapping(value = "/updateGovPersonDie", method = RequestMethod.POST)
    public int updateGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo);

    @RequestMapping(value = "/deleteGovPersonDie", method = RequestMethod.POST)
    public int deleteGovPersonDie(@RequestBody  GovPersonDiePo govPersonDiePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPersonDieDto 数据对象分享
     * @return GovPersonDieDto 对象数据
     */
    @RequestMapping(value = "/queryGovPersonDies", method = RequestMethod.POST)
    List<GovPersonDieDto> queryGovPersonDies(@RequestBody GovPersonDieDto govPersonDieDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPersonDieDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPersonDiesCount", method = RequestMethod.POST)
    int queryGovPersonDiesCount(@RequestBody GovPersonDieDto govPersonDieDto);
}
