package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govPetitionType.GovPetitionTypeDto;
import com.java110.po.govPetitionType.GovPetitionTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovPetitionTypeInnerServiceSMO
 * @Description 信访类型表接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govPetitionTypeApi")
public interface IGovPetitionTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovPetitionType", method = RequestMethod.POST)
    public int saveGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo);

    @RequestMapping(value = "/updateGovPetitionType", method = RequestMethod.POST)
    public int updateGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo);

    @RequestMapping(value = "/deleteGovPetitionType", method = RequestMethod.POST)
    public int deleteGovPetitionType(@RequestBody  GovPetitionTypePo govPetitionTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govPetitionTypeDto 数据对象分享
     * @return GovPetitionTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovPetitionTypes", method = RequestMethod.POST)
    List<GovPetitionTypeDto> queryGovPetitionTypes(@RequestBody GovPetitionTypeDto govPetitionTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govPetitionTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovPetitionTypesCount", method = RequestMethod.POST)
    int queryGovPetitionTypesCount(@RequestBody GovPetitionTypeDto govPetitionTypeDto);
}
