package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
import com.java110.po.govMajorEvents.GovMajorEventsPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMajorEventsInnerServiceSMO
 * @Description 重特大事件接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govMajorEventsApi")
public interface IGovMajorEventsInnerServiceSMO {


    @RequestMapping(value = "/saveGovMajorEvents", method = RequestMethod.POST)
    public int saveGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo);

    @RequestMapping(value = "/updateGovMajorEvents", method = RequestMethod.POST)
    public int updateGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo);

    @RequestMapping(value = "/deleteGovMajorEvents", method = RequestMethod.POST)
    public int deleteGovMajorEvents(@RequestBody  GovMajorEventsPo govMajorEventsPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMajorEventsDto 数据对象分享
     * @return GovMajorEventsDto 对象数据
     */
    @RequestMapping(value = "/queryGovMajorEventss", method = RequestMethod.POST)
    List<GovMajorEventsDto> queryGovMajorEventss(@RequestBody GovMajorEventsDto govMajorEventsDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMajorEventsDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMajorEventssCount", method = RequestMethod.POST)
    int queryGovMajorEventssCount(@RequestBody GovMajorEventsDto govMajorEventsDto);
}
