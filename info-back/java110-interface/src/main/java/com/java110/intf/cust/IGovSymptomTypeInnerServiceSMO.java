package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govSymptomType.GovSymptomTypeDto;
import com.java110.po.govSymptomType.GovSymptomTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovSymptomTypeInnerServiceSMO
 * @Description 症状类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govSymptomTypeApi")
public interface IGovSymptomTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovSymptomType", method = RequestMethod.POST)
    public int saveGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo);

    @RequestMapping(value = "/updateGovSymptomType", method = RequestMethod.POST)
    public int updateGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo);

    @RequestMapping(value = "/deleteGovSymptomType", method = RequestMethod.POST)
    public int deleteGovSymptomType(@RequestBody  GovSymptomTypePo govSymptomTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govSymptomTypeDto 数据对象分享
     * @return GovSymptomTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovSymptomTypes", method = RequestMethod.POST)
    List<GovSymptomTypeDto> queryGovSymptomTypes(@RequestBody GovSymptomTypeDto govSymptomTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govSymptomTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovSymptomTypesCount", method = RequestMethod.POST)
    int queryGovSymptomTypesCount(@RequestBody GovSymptomTypeDto govSymptomTypeDto);
}
