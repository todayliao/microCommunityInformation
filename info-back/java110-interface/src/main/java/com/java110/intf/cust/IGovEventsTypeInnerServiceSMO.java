package com.java110.intf.cust;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govEventsType.GovEventsTypeDto;
import com.java110.po.govEventsType.GovEventsTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovEventsTypeInnerServiceSMO
 * @Description 事件类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-cust", configuration = {FeignConfiguration.class})
@RequestMapping("/govEventsTypeApi")
public interface IGovEventsTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovEventsType", method = RequestMethod.POST)
    public int saveGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo);

    @RequestMapping(value = "/updateGovEventsType", method = RequestMethod.POST)
    public int updateGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo);

    @RequestMapping(value = "/deleteGovEventsType", method = RequestMethod.POST)
    public int deleteGovEventsType(@RequestBody  GovEventsTypePo govEventsTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govEventsTypeDto 数据对象分享
     * @return GovEventsTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovEventsTypes", method = RequestMethod.POST)
    List<GovEventsTypeDto> queryGovEventsTypes(@RequestBody GovEventsTypeDto govEventsTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govEventsTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovEventsTypesCount", method = RequestMethod.POST)
    int queryGovEventsTypesCount(@RequestBody GovEventsTypeDto govEventsTypeDto);
}
