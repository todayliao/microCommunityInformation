package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govCarInout.GovCarInoutDto;
import com.java110.po.govCarInout.GovCarInoutPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovCarInoutInnerServiceSMO
 * @Description 车辆进出记录接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govCarInoutApi")
public interface IGovCarInoutInnerServiceSMO {


    @RequestMapping(value = "/saveGovCarInout", method = RequestMethod.POST)
    public int saveGovCarInout(@RequestBody GovCarInoutPo govCarInoutPo);

    @RequestMapping(value = "/updateGovCarInout", method = RequestMethod.POST)
    public int updateGovCarInout(@RequestBody  GovCarInoutPo govCarInoutPo);

    @RequestMapping(value = "/deleteGovCarInout", method = RequestMethod.POST)
    public int deleteGovCarInout(@RequestBody  GovCarInoutPo govCarInoutPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govCarInoutDto 数据对象分享
     * @return GovCarInoutDto 对象数据
     */
    @RequestMapping(value = "/queryGovCarInouts", method = RequestMethod.POST)
    List<GovCarInoutDto> queryGovCarInouts(@RequestBody GovCarInoutDto govCarInoutDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govCarInoutDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovCarInoutsCount", method = RequestMethod.POST)
    int queryGovCarInoutsCount(@RequestBody GovCarInoutDto govCarInoutDto);
}
