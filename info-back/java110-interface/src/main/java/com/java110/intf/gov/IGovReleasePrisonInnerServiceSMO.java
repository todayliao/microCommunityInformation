package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.po.govReleasePrison.GovReleasePrisonPo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovReleasePrisonInnerServiceSMO
 * @Description 刑满释放人员接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govReleasePrisonApi")
public interface IGovReleasePrisonInnerServiceSMO {


    @RequestMapping(value = "/saveGovReleasePrison", method = RequestMethod.POST)
    public int saveGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo);

    @RequestMapping(value = "/updateGovReleasePrison", method = RequestMethod.POST)
    public int updateGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo);

    @RequestMapping(value = "/deleteGovReleasePrison", method = RequestMethod.POST)
    public int deleteGovReleasePrison(@RequestBody  GovReleasePrisonPo govReleasePrisonPo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govReleasePrisonDto 数据对象分享
     * @return GovReleasePrisonDto 对象数据
     */
    @RequestMapping(value = "/queryGovReleasePrisons", method = RequestMethod.POST)
    List<GovReleasePrisonDto> queryGovReleasePrisons(@RequestBody GovReleasePrisonDto govReleasePrisonDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govReleasePrisonDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovReleasePrisonsCount", method = RequestMethod.POST)
    int queryGovReleasePrisonsCount(@RequestBody GovReleasePrisonDto govReleasePrisonDto);
}
