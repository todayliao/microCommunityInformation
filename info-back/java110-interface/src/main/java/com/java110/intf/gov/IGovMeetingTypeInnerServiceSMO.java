package com.java110.intf.gov;

import com.java110.config.feign.FeignConfiguration;
import com.java110.dto.govMeetingType.GovMeetingTypeDto;
import com.java110.po.govMeetingType.GovMeetingTypePo;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * @ClassName IGovMeetingTypeInnerServiceSMO
 * @Description 会议类型接口类
 * @Author wuxw
 * @Date 2019/4/24 9:04
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@FeignClient(name = "service-info-gov", configuration = {FeignConfiguration.class})
@RequestMapping("/govMeetingTypeApi")
public interface IGovMeetingTypeInnerServiceSMO {


    @RequestMapping(value = "/saveGovMeetingType", method = RequestMethod.POST)
    public int saveGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo);

    @RequestMapping(value = "/updateGovMeetingType", method = RequestMethod.POST)
    public int updateGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo);

    @RequestMapping(value = "/deleteGovMeetingType", method = RequestMethod.POST)
    public int deleteGovMeetingType(@RequestBody  GovMeetingTypePo govMeetingTypePo);

    /**
     * <p>查询小区楼信息</p>
     *
     *
     * @param govMeetingTypeDto 数据对象分享
     * @return GovMeetingTypeDto 对象数据
     */
    @RequestMapping(value = "/queryGovMeetingTypes", method = RequestMethod.POST)
    List<GovMeetingTypeDto> queryGovMeetingTypes(@RequestBody GovMeetingTypeDto govMeetingTypeDto);

    /**
     * 查询<p>小区楼</p>总记录数
     *
     * @param govMeetingTypeDto 数据对象分享
     * @return 小区下的小区楼记录数
     */
    @RequestMapping(value = "/queryGovMeetingTypesCount", method = RequestMethod.POST)
    int queryGovMeetingTypesCount(@RequestBody GovMeetingTypeDto govMeetingTypeDto);
}
