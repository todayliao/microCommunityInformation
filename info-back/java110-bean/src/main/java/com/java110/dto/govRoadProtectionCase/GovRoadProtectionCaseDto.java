package com.java110.dto.govRoadProtectionCase;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 路案事件数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovRoadProtectionCaseDto extends PageDto implements Serializable {

    private String caseCode;
private String caseContent;
private String idType;
private String happenedTime;
private String roadProtectionId;
private String govCommunityId;
private String idCard;
private String arrestsNum;
private String roadCaseId;
private String happenedPlace;
private String personNum;
private String personName;
private String fleeingNum;
private String isSolved;
private String caId;
private String caseName;
private String caseType;
private String detectionContent;


    private Date createTime;

    private String statusCd = "0";


    public String getCaseCode() {
        return caseCode;
    }
public void setCaseCode(String caseCode) {
        this.caseCode = caseCode;
    }
public String getCaseContent() {
        return caseContent;
    }
public void setCaseContent(String caseContent) {
        this.caseContent = caseContent;
    }
public String getIdType() {
        return idType;
    }
public void setIdType(String idType) {
        this.idType = idType;
    }
public String getHappenedTime() {
        return happenedTime;
    }
public void setHappenedTime(String happenedTime) {
        this.happenedTime = happenedTime;
    }
public String getRoadProtectionId() {
        return roadProtectionId;
    }
public void setRoadProtectionId(String roadProtectionId) {
        this.roadProtectionId = roadProtectionId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getArrestsNum() {
        return arrestsNum;
    }
public void setArrestsNum(String arrestsNum) {
        this.arrestsNum = arrestsNum;
    }
public String getRoadCaseId() {
        return roadCaseId;
    }
public void setRoadCaseId(String roadCaseId) {
        this.roadCaseId = roadCaseId;
    }
public String getHappenedPlace() {
        return happenedPlace;
    }
public void setHappenedPlace(String happenedPlace) {
        this.happenedPlace = happenedPlace;
    }
public String getPersonNum() {
        return personNum;
    }
public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getFleeingNum() {
        return fleeingNum;
    }
public void setFleeingNum(String fleeingNum) {
        this.fleeingNum = fleeingNum;
    }
public String getIsSolved() {
        return isSolved;
    }
public void setIsSolved(String isSolved) {
        this.isSolved = isSolved;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getCaseName() {
        return caseName;
    }
public void setCaseName(String caseName) {
        this.caseName = caseName;
    }
public String getDetectionContent() {
        return detectionContent;
    }
public void setDetectionContent(String detectionContent) {
        this.detectionContent = detectionContent;
    }

    public String getCaseType() {
        return caseType;
    }

    public void setCaseType(String caseType) {
        this.caseType = caseType;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
