package com.java110.dto.govMentalDisorders;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 障碍者数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMentalDisordersDto extends PageDto implements Serializable {

    private String emergencyTel;
private String address;
private String disordersReason;
private String caId;
private String emergencyPerson;
private String name;
private String tel;
private String disordersId;
private String disordersStartTime;
private String ramark;


    private Date createTime;

    private String statusCd = "0";


    public String getEmergencyTel() {
        return emergencyTel;
    }
public void setEmergencyTel(String emergencyTel) {
        this.emergencyTel = emergencyTel;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getDisordersReason() {
        return disordersReason;
    }
public void setDisordersReason(String disordersReason) {
        this.disordersReason = disordersReason;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEmergencyPerson() {
        return emergencyPerson;
    }
public void setEmergencyPerson(String emergencyPerson) {
        this.emergencyPerson = emergencyPerson;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getDisordersId() {
        return disordersId;
    }
public void setDisordersId(String disordersId) {
        this.disordersId = disordersId;
    }
public String getDisordersStartTime() {
        return disordersStartTime;
    }
public void setDisordersStartTime(String disordersStartTime) {
        this.disordersStartTime = disordersStartTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
