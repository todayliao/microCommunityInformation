package com.java110.dto.govSymptomType;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 症状类型数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovSymptomTypeDto extends PageDto implements Serializable {

    private String hospitalId;
private String caId;
private String name;
private String remark;
private String symptomId;
private String seq;


    private Date createTime;

    private String statusCd = "0";


    public String getHospitalId() {
        return hospitalId;
    }
public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getSymptomId() {
        return symptomId;
    }
public void setSymptomId(String symptomId) {
        this.symptomId = symptomId;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
