package com.java110.dto.reportPool;

import com.java110.dto.PageDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 报事管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class LargeReportPoolDto extends PageDto implements Serializable {

    //待处理
    public static final String STATE_WAIT = "1000";
    //接单
    public static final String STATE_TAKING = "1100";
    //退单
    public static final String STATE_BACK = "1200";
    //转单
    public static final String STATE_TRANSFER = "1300";
    //申请支付
    public static final String STATE_PAY = "1400";
    //支付失败
    public static final String STATE_PAY_ERROR = "1500";
    //待评价
    public static final String STATE_APPRAISE = "1700";
    //待回访
    public static final String STATE_RETURN_VISIT = "1800";
    //办理完成
    public static final String STATE_COMPLATE = "1900";
    //未处理主动结单
    public static final String STATE_UNPROCESSED = "2000";

    public static final String REPAIR_WAY_GRABBING = "100"; //抢单模式
    public static final String REPAIR_WAY_ASSIGN = "200"; //指派模式
    public static final String REPAIR_WAY_TRAINING = "300"; //轮训模式
    //报事渠道  D员工代客报事  T电话报事 Z业主自主报事
    public static final String REPAIR_D_CHANNEL = "D"; //员工代客报事
    public static final String REPAIR_T_CHANNEL = "T"; //电话报事
    public static final String REPAIR_Z_CHANNEL = "Z"; //业主自主报事


private String caId;
private String state;
    private List<String> states;
    private String staffId;

    private Date createTime;
    private String statusCd = "0";


    private String totalCoun; //总
    private String withCoun; //待
    private String doneCoun; //已处理
    private String finishCoun; //已回访

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getTotalCoun() {
        return totalCoun;
    }

    public void setTotalCoun(String totalCoun) {
        this.totalCoun = totalCoun;
    }

    public String getWithCoun() {
        return withCoun;
    }

    public void setWithCoun(String withCoun) {
        this.withCoun = withCoun;
    }

    public String getDoneCoun() {
        return doneCoun;
    }

    public void setDoneCoun(String doneCoun) {
        this.doneCoun = doneCoun;
    }

    public String getFinishCoun() {
        return finishCoun;
    }

    public void setFinishCoun(String finishCoun) {
        this.finishCoun = finishCoun;
    }
}
