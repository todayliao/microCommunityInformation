package com.java110.dto.govMeetingList;

import com.java110.dto.PageDto;
import com.java110.dto.govMeetingMemberRel.GovMeetingMemberRelDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 会议列表数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMeetingListDto extends PageDto implements Serializable {

    private String meetingBrief;
private String govMeetingId;
private String govMemberId;
    private String memberName;
private String orgId;
private String meetingTime;
private String meetingAddress;
private String meetingImg;
private List<String> meetingImgs;
private String meetingName;
private String caId;
private String meetingPoints;
private String meetingContent;
private String typeId;
    private String orgName;
    private String meetingTypeName;
private List<GovMeetingMemberRelDto> govMeetingMemberRelDtos;


    private Date createTime;

    private String statusCd = "0";


    public String getMeetingBrief() {
        return meetingBrief;
    }
public void setMeetingBrief(String meetingBrief) {
        this.meetingBrief = meetingBrief;
    }
public String getGovMeetingId() {
        return govMeetingId;
    }
public void setGovMeetingId(String govMeetingId) {
        this.govMeetingId = govMeetingId;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getMeetingTime() {
        return meetingTime;
    }
public void setMeetingTime(String meetingTime) {
        this.meetingTime = meetingTime;
    }
public String getMeetingAddress() {
        return meetingAddress;
    }
public void setMeetingAddress(String meetingAddress) {
        this.meetingAddress = meetingAddress;
    }
public String getMeetingImg() {
        return meetingImg;
    }
public void setMeetingImg(String meetingImg) {
        this.meetingImg = meetingImg;
    }
public String getMeetingName() {
        return meetingName;
    }
public void setMeetingName(String meetingName) {
        this.meetingName = meetingName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getMeetingPoints() {
        return meetingPoints;
    }
public void setMeetingPoints(String meetingPoints) {
        this.meetingPoints = meetingPoints;
    }
public String getMeetingContent() {
        return meetingContent;
    }
public void setMeetingContent(String meetingContent) {
        this.meetingContent = meetingContent;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public List<GovMeetingMemberRelDto> getGovMeetingMemberRelDtos() {
        return govMeetingMemberRelDtos;
    }

    public void setGovMeetingMemberRelDtos(List<GovMeetingMemberRelDto> govMeetingMemberRelDtos) {
        this.govMeetingMemberRelDtos = govMeetingMemberRelDtos;
    }

    public List<String> getMeetingImgs() {
        return meetingImgs;
    }

    public void setMeetingImgs(List<String> meetingImgs) {
        this.meetingImgs = meetingImgs;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getMeetingTypeName() {
        return meetingTypeName;
    }

    public void setMeetingTypeName(String meetingTypeName) {
        this.meetingTypeName = meetingTypeName;
    }
}
