package com.java110.dto.govCase;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 命案基本信息数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCaseDto extends PageDto implements Serializable {

    private String settleTime;
private String caseNum;
private String govSuspectId;
private String ramark;
private String govVictimId;
private String caseId;
private String victimName;
private String caId;
private String briefCondition;
private String caseName;
private String suspectName;
private String happenTime;


    private Date createTime;

    private String statusCd = "0";


    public String getSettleTime() {
        return settleTime;
    }
public void setSettleTime(String settleTime) {
        this.settleTime = settleTime;
    }
public String getCaseNum() {
        return caseNum;
    }
public void setCaseNum(String caseNum) {
        this.caseNum = caseNum;
    }
public String getGovSuspectId() {
        return govSuspectId;
    }
public void setGovSuspectId(String govSuspectId) {
        this.govSuspectId = govSuspectId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getGovVictimId() {
        return govVictimId;
    }
public void setGovVictimId(String govVictimId) {
        this.govVictimId = govVictimId;
    }
public String getCaseId() {
        return caseId;
    }
public void setCaseId(String caseId) {
        this.caseId = caseId;
    }
public String getVictimName() {
        return victimName;
    }
public void setVictimName(String victimName) {
        this.victimName = victimName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getBriefCondition() {
        return briefCondition;
    }
public void setBriefCondition(String briefCondition) {
        this.briefCondition = briefCondition;
    }
public String getCaseName() {
        return caseName;
    }
public void setCaseName(String caseName) {
        this.caseName = caseName;
    }
public String getSuspectName() {
        return suspectName;
    }
public void setSuspectName(String suspectName) {
        this.suspectName = suspectName;
    }
public String getHappenTime() {
        return happenTime;
    }
public void setHappenTime(String happenTime) {
        this.happenTime = happenTime;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
