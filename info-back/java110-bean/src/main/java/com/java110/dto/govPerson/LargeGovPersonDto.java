package com.java110.dto.govPerson;

import com.java110.dto.PageDto;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.dto.govVolunteer.GovVolunteerDto;

import java.io.Serializable;
import java.util.Date;


/**
 * @ClassName FloorDto
 * @Description 人口管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class LargeGovPersonDto extends PageDto implements Serializable {


    private String gridCoun;
    private String oldCoun;
    private String relCoun;
    private String drugsCoun;
    private String correCoun;
    private String mentalCoun;
    private String aidssCoun;
    private String volunCoun;

    public String getGridCoun() {
        return gridCoun;
    }

    public void setGridCoun(String gridCoun) {
        this.gridCoun = gridCoun;
    }

    public String getOldCoun() {
        return oldCoun;
    }

    public void setOldCoun(String oldCoun) {
        this.oldCoun = oldCoun;
    }

    public String getRelCoun() {
        return relCoun;
    }

    public void setRelCoun(String relCoun) {
        this.relCoun = relCoun;
    }

    public String getDrugsCoun() {
        return drugsCoun;
    }

    public void setDrugsCoun(String drugsCoun) {
        this.drugsCoun = drugsCoun;
    }

    public String getCorreCoun() {
        return correCoun;
    }

    public void setCorreCoun(String correCoun) {
        this.correCoun = correCoun;
    }

    public String getMentalCoun() {
        return mentalCoun;
    }

    public void setMentalCoun(String mentalCoun) {
        this.mentalCoun = mentalCoun;
    }

    public String getAidssCoun() {
        return aidssCoun;
    }

    public void setAidssCoun(String aidssCoun) {
        this.aidssCoun = aidssCoun;
    }

    public String getVolunCoun() {
        return volunCoun;
    }

    public void setVolunCoun(String volunCoun) {
        this.volunCoun = volunCoun;
    }
}
