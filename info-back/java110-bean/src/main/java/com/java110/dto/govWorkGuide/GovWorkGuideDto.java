package com.java110.dto.govWorkGuide;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 办事指南数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovWorkGuideDto extends PageDto implements Serializable {

    private String data;
private String person;
private String caId;
private String link;
private String guideType;
private String state;
private String wgId;
private String guideName;
private String flow;
private String subscribe;

    private Date createTime;

    private String statusCd = "0";


    public String getData() {
        return data;
    }
public void setData(String data) {
        this.data = data;
    }
public String getPerson() {
        return person;
    }
public void setPerson(String person) {
        this.person = person;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLink() {
        return link;
    }
public void setLink(String link) {
        this.link = link;
    }
public String getGuideType() {
        return guideType;
    }
public void setGuideType(String guideType) {
        this.guideType = guideType;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getWgId() {
        return wgId;
    }
public void setWgId(String wgId) {
        this.wgId = wgId;
    }
public String getGuideName() {
        return guideName;
    }
public void setGuideName(String guideName) {
        this.guideName = guideName;
    }
public String getFlow() {
        return flow;
    }
public void setFlow(String flow) {
        this.flow = flow;
    }

    public String getSubscribe() {
        return subscribe;
    }

    public void setSubscribe(String subscribe) {
        this.subscribe = subscribe;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
