package com.java110.dto.govMedicalClassify;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 医疗分类数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMedicalClassifyDto extends PageDto implements Serializable {

    private String caId;
private String classifyType;
private String classifyName;
private String seq;
private String ramark;
private String medicalClassifyId;


    private Date createTime;

    private String statusCd = "0";


    public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getClassifyType() {
        return classifyType;
    }
public void setClassifyType(String classifyType) {
        this.classifyType = classifyType;
    }
public String getClassifyName() {
        return classifyName;
    }
public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getMedicalClassifyId() {
        return medicalClassifyId;
    }
public void setMedicalClassifyId(String medicalClassifyId) {
        this.medicalClassifyId = medicalClassifyId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
