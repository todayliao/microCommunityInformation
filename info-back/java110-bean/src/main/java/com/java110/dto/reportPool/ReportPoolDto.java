package com.java110.dto.reportPool;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 报事管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class ReportPoolDto extends PageDto implements Serializable {

    //待处理
    public static final String STATE_WAIT = "1000";
    //接单
    public static final String STATE_TAKING = "1100";
    //退单
    public static final String STATE_BACK = "1200";
    //转单
    public static final String STATE_TRANSFER = "1300";
    //申请支付
    public static final String STATE_PAY = "1400";
    //支付失败
    public static final String STATE_PAY_ERROR = "1500";
    //待评价
    public static final String STATE_APPRAISE = "1700";
    //待回访
    public static final String STATE_RETURN_VISIT = "1800";
    //办理完成
    public static final String STATE_COMPLATE = "1900";
    //未处理主动结单
    public static final String STATE_UNPROCESSED = "2000";

    public static final String REPAIR_WAY_GRABBING = "100"; //抢单模式
    public static final String REPAIR_WAY_ASSIGN = "200"; //指派模式
    public static final String REPAIR_WAY_TRAINING = "300"; //轮训模式
    //报事渠道  D员工代客报事  T电话报事 Z业主自主报事
    public static final String REPAIR_D_CHANNEL = "D"; //员工代客报事
    public static final String REPAIR_T_CHANNEL = "T"; //电话报事
    public static final String REPAIR_Z_CHANNEL = "Z"; //业主自主报事

    private String reportType;
private String createUserId;
private String createName;
private String reportId;
private String reportName;
private String appointmentTime;
private String caId;
private String context;
private String tel;
private String repairChannel;
private String state;
    private List<String> states;
    private String staffId;

    private Date createTime;
    private String statusCd = "0";


    private String stateName;
    private String ruId;
    private String preRuId;
    private String preStaffId;
    private String preStaffName;
    private String uState;
    private String stateUname;
    private String reportTypeName;
    private String visitType;
    private String visitContext;

    public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getCreateUserId() {
        return createUserId;
    }
public void setCreateUserId(String createUserId) {
        this.createUserId = createUserId;
    }
public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getReportName() {
        return reportName;
    }
public void setReportName(String reportName) {
        this.reportName = reportName;
    }
public String getAppointmentTime() {
        return appointmentTime;
    }
public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getRepairChannel() {
        return repairChannel;
    }
public void setRepairChannel(String repairChannel) {
        this.repairChannel = repairChannel;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getRuId() {
        return ruId;
    }

    public void setRuId(String ruId) {
        this.ruId = ruId;
    }

    public String getPreRuId() {
        return preRuId;
    }

    public void setPreRuId(String preRuId) {
        this.preRuId = preRuId;
    }

    public String getPreStaffId() {
        return preStaffId;
    }

    public void setPreStaffId(String preStaffId) {
        this.preStaffId = preStaffId;
    }

    public String getPreStaffName() {
        return preStaffName;
    }

    public void setPreStaffName(String preStaffName) {
        this.preStaffName = preStaffName;
    }

    public String getuState() {
        return uState;
    }

    public void setuState(String uState) {
        this.uState = uState;
    }

    public String getStateUname() {
        return stateUname;
    }

    public void setStateUname(String stateUname) {
        this.stateUname = stateUname;
    }

    public String getReportTypeName() {
        return reportTypeName;
    }

    public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getCreateName() {
        return createName;
    }

    public void setCreateName(String createName) {
        this.createName = createName;
    }

    public String getVisitType() {
        return visitType;
    }

    public void setVisitType(String visitType) {
        this.visitType = visitType;
    }

    public String getVisitContext() {
        return visitContext;
    }

    public void setVisitContext(String visitContext) {
        this.visitContext = visitContext;
    }

    public List<String> getStates() {
        return states;
    }

    public void setStates(List<String> states) {
        this.states = states;
    }
}
