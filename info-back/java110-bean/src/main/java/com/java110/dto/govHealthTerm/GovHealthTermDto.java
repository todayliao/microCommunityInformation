package com.java110.dto.govHealthTerm;

import com.java110.dto.PageDto;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 体检项数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovHealthTermDto extends PageDto implements Serializable {

    private String termId;
    private String [] termIds;
    private String caId;
    private String termName;
    private String seq;


    private Date createTime;
    private List<GovHealthTitleDto> govHealthTitleDtoList;

    private String statusCd = "0";


    public String getTermId() {
        return termId;
    }

    public void setTermId(String termId) {
        this.termId = termId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getTermName() {
        return termName;
    }

    public void setTermName(String termName) {
        this.termName = termName;
    }

    public String getSeq() {
        return seq;
    }

    public void setSeq(String seq) {
        this.seq = seq;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public List<GovHealthTitleDto> getGovHealthTitleDtoList() {
        return govHealthTitleDtoList;
    }

    public void setGovHealthTitleDtoList(List<GovHealthTitleDto> govHealthTitleDtoList) {
        this.govHealthTitleDtoList = govHealthTitleDtoList;
    }

    public String[] getTermIds() {
        return termIds;
    }

    public void setTermIds(String[] termIds) {
        this.termIds = termIds;
    }
}
