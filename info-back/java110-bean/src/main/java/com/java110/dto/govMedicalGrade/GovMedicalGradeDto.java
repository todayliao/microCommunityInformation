package com.java110.dto.govMedicalGrade;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 医疗分级数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovMedicalGradeDto extends PageDto implements Serializable {

    private String gradeName;
private String caId;
private String gradeType;
private String seq;
private String ramark;
private String medicalGradeId;
private String medicalClassifyId;
private String classifyName;


    private Date createTime;

    private String statusCd = "0";


    public String getGradeName() {
        return gradeName;
    }
public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGradeType() {
        return gradeType;
    }
public void setGradeType(String gradeType) {
        this.gradeType = gradeType;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getMedicalGradeId() {
        return medicalGradeId;
    }
public void setMedicalGradeId(String medicalGradeId) {
        this.medicalGradeId = medicalGradeId;
    }
public String getMedicalClassifyId() {
        return medicalClassifyId;
    }
public void setMedicalClassifyId(String medicalClassifyId) {
        this.medicalClassifyId = medicalClassifyId;
    }

    public String getClassifyName() {
        return classifyName;
    }

    public void setClassifyName(String classifyName) {
        this.classifyName = classifyName;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
