package com.java110.dto.machine;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 设备管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class MachineDto extends PageDto implements Serializable {
    //查询sip会话 sip_query_session
    public static final String SIP_QUERY_SESSION = "sip_query_session";
    //开启推流
    public static final String SIP_INVITE = "sip_invite";
    //关闭推流
    public static final String SIP_BYE = "sip_bye";
    //客户端
    public static final String CLIENT_ID = "sip_cltent_id";

    private String heartbeatTime;
private String machineCode;
private String authCode;
private String govCommunityId;
private String locationTypeCd;
private String machineVersion;
private String machineName;
private String machineMac;
private String machineId;
private String caId;
private String state;
private String locationObjId;
private String machineTypeCd;
private String machineIp;
private String typeId;
private String govCommunityName;
private String machineTypeName;
private String typeName;
private String isShow;
private String action;
private String clientId;
private String websocketUrl;


    private Date createTime;

    private String statusCd = "0";


    public String getHeartbeatTime() {
        return heartbeatTime;
    }
public void setHeartbeatTime(String heartbeatTime) {
        this.heartbeatTime = heartbeatTime;
    }
public String getMachineCode() {
        return machineCode;
    }
public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }
public String getAuthCode() {
        return authCode;
    }
public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getLocationTypeCd() {
        return locationTypeCd;
    }
public void setLocationTypeCd(String locationTypeCd) {
        this.locationTypeCd = locationTypeCd;
    }
public String getMachineVersion() {
        return machineVersion;
    }
public void setMachineVersion(String machineVersion) {
        this.machineVersion = machineVersion;
    }
public String getMachineName() {
        return machineName;
    }
public void setMachineName(String machineName) {
        this.machineName = machineName;
    }
public String getMachineMac() {
        return machineMac;
    }
public void setMachineMac(String machineMac) {
        this.machineMac = machineMac;
    }
public String getMachineId() {
        return machineId;
    }
public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getLocationObjId() {
        return locationObjId;
    }
public void setLocationObjId(String locationObjId) {
        this.locationObjId = locationObjId;
    }
public String getMachineTypeCd() {
        return machineTypeCd;
    }
public void setMachineTypeCd(String machineTypeCd) {
        this.machineTypeCd = machineTypeCd;
    }
public String getMachineIp() {
        return machineIp;
    }
public void setMachineIp(String machineIp) {
        this.machineIp = machineIp;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getGovCommunityName() {
        return govCommunityName;
    }

    public void setGovCommunityName(String govCommunityName) {
        this.govCommunityName = govCommunityName;
    }

    public String getMachineTypeName() {
        return machineTypeName;
    }

    public void setMachineTypeName(String machineTypeName) {
        this.machineTypeName = machineTypeName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getWebsocketUrl() {
        return websocketUrl;
    }

    public void setWebsocketUrl(String websocketUrl) {
        this.websocketUrl = websocketUrl;
    }
}
