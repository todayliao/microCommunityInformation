package com.java110.dto.govReportSetting;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 报事设置数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovReportSettingDto extends PageDto implements Serializable {

    private String reportType;
private String returnVisitFlag;
private String caId;
private String remark;
private String reportWay;
private String reportTypeName;
private String settingId;


    private Date createTime;

    private String statusCd = "0";


    public String getReportType() {
        return reportType;
    }
public void setReportType(String reportType) {
        this.reportType = reportType;
    }
public String getReturnVisitFlag() {
        return returnVisitFlag;
    }
public void setReturnVisitFlag(String returnVisitFlag) {
        this.returnVisitFlag = returnVisitFlag;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getReportWay() {
        return reportWay;
    }
public void setReportWay(String reportWay) {
        this.reportWay = reportWay;
    }
public String getReportTypeName() {
        return reportTypeName;
    }
public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }
public String getSettingId() {
        return settingId;
    }
public void setSettingId(String settingId) {
        this.settingId = settingId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
