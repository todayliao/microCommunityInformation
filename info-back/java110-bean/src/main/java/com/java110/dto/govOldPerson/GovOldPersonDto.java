package com.java110.dto.govOldPerson;

import com.java110.dto.PageDto;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * @ClassName FloorDto
 * @Description 老人管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovOldPersonDto extends PageDto implements Serializable {

    public static final String ACCT_TYPE = "2007";
    public static final String EXT_USER_SPEC_CD = "96000001";
    public static final String OBJ_TYPE = "6006";
    private String servPerson;
    private String govCommunityId;
    private String servTel;
    private String contactPerson;
    private String oldId;
    private String[] oldIds;
    private String contactTel;
    private String ramark;
    private String personName;
    private String personAge;
    private String datasourceType;
    private String govPersonId;
    private String caId;
    private String personTel;
    private String birthday;

    private String communityName;
    private String caName;
    private String idCard;
    private String nation;
    private String maritalStatus;
    private String personSex;
    private String timeAmount;
    private String typeId;
    private String typeName;
    private String detailType;
    private String orderId;
    private String num;

    private List<GovHealthAnswerValueDto> govHealthAnswerValueDtos;


    private Date createTime;

    private String statusCd = "0";


    public String getServPerson() {
        return servPerson;
    }

    public void setServPerson(String servPerson) {
        this.servPerson = servPerson;
    }

    public String getGovCommunityId() {
        return govCommunityId;
    }

    public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }

    public String getServTel() {
        return servTel;
    }

    public void setServTel(String servTel) {
        this.servTel = servTel;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public String getOldId() {
        return oldId;
    }

    public void setOldId(String oldId) {
        this.oldId = oldId;
    }

    public String getContactTel() {
        return contactTel;
    }

    public void setContactTel(String contactTel) {
        this.contactTel = contactTel;
    }

    public String getRamark() {
        return ramark;
    }

    public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonAge() {
        return personAge;
    }

    public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }

    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

    public String getNation() {
        return nation;
    }

    public void setNation(String nation) {
        this.nation = nation;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String[] getOldIds() {
        return oldIds;
    }

    public void setOldIds(String[] oldIds) {
        this.oldIds = oldIds;
    }

    public List<GovHealthAnswerValueDto> getGovHealthAnswerValueDtos() {
        return govHealthAnswerValueDtos;
    }

    public void setGovHealthAnswerValueDtos(List<GovHealthAnswerValueDto> govHealthAnswerValueDtos) {
        this.govHealthAnswerValueDtos = govHealthAnswerValueDtos;
    }

    public String getTimeAmount() {
        return timeAmount;
    }

    public void setTimeAmount(String timeAmount) {
        this.timeAmount = timeAmount;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getDetailType() {
        return detailType;
    }

    public void setDetailType(String detailType) {
        this.detailType = detailType;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }
}
