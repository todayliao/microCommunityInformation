package com.java110.dto.govCompanyPerson;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 小区位置数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovCompanyPersonDto extends PageDto implements Serializable {

    private String relId;
private String govCompanyId;
private String govPersonId;
private String caId;
private String govOrgName;
private String state;
private String relCd;


    private Date createTime;

    private String statusCd = "0";


    public String getRelId() {
        return relId;
    }
public void setRelId(String relId) {
        this.relId = relId;
    }
public String getGovCompanyId() {
        return govCompanyId;
    }
public void setGovCompanyId(String govCompanyId) {
        this.govCompanyId = govCompanyId;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovOrgName() {
        return govOrgName;
    }
public void setGovOrgName(String govOrgName) {
        this.govOrgName = govOrgName;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getRelCd() {
        return relCd;
    }
public void setRelCd(String relCd) {
        this.relCd = relCd;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
