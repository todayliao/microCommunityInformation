package com.java110.dto.govReportReturnVisit;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 回访管理数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovReportReturnVisitDto extends PageDto implements Serializable {

    private String visitId;
private String reportId;
private String caId;
private String context;
private String visitPersonName;
private String visitPersonId;
private String visitType;
private String reportTypeName;
private String tel;
private String reportName;
private String appointmentTime;
private String state;


    private Date createTime;

    private String statusCd = "0";


    public String getVisitId() {
        return visitId;
    }
public void setVisitId(String visitId) {
        this.visitId = visitId;
    }
public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getVisitPersonName() {
        return visitPersonName;
    }
public void setVisitPersonName(String visitPersonName) {
        this.visitPersonName = visitPersonName;
    }
public String getVisitPersonId() {
        return visitPersonId;
    }
public void setVisitPersonId(String visitPersonId) {
        this.visitPersonId = visitPersonId;
    }
public String getVisitType() {
        return visitType;
    }
public void setVisitType(String visitType) {
        this.visitType = visitType;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getReportTypeName() {
        return reportTypeName;
    }

    public void setReportTypeName(String reportTypeName) {
        this.reportTypeName = reportTypeName;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getAppointmentTime() {
        return appointmentTime;
    }

    public void setAppointmentTime(String appointmentTime) {
        this.appointmentTime = appointmentTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
