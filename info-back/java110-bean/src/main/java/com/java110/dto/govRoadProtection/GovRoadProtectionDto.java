package com.java110.dto.govRoadProtection;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 护路护线数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovRoadProtectionDto extends PageDto implements Serializable {

    private String departmentAddress;
private String leaderLink;
private String departmentTel;
private String roadProtectionId;
private String govCommunityId;
private String branchLeaders;
private String belongToDepartment;
private String roadName;
private String safeHiddenGrade;
private String manageDepAddress;
private String branchLeadersTel;
private String roadType;
private String manageDepartment;
private String manageDepTel;
private String caId;
private String leadingCadreName;
private String safeTrouble;


    private Date createTime;

    private String statusCd = "0";


    public String getDepartmentAddress() {
        return departmentAddress;
    }
public void setDepartmentAddress(String departmentAddress) {
        this.departmentAddress = departmentAddress;
    }
public String getLeaderLink() {
        return leaderLink;
    }
public void setLeaderLink(String leaderLink) {
        this.leaderLink = leaderLink;
    }
public String getDepartmentTel() {
        return departmentTel;
    }
public void setDepartmentTel(String departmentTel) {
        this.departmentTel = departmentTel;
    }
public String getRoadProtectionId() {
        return roadProtectionId;
    }
public void setRoadProtectionId(String roadProtectionId) {
        this.roadProtectionId = roadProtectionId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getBranchLeaders() {
        return branchLeaders;
    }
public void setBranchLeaders(String branchLeaders) {
        this.branchLeaders = branchLeaders;
    }
public String getBelongToDepartment() {
        return belongToDepartment;
    }
public void setBelongToDepartment(String belongToDepartment) {
        this.belongToDepartment = belongToDepartment;
    }
public String getRoadName() {
        return roadName;
    }
public void setRoadName(String roadName) {
        this.roadName = roadName;
    }
public String getSafeHiddenGrade() {
        return safeHiddenGrade;
    }
public void setSafeHiddenGrade(String safeHiddenGrade) {
        this.safeHiddenGrade = safeHiddenGrade;
    }
public String getManageDepAddress() {
        return manageDepAddress;
    }
public void setManageDepAddress(String manageDepAddress) {
        this.manageDepAddress = manageDepAddress;
    }
public String getBranchLeadersTel() {
        return branchLeadersTel;
    }
public void setBranchLeadersTel(String branchLeadersTel) {
        this.branchLeadersTel = branchLeadersTel;
    }
public String getRoadType() {
        return roadType;
    }
public void setRoadType(String roadType) {
        this.roadType = roadType;
    }
public String getManageDepartment() {
        return manageDepartment;
    }
public void setManageDepartment(String manageDepartment) {
        this.manageDepartment = manageDepartment;
    }
public String getManageDepTel() {
        return manageDepTel;
    }
public void setManageDepTel(String manageDepTel) {
        this.manageDepTel = manageDepTel;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLeadingCadreName() {
        return leadingCadreName;
    }
public void setLeadingCadreName(String leadingCadreName) {
        this.leadingCadreName = leadingCadreName;
    }
public String getSafeTrouble() {
        return safeTrouble;
    }
public void setSafeTrouble(String safeTrouble) {
        this.safeTrouble = safeTrouble;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
