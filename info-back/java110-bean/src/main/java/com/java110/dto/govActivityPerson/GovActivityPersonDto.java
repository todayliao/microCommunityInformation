package com.java110.dto.govActivityPerson;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 活动报名人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovActivityPersonDto extends PageDto implements Serializable {

    private String personName;
private String personAge;
private String actPerId;
private String personAddress;
private String caId;
private String actId;
private String actName;
private String remark;
private String personLink;


    private Date createTime;

    private String statusCd = "0";


    public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getPersonAge() {
        return personAge;
    }
public void setPersonAge(String personAge) {
        this.personAge = personAge;
    }
public String getActPerId() {
        return actPerId;
    }
public void setActPerId(String actPerId) {
        this.actPerId = actPerId;
    }
public String getPersonAddress() {
        return personAddress;
    }
public void setPersonAddress(String personAddress) {
        this.personAddress = personAddress;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getActId() {
        return actId;
    }
public void setActId(String actId) {
        this.actId = actId;
    }
public String getActName() {
    return actName;
}
public void setActName(String actName) {
    this.actName = actName;
}
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
