package com.java110.dto.govReportUser;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 报事类型人员数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovReportUserDto extends PageDto implements Serializable {
    //开始用户
    public static final String POOL_EVENT_START_USER = "startUser";
    //审核用户
    public static final String POOL_EVENT_AUDIT_USER = "auditUser";

    public static final String POOL_EVENT_PAY_USER = "payUser";

    public static final String STATE_DOING = "10001";// 处理中
    public static final String STATE_CLOSE = "10002";// 结单
    public static final String STATE_BACK = "10003";// 退单
    public static final String STATE_TRANSFER = "10004";// 转单
    public static final String STATE_SUBMIT = "10005";// 提交
    public static final String STATE_DISPATCH = "10006";//派单
    public static final String STATE_FINISH = "10007";//评价完成
    public static final String STATE_FINISH_VISIT = "10008";//已回访
    public static final String STATE_PAY_FEE = "10009";//待支付
    public static final String STATE_EVALUATE = "11000";//待评价
    public static final String STATE_FINISH_PAY_FEE = "12000";//已支付
    private String reportId;
private String preStaffName;
private String reportEvent;
private String ruId;
private String caId;
private String preStaffId;
private String context;
private String staffName;
private String preRuId;
private Date startTime;
private String state;
private Date endTime;
private String staffId;
private String duration;
    private String[] states;

    private Date createTime;
    private String stateName;

    private String statusCd = "0";


    public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getPreStaffName() {
        return preStaffName;
    }
public void setPreStaffName(String preStaffName) {
        this.preStaffName = preStaffName;
    }
public String getReportEvent() {
        return reportEvent;
    }
public void setReportEvent(String reportEvent) {
        this.reportEvent = reportEvent;
    }
public String getRuId() {
        return ruId;
    }
public void setRuId(String ruId) {
        this.ruId = ruId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPreStaffId() {
        return preStaffId;
    }
public void setPreStaffId(String preStaffId) {
        this.preStaffId = preStaffId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStaffName() {
        return staffName;
    }
public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
public String getPreRuId() {
        return preRuId;
    }
public void setPreRuId(String preRuId) {
        this.preRuId = preRuId;
    }


public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }

public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String[] getStates() {
        return states;
    }

    public void setStates(String[] states) {
        this.states = states;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}
