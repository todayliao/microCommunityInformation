package com.java110.dto.govVolunteerPersonRel;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 服务记录人员关系表数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovVolunteerPersonRelDto extends PageDto implements Serializable {

    private String govPersonId;
private String serviceRecordId;
private String govCommunityId;
private String caId;
private String volunteerPersonRelId;
private String volunteerId;


    private Date createTime;

    private String statusCd = "0";


    public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getServiceRecordId() {
        return serviceRecordId;
    }
public void setServiceRecordId(String serviceRecordId) {
        this.serviceRecordId = serviceRecordId;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getVolunteerPersonRelId() {
        return volunteerPersonRelId;
    }
public void setVolunteerPersonRelId(String volunteerPersonRelId) {
        this.volunteerPersonRelId = volunteerPersonRelId;
    }
public String getVolunteerId() {
        return volunteerId;
    }
public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
}
