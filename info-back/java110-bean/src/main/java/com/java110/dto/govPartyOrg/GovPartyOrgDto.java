package com.java110.dto.govPartyOrg;

import com.java110.dto.PageDto;
import java.io.Serializable;
import java.util.Date;

/**
 * @ClassName FloorDto
 * @Description 党组织数据层封装
 * @Author wuxw
 * @Date 2019/4/24 8:52
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
public class GovPartyOrgDto extends PageDto implements Serializable {

    private String orgName;
private String address;
private String orgLevel;
private String orgSimpleName;
private String orgId;
private String preOrgId;
private String ramark;
private String personName;
private String datasourceType;
private String orgCode;
private String orgFlag;
private String preOrgName;
private String personTel;
private String communityOrArea;
private String context;


    private Date createTime;

    private String statusCd = "0";


    public String getOrgName() {
        return orgName;
    }
public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getOrgLevel() {
        return orgLevel;
    }
public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }
public String getOrgSimpleName() {
        return orgSimpleName;
    }
public void setOrgSimpleName(String orgSimpleName) {
        this.orgSimpleName = orgSimpleName;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getPreOrgId() {
        return preOrgId;
    }
public void setPreOrgId(String preOrgId) {
        this.preOrgId = preOrgId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getOrgCode() {
        return orgCode;
    }
public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }
public String getOrgFlag() {
        return orgFlag;
    }
public void setOrgFlag(String orgFlag) {
        this.orgFlag = orgFlag;
    }
public String getPreOrgName() {
        return preOrgName;
    }
public void setPreOrgName(String preOrgName) {
        this.preOrgName = preOrgName;
    }
public String getPersonTel() {
        return personTel;
    }
public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }


    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getStatusCd() {
        return statusCd;
    }

    public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    public String getCommunityOrArea() {
        return communityOrArea;
    }

    public void setCommunityOrArea(String communityOrArea) {
        this.communityOrArea = communityOrArea;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }
}
