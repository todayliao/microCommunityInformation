package com.java110.po.govVolunteerServiceRecord;

import java.io.Serializable;
import java.util.Date;

public class GovVolunteerServiceRecordPo implements Serializable {

    private String img;
private String typeCd;
private String serviceRecordId;
private String caId;
private String context;
private String startTime;
private String statusCd = "0";
private String endTime;
private String title;
public String getImg() {
        return img;
    }
public void setImg(String img) {
        this.img = img;
    }
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getServiceRecordId() {
        return serviceRecordId;
    }
public void setServiceRecordId(String serviceRecordId) {
        this.serviceRecordId = serviceRecordId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getTitle() {
        return title;
    }
public void setTitle(String title) {
        this.title = title;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
