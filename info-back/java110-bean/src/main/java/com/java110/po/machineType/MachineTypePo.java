package com.java110.po.machineType;

import java.io.Serializable;
import java.util.Date;

public class MachineTypePo implements Serializable {

    private String machineTypeName;
private String govCommunityId;
private String caId;
private String typeId;
private String statusCd = "0";
private String machineTypeCd;
public String getMachineTypeName() {
        return machineTypeName;
    }
public void setMachineTypeName(String machineTypeName) {
        this.machineTypeName = machineTypeName;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getMachineTypeCd() {
        return machineTypeCd;
    }
public void setMachineTypeCd(String machineTypeCd) {
        this.machineTypeCd = machineTypeCd;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
