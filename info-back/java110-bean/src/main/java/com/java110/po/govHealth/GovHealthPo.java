package com.java110.po.govHealth;

import java.io.Serializable;
import java.util.Date;

public class GovHealthPo implements Serializable {

    private String datasourceType;
private String caId;
private String name;
private String healthId;
private String statusCd = "0";
private String ramark;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getHealthId() {
        return healthId;
    }
public void setHealthId(String healthId) {
        this.healthId = healthId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }



}
