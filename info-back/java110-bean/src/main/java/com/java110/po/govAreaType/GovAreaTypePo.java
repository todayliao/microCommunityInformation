package com.java110.po.govAreaType;

import java.io.Serializable;
import java.util.Date;

public class GovAreaTypePo implements Serializable {

    private String caId;
private String name;
private String typeId;
private String statusCd = "0";
private String ramark;
private String isShow;
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTypeId() {
        return typeId;
    }
public void setTypeId(String typeId) {
        this.typeId = typeId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getIsShow() {
        return isShow;
    }
public void setIsShow(String isShow) {
        this.isShow = isShow;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
