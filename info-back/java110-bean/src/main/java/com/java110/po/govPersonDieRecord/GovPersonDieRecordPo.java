package com.java110.po.govPersonDieRecord;

import java.io.Serializable;
import java.util.Date;

public class GovPersonDieRecordPo implements Serializable {

    private String dieRecordId;
private String typeCd;
private String govPersonId;
private String caId;
private String context;
private String dieId;
private String activityTime;
private String statusCd = "0";
private String title;
public String getDieRecordId() {
        return dieRecordId;
    }
public void setDieRecordId(String dieRecordId) {
        this.dieRecordId = dieRecordId;
    }
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getDieId() {
        return dieId;
    }
public void setDieId(String dieId) {
        this.dieId = dieId;
    }
public String getActivityTime() {
        return activityTime;
    }
public void setActivityTime(String activityTime) {
        this.activityTime = activityTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getTitle() {
        return title;
    }
public void setTitle(String title) {
        this.title = title;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
