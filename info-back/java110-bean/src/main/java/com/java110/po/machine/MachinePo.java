package com.java110.po.machine;

import java.io.Serializable;
import java.util.Date;

public class MachinePo implements Serializable {

    private String heartbeatTime;
private String machineCode;
private String authCode;
private String govCommunityId;
private String locationTypeCd;
private String machineVersion;
private String statusCd = "0";
private String machineName;
private String machineMac;
private String machineId;
private String caId;
private String state;
private String locationObjId;
private String machineTypeCd;
private String machineIp;
private String typeId;
private String isShow;
public String getHeartbeatTime() {
        return heartbeatTime;
    }
public void setHeartbeatTime(String heartbeatTime) {
        this.heartbeatTime = heartbeatTime;
    }
public String getMachineCode() {
        return machineCode;
    }
public void setMachineCode(String machineCode) {
        this.machineCode = machineCode;
    }
public String getAuthCode() {
        return authCode;
    }
public void setAuthCode(String authCode) {
        this.authCode = authCode;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getLocationTypeCd() {
        return locationTypeCd;
    }
public void setLocationTypeCd(String locationTypeCd) {
        this.locationTypeCd = locationTypeCd;
    }
public String getMachineVersion() {
        return machineVersion;
    }
public void setMachineVersion(String machineVersion) {
        this.machineVersion = machineVersion;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getMachineName() {
        return machineName;
    }
public void setMachineName(String machineName) {
        this.machineName = machineName;
    }
public String getMachineMac() {
        return machineMac;
    }
public void setMachineMac(String machineMac) {
        this.machineMac = machineMac;
    }
public String getMachineId() {
        return machineId;
    }
public void setMachineId(String machineId) {
        this.machineId = machineId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getLocationObjId() {
        return locationObjId;
    }
public void setLocationObjId(String locationObjId) {
        this.locationObjId = locationObjId;
    }
public String getMachineTypeCd() {
        return machineTypeCd;
    }
public void setMachineTypeCd(String machineTypeCd) {
        this.machineTypeCd = machineTypeCd;
    }
public String getMachineIp() {
        return machineIp;
    }
public void setMachineIp(String machineIp) {
        this.machineIp = machineIp;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getIsShow() {
        return isShow;
    }

    public void setIsShow(String isShow) {
        this.isShow = isShow;
    }
}
