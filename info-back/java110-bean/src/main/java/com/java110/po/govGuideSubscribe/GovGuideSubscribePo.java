package com.java110.po.govGuideSubscribe;

import java.io.Serializable;
import java.util.Date;

public class GovGuideSubscribePo implements Serializable {

    private String wgsId;
private String person;
private String caId;
private String link;
private String startTime;
private String statusCd = "0";
private String state;
private String endTime;
private String wgId;
public String getWgsId() {
        return wgsId;
    }
public void setWgsId(String wgsId) {
        this.wgsId = wgsId;
    }
public String getPerson() {
        return person;
    }
public void setPerson(String person) {
        this.person = person;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getLink() {
        return link;
    }
public void setLink(String link) {
        this.link = link;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getWgId() {
        return wgId;
    }
public void setWgId(String wgId) {
        this.wgId = wgId;
    }



}
