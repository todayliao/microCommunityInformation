package com.java110.po.govHelpPolicy;

import java.io.Serializable;
import java.util.Date;

public class GovHelpPolicyPo implements Serializable {

    private String helpBrief;
private String govHelpId;
private String helpTime;
private String helpName;
private String caId;
private String statusCd = "0";
public String getHelpBrief() {
        return helpBrief;
    }
public void setHelpBrief(String helpBrief) {
        this.helpBrief = helpBrief;
    }
public String getGovHelpId() {
        return govHelpId;
    }
public void setGovHelpId(String govHelpId) {
        this.govHelpId = govHelpId;
    }
public String getHelpTime() {
        return helpTime;
    }
public void setHelpTime(String helpTime) {
        this.helpTime = helpTime;
    }
public String getHelpName() {
        return helpName;
    }
public void setHelpName(String helpName) {
        this.helpName = helpName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
