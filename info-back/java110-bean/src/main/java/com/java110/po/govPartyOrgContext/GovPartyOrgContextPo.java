package com.java110.po.govPartyOrgContext;

import java.io.Serializable;
import java.util.Date;

public class GovPartyOrgContextPo implements Serializable {

    private String datasourceType="999999";
private String context;
private String statusCd = "0";
private String orgId;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }



}
