package com.java110.po.govAdvertItem;

import java.io.Serializable;
import java.util.Date;

public class GovAdvertItemPo implements Serializable {

    private String caId;
private String itemTypeCd;
private String statusCd = "0";
private String advertItemId;
private String advertId;
private String url;
private String seq;
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getItemTypeCd() {
        return itemTypeCd;
    }
public void setItemTypeCd(String itemTypeCd) {
        this.itemTypeCd = itemTypeCd;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getAdvertItemId() {
        return advertItemId;
    }
public void setAdvertItemId(String advertItemId) {
        this.advertItemId = advertItemId;
    }
public String getAdvertId() {
        return advertId;
    }
public void setAdvertId(String advertId) {
        this.advertId = advertId;
    }
public String getUrl() {
        return url;
    }
public void setUrl(String url) {
        this.url = url;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }



}
