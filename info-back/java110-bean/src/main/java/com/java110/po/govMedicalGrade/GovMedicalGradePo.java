package com.java110.po.govMedicalGrade;

import java.io.Serializable;
import java.util.Date;

public class GovMedicalGradePo implements Serializable {

    private String gradeName;
private String caId;
private String gradeType;
private String statusCd = "0";
private String seq;
private String ramark;
private String medicalGradeId;
private String medicalClassifyId;
public String getGradeName() {
        return gradeName;
    }
public void setGradeName(String gradeName) {
        this.gradeName = gradeName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGradeType() {
        return gradeType;
    }
public void setGradeType(String gradeType) {
        this.gradeType = gradeType;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getMedicalGradeId() {
        return medicalGradeId;
    }
public void setMedicalGradeId(String medicalGradeId) {
        this.medicalGradeId = medicalGradeId;
    }
public String getMedicalClassifyId() {
        return medicalClassifyId;
    }
public void setMedicalClassifyId(String medicalClassifyId) {
        this.medicalClassifyId = medicalClassifyId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
