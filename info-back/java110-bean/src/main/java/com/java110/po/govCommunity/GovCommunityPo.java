package com.java110.po.govCommunity;

import java.io.Serializable;
import java.util.Date;

public class GovCommunityPo implements Serializable {

    private String personName;
private String communitySecure;
private String govCommunityId;
private String caId;
private String propertyType;
private String communityIcon;
private String oldCommunityIcon;
private String communityName;
private String statusCd = "0";
private String managerName;
private String ramark;
private String personLink;
private String topic;
private String mapX;
private String mapY;
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getCommunitySecure() {
        return communitySecure;
    }
public void setCommunitySecure(String communitySecure) {
        this.communitySecure = communitySecure;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPropertyType() {
        return propertyType;
    }
public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }
public String getCommunityIcon() {
        return communityIcon;
    }
public void setCommunityIcon(String communityIcon) {
        this.communityIcon = communityIcon;
    }
public String getCommunityName() {
        return communityName;
    }
public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getManagerName() {
        return managerName;
    }
public void setManagerName(String managerName) {
        this.managerName = managerName;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getPersonLink() {
        return personLink;
    }
public void setPersonLink(String personLink) {
        this.personLink = personLink;
    }

    public String getOldCommunityIcon() {
        return oldCommunityIcon;
    }

    public void setOldCommunityIcon(String oldCommunityIcon) {
        this.oldCommunityIcon = oldCommunityIcon;
    }
    private String datasourceType = "999999";
    public String getDatasourceType() {
        return datasourceType;
    }

    public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getMapX() {
        return mapX;
    }

    public void setMapX(String mapX) {
        this.mapX = mapX;
    }

    public String getMapY() {
        return mapY;
    }

    public void setMapY(String mapY) {
        this.mapY = mapY;
    }
}
