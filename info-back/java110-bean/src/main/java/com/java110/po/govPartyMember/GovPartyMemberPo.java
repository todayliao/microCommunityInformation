package com.java110.po.govPartyMember;

import java.io.Serializable;
import java.util.Date;

public class GovPartyMemberPo implements Serializable {

    private String datasourceType="999999";
private String orgName;
private String caId;
private String govMemberId;
private String personId;
private String statusCd = "0";
private String govTypeId;
private String orgId;
private String ramark;
private String memberFlag;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getOrgName() {
        return orgName;
    }
public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getGovTypeId() {
        return govTypeId;
    }
public void setGovTypeId(String govTypeId) {
        this.govTypeId = govTypeId;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getMemberFlag() {
        return memberFlag;
    }
public void setMemberFlag(String memberFlag) {
        this.memberFlag = memberFlag;
    }



}
