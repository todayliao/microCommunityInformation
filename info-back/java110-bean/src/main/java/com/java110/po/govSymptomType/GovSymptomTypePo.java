package com.java110.po.govSymptomType;

import java.io.Serializable;
import java.util.Date;

public class GovSymptomTypePo implements Serializable {

    private String hospitalId;
private String caId;
private String name;
private String remark;
private String statusCd = "0";
private String symptomId;
private String seq;
public String getHospitalId() {
        return hospitalId;
    }
public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getSymptomId() {
        return symptomId;
    }
public void setSymptomId(String symptomId) {
        this.symptomId = symptomId;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
