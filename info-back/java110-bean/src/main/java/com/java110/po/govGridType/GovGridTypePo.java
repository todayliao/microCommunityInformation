package com.java110.po.govGridType;

import java.io.Serializable;
import java.util.Date;

public class GovGridTypePo implements Serializable {

    private String datasourceType;
private String caId;
private String typeName;
private String statusCd = "0";
private String govTypeId;
private String ramark;
private String orgId;
public String getDatasourceType() {
        return datasourceType;
    }
public void setDatasourceType(String datasourceType) {
        this.datasourceType = datasourceType;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeName() {
        return typeName;
    }
public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getGovTypeId() {
        return govTypeId;
    }
public void setGovTypeId(String govTypeId) {
        this.govTypeId = govTypeId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId;
    }
}
