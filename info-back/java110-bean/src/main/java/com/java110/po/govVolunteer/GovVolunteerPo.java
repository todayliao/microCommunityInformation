package com.java110.po.govVolunteer;

import java.io.Serializable;
import java.util.Date;

public class GovVolunteerPo implements Serializable {

    private String volWork;
private String address;
private String student;
private String statusCd = "0";
private String volunteerId;
private String ramark;
private String caId;
private String edu;
private String freeTime;
private String name;
private String tel;
private String company;
private String goodAtSkills;
private String govPersonId;
private String state;
private String reviewComm;
public String getVolWork() {
        return volWork;
    }
public void setVolWork(String volWork) {
        this.volWork = volWork;
    }
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getStudent() {
        return student;
    }
public void setStudent(String student) {
        this.student = student;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getVolunteerId() {
        return volunteerId;
    }
public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getEdu() {
        return edu;
    }
public void setEdu(String edu) {
        this.edu = edu;
    }
public String getFreeTime() {
        return freeTime;
    }
public void setFreeTime(String freeTime) {
        this.freeTime = freeTime;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getCompany() {
        return company;
    }
public void setCompany(String company) {
        this.company = company;
    }
public String getGoodAtSkills() {
        return goodAtSkills;
    }
public void setGoodAtSkills(String goodAtSkills) {
        this.goodAtSkills = goodAtSkills;
    }

    public String getGovPersonId() {
        return govPersonId;
    }

    public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getReviewComm() {
        return reviewComm;
    }

    public void setReviewComm(String reviewComm) {
        this.reviewComm = reviewComm;
    }
}
