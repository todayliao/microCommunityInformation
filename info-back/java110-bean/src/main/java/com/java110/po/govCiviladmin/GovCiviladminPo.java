package com.java110.po.govCiviladmin;

import java.io.Serializable;
import java.util.Date;

public class GovCiviladminPo implements Serializable {

    private String typeCd;
private String caId;
private String context;
private String civiladminId;
private String startTime;
private String statusCd = "0";
private String endTime;
private String state;
private String title;
private String userName;
private String userId;
private String headerImg;
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getCiviladminId() {
        return civiladminId;
    }
public void setCiviladminId(String civiladminId) {
        this.civiladminId = civiladminId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getTitle() {
        return title;
    }
public void setTitle(String title) {
        this.title = title;
    }
public String getUserName() {
        return userName;
    }
public void setUserName(String userName) {
        this.userName = userName;
    }
public String getUserId() {
        return userId;
    }
public void setUserId(String userId) {
        this.userId = userId;
    }
public String getHeaderImg() {
        return headerImg;
    }
public void setHeaderImg(String headerImg) {
        this.headerImg = headerImg;
    }



}
