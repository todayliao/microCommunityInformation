package com.java110.po.govSpecialFollow;

import java.io.Serializable;
import java.util.Date;

public class GovSpecialFollowPo implements Serializable {

    private String workPersonTel;
private String followId;
private String isWork;
private String caId;
private String specialPersonId;
private String name;
private String specialReason;
private String statusCd = "0";
private String workAddress;
private String workPerson;
private String ramark;
private String specialStartTime;
public String getWorkPersonTel() {
        return workPersonTel;
    }
public void setWorkPersonTel(String workPersonTel) {
        this.workPersonTel = workPersonTel;
    }
public String getFollowId() {
        return followId;
    }
public void setFollowId(String followId) {
        this.followId = followId;
    }
public String getIsWork() {
        return isWork;
    }
public void setIsWork(String isWork) {
        this.isWork = isWork;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSpecialPersonId() {
        return specialPersonId;
    }
public void setSpecialPersonId(String specialPersonId) {
        this.specialPersonId = specialPersonId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getSpecialReason() {
        return specialReason;
    }
public void setSpecialReason(String specialReason) {
        this.specialReason = specialReason;
    }

public String getWorkAddress() {
        return workAddress;
    }
public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }
public String getWorkPerson() {
        return workPerson;
    }
public void setWorkPerson(String workPerson) {
        this.workPerson = workPerson;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getSpecialStartTime() {
        return specialStartTime;
    }
public void setSpecialStartTime(String specialStartTime) {
        this.specialStartTime = specialStartTime;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
