package com.java110.po.uOrg;

import java.io.Serializable;
import java.util.Date;

public class UOrgPo implements Serializable {

    private String belongCommunityId;
private String orgName;
private String parentOrgId;
private String allowOperation;
private String description;
private String orgLevel;
private String statusCd = "0";
private String storeId;
private String orgId;
private String bId;
public String getBelongCommunityId() {
        return belongCommunityId;
    }
public void setBelongCommunityId(String belongCommunityId) {
        this.belongCommunityId = belongCommunityId;
    }
public String getOrgName() {
        return orgName;
    }
public void setOrgName(String orgName) {
        this.orgName = orgName;
    }
public String getParentOrgId() {
        return parentOrgId;
    }
public void setParentOrgId(String parentOrgId) {
        this.parentOrgId = parentOrgId;
    }
public String getAllowOperation() {
        return allowOperation;
    }
public void setAllowOperation(String allowOperation) {
        this.allowOperation = allowOperation;
    }
public String getDescription() {
        return description;
    }
public void setDescription(String description) {
        this.description = description;
    }
public String getOrgLevel() {
        return orgLevel;
    }
public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getStoreId() {
        return storeId;
    }
public void setStoreId(String storeId) {
        this.storeId = storeId;
    }
public String getOrgId() {
        return orgId;
    }
public void setOrgId(String orgId) {
        this.orgId = orgId;
    }

    public String getbId() {
        return bId;
    }

    public void setbId(String bId) {
        this.bId = bId;
    }
}
