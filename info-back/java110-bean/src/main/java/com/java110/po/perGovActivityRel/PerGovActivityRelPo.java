package com.java110.po.perGovActivityRel;

import java.io.Serializable;
import java.util.Date;

public class PerGovActivityRelPo implements Serializable {

    private String actRelId;
private String govPersonId;
private String caId;
private String perActivitiesId;
private String statusCd = "0";
private String state;
    private String govCommunityId;
public String getActRelId() {
        return actRelId;
    }
public void setActRelId(String actRelId) {
        this.actRelId = actRelId;
    }
public String getGovPersonId() {
        return govPersonId;
    }
public void setGovPersonId(String govPersonId) {
        this.govPersonId = govPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPerActivitiesId() {
        return perActivitiesId;
    }
public void setPerActivitiesId(String perActivitiesId) {
        this.perActivitiesId = perActivitiesId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getGovCommunityId() {
        return govCommunityId;
    }

    public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
}
