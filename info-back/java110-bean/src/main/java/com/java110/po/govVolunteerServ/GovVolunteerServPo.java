package com.java110.po.govVolunteerServ;

import java.io.Serializable;
import java.util.Date;

public class GovVolunteerServPo implements Serializable {

    private String servId;
private String caId;
private String name;
private String volunteerId;
private String ramark;
private String servFieldId;
public String getServId() {
        return servId;
    }
public void setServId(String servId) {
        this.servId = servId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getVolunteerId() {
        return volunteerId;
    }
public void setVolunteerId(String volunteerId) {
        this.volunteerId = volunteerId;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

    public String getServFieldId() {
        return servFieldId;
    }

    public void setServFieldId(String servFieldId) {
        this.servFieldId = servFieldId;
    }
}
