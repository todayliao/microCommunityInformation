package com.java110.po.govReleasePrison;

import java.io.Serializable;
import java.util.Date;

public class GovReleasePrisonPo implements Serializable {

    private String address;
private String releaseTime;
private String caId;
private String sentenceStartTime;
private String name;
private String sentenceReason;
private String tel;
private String sentenceEndTime;
private String ramark;
private String releasePrisonId;
public String getAddress() {
        return address;
    }
public void setAddress(String address) {
        this.address = address;
    }
public String getReleaseTime() {
        return releaseTime;
    }
public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getSentenceStartTime() {
        return sentenceStartTime;
    }
public void setSentenceStartTime(String sentenceStartTime) {
        this.sentenceStartTime = sentenceStartTime;
    }
public String getName() {
        return name;
    }
public void setName(String name) {
        this.name = name;
    }
public String getSentenceReason() {
        return sentenceReason;
    }
public void setSentenceReason(String sentenceReason) {
        this.sentenceReason = sentenceReason;
    }
public String getTel() {
        return tel;
    }
public void setTel(String tel) {
        this.tel = tel;
    }
public String getSentenceEndTime() {
        return sentenceEndTime;
    }
public void setSentenceEndTime(String sentenceEndTime) {
        this.sentenceEndTime = sentenceEndTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }
public String getReleasePrisonId() {
        return releasePrisonId;
    }
public void setReleasePrisonId(String releasePrisonId) {
        this.releasePrisonId = releasePrisonId;
    }

    private Date createTime;

    private String statusCd = "0";

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

        public String getStatusCd() {
            return statusCd;
        }

        public void setStatusCd(String statusCd) {
            this.statusCd = statusCd;
        }

}
