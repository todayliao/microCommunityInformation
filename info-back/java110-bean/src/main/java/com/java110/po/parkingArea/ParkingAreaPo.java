package com.java110.po.parkingArea;

import java.io.Serializable;
import java.util.Date;

public class ParkingAreaPo implements Serializable {

    private String typeCd;
private String govCommunityId;
private String caId;
private String num;
private String paId;
private String parkingCount;
private String remark;
private String statusCd = "0";
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getNum() {
        return num;
    }
public void setNum(String num) {
        this.num = num;
    }
public String getPaId() {
        return paId;
    }
public void setPaId(String paId) {
        this.paId = paId;
    }
public String getParkingCount() {
        return parkingCount;
    }
public void setParkingCount(String parkingCount) {
        this.parkingCount = parkingCount;
    }
public String getRemark() {
        return remark;
    }
public void setRemark(String remark) {
        this.remark = remark;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }



}
