package com.java110.po.govCiviladminType;

import java.io.Serializable;
import java.util.Date;

public class GovCiviladminTypePo implements Serializable {

    private String typeDesc;
private String typeCd;
private String caId;
private String typeName;
private String statusCd = "0";
private String defalutShow;
private String seq;
public String getTypeDesc() {
        return typeDesc;
    }
public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }
public String getTypeCd() {
        return typeCd;
    }
public void setTypeCd(String typeCd) {
        this.typeCd = typeCd;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getTypeName() {
        return typeName;
    }
public void setTypeName(String typeName) {
        this.typeName = typeName;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getDefalutShow() {
        return defalutShow;
    }
public void setDefalutShow(String defalutShow) {
        this.defalutShow = defalutShow;
    }
public String getSeq() {
        return seq;
    }
public void setSeq(String seq) {
        this.seq = seq;
    }



}
