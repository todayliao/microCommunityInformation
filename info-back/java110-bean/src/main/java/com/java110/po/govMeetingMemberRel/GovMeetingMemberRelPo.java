package com.java110.po.govMeetingMemberRel;

import java.io.Serializable;
import java.util.Date;

public class GovMeetingMemberRelPo implements Serializable {

    private String personName;
private String govMeetingId;
private String caId;
private String govMemberId;
private String statusCd = "0";
private String memberRelId;
    private String [] memberRelIds;
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getGovMeetingId() {
        return govMeetingId;
    }
public void setGovMeetingId(String govMeetingId) {
        this.govMeetingId = govMeetingId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getGovMemberId() {
        return govMemberId;
    }
public void setGovMemberId(String govMemberId) {
        this.govMemberId = govMemberId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getMemberRelId() {
        return memberRelId;
    }
public void setMemberRelId(String memberRelId) {
        this.memberRelId = memberRelId;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String[] getMemberRelIds() {
        return memberRelIds;
    }

    public void setMemberRelIds(String[] memberRelIds) {
        this.memberRelIds = memberRelIds;
    }
}
