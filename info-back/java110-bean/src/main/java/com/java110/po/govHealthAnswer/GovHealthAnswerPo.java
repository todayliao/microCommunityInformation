package com.java110.po.govHealthAnswer;

import java.io.Serializable;
import java.util.Date;

public class GovHealthAnswerPo implements Serializable {

    private String healthPersonId;
private String caId;
private String healthId;
private String personId;
private String statusCd = "0";
private String state;
public String getHealthPersonId() {
        return healthPersonId;
    }
public void setHealthPersonId(String healthPersonId) {
        this.healthPersonId = healthPersonId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getHealthId() {
        return healthId;
    }
public void setHealthId(String healthId) {
        this.healthId = healthId;
    }
public String getPersonId() {
        return personId;
    }
public void setPersonId(String personId) {
        this.personId = personId;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }



}
