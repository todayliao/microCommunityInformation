package com.java110.po.govReportUser;

import java.io.Serializable;
import java.util.Date;

public class GovReportUserPo implements Serializable {

    private String reportId;
private String preStaffName;
private String reportEvent;
private String statusCd = "0";
private String ruId;
private String caId;
private String preStaffId;
private String context;
private String staffName;
private String preRuId;
private String startTime;
private String state;
private String endTime;
private String staffId;
public String getReportId() {
        return reportId;
    }
public void setReportId(String reportId) {
        this.reportId = reportId;
    }
public String getPreStaffName() {
        return preStaffName;
    }
public void setPreStaffName(String preStaffName) {
        this.preStaffName = preStaffName;
    }
public String getReportEvent() {
        return reportEvent;
    }
public void setReportEvent(String reportEvent) {
        this.reportEvent = reportEvent;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getRuId() {
        return ruId;
    }
public void setRuId(String ruId) {
        this.ruId = ruId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getPreStaffId() {
        return preStaffId;
    }
public void setPreStaffId(String preStaffId) {
        this.preStaffId = preStaffId;
    }
public String getContext() {
        return context;
    }
public void setContext(String context) {
        this.context = context;
    }
public String getStaffName() {
        return staffName;
    }
public void setStaffName(String staffName) {
        this.staffName = staffName;
    }
public String getPreRuId() {
        return preRuId;
    }
public void setPreRuId(String preRuId) {
        this.preRuId = preRuId;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getState() {
        return state;
    }
public void setState(String state) {
        this.state = state;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getStaffId() {
        return staffId;
    }
public void setStaffId(String staffId) {
        this.staffId = staffId;
    }



}
