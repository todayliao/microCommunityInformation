package com.java110.po.govGridObjRel;

import java.io.Serializable;
import java.util.Date;

public class GovGridObjRelPo implements Serializable {

    private String workStatus;
private String govGridId;
private String govGridObjId;
private String caId;
private String objTypeName;
private String objId;
private String objName;
private String objTypeCd;
private String startTime;
private String statusCd = "0";
private String endTime;
private String ramark;
public String getWorkStatus() {
        return workStatus;
    }
public void setWorkStatus(String workStatus) {
        this.workStatus = workStatus;
    }
public String getGovGridId() {
        return govGridId;
    }
public void setGovGridId(String govGridId) {
        this.govGridId = govGridId;
    }
public String getGovGridObjId() {
        return govGridObjId;
    }
public void setGovGridObjId(String govGridObjId) {
        this.govGridObjId = govGridObjId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getObjTypeName() {
        return objTypeName;
    }
public void setObjTypeName(String objTypeName) {
        this.objTypeName = objTypeName;
    }
public String getObjId() {
        return objId;
    }
public void setObjId(String objId) {
        this.objId = objId;
    }
public String getObjTypeCd() {
        return objTypeCd;
    }
public void setObjTypeCd(String objTypeCd) {
        this.objTypeCd = objTypeCd;
    }
public String getStartTime() {
        return startTime;
    }
public void setStartTime(String startTime) {
        this.startTime = startTime;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getEndTime() {
        return endTime;
    }
public void setEndTime(String endTime) {
        this.endTime = endTime;
    }
public String getRamark() {
        return ramark;
    }
public void setRamark(String ramark) {
        this.ramark = ramark;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }

    public String getObjName() {
        return objName;
    }

    public void setObjName(String objName) {
        this.objName = objName;
    }
}
