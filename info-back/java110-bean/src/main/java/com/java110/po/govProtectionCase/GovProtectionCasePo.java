package com.java110.po.govProtectionCase;

import java.io.Serializable;
import java.util.Date;

public class GovProtectionCasePo implements Serializable {

    private String caseCode;
private String caseContent;
private String idType;
private String happenedTime;
private String govCommunityId;
private String idCard;
private String arrestsNum;
private String happenedPlace;
private String statusCd = "0";
private String objName;
private String personNum;
private String caseType;
private String personName;
private String areaCode;
private String fleeingNum;
private String isSolved;
private String caseId;
private String caId;
private String objId;
private String caseName;
private String detectionContent;
private String objType;
public String getCaseCode() {
        return caseCode;
    }
public void setCaseCode(String caseCode) {
        this.caseCode = caseCode;
    }
public String getCaseContent() {
        return caseContent;
    }
public void setCaseContent(String caseContent) {
        this.caseContent = caseContent;
    }
public String getIdType() {
        return idType;
    }
public void setIdType(String idType) {
        this.idType = idType;
    }
public String getHappenedTime() {
        return happenedTime;
    }
public void setHappenedTime(String happenedTime) {
        this.happenedTime = happenedTime;
    }
public String getGovCommunityId() {
        return govCommunityId;
    }
public void setGovCommunityId(String govCommunityId) {
        this.govCommunityId = govCommunityId;
    }
public String getIdCard() {
        return idCard;
    }
public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
public String getArrestsNum() {
        return arrestsNum;
    }
public void setArrestsNum(String arrestsNum) {
        this.arrestsNum = arrestsNum;
    }
public String getHappenedPlace() {
        return happenedPlace;
    }
public void setHappenedPlace(String happenedPlace) {
        this.happenedPlace = happenedPlace;
    }
public String getStatusCd() {
        return statusCd;
    }
public void setStatusCd(String statusCd) {
        this.statusCd = statusCd;
    }
public String getObjName() {
        return objName;
    }
public void setObjName(String objName) {
        this.objName = objName;
    }
public String getPersonNum() {
        return personNum;
    }
public void setPersonNum(String personNum) {
        this.personNum = personNum;
    }
public String getCaseType() {
        return caseType;
    }
public void setCaseType(String caseType) {
        this.caseType = caseType;
    }
public String getPersonName() {
        return personName;
    }
public void setPersonName(String personName) {
        this.personName = personName;
    }
public String getAreaCode() {
        return areaCode;
    }
public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }
public String getFleeingNum() {
        return fleeingNum;
    }
public void setFleeingNum(String fleeingNum) {
        this.fleeingNum = fleeingNum;
    }
public String getIsSolved() {
        return isSolved;
    }
public void setIsSolved(String isSolved) {
        this.isSolved = isSolved;
    }
public String getCaseId() {
        return caseId;
    }
public void setCaseId(String caseId) {
        this.caseId = caseId;
    }
public String getCaId() {
        return caId;
    }
public void setCaId(String caId) {
        this.caId = caId;
    }
public String getObjId() {
        return objId;
    }
public void setObjId(String objId) {
        this.objId = objId;
    }
public String getCaseName() {
        return caseName;
    }
public void setCaseName(String caseName) {
        this.caseName = caseName;
    }
public String getDetectionContent() {
        return detectionContent;
    }
public void setDetectionContent(String detectionContent) {
        this.detectionContent = detectionContent;
    }
public String getObjType() {
        return objType;
    }
public void setObjType(String objType) {
        this.objType = objType;
    }

    private Date createTime;

     public Date getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Date createTime) {
            this.createTime = createTime;
        }


}
