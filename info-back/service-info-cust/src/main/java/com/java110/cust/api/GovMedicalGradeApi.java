package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMedicalGrade.GovMedicalGradeDto;
import com.java110.po.govMedicalGrade.GovMedicalGradePo;
import com.java110.cust.bmo.govMedicalGrade.IDeleteGovMedicalGradeBMO;
import com.java110.cust.bmo.govMedicalGrade.IGetGovMedicalGradeBMO;
import com.java110.cust.bmo.govMedicalGrade.ISaveGovMedicalGradeBMO;
import com.java110.cust.bmo.govMedicalGrade.IUpdateGovMedicalGradeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMedicalGrade")
public class GovMedicalGradeApi {

    @Autowired
    private ISaveGovMedicalGradeBMO saveGovMedicalGradeBMOImpl;
    @Autowired
    private IUpdateGovMedicalGradeBMO updateGovMedicalGradeBMOImpl;
    @Autowired
    private IDeleteGovMedicalGradeBMO deleteGovMedicalGradeBMOImpl;

    @Autowired
    private IGetGovMedicalGradeBMO getGovMedicalGradeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGrade/saveGovMedicalGrade
     * @path /app/govMedicalGrade/saveGovMedicalGrade
     */
    @RequestMapping(value = "/saveGovMedicalGrade", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMedicalGrade(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "medicalClassifyId", "请求报文中未包含medicalClassifyId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "gradeName", "请求报文中未包含gradeName");
        Assert.hasKeyAndValue(reqJson, "gradeType", "请求报文中未包含gradeType");


        GovMedicalGradePo govMedicalGradePo = BeanConvertUtil.covertBean(reqJson, GovMedicalGradePo.class);
        return saveGovMedicalGradeBMOImpl.save(govMedicalGradePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGrade/updateGovMedicalGrade
     * @path /app/govMedicalGrade/updateGovMedicalGrade
     */
    @RequestMapping(value = "/updateGovMedicalGrade", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMedicalGrade(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "medicalGradeId", "请求报文中未包含medicalGradeId");
        Assert.hasKeyAndValue(reqJson, "medicalClassifyId", "请求报文中未包含medicalClassifyId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "gradeName", "请求报文中未包含gradeName");
        Assert.hasKeyAndValue(reqJson, "gradeType", "请求报文中未包含gradeType");

        GovMedicalGradePo govMedicalGradePo = BeanConvertUtil.covertBean(reqJson, GovMedicalGradePo.class);
        return updateGovMedicalGradeBMOImpl.update(govMedicalGradePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGrade/deleteGovMedicalGrade
     * @path /app/govMedicalGrade/deleteGovMedicalGrade
     */
    @RequestMapping(value = "/deleteGovMedicalGrade", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMedicalGrade(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "medicalGradeId", "medicalGradeId不能为空");

        GovMedicalGradePo govMedicalGradePo = BeanConvertUtil.covertBean(reqJson, GovMedicalGradePo.class);
        return deleteGovMedicalGradeBMOImpl.delete(govMedicalGradePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMedicalGrade/queryGovMedicalGrade
     * @path /app/govMedicalGrade/queryGovMedicalGrade
     */
    @RequestMapping(value = "/queryGovMedicalGrade", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMedicalGrade(@RequestParam(value = "caId") String caId,
                                                       @RequestParam(value = "gradeName",required = false) String gradeName,
                                                       @RequestParam(value = "classifyName",required = false) String classifyName,
                                                       @RequestParam(value = "medicalClassifyId",required = false) String medicalClassifyId,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {
        GovMedicalGradeDto govMedicalGradeDto = new GovMedicalGradeDto();
        govMedicalGradeDto.setPage(page);
        govMedicalGradeDto.setRow(row);
        govMedicalGradeDto.setCaId(caId);
        govMedicalGradeDto.setGradeName(gradeName);
        govMedicalGradeDto.setClassifyName(classifyName);
        govMedicalGradeDto.setMedicalClassifyId(medicalClassifyId);
        return getGovMedicalGradeBMOImpl.get(govMedicalGradeDto);
    }
}
