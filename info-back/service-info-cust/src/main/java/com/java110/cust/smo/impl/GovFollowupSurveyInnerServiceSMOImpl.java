package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovFollowupSurveyServiceDao;
import com.java110.intf.cust.IGovFollowupSurveyInnerServiceSMO;
import com.java110.dto.govFollowupSurvey.GovFollowupSurveyDto;
import com.java110.po.govFollowupSurvey.GovFollowupSurveyPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 随访登记内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovFollowupSurveyInnerServiceSMOImpl extends BaseServiceSMO implements IGovFollowupSurveyInnerServiceSMO {

    @Autowired
    private IGovFollowupSurveyServiceDao govFollowupSurveyServiceDaoImpl;


    @Override
    public int saveGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo) {
        int saveFlag = 1;
        govFollowupSurveyServiceDaoImpl.saveGovFollowupSurveyInfo(BeanConvertUtil.beanCovertMap(govFollowupSurveyPo));
        return saveFlag;
    }

     @Override
    public int updateGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo) {
        int saveFlag = 1;
         govFollowupSurveyServiceDaoImpl.updateGovFollowupSurveyInfo(BeanConvertUtil.beanCovertMap(govFollowupSurveyPo));
        return saveFlag;
    }

     @Override
    public int deleteGovFollowupSurvey(@RequestBody  GovFollowupSurveyPo govFollowupSurveyPo) {
        int saveFlag = 1;
        govFollowupSurveyPo.setStatusCd("1");
        govFollowupSurveyServiceDaoImpl.updateGovFollowupSurveyInfo(BeanConvertUtil.beanCovertMap(govFollowupSurveyPo));
        return saveFlag;
    }

    @Override
    public List<GovFollowupSurveyDto> queryGovFollowupSurveys(@RequestBody  GovFollowupSurveyDto govFollowupSurveyDto) {

        //校验是否传了 分页信息

        int page = govFollowupSurveyDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govFollowupSurveyDto.setPage((page - 1) * govFollowupSurveyDto.getRow());
        }

        List<GovFollowupSurveyDto> govFollowupSurveys = BeanConvertUtil.covertBeanList(govFollowupSurveyServiceDaoImpl.getGovFollowupSurveyInfo(BeanConvertUtil.beanCovertMap(govFollowupSurveyDto)), GovFollowupSurveyDto.class);

        return govFollowupSurveys;
    }


    @Override
    public int queryGovFollowupSurveysCount(@RequestBody GovFollowupSurveyDto govFollowupSurveyDto) {
        return govFollowupSurveyServiceDaoImpl.queryGovFollowupSurveysCount(BeanConvertUtil.beanCovertMap(govFollowupSurveyDto));    }

    public IGovFollowupSurveyServiceDao getGovFollowupSurveyServiceDaoImpl() {
        return govFollowupSurveyServiceDaoImpl;
    }

    public void setGovFollowupSurveyServiceDaoImpl(IGovFollowupSurveyServiceDao govFollowupSurveyServiceDaoImpl) {
        this.govFollowupSurveyServiceDaoImpl = govFollowupSurveyServiceDaoImpl;
    }
}
