package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovHomicideBasicServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 命案基本信息服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHomicideBasicServiceDaoImpl")
//@Transactional
public class GovHomicideBasicServiceDaoImpl extends BaseServiceDao implements IGovHomicideBasicServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHomicideBasicServiceDaoImpl.class);





    /**
     * 保存命案基本信息信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHomicideBasicInfo(Map info) throws DAOException {
        logger.debug("保存命案基本信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHomicideBasicServiceDaoImpl.saveGovHomicideBasicInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存命案基本信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询命案基本信息信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHomicideBasicInfo(Map info) throws DAOException {
        logger.debug("查询命案基本信息信息 入参 info : {}",info);

        List<Map> businessGovHomicideBasicInfos = sqlSessionTemplate.selectList("govHomicideBasicServiceDaoImpl.getGovHomicideBasicInfo",info);

        return businessGovHomicideBasicInfos;
    }


    /**
     * 修改命案基本信息信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHomicideBasicInfo(Map info) throws DAOException {
        logger.debug("修改命案基本信息信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHomicideBasicServiceDaoImpl.updateGovHomicideBasicInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改命案基本信息信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询命案基本信息数量
     * @param info 命案基本信息信息
     * @return 命案基本信息数量
     */
    @Override
    public int queryGovHomicideBasicsCount(Map info) {
        logger.debug("查询命案基本信息数据 入参 info : {}",info);

        List<Map> businessGovHomicideBasicInfos = sqlSessionTemplate.selectList("govHomicideBasicServiceDaoImpl.queryGovHomicideBasicsCount", info);
        if (businessGovHomicideBasicInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHomicideBasicInfos.get(0).get("count").toString());
    }


}
