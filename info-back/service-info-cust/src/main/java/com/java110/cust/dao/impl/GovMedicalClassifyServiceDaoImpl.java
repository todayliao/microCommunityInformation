package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovMedicalClassifyServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 医疗分类服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMedicalClassifyServiceDaoImpl")
//@Transactional
public class GovMedicalClassifyServiceDaoImpl extends BaseServiceDao implements IGovMedicalClassifyServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMedicalClassifyServiceDaoImpl.class);





    /**
     * 保存医疗分类信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMedicalClassifyInfo(Map info) throws DAOException {
        logger.debug("保存医疗分类信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMedicalClassifyServiceDaoImpl.saveGovMedicalClassifyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存医疗分类信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询医疗分类信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMedicalClassifyInfo(Map info) throws DAOException {
        logger.debug("查询医疗分类信息 入参 info : {}",info);

        List<Map> businessGovMedicalClassifyInfos = sqlSessionTemplate.selectList("govMedicalClassifyServiceDaoImpl.getGovMedicalClassifyInfo",info);

        return businessGovMedicalClassifyInfos;
    }


    /**
     * 修改医疗分类信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMedicalClassifyInfo(Map info) throws DAOException {
        logger.debug("修改医疗分类信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMedicalClassifyServiceDaoImpl.updateGovMedicalClassifyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改医疗分类信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询医疗分类数量
     * @param info 医疗分类信息
     * @return 医疗分类数量
     */
    @Override
    public int queryGovMedicalClassifysCount(Map info) {
        logger.debug("查询医疗分类数据 入参 info : {}",info);

        List<Map> businessGovMedicalClassifyInfos = sqlSessionTemplate.selectList("govMedicalClassifyServiceDaoImpl.queryGovMedicalClassifysCount", info);
        if (businessGovMedicalClassifyInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMedicalClassifyInfos.get(0).get("count").toString());
    }


}
