package com.java110.cust.bmo.govHealthTitleValue;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovHealthTitleValueBMO {


    /**
     * 查询体检项值
     * add by wuxw
     * @param  govHealthTitleValueDto
     * @return
     */
    ResponseEntity<String> get(GovHealthTitleValueDto govHealthTitleValueDto);


}
