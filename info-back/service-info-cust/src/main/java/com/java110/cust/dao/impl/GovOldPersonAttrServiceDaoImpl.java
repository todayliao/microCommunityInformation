package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovOldPersonAttrServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 老人属性服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govOldPersonAttrServiceDaoImpl")
//@Transactional
public class GovOldPersonAttrServiceDaoImpl extends BaseServiceDao implements IGovOldPersonAttrServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovOldPersonAttrServiceDaoImpl.class);





    /**
     * 保存老人属性信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovOldPersonAttrInfo(Map info) throws DAOException {
        logger.debug("保存老人属性信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govOldPersonAttrServiceDaoImpl.saveGovOldPersonAttrInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存老人属性信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询老人属性信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOldPersonAttrInfo(Map info) throws DAOException {
        logger.debug("查询老人属性信息 入参 info : {}",info);

        List<Map> businessGovOldPersonAttrInfos = sqlSessionTemplate.selectList("govOldPersonAttrServiceDaoImpl.getGovOldPersonAttrInfo",info);

        return businessGovOldPersonAttrInfos;
    }


    /**
     * 修改老人属性信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovOldPersonAttrInfo(Map info) throws DAOException {
        logger.debug("修改老人属性信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govOldPersonAttrServiceDaoImpl.updateGovOldPersonAttrInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改老人属性信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询老人属性数量
     * @param info 老人属性信息
     * @return 老人属性数量
     */
    @Override
    public int queryGovOldPersonAttrsCount(Map info) {
        logger.debug("查询老人属性数据 入参 info : {}",info);

        List<Map> businessGovOldPersonAttrInfos = sqlSessionTemplate.selectList("govOldPersonAttrServiceDaoImpl.queryGovOldPersonAttrsCount", info);
        if (businessGovOldPersonAttrInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOldPersonAttrInfos.get(0).get("count").toString());
    }


}
