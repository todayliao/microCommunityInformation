package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovPersonServiceDao;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 人口管理内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPersonInnerServiceSMOImpl extends BaseServiceSMO implements IGovPersonInnerServiceSMO {

    @Autowired
    private IGovPersonServiceDao govPersonServiceDaoImpl;


    @Override
    public int saveGovPerson(@RequestBody GovPersonPo govPersonPo) {
        int saveFlag = 1;
        govPersonServiceDaoImpl.saveGovPersonInfo(BeanConvertUtil.beanCovertMap(govPersonPo));
        return saveFlag;
    }

     @Override
    public int updateGovPerson(@RequestBody  GovPersonPo govPersonPo) {
        int saveFlag = 1;
         govPersonServiceDaoImpl.updateGovPersonInfo(BeanConvertUtil.beanCovertMap(govPersonPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPerson(@RequestBody  GovPersonPo govPersonPo) {
        int saveFlag = 1;
        govPersonPo.setStatusCd("1");
        govPersonServiceDaoImpl.updateGovPersonInfo(BeanConvertUtil.beanCovertMap(govPersonPo));
        return saveFlag;
    }

    @Override
    public List<GovPersonDto> queryGovPersons(@RequestBody  GovPersonDto govPersonDto) {

        //校验是否传了 分页信息

        int page = govPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDto.setPage((page - 1) * govPersonDto.getRow());
        }

        List<GovPersonDto> govPersons = BeanConvertUtil.covertBeanList(govPersonServiceDaoImpl.getGovPersonInfo(BeanConvertUtil.beanCovertMap(govPersonDto)), GovPersonDto.class);

        return govPersons;
    }
    @Override
    public List<GovPersonDto> getGovPersonHomicide(@RequestBody  GovPersonDto govPersonDto) {

        //校验是否传了 分页信息

        int page = govPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDto.setPage((page - 1) * govPersonDto.getRow());
        }

        List<GovPersonDto> govPersons = BeanConvertUtil.covertBeanList(govPersonServiceDaoImpl.getGovPersonHomicide(BeanConvertUtil.beanCovertMap(govPersonDto)), GovPersonDto.class);

        return govPersons;
    }
    @Override
    public List<GovPersonDto> getGovPersonHelpPolicy(@RequestBody  GovPersonDto govPersonDto) {

        //校验是否传了 分页信息

        int page = govPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDto.setPage((page - 1) * govPersonDto.getRow());
        }

        List<GovPersonDto> govPersons = BeanConvertUtil.covertBeanList(govPersonServiceDaoImpl.getGovPersonHelpPolicy(BeanConvertUtil.beanCovertMap(govPersonDto)), GovPersonDto.class);

        return govPersons;
    }


    @Override
    public int queryGovPersonsCount(@RequestBody GovPersonDto govPersonDto) {
        return govPersonServiceDaoImpl.queryGovPersonsCount(BeanConvertUtil.beanCovertMap(govPersonDto));    }
    @Override
    public int getGovPersonHelpPolicyCount(@RequestBody GovPersonDto govPersonDto) {
        return govPersonServiceDaoImpl.getGovPersonHelpPolicyCount(BeanConvertUtil.beanCovertMap(govPersonDto));    }

    @Override
    public int getGovPersonHomicideCount(@RequestBody GovPersonDto govPersonDto) {
        return govPersonServiceDaoImpl.getGovPersonHomicideCount(BeanConvertUtil.beanCovertMap(govPersonDto));    }
    @Override
    public List<GovPersonDto> getGovPersonCompany(@RequestBody  GovPersonDto govPersonDto) {

        //校验是否传了 分页信息

        int page = govPersonDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDto.setPage((page - 1) * govPersonDto.getRow());
        }

        List<GovPersonDto> govPersons = BeanConvertUtil.covertBeanList(govPersonServiceDaoImpl.getGovPersonCompanyInfo(BeanConvertUtil.beanCovertMap(govPersonDto)), GovPersonDto.class);

        return govPersons;
    }


    @Override
    public int getGovPersonCompanyCount(@RequestBody GovPersonDto govPersonDto) {
        return govPersonServiceDaoImpl.getGovPersonCompanyCount(BeanConvertUtil.beanCovertMap(govPersonDto));    }

    public IGovPersonServiceDao getGovPersonServiceDaoImpl() {
        return govPersonServiceDaoImpl;
    }

    public void setGovPersonServiceDaoImpl(IGovPersonServiceDao govPersonServiceDaoImpl) {
        this.govPersonServiceDaoImpl = govPersonServiceDaoImpl;
    }
}
