package com.java110.cust.bmo.govMajorEvents.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMajorEvents.IGetGovMajorEventsBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMajorEventsInnerServiceSMO;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMajorEventsBMOImpl")
public class GetGovMajorEventsBMOImpl implements IGetGovMajorEventsBMO {

    @Autowired
    private IGovMajorEventsInnerServiceSMO govMajorEventsInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMajorEventsDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMajorEventsDto govMajorEventsDto) {


        int count = govMajorEventsInnerServiceSMOImpl.queryGovMajorEventssCount(govMajorEventsDto);

        List<GovMajorEventsDto> govMajorEventsDtos = null;
        if (count > 0) {
            govMajorEventsDtos = govMajorEventsInnerServiceSMOImpl.queryGovMajorEventss(govMajorEventsDto);
        } else {
            govMajorEventsDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMajorEventsDto.getRow()), count, govMajorEventsDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
