package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 医疗团队组件内部之间使用，没有给外围系统提供服务能力
 * 医疗团队服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovMedicalGroupServiceDao {


    /**
     * 保存 医疗团队信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovMedicalGroupInfo(Map info) throws DAOException;




    /**
     * 查询医疗团队信息（instance过程）
     * 根据bId 查询医疗团队信息
     * @param info bId 信息
     * @return 医疗团队信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovMedicalGroupInfo(Map info) throws DAOException;



    /**
     * 修改医疗团队信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovMedicalGroupInfo(Map info) throws DAOException;


    /**
     * 查询医疗团队总数
     *
     * @param info 医疗团队信息
     * @return 医疗团队数量
     */
    int queryGovMedicalGroupsCount(Map info);

}
