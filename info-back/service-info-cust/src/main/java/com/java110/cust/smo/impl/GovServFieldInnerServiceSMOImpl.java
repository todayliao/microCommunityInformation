package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovServFieldServiceDao;
import com.java110.intf.cust.IGovServFieldInnerServiceSMO;
import com.java110.dto.govServField.GovServFieldDto;
import com.java110.po.govServField.GovServFieldPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 服务领域内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovServFieldInnerServiceSMOImpl extends BaseServiceSMO implements IGovServFieldInnerServiceSMO {

    @Autowired
    private IGovServFieldServiceDao govServFieldServiceDaoImpl;


    @Override
    public int saveGovServField(@RequestBody  GovServFieldPo govServFieldPo) {
        int saveFlag = 1;
        govServFieldServiceDaoImpl.saveGovServFieldInfo(BeanConvertUtil.beanCovertMap(govServFieldPo));
        return saveFlag;
    }

     @Override
    public int updateGovServField(@RequestBody  GovServFieldPo govServFieldPo) {
        int saveFlag = 1;
         govServFieldServiceDaoImpl.updateGovServFieldInfo(BeanConvertUtil.beanCovertMap(govServFieldPo));
        return saveFlag;
    }

     @Override
    public int deleteGovServField(@RequestBody  GovServFieldPo govServFieldPo) {
        int saveFlag = 1;
        govServFieldPo.setStatusCd("1");
        govServFieldServiceDaoImpl.updateGovServFieldInfo(BeanConvertUtil.beanCovertMap(govServFieldPo));
        return saveFlag;
    }

    @Override
    public List<GovServFieldDto> queryGovServFields(@RequestBody  GovServFieldDto govServFieldDto) {

        //校验是否传了 分页信息

        int page = govServFieldDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govServFieldDto.setPage((page - 1) * govServFieldDto.getRow());
        }

        List<GovServFieldDto> govServFields = BeanConvertUtil.covertBeanList(govServFieldServiceDaoImpl.getGovServFieldInfo(BeanConvertUtil.beanCovertMap(govServFieldDto)), GovServFieldDto.class);

        return govServFields;
    }


    @Override
    public int queryGovServFieldsCount(@RequestBody GovServFieldDto govServFieldDto) {
        return govServFieldServiceDaoImpl.queryGovServFieldsCount(BeanConvertUtil.beanCovertMap(govServFieldDto));    }

    public IGovServFieldServiceDao getGovServFieldServiceDaoImpl() {
        return govServFieldServiceDaoImpl;
    }

    public void setGovServFieldServiceDaoImpl(IGovServFieldServiceDao govServFieldServiceDaoImpl) {
        this.govServFieldServiceDaoImpl = govServFieldServiceDaoImpl;
    }
}
