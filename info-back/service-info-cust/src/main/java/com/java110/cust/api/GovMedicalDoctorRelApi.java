package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMedicalDoctorRel.GovMedicalDoctorRelDto;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import com.java110.cust.bmo.govMedicalDoctorRel.IDeleteGovMedicalDoctorRelBMO;
import com.java110.cust.bmo.govMedicalDoctorRel.IGetGovMedicalDoctorRelBMO;
import com.java110.cust.bmo.govMedicalDoctorRel.ISaveGovMedicalDoctorRelBMO;
import com.java110.cust.bmo.govMedicalDoctorRel.IUpdateGovMedicalDoctorRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMedicalDoctorRel")
public class GovMedicalDoctorRelApi {

    @Autowired
    private ISaveGovMedicalDoctorRelBMO saveGovMedicalDoctorRelBMOImpl;
    @Autowired
    private IUpdateGovMedicalDoctorRelBMO updateGovMedicalDoctorRelBMOImpl;
    @Autowired
    private IDeleteGovMedicalDoctorRelBMO deleteGovMedicalDoctorRelBMOImpl;

    @Autowired
    private IGetGovMedicalDoctorRelBMO getGovMedicalDoctorRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalDoctorRel/saveGovMedicalDoctorRel
     * @path /app/govMedicalDoctorRel/saveGovMedicalDoctorRel
     */
    @RequestMapping(value = "/saveGovMedicalDoctorRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMedicalDoctorRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "groupId", "请求报文中未包含groupId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "govDoctorId", "请求报文中未包含govDoctorId" );


        GovMedicalDoctorRelPo govMedicalDoctorRelPo = BeanConvertUtil.covertBean( reqJson, GovMedicalDoctorRelPo.class );
        return saveGovMedicalDoctorRelBMOImpl.save( govMedicalDoctorRelPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalDoctorRel/updateGovMedicalDoctorRel
     * @path /app/govMedicalDoctorRel/updateGovMedicalDoctorRel
     */
    @RequestMapping(value = "/updateGovMedicalDoctorRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMedicalDoctorRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "relId", "请求报文中未包含relId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "groupId", "请求报文中未包含groupId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "govDoctorId", "请求报文中未包含govDoctorId" );


        GovMedicalDoctorRelPo govMedicalDoctorRelPo = BeanConvertUtil.covertBean( reqJson, GovMedicalDoctorRelPo.class );
        return updateGovMedicalDoctorRelBMOImpl.update( govMedicalDoctorRelPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalDoctorRel/deleteGovMedicalDoctorRel
     * @path /app/govMedicalDoctorRel/deleteGovMedicalDoctorRel
     */
    @RequestMapping(value = "/deleteGovMedicalDoctorRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMedicalDoctorRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "relId", "relId不能为空" );


        GovMedicalDoctorRelPo govMedicalDoctorRelPo = BeanConvertUtil.covertBean( reqJson, GovMedicalDoctorRelPo.class );
        return deleteGovMedicalDoctorRelBMOImpl.delete( govMedicalDoctorRelPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govMedicalDoctorRel/queryGovMedicalDoctorRel
     * @path /app/govMedicalDoctorRel/queryGovMedicalDoctorRel
     */
    @RequestMapping(value = "/queryGovMedicalDoctorRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMedicalDoctorRel(@RequestParam(value = "caId") String caId,
                                                           @RequestParam(value = "page") int page,
                                                           @RequestParam(value = "row") int row) {

        GovMedicalDoctorRelDto govMedicalDoctorRelDto = new GovMedicalDoctorRelDto();
        govMedicalDoctorRelDto.setPage( page );
        govMedicalDoctorRelDto.setRow( row );
        govMedicalDoctorRelDto.setCaId( caId );
        return getGovMedicalDoctorRelBMOImpl.get( govMedicalDoctorRelDto );
    }
}
