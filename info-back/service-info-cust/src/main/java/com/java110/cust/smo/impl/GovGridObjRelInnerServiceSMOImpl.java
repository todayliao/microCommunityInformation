package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovGridObjRelServiceDao;
import com.java110.intf.cust.IGovGridObjRelInnerServiceSMO;
import com.java110.dto.govGridObjRel.GovGridObjRelDto;
import com.java110.po.govGridObjRel.GovGridObjRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 网格对象关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovGridObjRelInnerServiceSMOImpl extends BaseServiceSMO implements IGovGridObjRelInnerServiceSMO {

    @Autowired
    private IGovGridObjRelServiceDao govGridObjRelServiceDaoImpl;


    @Override
    public int saveGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo) {
        int saveFlag = 1;
        govGridObjRelServiceDaoImpl.saveGovGridObjRelInfo(BeanConvertUtil.beanCovertMap(govGridObjRelPo));
        return saveFlag;
    }

     @Override
    public int updateGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo) {
        int saveFlag = 1;
         govGridObjRelServiceDaoImpl.updateGovGridObjRelInfo(BeanConvertUtil.beanCovertMap(govGridObjRelPo));
        return saveFlag;
    }

     @Override
    public int deleteGovGridObjRel(@RequestBody  GovGridObjRelPo govGridObjRelPo) {
        int saveFlag = 1;
        govGridObjRelPo.setStatusCd("1");
        govGridObjRelServiceDaoImpl.updateGovGridObjRelInfo(BeanConvertUtil.beanCovertMap(govGridObjRelPo));
        return saveFlag;
    }

    @Override
    public List<GovGridObjRelDto> queryGovGridObjRels(@RequestBody  GovGridObjRelDto govGridObjRelDto) {

        //校验是否传了 分页信息

        int page = govGridObjRelDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govGridObjRelDto.setPage((page - 1) * govGridObjRelDto.getRow());
        }

        List<GovGridObjRelDto> govGridObjRels = BeanConvertUtil.covertBeanList(govGridObjRelServiceDaoImpl.getGovGridObjRelInfo(BeanConvertUtil.beanCovertMap(govGridObjRelDto)), GovGridObjRelDto.class);

        return govGridObjRels;
    }


    @Override
    public int queryGovGridObjRelsCount(@RequestBody GovGridObjRelDto govGridObjRelDto) {
        return govGridObjRelServiceDaoImpl.queryGovGridObjRelsCount(BeanConvertUtil.beanCovertMap(govGridObjRelDto));    }

    public IGovGridObjRelServiceDao getGovGridObjRelServiceDaoImpl() {
        return govGridObjRelServiceDaoImpl;
    }

    public void setGovGridObjRelServiceDaoImpl(IGovGridObjRelServiceDao govGridObjRelServiceDaoImpl) {
        this.govGridObjRelServiceDaoImpl = govGridObjRelServiceDaoImpl;
    }
}
