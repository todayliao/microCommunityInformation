package com.java110.cust.bmo.govOldPersonAttr;
import org.springframework.http.ResponseEntity;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;

public interface IUpdateGovOldPersonAttrBMO {


    /**
     * 修改老人属性
     * add by wuxw
     * @param govOldPersonAttrPo
     * @return
     */
    ResponseEntity<String> update(GovOldPersonAttrPo govOldPersonAttrPo);


}
