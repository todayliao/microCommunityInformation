package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthTermServiceDao;
import com.java110.intf.cust.IGovHealthTermInnerServiceSMO;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
import com.java110.po.govHealthTerm.GovHealthTermPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检项内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthTermInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthTermInnerServiceSMO {

    @Autowired
    private IGovHealthTermServiceDao govHealthTermServiceDaoImpl;


    @Override
    public int saveGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo) {
        int saveFlag = 1;
        govHealthTermServiceDaoImpl.saveGovHealthTermInfo(BeanConvertUtil.beanCovertMap(govHealthTermPo));
        return saveFlag;
    }

     @Override
    public int updateGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo) {
        int saveFlag = 1;
         govHealthTermServiceDaoImpl.updateGovHealthTermInfo(BeanConvertUtil.beanCovertMap(govHealthTermPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealthTerm(@RequestBody  GovHealthTermPo govHealthTermPo) {
        int saveFlag = 1;
        govHealthTermPo.setStatusCd("1");
        govHealthTermServiceDaoImpl.updateGovHealthTermInfo(BeanConvertUtil.beanCovertMap(govHealthTermPo));
        return saveFlag;
    }

    @Override
    public List<GovHealthTermDto> queryGovHealthTerms(@RequestBody  GovHealthTermDto govHealthTermDto) {

        //校验是否传了 分页信息

        int page = govHealthTermDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthTermDto.setPage((page - 1) * govHealthTermDto.getRow());
        }

        List<GovHealthTermDto> govHealthTerms = BeanConvertUtil.covertBeanList(govHealthTermServiceDaoImpl.getGovHealthTermInfo(BeanConvertUtil.beanCovertMap(govHealthTermDto)), GovHealthTermDto.class);

        return govHealthTerms;
    }


    @Override
    public int queryGovHealthTermsCount(@RequestBody GovHealthTermDto govHealthTermDto) {
        return govHealthTermServiceDaoImpl.queryGovHealthTermsCount(BeanConvertUtil.beanCovertMap(govHealthTermDto));    }

    public IGovHealthTermServiceDao getGovHealthTermServiceDaoImpl() {
        return govHealthTermServiceDaoImpl;
    }

    public void setGovHealthTermServiceDaoImpl(IGovHealthTermServiceDao govHealthTermServiceDaoImpl) {
        this.govHealthTermServiceDaoImpl = govHealthTermServiceDaoImpl;
    }
}
