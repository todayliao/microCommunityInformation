package com.java110.cust.bmo.govSymptomType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govSymptomType.IGetGovSymptomTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovSymptomTypeInnerServiceSMO;
import com.java110.dto.govSymptomType.GovSymptomTypeDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovSymptomTypeBMOImpl")
public class GetGovSymptomTypeBMOImpl implements IGetGovSymptomTypeBMO {

    @Autowired
    private IGovSymptomTypeInnerServiceSMO govSymptomTypeInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govSymptomTypeDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovSymptomTypeDto govSymptomTypeDto) {


        int count = govSymptomTypeInnerServiceSMOImpl.queryGovSymptomTypesCount(govSymptomTypeDto);

        List<GovSymptomTypeDto> govSymptomTypeDtos = null;
        if (count > 0) {
            govSymptomTypeDtos = govSymptomTypeInnerServiceSMOImpl.queryGovSymptomTypes(govSymptomTypeDto);
        } else {
            govSymptomTypeDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govSymptomTypeDto.getRow()), count, govSymptomTypeDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
