package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govGridObjRel.GovGridObjRelDto;
import com.java110.po.govGridObjRel.GovGridObjRelPo;
import com.java110.cust.bmo.govGridObjRel.IDeleteGovGridObjRelBMO;
import com.java110.cust.bmo.govGridObjRel.IGetGovGridObjRelBMO;
import com.java110.cust.bmo.govGridObjRel.ISaveGovGridObjRelBMO;
import com.java110.cust.bmo.govGridObjRel.IUpdateGovGridObjRelBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govGridObjRel")
public class GovGridObjRelApi {

    @Autowired
    private ISaveGovGridObjRelBMO saveGovGridObjRelBMOImpl;
    @Autowired
    private IUpdateGovGridObjRelBMO updateGovGridObjRelBMOImpl;
    @Autowired
    private IDeleteGovGridObjRelBMO deleteGovGridObjRelBMOImpl;

    @Autowired
    private IGetGovGridObjRelBMO getGovGridObjRelBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridObjRel/saveGovGridObjRel
     * @path /app/govGridObjRel/saveGovGridObjRel
     */
    @RequestMapping(value = "/saveGovGridObjRel", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovGridObjRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govGridId", "请求报文中未包含govGridId" );
        Assert.hasKeyAndValue( reqJson, "objTypeCd", "请求报文中未包含objTypeCd" );
        Assert.hasKeyAndValue( reqJson, "objId", "请求报文中未包含objId" );
        Assert.hasKeyAndValue( reqJson, "objName", "请求报文中未包含objName" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );


        GovGridObjRelPo govGridObjRelPo = BeanConvertUtil.covertBean( reqJson, GovGridObjRelPo.class );
        return saveGovGridObjRelBMOImpl.save( govGridObjRelPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridObjRel/updateGovGridObjRel
     * @path /app/govGridObjRel/updateGovGridObjRel
     */
    @RequestMapping(value = "/updateGovGridObjRel", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovGridObjRel(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govGridObjId", "请求报文中未包含govGridObjId" );
        Assert.hasKeyAndValue( reqJson, "govGridId", "请求报文中未包含govGridId" );
        Assert.hasKeyAndValue( reqJson, "objTypeCd", "请求报文中未包含objTypeCd" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "objId", "请求报文中未包含objId" );
        Assert.hasKeyAndValue( reqJson, "objName", "请求报文中未包含objName" );
        Assert.hasKeyAndValue( reqJson, "startTime", "请求报文中未包含startTime" );
        Assert.hasKeyAndValue( reqJson, "endTime", "请求报文中未包含endTime" );


        GovGridObjRelPo govGridObjRelPo = BeanConvertUtil.covertBean( reqJson, GovGridObjRelPo.class );
        return updateGovGridObjRelBMOImpl.update( govGridObjRelPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGridObjRel/deleteGovGridObjRel
     * @path /app/govGridObjRel/deleteGovGridObjRel
     */
    @RequestMapping(value = "/deleteGovGridObjRel", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovGridObjRel(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govGridObjId", "govGridObjId不能为空" );


        GovGridObjRelPo govGridObjRelPo = BeanConvertUtil.covertBean( reqJson, GovGridObjRelPo.class );
        return deleteGovGridObjRelBMOImpl.delete( govGridObjRelPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govGridObjRel/queryGovGridObjRel
     * @path /app/govGridObjRel/queryGovGridObjRel
     */
    @RequestMapping(value = "/queryGovGridObjRel", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovGridObjRel(@RequestParam(value = "caId",required = false) String caId,
                                                     @RequestParam(value = "govGridId",required = false) String govGridId,
                                                     @RequestParam(value = "page") int page,
                                                     @RequestParam(value = "row") int row) {

        GovGridObjRelDto govGridObjRelDto = new GovGridObjRelDto();
        govGridObjRelDto.setPage( page );
        govGridObjRelDto.setRow( row );
        govGridObjRelDto.setCaId( caId );
        govGridObjRelDto.setGovGridId( govGridId );
        return getGovGridObjRelBMOImpl.get( govGridObjRelDto );
    }
}
