package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovHealthAnswerServiceDao;
import com.java110.intf.cust.IGovHealthAnswerInnerServiceSMO;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import com.java110.po.govHealthAnswer.GovHealthAnswerPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 体检单提交者内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovHealthAnswerInnerServiceSMOImpl extends BaseServiceSMO implements IGovHealthAnswerInnerServiceSMO {

    @Autowired
    private IGovHealthAnswerServiceDao govHealthAnswerServiceDaoImpl;


    @Override
    public int saveGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo) {
        int saveFlag = 1;
        govHealthAnswerServiceDaoImpl.saveGovHealthAnswerInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerPo));
        return saveFlag;
    }

     @Override
    public int updateGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo) {
        int saveFlag = 1;
         govHealthAnswerServiceDaoImpl.updateGovHealthAnswerInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerPo));
        return saveFlag;
    }

     @Override
    public int deleteGovHealthAnswer(@RequestBody  GovHealthAnswerPo govHealthAnswerPo) {
        int saveFlag = 1;
        govHealthAnswerPo.setStatusCd("1");
        govHealthAnswerServiceDaoImpl.updateGovHealthAnswerInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerPo));
        return saveFlag;
    }

    @Override
    public List<GovHealthAnswerDto> queryGovHealthAnswers(@RequestBody  GovHealthAnswerDto govHealthAnswerDto) {

        //校验是否传了 分页信息

        int page = govHealthAnswerDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govHealthAnswerDto.setPage((page - 1) * govHealthAnswerDto.getRow());
        }

        List<GovHealthAnswerDto> govHealthAnswers = BeanConvertUtil.covertBeanList(govHealthAnswerServiceDaoImpl.getGovHealthAnswerInfo(BeanConvertUtil.beanCovertMap(govHealthAnswerDto)), GovHealthAnswerDto.class);

        return govHealthAnswers;
    }


    @Override
    public int queryGovHealthAnswersCount(@RequestBody GovHealthAnswerDto govHealthAnswerDto) {
        return govHealthAnswerServiceDaoImpl.queryGovHealthAnswersCount(BeanConvertUtil.beanCovertMap(govHealthAnswerDto));    }

    public IGovHealthAnswerServiceDao getGovHealthAnswerServiceDaoImpl() {
        return govHealthAnswerServiceDaoImpl;
    }

    public void setGovHealthAnswerServiceDaoImpl(IGovHealthAnswerServiceDao govHealthAnswerServiceDaoImpl) {
        this.govHealthAnswerServiceDaoImpl = govHealthAnswerServiceDaoImpl;
    }
}
