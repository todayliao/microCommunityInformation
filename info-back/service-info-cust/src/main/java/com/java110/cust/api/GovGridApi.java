package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govGrid.IDeleteGovGridBMO;
import com.java110.cust.bmo.govGrid.IGetGovGridBMO;
import com.java110.cust.bmo.govGrid.ISaveGovGridBMO;
import com.java110.cust.bmo.govGrid.IUpdateGovGridBMO;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govGrid")
public class GovGridApi {

    @Autowired
    private ISaveGovGridBMO saveGovGridBMOImpl;
    @Autowired
    private IUpdateGovGridBMO updateGovGridBMOImpl;
    @Autowired
    private IDeleteGovGridBMO deleteGovGridBMOImpl;

    @Autowired
    private IGetGovGridBMO getGovGridBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGrid/saveGovGrid
     * @path /app/govGrid/saveGovGrid
     */
    @RequestMapping(value = "/saveGovGrid", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovGrid(@RequestBody JSONObject reqJson,@RequestHeader(value = "store-id") String storeId) {

        Assert.hasKeyAndValue(reqJson, "govTypeId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");


        GovGridPo govGridPo = BeanConvertUtil.covertBean(reqJson, GovGridPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return saveGovGridBMOImpl.save(govGridPo,storeId,govPersonPo,reqJson);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGrid/updateGovGrid
     * @path /app/govGrid/updateGovGrid
     */
    @RequestMapping(value = "/updateGovGrid", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovGrid(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "govGridId", "请求报文中未包含govGridId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govTypeId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govTypeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "personName", "请求报文中未包含personName");
        Assert.hasKeyAndValue(reqJson, "personTel", "请求报文中未包含personTel");
        Assert.hasKeyAndValue(reqJson, "idCard", "请求报文中未包含idCard");
        Assert.hasKeyAndValue(reqJson, "personSex", "请求报文中未包含personSex");
        Assert.hasKeyAndValue(reqJson, "nation", "请求报文中未包含nation");
        Assert.hasKeyAndValue(reqJson, "maritalStatus", "请求报文中未包含maritalStatus");

        GovGridPo govGridPo = BeanConvertUtil.covertBean(reqJson, GovGridPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return updateGovGridBMOImpl.update(govGridPo,govPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govGrid/deleteGovGrid
     * @path /app/govGrid/deleteGovGrid
     */
    @RequestMapping(value = "/deleteGovGrid", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovGrid(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "caId不能为空");
        Assert.hasKeyAndValue(reqJson, "govGridId", "govGridId不能为空");


        GovGridPo govGridPo = BeanConvertUtil.covertBean(reqJson, GovGridPo.class);
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        return deleteGovGridBMOImpl.delete(govGridPo,govPersonPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govGrid/queryGovGrid
     * @path /app/govGrid/queryGovGrid
     */
    @RequestMapping(value = "/queryGovGrid", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovGrid(@RequestParam(value = "caId") String caId,
                                               @RequestParam(value = "personName",required = false) String personName,
                                               @RequestParam(value = "govTypeId",required = false) String govTypeId,
                                               @RequestParam(value = "personTel",required = false) String personTel,
                                               @RequestParam(value = "page") int page,
                                               @RequestParam(value = "row") int row) {
        GovGridDto govGridDto = new GovGridDto();
        govGridDto.setPage(page);
        govGridDto.setRow(row);
        govGridDto.setCaId(caId);
        govGridDto.setPersonName(personName);
        govGridDto.setPersonTel(personTel);
        govGridDto.setGovTypeId(govTypeId);
        return getGovGridBMOImpl.get(govGridDto);
    }
}
