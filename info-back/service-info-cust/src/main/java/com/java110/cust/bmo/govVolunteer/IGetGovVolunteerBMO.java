package com.java110.cust.bmo.govVolunteer;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govVolunteer.GovVolunteerDto;
public interface IGetGovVolunteerBMO {


    /**
     * 查询志愿者管理
     * add by wuxw
     * @param  govVolunteerDto
     * @return
     */
    ResponseEntity<String> get(GovVolunteerDto govVolunteerDto);
    /**
     * 查询志愿者管理
     * add by wuxw
     * @param  govVolunteerDto
     * @return
     */
    ResponseEntity<String> getGovStateVolunteerCount(GovVolunteerDto govVolunteerDto);
    /**
     * 查询志愿者管理
     * add by wuxw
     * @param  govVolunteerDto
     * @return
     */
    ResponseEntity<String> getVolunteerAccountDetail(GovVolunteerDto govVolunteerDto);


}
