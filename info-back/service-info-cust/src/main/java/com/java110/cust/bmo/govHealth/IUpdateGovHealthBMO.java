package com.java110.cust.bmo.govHealth;
import com.java110.po.govHealth.GovHealthPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IUpdateGovHealthBMO {


    /**
     * 修改体检项目
     * add by wuxw
     * @param govHealthPo
     * @return
     */
    ResponseEntity<String> update(GovHealthPo govHealthPo);


}
