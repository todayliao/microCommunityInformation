package com.java110.cust.bmo.uOrgCommunityV1;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IDeleteUOrgCommunityV1BMO {


    /**
     * 修改组织区域关系
     * add by wuxw
     * @param uOrgCommunityV1Po
     * @return
     */
    ResponseEntity<String> delete(UOrgCommunityV1Po uOrgCommunityV1Po);


}
