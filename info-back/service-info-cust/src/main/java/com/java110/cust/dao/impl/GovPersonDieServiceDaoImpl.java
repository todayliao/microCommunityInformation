package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovPersonDieServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 死亡登记服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPersonDieServiceDaoImpl")
//@Transactional
public class GovPersonDieServiceDaoImpl extends BaseServiceDao implements IGovPersonDieServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPersonDieServiceDaoImpl.class);





    /**
     * 保存死亡登记信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPersonDieInfo(Map info) throws DAOException {
        logger.debug("保存死亡登记信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPersonDieServiceDaoImpl.saveGovPersonDieInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存死亡登记信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询死亡登记信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPersonDieInfo(Map info) throws DAOException {
        logger.debug("查询死亡登记信息 入参 info : {}",info);

        List<Map> businessGovPersonDieInfos = sqlSessionTemplate.selectList("govPersonDieServiceDaoImpl.getGovPersonDieInfo",info);

        return businessGovPersonDieInfos;
    }


    /**
     * 修改死亡登记信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPersonDieInfo(Map info) throws DAOException {
        logger.debug("修改死亡登记信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPersonDieServiceDaoImpl.updateGovPersonDieInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改死亡登记信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询死亡登记数量
     * @param info 死亡登记信息
     * @return 死亡登记数量
     */
    @Override
    public int queryGovPersonDiesCount(Map info) {
        logger.debug("查询死亡登记数据 入参 info : {}",info);

        List<Map> businessGovPersonDieInfos = sqlSessionTemplate.selectList("govPersonDieServiceDaoImpl.queryGovPersonDiesCount", info);
        if (businessGovPersonDieInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPersonDieInfos.get(0).get("count").toString());
    }


}
