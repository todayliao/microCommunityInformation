package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govVolunteerServ.GovVolunteerServDto;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
import com.java110.cust.bmo.govVolunteerServ.IDeleteGovVolunteerServBMO;
import com.java110.cust.bmo.govVolunteerServ.IGetGovVolunteerServBMO;
import com.java110.cust.bmo.govVolunteerServ.ISaveGovVolunteerServBMO;
import com.java110.cust.bmo.govVolunteerServ.IUpdateGovVolunteerServBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govVolunteerServ")
public class GovVolunteerServApi {

    @Autowired
    private ISaveGovVolunteerServBMO saveGovVolunteerServBMOImpl;
    @Autowired
    private IUpdateGovVolunteerServBMO updateGovVolunteerServBMOImpl;
    @Autowired
    private IDeleteGovVolunteerServBMO deleteGovVolunteerServBMOImpl;

    @Autowired
    private IGetGovVolunteerServBMO getGovVolunteerServBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
         * @serviceCode /govVolunteerServ/saveGovVolunteerServ
     * @path /app/govVolunteerServ/saveGovVolunteerServ
     */
    @RequestMapping(value = "/saveGovVolunteerServ", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovVolunteerServ(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");


        GovVolunteerServPo govVolunteerServPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServPo.class);
        return saveGovVolunteerServBMOImpl.save(govVolunteerServPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerServ/updateGovVolunteerServ
     * @path /app/govVolunteerServ/updateGovVolunteerServ
     */
    @RequestMapping(value = "/updateGovVolunteerServ", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovVolunteerServ(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "servId", "请求报文中未包含servId");
        Assert.hasKeyAndValue(reqJson, "volunteerId", "请求报文中未包含volunteerId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");


        GovVolunteerServPo govVolunteerServPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServPo.class);
        return updateGovVolunteerServBMOImpl.update(govVolunteerServPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govVolunteerServ/deleteGovVolunteerServ
     * @path /app/govVolunteerServ/deleteGovVolunteerServ
     */
    @RequestMapping(value = "/deleteGovVolunteerServ", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovVolunteerServ(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "servId", "servId不能为空");


        GovVolunteerServPo govVolunteerServPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerServPo.class);
        return deleteGovVolunteerServBMOImpl.delete(govVolunteerServPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govVolunteerServ/queryGovVolunteerServ
     * @path /app/govVolunteerServ/queryGovVolunteerServ
     */
    @RequestMapping(value = "/queryGovVolunteerServ", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovVolunteerServ(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovVolunteerServDto govVolunteerServDto = new GovVolunteerServDto();
        govVolunteerServDto.setPage(page);
        govVolunteerServDto.setRow(row);
        govVolunteerServDto.setCaId(caId);
        return getGovVolunteerServBMOImpl.get(govVolunteerServDto);
    }
}
