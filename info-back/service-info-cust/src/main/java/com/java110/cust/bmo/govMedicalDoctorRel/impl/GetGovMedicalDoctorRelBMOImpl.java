package com.java110.cust.bmo.govMedicalDoctorRel.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMedicalDoctorRel.IGetGovMedicalDoctorRelBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMedicalDoctorRelInnerServiceSMO;
import com.java110.dto.govMedicalDoctorRel.GovMedicalDoctorRelDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMedicalDoctorRelBMOImpl")
public class GetGovMedicalDoctorRelBMOImpl implements IGetGovMedicalDoctorRelBMO {

    @Autowired
    private IGovMedicalDoctorRelInnerServiceSMO govMedicalDoctorRelInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMedicalDoctorRelDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMedicalDoctorRelDto govMedicalDoctorRelDto) {


        int count = govMedicalDoctorRelInnerServiceSMOImpl.queryGovMedicalDoctorRelsCount(govMedicalDoctorRelDto);

        List<GovMedicalDoctorRelDto> govMedicalDoctorRelDtos = null;
        if (count > 0) {
            govMedicalDoctorRelDtos = govMedicalDoctorRelInnerServiceSMOImpl.queryGovMedicalDoctorRels(govMedicalDoctorRelDto);
        } else {
            govMedicalDoctorRelDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMedicalDoctorRelDto.getRow()), count, govMedicalDoctorRelDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
