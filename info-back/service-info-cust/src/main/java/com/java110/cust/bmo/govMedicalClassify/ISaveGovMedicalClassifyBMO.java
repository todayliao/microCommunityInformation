package com.java110.cust.bmo.govMedicalClassify;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalClassify.GovMedicalClassifyPo;
public interface ISaveGovMedicalClassifyBMO {


    /**
     * 添加医疗分类
     * add by wuxw
     * @param govMedicalClassifyPo
     * @return
     */
    ResponseEntity<String> save(GovMedicalClassifyPo govMedicalClassifyPo);


}
