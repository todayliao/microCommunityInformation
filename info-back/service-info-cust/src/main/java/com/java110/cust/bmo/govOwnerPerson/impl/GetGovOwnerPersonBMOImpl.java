package com.java110.cust.bmo.govOwnerPerson.impl;

import com.java110.cust.bmo.govOwnerPerson.IGetGovOwnerPersonBMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.java110.dto.govOwnerPerson.GovOwnerPersonDto;


import java.util.ArrayList;
import java.util.List;

@Service("getGovOwnerPersonBMOImpl")
public class GetGovOwnerPersonBMOImpl implements IGetGovOwnerPersonBMO {

    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govOwnerPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovOwnerPersonDto govOwnerPersonDto) {


        int count = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersonsCount(govOwnerPersonDto);

        List<GovOwnerPersonDto> govOwnerPersonDtos = null;
        if (count > 0) {
            govOwnerPersonDtos = govOwnerPersonInnerServiceSMOImpl.queryGovOwnerPersons(govOwnerPersonDto);
        } else {
            govOwnerPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govOwnerPersonDto.getRow()), count, govOwnerPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
