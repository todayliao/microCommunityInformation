package com.java110.cust.bmo.govOldPerson;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IDeleteGovOldPersonBMO {


    /**
     * 修改老人管理
     * add by wuxw
     * @param govOldPersonPo
     * @return
     */
    ResponseEntity<String> delete(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo );


}
