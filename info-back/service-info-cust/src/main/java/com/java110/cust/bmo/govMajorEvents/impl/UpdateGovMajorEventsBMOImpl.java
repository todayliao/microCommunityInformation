package com.java110.cust.bmo.govMajorEvents.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMajorEvents.IUpdateGovMajorEventsBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMajorEventsInnerServiceSMO;
import com.java110.dto.govMajorEvents.GovMajorEventsDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govMajorEvents.GovMajorEventsPo;
import java.util.List;

@Service("updateGovMajorEventsBMOImpl")
public class UpdateGovMajorEventsBMOImpl implements IUpdateGovMajorEventsBMO {

    @Autowired
    private IGovMajorEventsInnerServiceSMO govMajorEventsInnerServiceSMOImpl;

    /**
     *
     *
     * @param govMajorEventsPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovMajorEventsPo govMajorEventsPo) {

        int flag = govMajorEventsInnerServiceSMOImpl.updateGovMajorEvents(govMajorEventsPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
