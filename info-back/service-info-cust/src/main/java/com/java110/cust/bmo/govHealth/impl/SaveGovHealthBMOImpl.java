package com.java110.cust.bmo.govHealth.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govHealth.ISaveGovHealthBMO;
import com.java110.po.govHealth.GovHealthPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IGovHealthInnerServiceSMO;
import com.java110.dto.govHealth.GovHealthDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovHealthBMOImpl")
public class SaveGovHealthBMOImpl implements ISaveGovHealthBMO {

    @Autowired
    private IGovHealthInnerServiceSMO govHealthInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govHealthPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovHealthPo govHealthPo) {

        govHealthPo.setHealthId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_healthId));
        govHealthPo.setDatasourceType( "999999" );
        int flag = govHealthInnerServiceSMOImpl.saveGovHealth(govHealthPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
