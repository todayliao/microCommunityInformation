package com.java110.cust.bmo.govPersonDie.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govPersonDie.IDeleteGovPersonDieBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import com.java110.po.govPersonDie.GovPersonDiePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovPersonDieInnerServiceSMO;
import com.java110.dto.govPersonDie.GovPersonDieDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovPersonDieBMOImpl")
public class DeleteGovPersonDieBMOImpl implements IDeleteGovPersonDieBMO {

    @Autowired
    private IGovPersonDieInnerServiceSMO govPersonDieInnerServiceSMOImpl;

    /**
     * @param govPersonDiePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovPersonDiePo govPersonDiePo) {

        int flag = govPersonDieInnerServiceSMOImpl.deleteGovPersonDie(govPersonDiePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
