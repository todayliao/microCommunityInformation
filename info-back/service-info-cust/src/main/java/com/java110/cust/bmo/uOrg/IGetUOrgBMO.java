package com.java110.cust.bmo.uOrg;
import com.java110.dto.uOrg.UOrgDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetUOrgBMO {


    /**
     * 查询组织管理
     * add by wuxw
     * @param  uOrgDto
     * @return
     */
    ResponseEntity<String> get(UOrgDto uOrgDto);


}
