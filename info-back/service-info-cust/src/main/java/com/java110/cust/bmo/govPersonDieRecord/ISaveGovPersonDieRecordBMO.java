package com.java110.cust.bmo.govPersonDieRecord;

import org.springframework.http.ResponseEntity;
import com.java110.po.govPersonDieRecord.GovPersonDieRecordPo;
public interface ISaveGovPersonDieRecordBMO {


    /**
     * 添加临终送别
     * add by wuxw
     * @param govPersonDieRecordPo
     * @return
     */
    ResponseEntity<String> save(GovPersonDieRecordPo govPersonDieRecordPo);


}
