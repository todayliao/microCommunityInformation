package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 命案基本信息组件内部之间使用，没有给外围系统提供服务能力
 * 命案基本信息服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovHomicideBasicServiceDao {


    /**
     * 保存 命案基本信息信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovHomicideBasicInfo(Map info) throws DAOException;




    /**
     * 查询命案基本信息信息（instance过程）
     * 根据bId 查询命案基本信息信息
     * @param info bId 信息
     * @return 命案基本信息信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovHomicideBasicInfo(Map info) throws DAOException;



    /**
     * 修改命案基本信息信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovHomicideBasicInfo(Map info) throws DAOException;


    /**
     * 查询命案基本信息总数
     *
     * @param info 命案基本信息信息
     * @return 命案基本信息数量
     */
    int queryGovHomicideBasicsCount(Map info);

}
