package com.java110.cust.bmo.govHomicideBasic.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHomicideBasic.IUpdateGovHomicideBasicBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.intf.gov.IGovLabelInnerServiceSMO;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.po.govLabel.GovLabelPo;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHomicideBasicInnerServiceSMO;
import com.java110.dto.govHomicideBasic.GovHomicideBasicDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.govHomicideBasic.GovHomicideBasicPo;

import java.util.List;

@Service("updateGovHomicideBasicBMOImpl")
public class UpdateGovHomicideBasicBMOImpl implements IUpdateGovHomicideBasicBMO {

    @Autowired
    private IGovHomicideBasicInnerServiceSMO govHomicideBasicInnerServiceSMOImpl;
    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;
    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;

    /**
     * @param govHomicideBasicPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovHomicideBasicPo govHomicideBasicPo) {

        int flag = govHomicideBasicInnerServiceSMOImpl.updateGovHomicideBasic( govHomicideBasicPo );
        if (flag < 1) {
            throw new IllegalArgumentException( "修改犯罪信息失败" );
        }
        //处理标签信息
        isGovLabelNull(GovLabelDto.LABLE_SUSPICION_CD,"P",govHomicideBasicPo.getCaId(),"犯罪嫌疑人");
        isGovLabelNull(GovLabelDto.LABLE_VICTIM_CD,"P",govHomicideBasicPo.getCaId(),"受害人");
        //处理人员与标签关系
        isGovLabelRelNull(govHomicideBasicPo.getSuspicionId(),GovLabelDto.LABLE_SUSPICION_CD,govHomicideBasicPo.getCaId());
        isGovLabelRelNull(govHomicideBasicPo.getVictimId(),GovLabelDto.LABLE_VICTIM_CD,govHomicideBasicPo.getCaId());

        return ResultVo.createResponseEntity( ResultVo.CODE_OK, "保存成功" );
    }
    protected void isGovLabelNull(String lableCd,String labelType,String cdId,String lableName){
        //处理标签信息
        GovLabelDto govLabelDto = new GovLabelDto();
        govLabelDto.setLabelCd( lableCd);
        govLabelDto.setLabelType( labelType );
        govLabelDto.setCaId( cdId );
        List<GovLabelDto> govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels( govLabelDto );
        if (govLabelDtos == null || govLabelDtos.size() < 1) {
            GovLabelPo govLabelPo = new GovLabelPo();
            govLabelPo.setGovLabelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govLabelPo.setLabelCd( lableCd );
            govLabelPo.setLabelType( labelType );
            govLabelPo.setCaId( cdId );
            govLabelPo.setLabelName( lableName );
            int flag = govLabelInnerServiceSMOImpl.saveGovLabel( govLabelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "保存"+lableName+"信息失败" );
            }
        }
    }

    protected void isGovLabelRelNull(String govPersonId,String lableCd,String cdId){
        //处理人员与标签关系
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setGovPersonId( govPersonId );
        govPersonLabelRelDto.setLabelCd( lableCd );
        govPersonLabelRelDto.setCaId( cdId );
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels( govPersonLabelRelDto );
        if (govPersonLabelRelDtos == null || govPersonLabelRelDtos.size() < 1) {
            GovPersonLabelRelPo govPersonLabelRelPo = new GovPersonLabelRelPo();
            govPersonLabelRelPo.setLabelRelId( GenerateCodeFactory.getGeneratorId( GenerateCodeFactory.CODE_PREFIX_govHomicideId ) );
            govPersonLabelRelPo.setGovPersonId( govPersonId );
            govPersonLabelRelPo.setLabelCd( lableCd );
            govPersonLabelRelPo.setCaId( cdId );
            int flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel( govPersonLabelRelPo );
            if (flag < 1) {
                throw new IllegalArgumentException( "打标签嫌疑人失败" );
            }
        }
    }
}
