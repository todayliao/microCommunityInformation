package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthTitle.IDeleteGovHealthTitleBMO;
import com.java110.cust.bmo.govHealthTitle.IGetGovHealthTitleBMO;
import com.java110.cust.bmo.govHealthTitle.ISaveGovHealthTitleBMO;
import com.java110.cust.bmo.govHealthTitle.IUpdateGovHealthTitleBMO;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealthTitle")
public class GovHealthTitleApi {

    @Autowired
    private ISaveGovHealthTitleBMO saveGovHealthTitleBMOImpl;
    @Autowired
    private IUpdateGovHealthTitleBMO updateGovHealthTitleBMOImpl;
    @Autowired
    private IDeleteGovHealthTitleBMO deleteGovHealthTitleBMOImpl;

    @Autowired
    private IGetGovHealthTitleBMO getGovHealthTitleBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitle/saveGovHealthTitle
     * @path /app/govHealthTitle/saveGovHealthTitle
     */
    @RequestMapping(value = "/saveGovHealthTitle", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealthTitle(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "titleType", "请求报文中未包含titleType");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "termId", "请求报文中未包含termId");
        JSONArray titleValues = null;
        if (!GovHealthTitleDto.TITLE_TYPE_QUESTIONS.equals(reqJson.getString("titleType"))) {
            titleValues = reqJson.getJSONArray("titleValues");
            if (titleValues.size() < 1) {
                throw new IllegalArgumentException("未包含选项");
            }
        }

        GovHealthTitlePo govHealthTitlePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitlePo.class);
        return saveGovHealthTitleBMOImpl.save(govHealthTitlePo,titleValues);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitle/updateGovHealthTitle
     * @path /app/govHealthTitle/updateGovHealthTitle
     */
    @RequestMapping(value = "/updateGovHealthTitle", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealthTitle(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "titleId", "请求报文中未包含titleId");
        Assert.hasKeyAndValue(reqJson, "healthId", "请求报文中未包含healthId");
        Assert.hasKeyAndValue(reqJson, "title", "请求报文中未包含title");
        Assert.hasKeyAndValue(reqJson, "titleType", "请求报文中未包含titleType");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "seq", "请求报文中未包含seq");
        Assert.hasKeyAndValue(reqJson, "termId", "请求报文中未包含termId");

        JSONArray titleValues = null;
        if (!GovHealthTitleDto.TITLE_TYPE_QUESTIONS.equals(reqJson.getString("titleType"))) {
            titleValues = reqJson.getJSONArray("titleValues");

            if (titleValues.size() < 1) {
                throw new IllegalArgumentException("未包含选项");
            }
        }

        GovHealthTitlePo govHealthTitlePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitlePo.class);
        deleteGovHealthTitleBMOImpl.delete(govHealthTitlePo);
        return saveGovHealthTitleBMOImpl.save(govHealthTitlePo,titleValues);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTitle/deleteGovHealthTitle
     * @path /app/govHealthTitle/deleteGovHealthTitle
     */
    @RequestMapping(value = "/deleteGovHealthTitle", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealthTitle(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "titleId", "titleId不能为空");


        GovHealthTitlePo govHealthTitlePo = BeanConvertUtil.covertBean(reqJson, GovHealthTitlePo.class);
        return deleteGovHealthTitleBMOImpl.delete(govHealthTitlePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealthTitle/queryGovHealthTitle
     * @path /app/govHealthTitle/queryGovHealthTitle
     */
    @RequestMapping(value = "/queryGovHealthTitle", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealthTitle(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "healthId",required = false) String healthId,
                                                      @RequestParam(value = "titleType",required = false) String titleType,
                                                      @RequestParam(value = "termId",required = false) String termId,
                                                      @RequestParam(value = "title",required = false) String title,
                                                      @RequestParam(value = "titleId",required = false) String titleId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovHealthTitleDto govHealthTitleDto = new GovHealthTitleDto();
        govHealthTitleDto.setPage(page);
        govHealthTitleDto.setRow(row);
        govHealthTitleDto.setCaId(caId);
        govHealthTitleDto.setHealthId(healthId);
        govHealthTitleDto.setTitle(title);
        govHealthTitleDto.setTitleType(titleType);
        govHealthTitleDto.setTitleId(titleId);
        govHealthTitleDto.setTermId( termId );
        return getGovHealthTitleBMOImpl.get(govHealthTitleDto);
    }
    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govHealthTitle/getTermInfo
     * @path /app/govHealthTitle/getTermInfo
     */
    @RequestMapping(value = "/getTermInfo", method = RequestMethod.GET)
    public ResponseEntity<String> getTermInfo(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "healthId",required = false) String healthId,
                                                      @RequestParam(value = "titleType",required = false) String titleType,
                                                      @RequestParam(value = "termId",required = false) String termId,
                                                      @RequestParam(value = "title",required = false) String title,
                                                      @RequestParam(value = "titleId",required = false) String titleId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovHealthTitleDto govHealthTitleDto = new GovHealthTitleDto();
        govHealthTitleDto.setPage(page);
        govHealthTitleDto.setRow(row);
        govHealthTitleDto.setCaId(caId);
        govHealthTitleDto.setHealthId(healthId);
        govHealthTitleDto.setTitle(title);
        govHealthTitleDto.setTitleType(titleType);
        govHealthTitleDto.setTitleId(titleId);
        govHealthTitleDto.setTermId( termId );
        return getGovHealthTitleBMOImpl.getTermInfo(govHealthTitleDto);
    }
}
