package com.java110.cust.bmo.govHealthAnswer;
import com.java110.dto.govHealthAnswer.GovHealthAnswerDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovHealthAnswerBMO {


    /**
     * 查询体检单提交者
     * add by wuxw
     * @param  govHealthAnswerDto
     * @return
     */
    ResponseEntity<String> get(GovHealthAnswerDto govHealthAnswerDto);


}
