package com.java110.cust.bmo.StoreUserV1.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.StoreUserV1.ISaveStoreUserV1BMO;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IStoreUserV1InnerServiceSMO;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveStoreUserV1BMOImpl")
public class SaveStoreUserV1BMOImpl implements ISaveStoreUserV1BMO {

    @Autowired
    private IStoreUserV1InnerServiceSMO StoreUserV1InnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param StoreUserV1Po
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(StoreUserV1Po StoreUserV1Po) {

        StoreUserV1Po.setStoreUserId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_storeUserId));
        int flag = StoreUserV1InnerServiceSMOImpl.saveStoreUserV1(StoreUserV1Po);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
