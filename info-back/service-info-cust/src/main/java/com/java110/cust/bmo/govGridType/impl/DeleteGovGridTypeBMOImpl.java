package com.java110.cust.bmo.govGridType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govGridType.IDeleteGovGridTypeBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.po.govGridType.GovGridTypePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridTypeInnerServiceSMO;

import java.util.List;

@Service("deleteGovGridTypeBMOImpl")
public class DeleteGovGridTypeBMOImpl implements IDeleteGovGridTypeBMO {

    @Autowired
    private IGovGridTypeInnerServiceSMO govGridTypeInnerServiceSMOImpl;

    /**
     * @param govGridTypePo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovGridTypePo govGridTypePo) {

        int flag = govGridTypeInnerServiceSMOImpl.deleteGovGridType(govGridTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
