package com.java110.cust.bmo.govMedicalGroup;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMedicalGroup.GovMedicalGroupDto;
public interface IGetGovMedicalGroupBMO {


    /**
     * 查询医疗团队
     * add by wuxw
     * @param  govMedicalGroupDto
     * @return
     */
    ResponseEntity<String> get(GovMedicalGroupDto govMedicalGroupDto);


}
