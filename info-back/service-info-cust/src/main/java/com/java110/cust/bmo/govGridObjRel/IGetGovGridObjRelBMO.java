package com.java110.cust.bmo.govGridObjRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govGridObjRel.GovGridObjRelDto;
public interface IGetGovGridObjRelBMO {


    /**
     * 查询网格对象关系
     * add by wuxw
     * @param  govGridObjRelDto
     * @return
     */
    ResponseEntity<String> get(GovGridObjRelDto govGridObjRelDto);


}
