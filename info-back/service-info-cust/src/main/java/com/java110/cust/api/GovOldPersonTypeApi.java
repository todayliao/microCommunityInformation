package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
import com.java110.po.govOldPersonType.GovOldPersonTypePo;
import com.java110.cust.bmo.govOldPersonType.IDeleteGovOldPersonTypeBMO;
import com.java110.cust.bmo.govOldPersonType.IGetGovOldPersonTypeBMO;
import com.java110.cust.bmo.govOldPersonType.ISaveGovOldPersonTypeBMO;
import com.java110.cust.bmo.govOldPersonType.IUpdateGovOldPersonTypeBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govOldPersonType")
public class GovOldPersonTypeApi {

    @Autowired
    private ISaveGovOldPersonTypeBMO saveGovOldPersonTypeBMOImpl;
    @Autowired
    private IUpdateGovOldPersonTypeBMO updateGovOldPersonTypeBMOImpl;
    @Autowired
    private IDeleteGovOldPersonTypeBMO deleteGovOldPersonTypeBMOImpl;

    @Autowired
    private IGetGovOldPersonTypeBMO getGovOldPersonTypeBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonType/saveGovOldPersonType
     * @path /app/govOldPersonType/saveGovOldPersonType
     */
    @RequestMapping(value = "/saveGovOldPersonType", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovOldPersonType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");


        GovOldPersonTypePo govOldPersonTypePo = BeanConvertUtil.covertBean(reqJson, GovOldPersonTypePo.class);
        return saveGovOldPersonTypeBMOImpl.save(govOldPersonTypePo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonType/updateGovOldPersonType
     * @path /app/govOldPersonType/updateGovOldPersonType
     */
    @RequestMapping(value = "/updateGovOldPersonType", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovOldPersonType(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "typeId", "请求报文中未包含typeId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "typeName", "请求报文中未包含typeName");


        GovOldPersonTypePo govOldPersonTypePo = BeanConvertUtil.covertBean(reqJson, GovOldPersonTypePo.class);
        return updateGovOldPersonTypeBMOImpl.update(govOldPersonTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govOldPersonType/deleteGovOldPersonType
     * @path /app/govOldPersonType/deleteGovOldPersonType
     */
    @RequestMapping(value = "/deleteGovOldPersonType", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovOldPersonType(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "caId", "caId不能为空");

        Assert.hasKeyAndValue(reqJson, "typeId", "typeId不能为空");


        GovOldPersonTypePo govOldPersonTypePo = BeanConvertUtil.covertBean(reqJson, GovOldPersonTypePo.class);
        return deleteGovOldPersonTypeBMOImpl.delete(govOldPersonTypePo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govOldPersonType/queryGovOldPersonType
     * @path /app/govOldPersonType/queryGovOldPersonType
     */
    @RequestMapping(value = "/queryGovOldPersonType", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovOldPersonType(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovOldPersonTypeDto govOldPersonTypeDto = new GovOldPersonTypeDto();
        govOldPersonTypeDto.setPage(page);
        govOldPersonTypeDto.setRow(row);
        govOldPersonTypeDto.setCaId(caId);
        return getGovOldPersonTypeBMOImpl.get(govOldPersonTypeDto);
    }
}
