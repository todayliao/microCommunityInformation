package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovMedicalGroupServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 医疗团队服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govMedicalGroupServiceDaoImpl")
//@Transactional
public class GovMedicalGroupServiceDaoImpl extends BaseServiceDao implements IGovMedicalGroupServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovMedicalGroupServiceDaoImpl.class);





    /**
     * 保存医疗团队信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovMedicalGroupInfo(Map info) throws DAOException {
        logger.debug("保存医疗团队信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govMedicalGroupServiceDaoImpl.saveGovMedicalGroupInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存医疗团队信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询医疗团队信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovMedicalGroupInfo(Map info) throws DAOException {
        logger.debug("查询医疗团队信息 入参 info : {}",info);

        List<Map> businessGovMedicalGroupInfos = sqlSessionTemplate.selectList("govMedicalGroupServiceDaoImpl.getGovMedicalGroupInfo",info);

        return businessGovMedicalGroupInfos;
    }


    /**
     * 修改医疗团队信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovMedicalGroupInfo(Map info) throws DAOException {
        logger.debug("修改医疗团队信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govMedicalGroupServiceDaoImpl.updateGovMedicalGroupInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改医疗团队信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询医疗团队数量
     * @param info 医疗团队信息
     * @return 医疗团队数量
     */
    @Override
    public int queryGovMedicalGroupsCount(Map info) {
        logger.debug("查询医疗团队数据 入参 info : {}",info);

        List<Map> businessGovMedicalGroupInfos = sqlSessionTemplate.selectList("govMedicalGroupServiceDaoImpl.queryGovMedicalGroupsCount", info);
        if (businessGovMedicalGroupInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovMedicalGroupInfos.get(0).get("count").toString());
    }


}
