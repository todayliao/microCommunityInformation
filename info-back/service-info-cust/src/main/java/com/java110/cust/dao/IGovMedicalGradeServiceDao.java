package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 医疗分级组件内部之间使用，没有给外围系统提供服务能力
 * 医疗分级服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovMedicalGradeServiceDao {


    /**
     * 保存 医疗分级信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovMedicalGradeInfo(Map info) throws DAOException;




    /**
     * 查询医疗分级信息（instance过程）
     * 根据bId 查询医疗分级信息
     * @param info bId 信息
     * @return 医疗分级信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovMedicalGradeInfo(Map info) throws DAOException;



    /**
     * 修改医疗分级信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovMedicalGradeInfo(Map info) throws DAOException;


    /**
     * 查询医疗分级总数
     *
     * @param info 医疗分级信息
     * @return 医疗分级数量
     */
    int queryGovMedicalGradesCount(Map info);

}
