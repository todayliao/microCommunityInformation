package com.java110.cust.bmo.govGrid.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govGrid.IDeleteGovGridBMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govGrid.GovGridPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.vo.ResultVo;
import com.java110.po.store.StorePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridInnerServiceSMO;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovGridBMOImpl")
public class DeleteGovGridBMOImpl implements IDeleteGovGridBMO {

    @Autowired
    private IGovGridInnerServiceSMO govGridInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;

    /**
     * @param govGridPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovGridPo govGridPo, GovPersonPo govPersonPo) {

        int flag = govGridInnerServiceSMOImpl.deleteGovGrid(govGridPo);

        if (flag < 1) {
            throw new IllegalArgumentException("删除网格人员失败");
        }
        flag = govPersonInnerServiceSMOImpl.deleteGovPerson(govPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除对应人口失败");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
