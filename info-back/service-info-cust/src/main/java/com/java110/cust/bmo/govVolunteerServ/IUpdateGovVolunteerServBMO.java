package com.java110.cust.bmo.govVolunteerServ;
import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;

public interface IUpdateGovVolunteerServBMO {


    /**
     * 修改服务领域
     * add by wuxw
     * @param govVolunteerServPo
     * @return
     */
    ResponseEntity<String> update(GovVolunteerServPo govVolunteerServPo);


}
