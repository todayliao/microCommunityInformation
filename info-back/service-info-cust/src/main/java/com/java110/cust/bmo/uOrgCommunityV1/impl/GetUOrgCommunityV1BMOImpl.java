package com.java110.cust.bmo.uOrgCommunityV1.impl;

import com.java110.cust.bmo.uOrgCommunityV1.IGetUOrgCommunityV1BMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUOrgCommunityV1InnerServiceSMO;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getUOrgCommunityV1BMOImpl")
public class GetUOrgCommunityV1BMOImpl implements IGetUOrgCommunityV1BMO {

    @Autowired
    private IUOrgCommunityV1InnerServiceSMO uOrgCommunityV1InnerServiceSMOImpl;

    /**
     *
     *
     * @param  uOrgCommunityV1Dto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(UOrgCommunityV1Dto uOrgCommunityV1Dto) {


        int count = uOrgCommunityV1InnerServiceSMOImpl.queryUOrgCommunityV1sCount(uOrgCommunityV1Dto);

        List<UOrgCommunityV1Dto> uOrgCommunityV1Dtos = null;
        if (count > 0) {
            uOrgCommunityV1Dtos = uOrgCommunityV1InnerServiceSMOImpl.queryUOrgCommunityV1s(uOrgCommunityV1Dto);
        } else {
            uOrgCommunityV1Dtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) uOrgCommunityV1Dto.getRow()), count, uOrgCommunityV1Dtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
