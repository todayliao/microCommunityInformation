package com.java110.cust.bmo.govHomicideBasic;

import org.springframework.http.ResponseEntity;
import com.java110.po.govHomicideBasic.GovHomicideBasicPo;
public interface ISaveGovHomicideBasicBMO {


    /**
     * 添加命案基本信息
     * add by wuxw
     * @param govHomicideBasicPo
     * @return
     */
    ResponseEntity<String> save(GovHomicideBasicPo govHomicideBasicPo);


}
