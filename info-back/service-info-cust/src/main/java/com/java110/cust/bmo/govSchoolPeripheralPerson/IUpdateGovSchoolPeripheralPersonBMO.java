package com.java110.cust.bmo.govSchoolPeripheralPerson;
import org.springframework.http.ResponseEntity;
import com.java110.po.govSchoolPeripheralPerson.GovSchoolPeripheralPersonPo;

public interface IUpdateGovSchoolPeripheralPersonBMO {


    /**
     * 修改校园周边重点人员
     * add by wuxw
     * @param govSchoolPeripheralPersonPo
     * @return
     */
    ResponseEntity<String> update(GovSchoolPeripheralPersonPo govSchoolPeripheralPersonPo);


}
