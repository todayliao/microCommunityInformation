package com.java110.cust.bmo.uOrgCommunityV1;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetUOrgCommunityV1BMO {


    /**
     * 查询组织区域关系
     * add by wuxw
     * @param  uOrgCommunityV1Dto
     * @return
     */
    ResponseEntity<String> get(UOrgCommunityV1Dto uOrgCommunityV1Dto);


}
