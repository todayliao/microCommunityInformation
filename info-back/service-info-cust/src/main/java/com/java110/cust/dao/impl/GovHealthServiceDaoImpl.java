package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovHealthServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 体检项目服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govHealthServiceDaoImpl")
//@Transactional
public class GovHealthServiceDaoImpl extends BaseServiceDao implements IGovHealthServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovHealthServiceDaoImpl.class);





    /**
     * 保存体检项目信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovHealthInfo(Map info) throws DAOException {
        logger.debug("保存体检项目信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govHealthServiceDaoImpl.saveGovHealthInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存体检项目信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询体检项目信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovHealthInfo(Map info) throws DAOException {
        logger.debug("查询体检项目信息 入参 info : {}",info);

        List<Map> businessGovHealthInfos = sqlSessionTemplate.selectList("govHealthServiceDaoImpl.getGovHealthInfo",info);

        return businessGovHealthInfos;
    }


    /**
     * 修改体检项目信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovHealthInfo(Map info) throws DAOException {
        logger.debug("修改体检项目信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govHealthServiceDaoImpl.updateGovHealthInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改体检项目信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询体检项目数量
     * @param info 体检项目信息
     * @return 体检项目数量
     */
    @Override
    public int queryGovHealthsCount(Map info) {
        logger.debug("查询体检项目数据 入参 info : {}",info);

        List<Map> businessGovHealthInfos = sqlSessionTemplate.selectList("govHealthServiceDaoImpl.queryGovHealthsCount", info);
        if (businessGovHealthInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovHealthInfos.get(0).get("count").toString());
    }


}
