package com.java110.cust.bmo.govOldPersonAttr;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
public interface IGetGovOldPersonAttrBMO {


    /**
     * 查询老人属性
     * add by wuxw
     * @param  govOldPersonAttrDto
     * @return
     */
    ResponseEntity<String> get(GovOldPersonAttrDto govOldPersonAttrDto);


}
