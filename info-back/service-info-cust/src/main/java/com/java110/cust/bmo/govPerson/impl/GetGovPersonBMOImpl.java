package com.java110.cust.bmo.govPerson.impl;

import com.alibaba.fastjson.JSONArray;
import com.java110.cust.bmo.govPerson.IGetGovPersonBMO;
import com.java110.dto.govAids.GovAidsDto;
import com.java110.dto.govCommunityarCorrection.GovCommunityarCorrectionDto;
import com.java110.dto.govDrug.GovDrugDto;
import com.java110.dto.govGrid.GovGridDto;
import com.java110.dto.govLabel.GovLabelDto;
import com.java110.dto.govMentalDisorders.GovMentalDisordersDto;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.dto.govPerson.LargeGovPersonDto;
import com.java110.dto.govPersonLabelRel.GovPersonLabelRelDto;
import com.java110.dto.govReleasePrison.GovReleasePrisonDto;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.intf.cust.IGovGridInnerServiceSMO;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.intf.cust.IGovVolunteerInnerServiceSMO;
import com.java110.intf.gov.*;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovPersonBMOImpl")
public class GetGovPersonBMOImpl implements IGetGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovGridInnerServiceSMO govGridInnerServiceSMOImpl;
    @Autowired
    private IGovOldPersonInnerServiceSMO govOldPersonInnerServiceSMOImpl;
    @Autowired
    private IGovReleasePrisonInnerServiceSMO govReleasePrisonInnerServiceSMOImpl;
    @Autowired
    private IGovDrugInnerServiceSMO govDrugInnerServiceSMOImpl;
    @Autowired
    private IGovCommunityarCorrectionInnerServiceSMO govCommunityarCorrectionInnerServiceSMOImpl;
    @Autowired
    private IGovMentalDisordersInnerServiceSMO govMentalDisordersInnerServiceSMOImpl;
    @Autowired
    private IGovAidsInnerServiceSMO govAidsInnerServiceSMOImpl;
    @Autowired
    private IGovVolunteerInnerServiceSMO govVolunteerInnerServiceSMOImpl;
    @Autowired
    private IGovLabelInnerServiceSMO govLabelInnerServiceSMOImpl;
    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;


    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovPersonDto govPersonDto) {


        int count = govPersonInnerServiceSMOImpl.queryGovPersonsCount(govPersonDto);

        List<GovPersonDto> govPersonDtos = null;
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }


    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getLabelPersons(GovPersonDto govPersonDto,String labelCd,String labelCds) {
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setLabelCd(labelCd);
        if(labelCds !=null && labelCds.length() >0){
            govPersonLabelRelDto.setLabelCds(labelCds.split(","));
        }
        govPersonLabelRelDto.setPage(govPersonDto.getPage());
        govPersonLabelRelDto.setRow(govPersonDto.getRow());
        int count = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRelsCount(govPersonLabelRelDto);
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = null;
        if (count > 0) {
            govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels(govPersonLabelRelDto);
        } else {
            govPersonLabelRelDtos = new ArrayList<>();
        }
        List<GovPersonDto> govPersonDtos = null;
        if (govPersonLabelRelDtos.size() > 0){
        List<String> govPersonIds = new ArrayList<>();
        for (GovPersonLabelRelDto labelRelDto : govPersonLabelRelDtos) {
            govPersonIds.add(labelRelDto.getGovPersonId());
        }
        govPersonDto.setGovPersonIds(govPersonIds.toArray(new String[govPersonIds.size()]));
        count = govPersonInnerServiceSMOImpl.queryGovPersonsCount(govPersonDto);
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }
        }else{
            govPersonDtos = new ArrayList<>();
        }
        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);
        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);
        return responseEntity;
    }


    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovPersonHomicide(GovPersonDto govPersonDto) {


        int count = govPersonInnerServiceSMOImpl.getGovPersonHomicideCount(govPersonDto);

        List<GovPersonDto> govPersonDtos = null;
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.getGovPersonHomicide(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *  查询贫困、扶贫干部人口信息
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovPersonHelpPolicy(GovPersonDto govPersonDto) {


        int count = govPersonInnerServiceSMOImpl.getGovPersonHelpPolicyCount(govPersonDto);

        List<GovPersonDto> govPersonDtos = null;
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.getGovPersonHelpPolicy(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void refReshLable(List<GovPersonDto> govPersonDtos) {

        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            return;
        }

        List<String> govPersonIds = new ArrayList<>();
        for (GovPersonDto GovPerson : govPersonDtos) {
            govPersonIds.add(GovPerson.getGovPersonId());
        }
        GovPersonLabelRelDto govPersonLabelRelDto = new GovPersonLabelRelDto();
        govPersonLabelRelDto.setCaId( govPersonDtos.get( 0 ).getCaId() );
        govPersonLabelRelDto.setGovPersonIds( govPersonIds.toArray(new String[govPersonIds.size()]) );
        List<GovPersonLabelRelDto> govPersonLabelRelDtos = govPersonLabelRelInnerServiceSMOImpl.queryGovPersonLabelRels( govPersonLabelRelDto );

        if (govPersonLabelRelDtos == null || govPersonLabelRelDtos.size() < 1) {
            return;
        }

        for (GovPersonDto govPersonDto : govPersonDtos) {
            GovLabelDto govLabelDto = new GovLabelDto();
            List<String> temLabelCd = new ArrayList<>();
            for (GovPersonLabelRelDto govPersonLabelRel : govPersonLabelRelDtos) {
                if (govPersonDto.getGovPersonId().equals(govPersonLabelRel.getGovPersonId())) {
                    temLabelCd.add( govPersonLabelRel.getLabelCd() );
                }
            }
            govLabelDto.setLabelCds( temLabelCd.toArray( new String[temLabelCd.size()] ) );
            govLabelDto.setCaId( govPersonDto.getCaId() );
            List<GovLabelDto>  govLabelDtos = govLabelInnerServiceSMOImpl.queryGovLabels( govLabelDto );
            govPersonDto.setGovLabelDtoList( govLabelDtos );
        }

    }

    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovPersonCompany(GovPersonDto govPersonDto) {


        int count = govPersonInnerServiceSMOImpl.getGovPersonCompanyCount(govPersonDto);

        List<GovPersonDto> govPersonDtos = null;
        if (count > 0) {
            govPersonDtos = govPersonInnerServiceSMOImpl.getGovPersonCompany(govPersonDto);
        } else {
            govPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govPersonDto.getRow()), count, govPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  govPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> queryLargeGovPersonCoun(GovPersonDto govPersonDto) {
        LargeGovPersonDto largeGovPersonDto = new LargeGovPersonDto();

        GovGridDto govGridDto = new GovGridDto();
        govGridDto.setCaId(govPersonDto.getCaId());
        //网格人员
        int gridCoun = govGridInnerServiceSMOImpl.queryGovGridsCount(govGridDto);
        largeGovPersonDto.setGridCoun(Integer.toString(gridCoun));

        GovOldPersonDto govOldPersonDto = new GovOldPersonDto();
        govOldPersonDto.setCaId(govPersonDto.getCaId());
        //老年人员
        int oldCoun = govOldPersonInnerServiceSMOImpl.queryGovOldPersonsCount(govOldPersonDto);
        largeGovPersonDto.setOldCoun(Integer.toString(oldCoun));

        //特殊人员
        //刑满释放
        GovReleasePrisonDto govReleasePrisonDto = new GovReleasePrisonDto();
        govReleasePrisonDto.setCaId(govPersonDto.getCaId());
        int relCoun = govReleasePrisonInnerServiceSMOImpl.queryGovReleasePrisonsCount(govReleasePrisonDto);
        largeGovPersonDto.setRelCoun(Integer.toString(relCoun));
        //吸毒人员
        GovDrugDto govDrugDto = new GovDrugDto();
        govDrugDto.setCaId(govPersonDto.getCaId());
        int drugsCoun = govDrugInnerServiceSMOImpl.queryGovDrugsCount(govDrugDto);
        largeGovPersonDto.setDrugsCoun(Integer.toString(drugsCoun));

        //社区矫正人员
        GovCommunityarCorrectionDto govCommunityarCorrectionDto = new GovCommunityarCorrectionDto();
        govCommunityarCorrectionDto.setCaId(govPersonDto.getCaId());
        int correCoun = govCommunityarCorrectionInnerServiceSMOImpl.queryGovCommunityarCorrectionsCount(govCommunityarCorrectionDto);
        largeGovPersonDto.setCorreCoun(Integer.toString(correCoun));

        //精神障碍人员
        GovMentalDisordersDto govMentalDisordersDto = new GovMentalDisordersDto();
        govMentalDisordersDto.setCaId(govPersonDto.getCaId());
        int mentalCoun = govMentalDisordersInnerServiceSMOImpl.queryGovMentalDisorderssCount(govMentalDisordersDto);
        largeGovPersonDto.setMentalCoun(Integer.toString(mentalCoun));
        //艾滋病人员
        GovAidsDto govAidsDto = new GovAidsDto();
        govAidsDto.setCaId(govPersonDto.getCaId());
        int aidssCoun = govAidsInnerServiceSMOImpl.queryGovAidssCount(govAidsDto);
        largeGovPersonDto.setAidssCoun(Integer.toString(aidssCoun));
        //志愿人员
        GovVolunteerDto govVolunteerDto = new GovVolunteerDto();
        govVolunteerDto.setCaId(govPersonDto.getCaId());
        int volunCoun = govVolunteerInnerServiceSMOImpl.queryGovVolunteersCount(govVolunteerDto);
        largeGovPersonDto.setVolunCoun(Integer.toString(volunCoun));

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) 1 / (double) govPersonDto.getRow()), 1, largeGovPersonDto);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
