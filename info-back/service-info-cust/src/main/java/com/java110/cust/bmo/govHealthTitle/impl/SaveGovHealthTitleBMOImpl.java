package com.java110.cust.bmo.govHealthTitle.impl;

import com.alibaba.fastjson.JSONArray;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govHealthTitle.ISaveGovHealthTitleBMO;
import com.java110.intf.cust.IGovHealthTitleValueInnerServiceSMO;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import com.java110.po.govHealthTitleValue.GovHealthTitleValuePo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.store.StorePo;
import com.java110.intf.cust.IGovHealthTitleInnerServiceSMO;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovHealthTitleBMOImpl")
public class SaveGovHealthTitleBMOImpl implements ISaveGovHealthTitleBMO {

    @Autowired
    private IGovHealthTitleInnerServiceSMO govHealthTitleInnerServiceSMOImpl;
    @Autowired
    private IGovHealthTitleValueInnerServiceSMO govHealthTitleValueInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govHealthTitlePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovHealthTitlePo govHealthTitlePo, JSONArray titleValues) {

        govHealthTitlePo.setTitleId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_titleId));
        int flag = govHealthTitleInnerServiceSMOImpl.saveGovHealthTitle(govHealthTitlePo);


        if (flag < 0) {
            throw new IllegalArgumentException("保存题目失败");
        }
        if (GovHealthTitleDto.TITLE_TYPE_QUESTIONS.equals(govHealthTitlePo.getTitleType())) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
        GovHealthTitleValuePo govHealthTitleValuePo = null;
        for (int titleValueIndex = 0; titleValueIndex < titleValues.size(); titleValueIndex++) {
            govHealthTitleValuePo = new GovHealthTitleValuePo();
            govHealthTitleValuePo.setValue(titleValues.getJSONObject(titleValueIndex).getString("value"));
            govHealthTitleValuePo.setSeq(titleValues.getJSONObject(titleValueIndex).getString("seq"));
            govHealthTitleValuePo.setTitleId(govHealthTitlePo.getTitleId());
            govHealthTitleValuePo.setValueId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_valueId));
            govHealthTitleValuePo.setCaId(govHealthTitlePo.getCaId());
            flag = govHealthTitleValueInnerServiceSMOImpl.saveGovHealthTitleValue(govHealthTitleValuePo);
            if (flag < 0) {
                throw new IllegalArgumentException("保存题目选项失败");
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
