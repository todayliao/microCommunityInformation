package com.java110.cust.bmo.govCompanyPerson;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCompanyPersonBMO {


    /**
     * 查询小区位置
     * add by wuxw
     * @param  govCompanyPersonDto
     * @return
     */
    ResponseEntity<String> get(GovCompanyPersonDto govCompanyPersonDto);


}
