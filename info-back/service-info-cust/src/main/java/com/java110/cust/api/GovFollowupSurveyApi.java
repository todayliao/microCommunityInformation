package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govFollowupSurvey.GovFollowupSurveyDto;
import com.java110.po.govFollowupSurvey.GovFollowupSurveyPo;
import com.java110.cust.bmo.govFollowupSurvey.IDeleteGovFollowupSurveyBMO;
import com.java110.cust.bmo.govFollowupSurvey.IGetGovFollowupSurveyBMO;
import com.java110.cust.bmo.govFollowupSurvey.ISaveGovFollowupSurveyBMO;
import com.java110.cust.bmo.govFollowupSurvey.IUpdateGovFollowupSurveyBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govFollowupSurvey")
public class GovFollowupSurveyApi {

    @Autowired
    private ISaveGovFollowupSurveyBMO saveGovFollowupSurveyBMOImpl;
    @Autowired
    private IUpdateGovFollowupSurveyBMO updateGovFollowupSurveyBMOImpl;
    @Autowired
    private IDeleteGovFollowupSurveyBMO deleteGovFollowupSurveyBMOImpl;

    @Autowired
    private IGetGovFollowupSurveyBMO getGovFollowupSurveyBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govFollowupSurvey/saveGovFollowupSurvey
     * @path /app/govFollowupSurvey/saveGovFollowupSurvey
     */
    @RequestMapping(value = "/saveGovFollowupSurvey", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovFollowupSurvey(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "surveyTime", "请求报文中未包含surveyTime");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "surveyWay", "请求报文中未包含surveyWay");
        Assert.hasKeyAndValue(reqJson, "symptoms", "请求报文中未包含symptoms");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "lifeStyleGuide", "请求报文中未包含lifeStyleGuide");
        Assert.hasKeyAndValue(reqJson, "drugCompliance", "请求报文中未包含drugCompliance");
        Assert.hasKeyAndValue(reqJson, "adrs", "请求报文中未包含adrs");
        Assert.hasKeyAndValue(reqJson, "surveyType", "请求报文中未包含surveyType");
        Assert.hasKeyAndValue(reqJson, "medication", "请求报文中未包含medication");
        Assert.hasKeyAndValue(reqJson, "referralReason", "请求报文中未包含referralReason");
        Assert.hasKeyAndValue(reqJson, "referralDepartment", "请求报文中未包含referralDepartment");
        Assert.hasKeyAndValue(reqJson, "surveyAdvice", "请求报文中未包含surveyAdvice");
        Assert.hasKeyAndValue(reqJson, "surveyConclusion", "请求报文中未包含surveyConclusion");
        Assert.hasKeyAndValue(reqJson, "nextSurveyTime", "请求报文中未包含nextSurveyTime");
        Assert.hasKeyAndValue(reqJson, "surveyDoctor", "请求报文中未包含surveyDoctor");

        GovFollowupSurveyPo govFollowupSurveyPo = BeanConvertUtil.covertBean(reqJson, GovFollowupSurveyPo.class);
        return saveGovFollowupSurveyBMOImpl.save(govFollowupSurveyPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govFollowupSurvey/updateGovFollowupSurvey
     * @path /app/govFollowupSurvey/updateGovFollowupSurvey
     */
    @RequestMapping(value = "/updateGovFollowupSurvey", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovFollowupSurvey(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "surveyId", "请求报文中未包含surveyId");
        Assert.hasKeyAndValue(reqJson, "surveyTime", "请求报文中未包含surveyTime");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "surveyWay", "请求报文中未包含surveyWay");
        Assert.hasKeyAndValue(reqJson, "symptoms", "请求报文中未包含symptoms");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "lifeStyleGuide", "请求报文中未包含lifeStyleGuide");
        Assert.hasKeyAndValue(reqJson, "drugCompliance", "请求报文中未包含drugCompliance");
        Assert.hasKeyAndValue(reqJson, "adrs", "请求报文中未包含adrs");
        Assert.hasKeyAndValue(reqJson, "surveyType", "请求报文中未包含surveyType");
        Assert.hasKeyAndValue(reqJson, "medication", "请求报文中未包含medication");
        Assert.hasKeyAndValue(reqJson, "referralReason", "请求报文中未包含referralReason");
        Assert.hasKeyAndValue(reqJson, "referralDepartment", "请求报文中未包含referralDepartment");
        Assert.hasKeyAndValue(reqJson, "surveyAdvice", "请求报文中未包含surveyAdvice");
        Assert.hasKeyAndValue(reqJson, "surveyConclusion", "请求报文中未包含surveyConclusion");
        Assert.hasKeyAndValue(reqJson, "nextSurveyTime", "请求报文中未包含nextSurveyTime");
        Assert.hasKeyAndValue(reqJson, "surveyDoctor", "请求报文中未包含surveyDoctor");

        GovFollowupSurveyPo govFollowupSurveyPo = BeanConvertUtil.covertBean(reqJson, GovFollowupSurveyPo.class);
        return updateGovFollowupSurveyBMOImpl.update(govFollowupSurveyPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govFollowupSurvey/deleteGovFollowupSurvey
     * @path /app/govFollowupSurvey/deleteGovFollowupSurvey
     */
    @RequestMapping(value = "/deleteGovFollowupSurvey", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovFollowupSurvey(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "surveyId", "surveyId不能为空");

        GovFollowupSurveyPo govFollowupSurveyPo = BeanConvertUtil.covertBean(reqJson, GovFollowupSurveyPo.class);
        return deleteGovFollowupSurveyBMOImpl.delete(govFollowupSurveyPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govFollowupSurvey/queryGovFollowupSurvey
     * @path /app/govFollowupSurvey/queryGovFollowupSurvey
     */
    @RequestMapping(value = "/queryGovFollowupSurvey", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovFollowupSurvey(@RequestParam(value = "caId") String caId,
                                                         @RequestParam(value = "govPersonName", required = false) String govPersonName,
                                                         @RequestParam(value = "surveyDoctor", required = false) String surveyDoctor,
                                                         @RequestParam(value = "surveyTime", required = false) String surveyTime,
                                                         @RequestParam(value = "surveyWay", required = false) String surveyWay,
                                                         @RequestParam(value = "page") int page,
                                                         @RequestParam(value = "row") int row) {
        GovFollowupSurveyDto govFollowupSurveyDto = new GovFollowupSurveyDto();
        govFollowupSurveyDto.setPage(page);
        govFollowupSurveyDto.setRow(row);
        govFollowupSurveyDto.setCaId(caId);
        govFollowupSurveyDto.setGovPersonName(govPersonName);
        govFollowupSurveyDto.setSurveyDoctor(surveyDoctor);
        govFollowupSurveyDto.setSurveyTime(surveyTime);
        govFollowupSurveyDto.setSurveyType(surveyWay);
        return getGovFollowupSurveyBMOImpl.get(govFollowupSurveyDto);
    }
}
