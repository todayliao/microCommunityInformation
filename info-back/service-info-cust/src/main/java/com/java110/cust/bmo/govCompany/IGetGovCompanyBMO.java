package com.java110.cust.bmo.govCompany;
import com.java110.dto.govCompany.GovCompanyDto;
import org.springframework.http.ResponseEntity;
public interface IGetGovCompanyBMO {


    /**
     * 查询公司组织
     * add by wuxw
     * @param  govCompanyDto
     * @return
     */
    ResponseEntity<String> get(GovCompanyDto govCompanyDto);


}
