package com.java110.cust.bmo.machineStaffShow;
import org.springframework.http.ResponseEntity;
import com.java110.po.machineStaffShow.MachineStaffShowPo;

public interface IUpdateMachineStaffShowBMO {


    /**
     * 修改摄像头员工关系
     * add by wuxw
     * @param machineStaffShowPo
     * @return
     */
    ResponseEntity<String> update(MachineStaffShowPo machineStaffShowPo);


}
