package com.java110.cust.bmo.govMedicalDoctorRel;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMedicalDoctorRel.GovMedicalDoctorRelDto;
public interface IGetGovMedicalDoctorRelBMO {


    /**
     * 查询档案医生
     * add by wuxw
     * @param  govMedicalDoctorRelDto
     * @return
     */
    ResponseEntity<String> get(GovMedicalDoctorRelDto govMedicalDoctorRelDto);


}
