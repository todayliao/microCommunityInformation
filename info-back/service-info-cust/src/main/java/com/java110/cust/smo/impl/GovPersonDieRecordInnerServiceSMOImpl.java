package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovPersonDieRecordServiceDao;
import com.java110.intf.cust.IGovPersonDieRecordInnerServiceSMO;
import com.java110.dto.govPersonDieRecord.GovPersonDieRecordDto;
import com.java110.po.govPersonDieRecord.GovPersonDieRecordPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 临终送别内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovPersonDieRecordInnerServiceSMOImpl extends BaseServiceSMO implements IGovPersonDieRecordInnerServiceSMO {

    @Autowired
    private IGovPersonDieRecordServiceDao govPersonDieRecordServiceDaoImpl;


    @Override
    public int saveGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo) {
        int saveFlag = 1;
        govPersonDieRecordServiceDaoImpl.saveGovPersonDieRecordInfo(BeanConvertUtil.beanCovertMap(govPersonDieRecordPo));
        return saveFlag;
    }

     @Override
    public int updateGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo) {
        int saveFlag = 1;
         govPersonDieRecordServiceDaoImpl.updateGovPersonDieRecordInfo(BeanConvertUtil.beanCovertMap(govPersonDieRecordPo));
        return saveFlag;
    }

     @Override
    public int deleteGovPersonDieRecord(@RequestBody  GovPersonDieRecordPo govPersonDieRecordPo) {
        int saveFlag = 1;
        govPersonDieRecordPo.setStatusCd("1");
        govPersonDieRecordServiceDaoImpl.updateGovPersonDieRecordInfo(BeanConvertUtil.beanCovertMap(govPersonDieRecordPo));
        return saveFlag;
    }

    @Override
    public List<GovPersonDieRecordDto> queryGovPersonDieRecords(@RequestBody  GovPersonDieRecordDto govPersonDieRecordDto) {

        //校验是否传了 分页信息

        int page = govPersonDieRecordDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govPersonDieRecordDto.setPage((page - 1) * govPersonDieRecordDto.getRow());
        }

        List<GovPersonDieRecordDto> govPersonDieRecords = BeanConvertUtil.covertBeanList(govPersonDieRecordServiceDaoImpl.getGovPersonDieRecordInfo(BeanConvertUtil.beanCovertMap(govPersonDieRecordDto)), GovPersonDieRecordDto.class);

        return govPersonDieRecords;
    }


    @Override
    public int queryGovPersonDieRecordsCount(@RequestBody GovPersonDieRecordDto govPersonDieRecordDto) {
        return govPersonDieRecordServiceDaoImpl.queryGovPersonDieRecordsCount(BeanConvertUtil.beanCovertMap(govPersonDieRecordDto));    }

    public IGovPersonDieRecordServiceDao getGovPersonDieRecordServiceDaoImpl() {
        return govPersonDieRecordServiceDaoImpl;
    }

    public void setGovPersonDieRecordServiceDaoImpl(IGovPersonDieRecordServiceDao govPersonDieRecordServiceDaoImpl) {
        this.govPersonDieRecordServiceDaoImpl = govPersonDieRecordServiceDaoImpl;
    }
}
