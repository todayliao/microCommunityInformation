package com.java110.cust.bmo.govCompany.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govCompany.ISaveGovCompanyBMO;
import com.java110.intf.assets.IGovCommunityCompanyInnerServiceSMO;
import com.java110.intf.cust.IGovCompanyInnerServiceSMO;
import com.java110.po.govCommunityCompany.GovCommunityCompanyPo;
import com.java110.po.govCompany.GovCompanyPo;
import com.java110.utils.util.StringUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovCompanyBMOImpl")
public class SaveGovCompanyBMOImpl implements ISaveGovCompanyBMO {

    @Autowired
    private IGovCompanyInnerServiceSMO govCompanyInnerServiceSMOImpl;

    @Autowired
    private IGovCommunityCompanyInnerServiceSMO govCommunityCompanyInnerServiceSMOImpl;



    /**
     * 添加小区信息
     *
     * @param govCompanyPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovCompanyPo govCompanyPo) {

        govCompanyPo.setGovCompanyId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govCompanyId));
        int flag = govCompanyInnerServiceSMOImpl.saveGovCompany(govCompanyPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存组织或企业失败");
        }
        if(!StringUtil.isEmpty(govCompanyPo.getGovCommunityId())){
            GovCommunityCompanyPo govCommunityCompanyPo = new GovCommunityCompanyPo();
            govCommunityCompanyPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
            govCommunityCompanyPo.setGovCompanyId(govCompanyPo.getGovCompanyId());
            govCommunityCompanyPo.setGovCommunityId(govCompanyPo.getGovCommunityId());
            govCommunityCompanyPo.setCaId(govCompanyPo.getCaId());
            flag = govCommunityCompanyInnerServiceSMOImpl.saveGovCommunityCompany(govCommunityCompanyPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存组织或企业与小区关系失败");
            }
        }


        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

}
