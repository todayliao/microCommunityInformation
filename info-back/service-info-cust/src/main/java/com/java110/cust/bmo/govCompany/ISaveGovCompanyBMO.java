package com.java110.cust.bmo.govCompany;

import com.java110.po.govCompany.GovCompanyPo;
import org.springframework.http.ResponseEntity;
public interface ISaveGovCompanyBMO {


    /**
     * 添加公司组织
     * add by wuxw
     * @param govCompanyPo
     * @return
     */
    ResponseEntity<String> save(GovCompanyPo govCompanyPo);


}
