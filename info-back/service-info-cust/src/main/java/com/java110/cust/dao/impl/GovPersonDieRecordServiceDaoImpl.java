package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovPersonDieRecordServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 临终送别服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govPersonDieRecordServiceDaoImpl")
//@Transactional
public class GovPersonDieRecordServiceDaoImpl extends BaseServiceDao implements IGovPersonDieRecordServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovPersonDieRecordServiceDaoImpl.class);





    /**
     * 保存临终送别信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovPersonDieRecordInfo(Map info) throws DAOException {
        logger.debug("保存临终送别信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govPersonDieRecordServiceDaoImpl.saveGovPersonDieRecordInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存临终送别信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询临终送别信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovPersonDieRecordInfo(Map info) throws DAOException {
        logger.debug("查询临终送别信息 入参 info : {}",info);

        List<Map> businessGovPersonDieRecordInfos = sqlSessionTemplate.selectList("govPersonDieRecordServiceDaoImpl.getGovPersonDieRecordInfo",info);

        return businessGovPersonDieRecordInfos;
    }


    /**
     * 修改临终送别信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovPersonDieRecordInfo(Map info) throws DAOException {
        logger.debug("修改临终送别信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govPersonDieRecordServiceDaoImpl.updateGovPersonDieRecordInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改临终送别信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询临终送别数量
     * @param info 临终送别信息
     * @return 临终送别数量
     */
    @Override
    public int queryGovPersonDieRecordsCount(Map info) {
        logger.debug("查询临终送别数据 入参 info : {}",info);

        List<Map> businessGovPersonDieRecordInfos = sqlSessionTemplate.selectList("govPersonDieRecordServiceDaoImpl.queryGovPersonDieRecordsCount", info);
        if (businessGovPersonDieRecordInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovPersonDieRecordInfos.get(0).get("count").toString());
    }


}
