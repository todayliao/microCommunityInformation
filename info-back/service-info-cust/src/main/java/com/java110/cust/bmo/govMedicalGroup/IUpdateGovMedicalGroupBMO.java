package com.java110.cust.bmo.govMedicalGroup;
import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;

public interface IUpdateGovMedicalGroupBMO {


    /**
     * 修改医疗团队
     * add by wuxw
     * @param govMedicalGroupPo
     * @return
     */
    ResponseEntity<String> update(GovMedicalGroupPo govMedicalGroupPo);


}
