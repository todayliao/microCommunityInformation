package com.java110.cust.bmo.govGrid;
import com.java110.dto.govGrid.GovGridDto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetGovGridBMO {


    /**
     * 查询网格人员
     * add by wuxw
     * @param  govGridDto
     * @return
     */
    ResponseEntity<String> get(GovGridDto govGridDto);


}
