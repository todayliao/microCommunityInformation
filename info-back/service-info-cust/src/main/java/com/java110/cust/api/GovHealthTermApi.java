package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
import com.java110.po.govHealthTerm.GovHealthTermPo;
import com.java110.cust.bmo.govHealthTerm.IDeleteGovHealthTermBMO;
import com.java110.cust.bmo.govHealthTerm.IGetGovHealthTermBMO;
import com.java110.cust.bmo.govHealthTerm.ISaveGovHealthTermBMO;
import com.java110.cust.bmo.govHealthTerm.IUpdateGovHealthTermBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govHealthTerm")
public class GovHealthTermApi {

    @Autowired
    private ISaveGovHealthTermBMO saveGovHealthTermBMOImpl;
    @Autowired
    private IUpdateGovHealthTermBMO updateGovHealthTermBMOImpl;
    @Autowired
    private IDeleteGovHealthTermBMO deleteGovHealthTermBMOImpl;

    @Autowired
    private IGetGovHealthTermBMO getGovHealthTermBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTerm/saveGovHealthTerm
     * @path /app/govHealthTerm/saveGovHealthTerm
     */
    @RequestMapping(value = "/saveGovHealthTerm", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovHealthTerm(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "termName", "请求报文中未包含termName" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "seq", "请求报文中未包含seq" );


        GovHealthTermPo govHealthTermPo = BeanConvertUtil.covertBean( reqJson, GovHealthTermPo.class );
        return saveGovHealthTermBMOImpl.save( govHealthTermPo );
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTerm/updateGovHealthTerm
     * @path /app/govHealthTerm/updateGovHealthTerm
     */
    @RequestMapping(value = "/updateGovHealthTerm", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovHealthTerm(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "termId", "请求报文中未包含termId" );
        Assert.hasKeyAndValue( reqJson, "termName", "请求报文中未包含termName" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "seq", "请求报文中未包含seq" );


        GovHealthTermPo govHealthTermPo = BeanConvertUtil.covertBean( reqJson, GovHealthTermPo.class );
        return updateGovHealthTermBMOImpl.update( govHealthTermPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govHealthTerm/deleteGovHealthTerm
     * @path /app/govHealthTerm/deleteGovHealthTerm
     */
    @RequestMapping(value = "/deleteGovHealthTerm", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovHealthTerm(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "termId", "termId不能为空" );


        GovHealthTermPo govHealthTermPo = BeanConvertUtil.covertBean( reqJson, GovHealthTermPo.class );
        return deleteGovHealthTermBMOImpl.delete( govHealthTermPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govHealthTerm/queryGovHealthTerm
     * @path /app/govHealthTerm/queryGovHealthTerm
     */
    @RequestMapping(value = "/queryGovHealthTerm", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovHealthTerm(@RequestParam(value = "caId") String caId,
                                                     @RequestParam(value = "page") int page,
                                                     @RequestParam(value = "row") int row) {

        GovHealthTermDto govHealthTermDto = new GovHealthTermDto();
        govHealthTermDto.setPage( page );
        govHealthTermDto.setRow( row );
        govHealthTermDto.setCaId( caId );
        return getGovHealthTermBMOImpl.get( govHealthTermDto );
    }
}
