package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govDoctorHealthy.GovDoctorHealthyDto;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;
import com.java110.cust.bmo.govDoctorHealthy.IDeleteGovDoctorHealthyBMO;
import com.java110.cust.bmo.govDoctorHealthy.IGetGovDoctorHealthyBMO;
import com.java110.cust.bmo.govDoctorHealthy.ISaveGovDoctorHealthyBMO;
import com.java110.cust.bmo.govDoctorHealthy.IUpdateGovDoctorHealthyBMO;
import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govDoctorHealthy")
public class GovDoctorHealthyApi {

    @Autowired
    private ISaveGovDoctorHealthyBMO saveGovDoctorHealthyBMOImpl;
    @Autowired
    private IUpdateGovDoctorHealthyBMO updateGovDoctorHealthyBMOImpl;
    @Autowired
    private IDeleteGovDoctorHealthyBMO deleteGovDoctorHealthyBMOImpl;

    @Autowired
    private IGetGovDoctorHealthyBMO getGovDoctorHealthyBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govDoctorHealthy/saveGovDoctorHealthy
     * @path /app/govDoctorHealthy/saveGovDoctorHealthy
     */
    @RequestMapping(value = "/saveGovDoctorHealthy", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovDoctorHealthy(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "doctorNum", "请求报文中未包含doctorNum" );
        Assert.hasKeyAndValue( reqJson, "idType", "请求报文中未包含idType" );
        Assert.hasKeyAndValue( reqJson, "birthday", "请求报文中未包含birthday" );
        Assert.hasKeyAndValue( reqJson, "jobName", "请求报文中未包含jobName" );
        Assert.hasKeyAndValue( reqJson, "titleName", "请求报文中未包含titleName" );
        Assert.hasKeyAndValue( reqJson, "personName", "请求报文中未包含personName" );
        Assert.hasKeyAndValue( reqJson, "personTel", "请求报文中未包含personTel" );
        Assert.hasKeyAndValue( reqJson, "personSex", "请求报文中未包含personSex" );
        Assert.hasKeyAndValue( reqJson, "businessExpertise", "请求报文中未包含businessExpertise" );
        Assert.hasKeyAndValue( reqJson, "labelCd", "请求报文中未包含labelCd" );

        Assert.hasKeyAndValue( reqJson, "groupId", "请求报文中未包含groupId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );

        GovDoctorHealthyPo govDoctorHealthyPo = BeanConvertUtil.covertBean( reqJson, GovDoctorHealthyPo.class );
        govDoctorHealthyPo.setDatasourceType("999999");
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        govPersonPo.setIsWeb("F");
        govPersonPo.setDatasourceType("999999");

        GovMedicalDoctorRelPo govMedicalDoctorRelPo = BeanConvertUtil.covertBean( reqJson, GovMedicalDoctorRelPo.class );

        return saveGovDoctorHealthyBMOImpl.save( govDoctorHealthyPo ,govPersonPo,govMedicalDoctorRelPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govDoctorHealthy/updateGovDoctorHealthy
     * @path /app/govDoctorHealthy/updateGovDoctorHealthy
     */
    @RequestMapping(value = "/updateGovDoctorHealthy", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovDoctorHealthy(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue( reqJson, "govDoctorId", "请求报文中未包含govDoctorId" );
        Assert.hasKeyAndValue( reqJson, "caId", "请求报文中未包含caId" );
        Assert.hasKeyAndValue( reqJson, "doctorNum", "请求报文中未包含doctorNum" );
        Assert.hasKeyAndValue( reqJson, "idType", "请求报文中未包含idType" );
        Assert.hasKeyAndValue( reqJson, "govPersonId", "请求报文中未包含govPersonId" );
        Assert.hasKeyAndValue( reqJson, "birthday", "请求报文中未包含birthday" );
        Assert.hasKeyAndValue( reqJson, "jobName", "请求报文中未包含jobName" );
        Assert.hasKeyAndValue( reqJson, "titleName", "请求报文中未包含titleName" );
        Assert.hasKeyAndValue( reqJson, "personName", "请求报文中未包含personName" );
        Assert.hasKeyAndValue( reqJson, "personTel", "请求报文中未包含personTel" );
        Assert.hasKeyAndValue( reqJson, "personSex", "请求报文中未包含personSex" );
        Assert.hasKeyAndValue( reqJson, "businessExpertise", "请求报文中未包含businessExpertise" );
        Assert.hasKeyAndValue( reqJson, "labelCd", "请求报文中未包含labelCd" );

        Assert.hasKeyAndValue( reqJson, "groupId", "请求报文中未包含groupId" );
        Assert.hasKeyAndValue( reqJson, "name", "请求报文中未包含name" );
        Assert.hasKeyAndValue( reqJson, "relId", "请求报文中未包含relId" );

        GovDoctorHealthyPo govDoctorHealthyPo = BeanConvertUtil.covertBean( reqJson, GovDoctorHealthyPo.class );
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(reqJson, GovPersonPo.class);
        GovMedicalDoctorRelPo govMedicalDoctorRelPo = BeanConvertUtil.covertBean( reqJson, GovMedicalDoctorRelPo.class );

        return updateGovDoctorHealthyBMOImpl.update( govDoctorHealthyPo ,govPersonPo,govMedicalDoctorRelPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govDoctorHealthy/deleteGovDoctorHealthy
     * @path /app/govDoctorHealthy/deleteGovDoctorHealthy
     */
    @RequestMapping(value = "/deleteGovDoctorHealthy", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovDoctorHealthy(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue( reqJson, "caId", "caId不能为空" );

        Assert.hasKeyAndValue( reqJson, "govDoctorId", "govDoctorId不能为空" );


        GovDoctorHealthyPo govDoctorHealthyPo = BeanConvertUtil.covertBean( reqJson, GovDoctorHealthyPo.class );
        return deleteGovDoctorHealthyBMOImpl.delete( govDoctorHealthyPo );
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /govDoctorHealthy/queryGovDoctorHealthy
     * @path /app/govDoctorHealthy/queryGovDoctorHealthy
     */
    @RequestMapping(value = "/queryGovDoctorHealthy", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovDoctorHealthy(@RequestParam(value = "caId") String caId,
                                                        @RequestParam(value = "personNameLike",required = false) String personNameLike,
                                                        @RequestParam(value = "personTel",required = false) String personTel,
                                                        @RequestParam(value = "govPersonId",required = false) String govPersonId,
                                                        @RequestParam(value = "groupId",required = false) String groupId,
                                                        @RequestParam(value = "doctorNum",required = false) String doctorNum,
                                                        @RequestParam(value = "jobName",required = false) String jobName,
                                                        @RequestParam(value = "titleName",required = false) String titleName,
                                                        @RequestParam(value = "labelCd",required = false) String labelCd,
                                                        @RequestParam(value = "page") int page,
                                                        @RequestParam(value = "row") int row) {
        GovDoctorHealthyDto govDoctorHealthyDto = new GovDoctorHealthyDto();
        govDoctorHealthyDto.setPage( page );
        govDoctorHealthyDto.setRow( row );
        govDoctorHealthyDto.setCaId( caId );
        govDoctorHealthyDto.setPersonNameLike( personNameLike );
        govDoctorHealthyDto.setPersonTel( personTel );
        govDoctorHealthyDto.setGovPersonId( govPersonId );
        govDoctorHealthyDto.setLabelCd( labelCd );
        govDoctorHealthyDto.setGroupId( groupId );
        govDoctorHealthyDto.setDoctorNum( doctorNum );
        govDoctorHealthyDto.setJobName( jobName );
        govDoctorHealthyDto.setTitleName( titleName );
        return getGovDoctorHealthyBMOImpl.get( govDoctorHealthyDto );
    }
}
