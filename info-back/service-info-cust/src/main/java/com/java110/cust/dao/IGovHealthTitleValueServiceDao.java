package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 体检项值组件内部之间使用，没有给外围系统提供服务能力
 * 体检项值服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovHealthTitleValueServiceDao {


    /**
     * 保存 体检项值信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovHealthTitleValueInfo(Map info) throws DAOException;




    /**
     * 查询体检项值信息（instance过程）
     * 根据bId 查询体检项值信息
     * @param info bId 信息
     * @return 体检项值信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovHealthTitleValueInfo(Map info) throws DAOException;



    /**
     * 修改体检项值信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovHealthTitleValueInfo(Map info) throws DAOException;


    /**
     * 查询体检项值总数
     *
     * @param info 体检项值信息
     * @return 体检项值数量
     */
    int queryGovHealthTitleValuesCount(Map info);

}
