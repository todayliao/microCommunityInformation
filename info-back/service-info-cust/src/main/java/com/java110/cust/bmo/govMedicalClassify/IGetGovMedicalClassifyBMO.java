package com.java110.cust.bmo.govMedicalClassify;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govMedicalClassify.GovMedicalClassifyDto;
public interface IGetGovMedicalClassifyBMO {


    /**
     * 查询医疗分类
     * add by wuxw
     * @param  govMedicalClassifyDto
     * @return
     */
    ResponseEntity<String> get(GovMedicalClassifyDto govMedicalClassifyDto);


}
