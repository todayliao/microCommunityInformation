package com.java110.cust.bmo.govVolunteer.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govVolunteer.ISaveGovVolunteerBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.govPerson.GovPersonDto;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.intf.cust.IGovVolunteerServInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
import com.java110.utils.constant.MallConstant;
import com.java110.utils.util.DateUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.po.govVolunteer.GovVolunteerPo;
import com.java110.intf.cust.IGovVolunteerInnerServiceSMO;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovVolunteerBMOImpl")
public class SaveGovVolunteerBMOImpl implements ISaveGovVolunteerBMO {

    @Autowired
    private IGovVolunteerInnerServiceSMO govVolunteerInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovVolunteerServInnerServiceSMO govVolunteerServInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * 添加小区信息
     *
     * @param
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(JSONObject reqJson) {
        GovVolunteerPo govVolunteerPo = BeanConvertUtil.covertBean(reqJson, GovVolunteerPo.class);
        JSONArray goodAtSkills = reqJson.getJSONArray("goodAtSkills");
        JSONArray freeTimes = reqJson.getJSONArray("freeTime");
        JSONObject personss = reqJson.getJSONObject("persons");
        JSONArray volunteerServs = reqJson.getJSONArray("volunteerServ");
        govVolunteerPo.setGoodAtSkills(goodAtSkills.toJSONString());
        govVolunteerPo.setFreeTime(freeTimes.toJSONString());
        //处理人口信息
        GovPersonPo govPersonPo = BeanConvertUtil.covertBean(personss, GovPersonPo.class);
        GovPersonPo govPerson = getGovPersonPo(govPersonPo, govVolunteerPo);

        govVolunteerPo.setGovPersonId(govPerson.getGovPersonId());
        govVolunteerPo.setVolunteerId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_volunteerId));
        govVolunteerPo.setState("10001");
        int flag = govVolunteerInnerServiceSMOImpl.saveGovVolunteer(govVolunteerPo);
        if (flag < 1) {
            throw new IllegalArgumentException("保存志愿人员失败");
        }
        //处理服务领域
        GovVolunteerServPo govVolunteerServPo = new GovVolunteerServPo();
        govVolunteerServPo.setVolunteerId(govVolunteerPo.getVolunteerId());
        govVolunteerServPo.setCaId(govVolunteerPo.getCaId());
        for (int volunteerServsIndex = 0; volunteerServsIndex < volunteerServs.size(); volunteerServsIndex++) {
            govVolunteerServPo.setServId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_servId));
            govVolunteerServPo.setServFieldId(volunteerServs.getString(volunteerServsIndex));
            govVolunteerServPo.setName(govVolunteerPo.getName());
            govVolunteerServPo.setRamark(govVolunteerPo.getRamark());
            flag = govVolunteerServInnerServiceSMOImpl.saveGovVolunteerServ(govVolunteerServPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存服务领域失败");
            }
        }

        reqJson.put("volunteerId",govVolunteerPo.getVolunteerId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.ADD_VOLUNTEER_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
    }

    private GovPersonPo getGovPersonPo(GovPersonPo govPersonPo, GovVolunteerPo govVolunteerPo) {
        govPersonPo.setIsWeb("F");
        govPersonPo.setPersonName(govVolunteerPo.getName());
        govPersonPo.setMaritalStatus("N");
        govPersonPo.setPersonTel(govVolunteerPo.getTel());
        govPersonPo.setCaId(govVolunteerPo.getCaId());
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govGridId));
        govPersonPo.setPersonType("2002");
        govPersonPo.setRamark("系統自动生成");
        govPersonPo.setReligiousBelief("无");
        govPersonPo.setBirthday(DateUtil.getNow(DateUtil.DATE_FORMATE_STRING_B));
        govPersonPo.setDatasourceType("999999");
        govPersonPo.setPrePersonName(govPersonPo.getPersonName());

        GovPersonDto govPersonDto = new GovPersonDto();
        govPersonDto.setPersonTel(govPersonPo.getPersonTel());
        govPersonDto.setPersonName(govPersonPo.getPersonName());
        govPersonDto.setCaId(govPersonPo.getCaId());
        List<GovPersonDto> govPersonDtos = govPersonInnerServiceSMOImpl.queryGovPersons(govPersonDto);
        if (govPersonDtos == null || govPersonDtos.size() < 1) {
            int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存固定人口失败");
            }
        } else {
            govPersonPo.setGovPersonId(govPersonDtos.get(0).getGovPersonId());
            int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);
            if (flag < 1) {
                throw new IllegalArgumentException("修改固定人口失败");
            }
        }
        return govPersonPo;
    }

}
