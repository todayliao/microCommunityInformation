package com.java110.cust.bmo.StoreUserV1;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetStoreUserV1BMO {


    /**
     * 查询用戶管理
     * add by wuxw
     * @param  StoreUserV1Dto
     * @return
     */
    ResponseEntity<String> get(StoreUserV1Dto StoreUserV1Dto);


}
