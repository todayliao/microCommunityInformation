package com.java110.cust.bmo.machineStaffShow.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.machineStaffShow.IGetMachineStaffShowBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.dto.mapping.Mapping;
import com.java110.utils.cache.MappingCache;
import com.java110.utils.constant.MappingConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IMachineStaffShowInnerServiceSMO;
import com.java110.dto.machineStaffShow.MachineStaffShowDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getMachineStaffShowBMOImpl")
public class GetMachineStaffShowBMOImpl implements IGetMachineStaffShowBMO {

    @Autowired
    private IMachineStaffShowInnerServiceSMO machineStaffShowInnerServiceSMOImpl;

    /**
     * @param machineStaffShowDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(MachineStaffShowDto machineStaffShowDto) {


        int count = machineStaffShowInnerServiceSMOImpl.queryMachineStaffShowsCount( machineStaffShowDto );

        List<MachineStaffShowDto> machineStaffShowDtos = null;
        if (count > 0) {
            machineStaffShowDtos = machineStaffShowInnerServiceSMOImpl.queryMachineStaffShows( machineStaffShowDto );
            reshWebSocketUrl( machineStaffShowDtos );
        } else {
            machineStaffShowDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo( (int) Math.ceil( (double) count / (double) machineStaffShowDto.getRow() ), count, machineStaffShowDtos );

        ResponseEntity<String> responseEntity = new ResponseEntity<String>( resultVo.toString(), HttpStatus.OK );

        return responseEntity;
    }

    private void reshWebSocketUrl(List<MachineStaffShowDto> machineStaffShowDtos) {
        if (machineStaffShowDtos == null || machineStaffShowDtos.size() < 1) {
            return;
        }
        for (MachineStaffShowDto machineStaffShowDto : machineStaffShowDtos) {
            machineStaffShowDto.setWebsocketUrl( MappingCache.getValue( MappingConstant.KEY_DOMAIN_COMMON, MappingConstant.WEB_SOCKET_URL ) );
        }
    }

}
