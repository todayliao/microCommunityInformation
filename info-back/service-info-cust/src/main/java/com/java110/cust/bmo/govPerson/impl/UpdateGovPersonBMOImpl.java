package com.java110.cust.bmo.govPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govPerson.IUpdateGovPersonBMO;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("updateGovPersonBMOImpl")
public class UpdateGovPersonBMOImpl implements IUpdateGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;

    /**
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovPersonPo govPersonPo) {

        int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);

        if (flag > 0) {
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    /**
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> updateGovPersonCompany(GovPersonPo govPersonPo) {

        int flag = govPersonInnerServiceSMOImpl.updateGovPerson(govPersonPo);

        if (flag > 0) {
            //处理员工企业关系信息
            GovCompanyPersonPo govCompanyPersonPo = new GovCompanyPersonPo();
            govCompanyPersonPo.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_relId));
            govCompanyPersonPo.setGovCompanyId(govPersonPo.getGovCompanyId());
            govCompanyPersonPo.setGovPersonId(govPersonPo.getGovPersonId());
            govCompanyPersonPo.setCaId(govPersonPo.getCaId());
            govCompanyPersonPo.setState(govPersonPo.getState());
            govCompanyPersonPo.setRelCd(govPersonPo.getRelCd());
            govCompanyPersonPo.setGovOrgName(govPersonPo.getGovOrgName());

            GovCompanyPersonDto govCompanyPersonDto = new GovCompanyPersonDto();
            govCompanyPersonDto.setCaId(govPersonPo.getCaId());
            govCompanyPersonDto.setGovCompanyId(govPersonPo.getGovCompanyId());
            govCompanyPersonDto.setGovPersonId(govPersonPo.getGovPersonId());
            List<GovCompanyPersonDto> govCompanyPersonDtos = govCompanyPersonInnerServiceSMOImpl.queryGovCompanyPersons(govCompanyPersonDto);

            if (govCompanyPersonDtos == null || govCompanyPersonDtos.size() < 1) {
                flag = govCompanyPersonInnerServiceSMOImpl.saveGovCompanyPerson(govCompanyPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("保存员工企业关系信息失败");
                }
            } else {
                govCompanyPersonPo.setRelId(govCompanyPersonDtos.get(0).getRelId());
                flag = govCompanyPersonInnerServiceSMOImpl.updateGovCompanyPerson(govCompanyPersonPo);
                if (flag < 1) {
                    throw new IllegalArgumentException("修改员工企业关系信息失败");
                }
            }

            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
