package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovOldPersonTypeServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 老人类型服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govOldPersonTypeServiceDaoImpl")
//@Transactional
public class GovOldPersonTypeServiceDaoImpl extends BaseServiceDao implements IGovOldPersonTypeServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovOldPersonTypeServiceDaoImpl.class);





    /**
     * 保存老人类型信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovOldPersonTypeInfo(Map info) throws DAOException {
        logger.debug("保存老人类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govOldPersonTypeServiceDaoImpl.saveGovOldPersonTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存老人类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询老人类型信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovOldPersonTypeInfo(Map info) throws DAOException {
        logger.debug("查询老人类型信息 入参 info : {}",info);

        List<Map> businessGovOldPersonTypeInfos = sqlSessionTemplate.selectList("govOldPersonTypeServiceDaoImpl.getGovOldPersonTypeInfo",info);

        return businessGovOldPersonTypeInfos;
    }


    /**
     * 修改老人类型信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovOldPersonTypeInfo(Map info) throws DAOException {
        logger.debug("修改老人类型信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govOldPersonTypeServiceDaoImpl.updateGovOldPersonTypeInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改老人类型信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询老人类型数量
     * @param info 老人类型信息
     * @return 老人类型数量
     */
    @Override
    public int queryGovOldPersonTypesCount(Map info) {
        logger.debug("查询老人类型数据 入参 info : {}",info);

        List<Map> businessGovOldPersonTypeInfos = sqlSessionTemplate.selectList("govOldPersonTypeServiceDaoImpl.queryGovOldPersonTypesCount", info);
        if (businessGovOldPersonTypeInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovOldPersonTypeInfos.get(0).get("count").toString());
    }


}
