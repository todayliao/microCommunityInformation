package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 摄像头员工关系组件内部之间使用，没有给外围系统提供服务能力
 * 摄像头员工关系服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IMachineStaffShowServiceDao {


    /**
     * 保存 摄像头员工关系信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveMachineStaffShowInfo(Map info) throws DAOException;




    /**
     * 查询摄像头员工关系信息（instance过程）
     * 根据bId 查询摄像头员工关系信息
     * @param info bId 信息
     * @return 摄像头员工关系信息
     * @throws DAOException DAO异常
     */
    List<Map> getMachineStaffShowInfo(Map info) throws DAOException;



    /**
     * 修改摄像头员工关系信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateMachineStaffShowInfo(Map info) throws DAOException;


    /**
     * 查询摄像头员工关系总数
     *
     * @param info 摄像头员工关系信息
     * @return 摄像头员工关系数量
     */
    int queryMachineStaffShowsCount(Map info);

}
