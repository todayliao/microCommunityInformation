package com.java110.cust.bmo.govMedicalGroup.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govMedicalGroup.IGetGovMedicalGroupBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovMedicalGroupInnerServiceSMO;
import com.java110.dto.govMedicalGroup.GovMedicalGroupDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovMedicalGroupBMOImpl")
public class GetGovMedicalGroupBMOImpl implements IGetGovMedicalGroupBMO {

    @Autowired
    private IGovMedicalGroupInnerServiceSMO govMedicalGroupInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govMedicalGroupDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovMedicalGroupDto govMedicalGroupDto) {


        int count = govMedicalGroupInnerServiceSMOImpl.queryGovMedicalGroupsCount(govMedicalGroupDto);

        List<GovMedicalGroupDto> govMedicalGroupDtos = null;
        if (count > 0) {
            govMedicalGroupDtos = govMedicalGroupInnerServiceSMOImpl.queryGovMedicalGroups(govMedicalGroupDto);
        } else {
            govMedicalGroupDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govMedicalGroupDto.getRow()), count, govMedicalGroupDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
