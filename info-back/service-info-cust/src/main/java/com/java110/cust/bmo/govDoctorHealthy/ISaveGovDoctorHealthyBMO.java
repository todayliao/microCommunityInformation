package com.java110.cust.bmo.govDoctorHealthy;

import com.java110.po.govMedicalDoctorRel.GovMedicalDoctorRelPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.govDoctorHealthy.GovDoctorHealthyPo;
public interface ISaveGovDoctorHealthyBMO {


    /**
     * 添加档案医生
     * add by wuxw
     * @param govDoctorHealthyPo
     * @return
     */
    ResponseEntity<String> save(GovDoctorHealthyPo govDoctorHealthyPo, GovPersonPo govPersonPo, GovMedicalDoctorRelPo govMedicalDoctorRelPo);


}
