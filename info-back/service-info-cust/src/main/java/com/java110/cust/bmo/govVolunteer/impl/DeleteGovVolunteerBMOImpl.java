package com.java110.cust.bmo.govVolunteer.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govVolunteer.IDeleteGovVolunteerBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.intf.cust.IGovVolunteerServInnerServiceSMO;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import com.java110.po.govVolunteer.GovVolunteerPo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovVolunteerInnerServiceSMO;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteGovVolunteerBMOImpl")
public class DeleteGovVolunteerBMOImpl implements IDeleteGovVolunteerBMO {

    @Autowired
    private IGovVolunteerInnerServiceSMO govVolunteerInnerServiceSMOImpl;
    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovVolunteerServInnerServiceSMO govVolunteerServInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;
    /**
     * @param govVolunteerPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovVolunteerPo govVolunteerPo, GovPersonPo govPersonPo) {

        int flag = govVolunteerInnerServiceSMOImpl.deleteGovVolunteer(govVolunteerPo);

        if (flag < 1) {
            throw new IllegalArgumentException("删除志愿者失败");
        }

        flag = govPersonInnerServiceSMOImpl.deleteGovPerson(govPersonPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除对应人口失败");
        }
        GovVolunteerServPo govVolunteerServPo = new GovVolunteerServPo();
        govVolunteerServPo.setVolunteerId(govVolunteerPo.getVolunteerId());
        govVolunteerServPo.setCaId(govVolunteerPo.getCaId());
        flag = govVolunteerServInnerServiceSMOImpl.deleteGovVolunteerServ(govVolunteerServPo);
        if (flag < 1) {
            throw new IllegalArgumentException("删除对应领域  失败");
        }

        JSONObject reqJson = new JSONObject();
        reqJson.put("volunteerId",govVolunteerPo.getVolunteerId());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.DELETE_VOLUNTEER_MALL, HttpMethod.POST);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "删除成功");
    }

}
