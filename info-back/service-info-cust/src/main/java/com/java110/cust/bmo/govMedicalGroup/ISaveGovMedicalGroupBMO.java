package com.java110.cust.bmo.govMedicalGroup;

import org.springframework.http.ResponseEntity;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;
public interface ISaveGovMedicalGroupBMO {


    /**
     * 添加医疗团队
     * add by wuxw
     * @param govMedicalGroupPo
     * @return
     */
    ResponseEntity<String> save(GovMedicalGroupPo govMedicalGroupPo);


}
