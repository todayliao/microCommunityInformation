package com.java110.cust.bmo.govCompanyPerson.impl;


import com.java110.core.annotation.Java110Transactional;

import com.java110.cust.bmo.govCompanyPerson.IDeleteGovCompanyPersonBMO;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service("deleteGovCompanyPersonBMOImpl")
public class DeleteGovCompanyPersonBMOImpl implements IDeleteGovCompanyPersonBMO {

    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;

    /**
     * @param govCompanyPersonPo 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(GovCompanyPersonPo govCompanyPersonPo) {

        int flag = govCompanyPersonInnerServiceSMOImpl.deleteGovCompanyPerson(govCompanyPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
