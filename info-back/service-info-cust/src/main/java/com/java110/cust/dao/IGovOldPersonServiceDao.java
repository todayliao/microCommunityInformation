package com.java110.cust.dao;


import com.java110.utils.exception.DAOException;


import java.util.List;
import java.util.Map;

/**
 * 老人管理组件内部之间使用，没有给外围系统提供服务能力
 * 老人管理服务接口类，要求全部以字符串传输，方便微服务化
 * 新建客户，修改客户，删除客户，查询客户等功能
 *
 * Created by wuxw on 2016/12/27.
 */
public interface IGovOldPersonServiceDao {


    /**
     * 保存 老人管理信息
     * @param info
     * @throws DAOException DAO异常
     */
    void saveGovOldPersonInfo(Map info) throws DAOException;




    /**
     * 查询老人管理信息（instance过程）
     * 根据bId 查询老人管理信息
     * @param info bId 信息
     * @return 老人管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovOldPersonInfo(Map info) throws DAOException;

    /**
     * 查询老人管理信息（instance过程）
     * 根据bId 查询老人管理信息
     * @param info bId 信息
     * @return 老人管理信息
     * @throws DAOException DAO异常
     */
    List<Map> getGovOldTypePersonCount(Map info) throws DAOException;



    /**
     * 修改老人管理信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    void updateGovOldPersonInfo(Map info) throws DAOException;


    /**
     * 查询老人管理总数
     *
     * @param info 老人管理信息
     * @return 老人管理数量
     */
    int queryGovOldPersonsCount(Map info);

}
