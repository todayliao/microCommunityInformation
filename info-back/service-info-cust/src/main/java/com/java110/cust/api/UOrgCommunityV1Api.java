package com.java110.cust.api;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.uOrgCommunityV1.IDeleteUOrgCommunityV1BMO;
import com.java110.cust.bmo.uOrgCommunityV1.IGetUOrgCommunityV1BMO;
import com.java110.cust.bmo.uOrgCommunityV1.ISaveUOrgCommunityV1BMO;
import com.java110.cust.bmo.uOrgCommunityV1.IUpdateUOrgCommunityV1BMO;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/uOrgCommunityV1")
public class UOrgCommunityV1Api {

    @Autowired
    private ISaveUOrgCommunityV1BMO saveUOrgCommunityV1BMOImpl;
    @Autowired
    private IUpdateUOrgCommunityV1BMO updateUOrgCommunityV1BMOImpl;
    @Autowired
    private IDeleteUOrgCommunityV1BMO deleteUOrgCommunityV1BMOImpl;

    @Autowired
    private IGetUOrgCommunityV1BMO getUOrgCommunityV1BMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgCommunityV1/saveUOrgCommunityV1
     * @path /app/uOrgCommunityV1/saveUOrgCommunityV1
     */
    @RequestMapping(value = "/saveUOrgCommunityV1", method = RequestMethod.POST)
    public ResponseEntity<String> saveUOrgCommunityV1(@RequestBody JSONObject reqJson, @RequestHeader(value = "store-id") String storeId) {

        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");
        Assert.isNotNull(reqJson.getJSONArray("communitys"),"未选择区域信息");
        reqJson.put("storeId", storeId);
        JSONArray communitys = reqJson.getJSONArray("communitys");

        UOrgCommunityV1Po uOrgCommunityV1Po = BeanConvertUtil.covertBean(reqJson, UOrgCommunityV1Po.class);
        return saveUOrgCommunityV1BMOImpl.save(uOrgCommunityV1Po, communitys);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgCommunityV1/updateUOrgCommunityV1
     * @path /app/uOrgCommunityV1/updateUOrgCommunityV1
     */
    @RequestMapping(value = "/updateUOrgCommunityV1", method = RequestMethod.POST)
    public ResponseEntity<String> updateUOrgCommunityV1(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "orgCommunityId", "请求报文中未包含orgCommunityId");
        Assert.hasKeyAndValue(reqJson, "orgId", "请求报文中未包含orgId");
        Assert.hasKeyAndValue(reqJson, "communityId", "请求报文中未包含communityId");
        Assert.hasKeyAndValue(reqJson, "communityName", "请求报文中未包含communityName");
        Assert.hasKeyAndValue(reqJson, "storeId", "请求报文中未包含storeId");
        Assert.hasKeyAndValue(reqJson, "orgName", "请求报文中未包含orgName");


        UOrgCommunityV1Po uOrgCommunityV1Po = BeanConvertUtil.covertBean(reqJson, UOrgCommunityV1Po.class);
        return updateUOrgCommunityV1BMOImpl.update(uOrgCommunityV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /uOrgCommunityV1/deleteUOrgCommunityV1
     * @path /app/uOrgCommunityV1/deleteUOrgCommunityV1
     */
    @RequestMapping(value = "/deleteUOrgCommunityV1", method = RequestMethod.POST)
    public ResponseEntity<String> deleteUOrgCommunityV1(@RequestBody JSONObject reqJson) {
        Assert.hasKeyAndValue(reqJson, "communityId", "小区ID不能为空");

        Assert.hasKeyAndValue(reqJson, "orgCommunityId", "orgCommunityId不能为空");


        UOrgCommunityV1Po uOrgCommunityV1Po = BeanConvertUtil.covertBean(reqJson, UOrgCommunityV1Po.class);
        return deleteUOrgCommunityV1BMOImpl.delete(uOrgCommunityV1Po);
    }

    /**
     * 微信删除消息模板
     *
     * @param
     * @return
     * @serviceCode /uOrgCommunityV1/queryUOrgCommunityV1
     * @path /app/uOrgCommunityV1/queryUOrgCommunityV1
     */
    @RequestMapping(value = "/queryUOrgCommunityV1", method = RequestMethod.GET)
    public ResponseEntity<String> queryUOrgCommunityV1(@RequestParam(value = "orgId") String orgId,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {
        UOrgCommunityV1Dto uOrgCommunityV1Dto = new UOrgCommunityV1Dto();
        uOrgCommunityV1Dto.setPage(page);
        uOrgCommunityV1Dto.setRow(row);
        uOrgCommunityV1Dto.setOrgId(orgId);
        return getUOrgCommunityV1BMOImpl.get(uOrgCommunityV1Dto);
    }
}
