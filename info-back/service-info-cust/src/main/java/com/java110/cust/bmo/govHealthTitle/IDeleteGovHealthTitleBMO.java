package com.java110.cust.bmo.govHealthTitle;
import com.java110.po.govHealthTitle.GovHealthTitlePo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IDeleteGovHealthTitleBMO {


    /**
     * 修改体检项
     * add by wuxw
     * @param govHealthTitlePo
     * @return
     */
    ResponseEntity<String> delete(GovHealthTitlePo govHealthTitlePo);


}
