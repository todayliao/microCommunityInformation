package com.java110.cust.bmo.govVolunteerServ;

import org.springframework.http.ResponseEntity;
import com.java110.po.govVolunteerServ.GovVolunteerServPo;
public interface ISaveGovVolunteerServBMO {


    /**
     * 添加服务领域
     * add by wuxw
     * @param govVolunteerServPo
     * @return
     */
    ResponseEntity<String> save(GovVolunteerServPo govVolunteerServPo);


}
