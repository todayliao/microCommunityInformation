package com.java110.cust.bmo.govEventsType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govEventsType.GovEventsTypeDto;
public interface IGetGovEventsTypeBMO {


    /**
     * 查询事件类型
     * add by wuxw
     * @param  govEventsTypeDto
     * @return
     */
    ResponseEntity<String> get(GovEventsTypeDto govEventsTypeDto);


}
