package com.java110.cust.bmo.govOwnerPerson.impl;


import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;

import com.java110.cust.bmo.govOwnerPerson.ISaveGovOwnerPersonBMO;
import com.java110.intf.cust.IGovOwnerPersonInnerServiceSMO;
import com.java110.po.govOwnerPerson.GovOwnerPersonPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;


@Service("saveGovOwnerPersonBMOImpl")
public class SaveGovOwnerPersonBMOImpl implements ISaveGovOwnerPersonBMO {

    @Autowired
    private IGovOwnerPersonInnerServiceSMO govOwnerPersonInnerServiceSMOImpl;

    /**
     * 添加小区信息
     *
     * @param govOwnerPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovOwnerPersonPo govOwnerPersonPo) {

        govOwnerPersonPo.setGovOpId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govOpId));
        int flag = govOwnerPersonInnerServiceSMOImpl.saveGovOwnerPerson(govOwnerPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
