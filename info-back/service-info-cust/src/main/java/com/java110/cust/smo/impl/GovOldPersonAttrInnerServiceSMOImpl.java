package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovOldPersonAttrServiceDao;
import com.java110.intf.cust.IGovOldPersonAttrInnerServiceSMO;
import com.java110.dto.govOldPersonAttr.GovOldPersonAttrDto;
import com.java110.po.govOldPersonAttr.GovOldPersonAttrPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 老人属性内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovOldPersonAttrInnerServiceSMOImpl extends BaseServiceSMO implements IGovOldPersonAttrInnerServiceSMO {

    @Autowired
    private IGovOldPersonAttrServiceDao govOldPersonAttrServiceDaoImpl;


    @Override
    public int saveGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo) {
        int saveFlag = 1;
        govOldPersonAttrServiceDaoImpl.saveGovOldPersonAttrInfo(BeanConvertUtil.beanCovertMap(govOldPersonAttrPo));
        return saveFlag;
    }

     @Override
    public int updateGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo) {
        int saveFlag = 1;
         govOldPersonAttrServiceDaoImpl.updateGovOldPersonAttrInfo(BeanConvertUtil.beanCovertMap(govOldPersonAttrPo));
        return saveFlag;
    }

     @Override
    public int deleteGovOldPersonAttr(@RequestBody  GovOldPersonAttrPo govOldPersonAttrPo) {
        int saveFlag = 1;
        govOldPersonAttrPo.setStatusCd("1");
        govOldPersonAttrServiceDaoImpl.updateGovOldPersonAttrInfo(BeanConvertUtil.beanCovertMap(govOldPersonAttrPo));
        return saveFlag;
    }

    @Override
    public List<GovOldPersonAttrDto> queryGovOldPersonAttrs(@RequestBody  GovOldPersonAttrDto govOldPersonAttrDto) {

        //校验是否传了 分页信息

        int page = govOldPersonAttrDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOldPersonAttrDto.setPage((page - 1) * govOldPersonAttrDto.getRow());
        }

        List<GovOldPersonAttrDto> govOldPersonAttrs = BeanConvertUtil.covertBeanList(govOldPersonAttrServiceDaoImpl.getGovOldPersonAttrInfo(BeanConvertUtil.beanCovertMap(govOldPersonAttrDto)), GovOldPersonAttrDto.class);

        return govOldPersonAttrs;
    }


    @Override
    public int queryGovOldPersonAttrsCount(@RequestBody GovOldPersonAttrDto govOldPersonAttrDto) {
        return govOldPersonAttrServiceDaoImpl.queryGovOldPersonAttrsCount(BeanConvertUtil.beanCovertMap(govOldPersonAttrDto));    }

    public IGovOldPersonAttrServiceDao getGovOldPersonAttrServiceDaoImpl() {
        return govOldPersonAttrServiceDaoImpl;
    }

    public void setGovOldPersonAttrServiceDaoImpl(IGovOldPersonAttrServiceDao govOldPersonAttrServiceDaoImpl) {
        this.govOldPersonAttrServiceDaoImpl = govOldPersonAttrServiceDaoImpl;
    }
}
