package com.java110.cust.bmo.uOrgStaffRelV1;
import com.java110.dto.uOrgStaffRelV1.UOrgStaffRelV1Dto;
import org.springframework.http.ResponseEntity;
import com.java110.dto.store.StoreDto;
public interface IGetUOrgStaffRelV1BMO {


    /**
     * 查询员工角色关系
     * add by wuxw
     * @param  uOrgStaffRelV1Dto
     * @return
     */
    ResponseEntity<String> get(UOrgStaffRelV1Dto uOrgStaffRelV1Dto);


}
