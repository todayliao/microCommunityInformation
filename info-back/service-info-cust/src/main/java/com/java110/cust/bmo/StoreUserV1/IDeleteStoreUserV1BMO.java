package com.java110.cust.bmo.StoreUserV1;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;

public interface IDeleteStoreUserV1BMO {


    /**
     * 修改用戶管理
     * add by wuxw
     * @param StoreUserV1Po
     * @return
     */
    ResponseEntity<String> delete(StoreUserV1Po StoreUserV1Po);


}
