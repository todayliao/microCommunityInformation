package com.java110.cust.bmo.govHealth.impl;

import com.java110.cust.bmo.govHealth.IGetGovHealthBMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthInnerServiceSMO;
import com.java110.dto.govHealth.GovHealthDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthBMOImpl")
public class GetGovHealthBMOImpl implements IGetGovHealthBMO {

    @Autowired
    private IGovHealthInnerServiceSMO govHealthInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHealthDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthDto govHealthDto) {


        int count = govHealthInnerServiceSMOImpl.queryGovHealthsCount(govHealthDto);

        List<GovHealthDto> govHealthDtos = null;
        if (count > 0) {
            govHealthDtos = govHealthInnerServiceSMOImpl.queryGovHealths(govHealthDto);
        } else {
            govHealthDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHealthDto.getRow()), count, govHealthDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
