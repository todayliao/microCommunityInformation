package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govCompanyPerson.IDeleteGovCompanyPersonBMO;
import com.java110.cust.bmo.govCompanyPerson.IGetGovCompanyPersonBMO;
import com.java110.cust.bmo.govCompanyPerson.ISaveGovCompanyPersonBMO;
import com.java110.cust.bmo.govCompanyPerson.IUpdateGovCompanyPersonBMO;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govCompanyPerson")
public class GovCompanyPersonApi {

    @Autowired
    private ISaveGovCompanyPersonBMO saveGovCompanyPersonBMOImpl;
    @Autowired
    private IUpdateGovCompanyPersonBMO updateGovCompanyPersonBMOImpl;
    @Autowired
    private IDeleteGovCompanyPersonBMO deleteGovCompanyPersonBMOImpl;

    @Autowired
    private IGetGovCompanyPersonBMO getGovCompanyPersonBMOImpl;

    /**
     * 微信保存消息模板
     * @serviceCode /govCompanyPerson/saveGovCompanyPerson
     * @path /app/govCompanyPerson/saveGovCompanyPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/saveGovCompanyPerson", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovCompanyPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govOrgName", "请求报文中未包含govOrgName");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");


        GovCompanyPersonPo govCompanyPersonPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPersonPo.class);
        return saveGovCompanyPersonBMOImpl.save(govCompanyPersonPo);
    }

    /**
     * 微信修改消息模板
     * @serviceCode /govCompanyPerson/updateGovCompanyPerson
     * @path /app/govCompanyPerson/updateGovCompanyPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/updateGovCompanyPerson", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovCompanyPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "relId", "请求报文中未包含relId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "govOrgName", "请求报文中未包含govOrgName");
        Assert.hasKeyAndValue(reqJson, "govPersonId", "请求报文中未包含govPersonId");
        Assert.hasKeyAndValue(reqJson, "govCompanyId", "请求报文中未包含govCompanyId");
        Assert.hasKeyAndValue(reqJson, "state", "请求报文中未包含state");
        Assert.hasKeyAndValue(reqJson, "relCd", "请求报文中未包含relCd");
        Assert.hasKeyAndValue(reqJson, "relId", "relId不能为空");


        GovCompanyPersonPo govCompanyPersonPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPersonPo.class);
        return updateGovCompanyPersonBMOImpl.update(govCompanyPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCompanyPerson/deleteGovCompanyPerson
     * @path /app/govCompanyPerson/deleteGovCompanyPerson
     * @param reqJson
     * @return
     */
    @RequestMapping(value = "/deleteGovCompanyPerson", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovCompanyPerson(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "relId", "relId不能为空");


        GovCompanyPersonPo govCompanyPersonPo = BeanConvertUtil.covertBean(reqJson, GovCompanyPersonPo.class);
        return deleteGovCompanyPersonBMOImpl.delete(govCompanyPersonPo);
    }

    /**
     * 微信删除消息模板
     * @serviceCode /govCompanyPerson/queryGovCompanyPerson
     * @path /app/govCompanyPerson/queryGovCompanyPerson
     * @param caId 小区ID
     * @return
     */
    @RequestMapping(value = "/queryGovCompanyPerson", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovCompanyPerson(@RequestParam(value = "caId") String caId,
                                                      @RequestParam(value = "page") int page,
                                                      @RequestParam(value = "row") int row) {
        GovCompanyPersonDto govCompanyPersonDto = new GovCompanyPersonDto();
        govCompanyPersonDto.setPage(page);
        govCompanyPersonDto.setRow(row);
        govCompanyPersonDto.setCaId(caId);
        return getGovCompanyPersonBMOImpl.get(govCompanyPersonDto);
    }
}
