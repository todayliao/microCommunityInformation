package com.java110.cust.bmo.StoreUserV1.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.StoreUserV1.IDeleteStoreUserV1BMO;
import com.java110.po.StoreUserV1.StoreUserV1Po;
import com.java110.vo.ResultVo;
import com.java110.po.store.StorePo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IStoreUserV1InnerServiceSMO;
import com.java110.dto.StoreUserV1.StoreUserV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("deleteStoreUserV1BMOImpl")
public class DeleteStoreUserV1BMOImpl implements IDeleteStoreUserV1BMO {

    @Autowired
    private IStoreUserV1InnerServiceSMO StoreUserV1InnerServiceSMOImpl;

    /**
     * @param StoreUserV1Po 数据
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> delete(StoreUserV1Po StoreUserV1Po) {

        int flag = StoreUserV1InnerServiceSMOImpl.deleteStoreUserV1(StoreUserV1Po);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
