package com.java110.cust.bmo.uOrgStaffRelV1;

import com.java110.po.uOrgStaffRelV1.UOrgStaffRelV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveUOrgStaffRelV1BMO {


    /**
     * 添加员工角色关系
     * add by wuxw
     * @param uOrgStaffRelV1Po
     * @return
     */
    ResponseEntity<String> save(UOrgStaffRelV1Po uOrgStaffRelV1Po);


}
