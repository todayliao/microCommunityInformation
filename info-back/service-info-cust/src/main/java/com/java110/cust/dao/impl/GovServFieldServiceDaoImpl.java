package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovServFieldServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 服务领域服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govServFieldServiceDaoImpl")
//@Transactional
public class GovServFieldServiceDaoImpl extends BaseServiceDao implements IGovServFieldServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovServFieldServiceDaoImpl.class);





    /**
     * 保存服务领域信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovServFieldInfo(Map info) throws DAOException {
        logger.debug("保存服务领域信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govServFieldServiceDaoImpl.saveGovServFieldInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存服务领域信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询服务领域信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovServFieldInfo(Map info) throws DAOException {
        logger.debug("查询服务领域信息 入参 info : {}",info);

        List<Map> businessGovServFieldInfos = sqlSessionTemplate.selectList("govServFieldServiceDaoImpl.getGovServFieldInfo",info);

        return businessGovServFieldInfos;
    }


    /**
     * 修改服务领域信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovServFieldInfo(Map info) throws DAOException {
        logger.debug("修改服务领域信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govServFieldServiceDaoImpl.updateGovServFieldInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改服务领域信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询服务领域数量
     * @param info 服务领域信息
     * @return 服务领域数量
     */
    @Override
    public int queryGovServFieldsCount(Map info) {
        logger.debug("查询服务领域数据 入参 info : {}",info);

        List<Map> businessGovServFieldInfos = sqlSessionTemplate.selectList("govServFieldServiceDaoImpl.queryGovServFieldsCount", info);
        if (businessGovServFieldInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovServFieldInfos.get(0).get("count").toString());
    }


}
