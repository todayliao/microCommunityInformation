package com.java110.cust.bmo.govOldPerson.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.java110.core.client.RestTemplate;
import com.java110.core.factory.RestTemplateFactory;
import com.java110.cust.bmo.govOldPerson.IGetGovOldPersonBMO;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.dto.govVolunteer.GovVolunteerDto;
import com.java110.intf.cust.IGovHealthAnswerValueInnerServiceSMO;
import com.java110.utils.constant.MallConstant;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovOldPersonInnerServiceSMO;
import com.java110.dto.govOldPerson.GovOldPersonDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service("getGovOldPersonBMOImpl")
public class GetGovOldPersonBMOImpl implements IGetGovOldPersonBMO {

    @Autowired
    private IGovOldPersonInnerServiceSMO govOldPersonInnerServiceSMOImpl;
    @Autowired
    private IGovHealthAnswerValueInnerServiceSMO govHealthAnswerValueInnerServiceSMOImpl;
    @Autowired
    private RestTemplate outRestTemplate;

    public static final String CART_TYPE_PRODUCT = "3306";//商品
    public static final String CART_TYPE_SERV = "3307";//服务
    /**
     *
     *
     * @param  govOldPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovOldPersonDto govOldPersonDto) {


        int count = govOldPersonInnerServiceSMOImpl.queryGovOldPersonsCount(govOldPersonDto);

        List<GovOldPersonDto> govOldPersonDtos = null;
        if (count > 0) {
            govOldPersonDtos = govOldPersonInnerServiceSMOImpl.queryGovOldPersons(govOldPersonDto);
            //resfGovHealthAnswer(govOldPersonDtos);
        } else {
            govOldPersonDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govOldPersonDto.getRow()), count, govOldPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }
    /**
     *
     *
     * @param  govOldPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getGovOldTypePersonCount(GovOldPersonDto govOldPersonDto) {



        List<GovOldPersonDto> govOldPersonDtos = govOldPersonInnerServiceSMOImpl.getGovOldTypePersonCount(govOldPersonDto);

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) 10 / (double) govOldPersonDto.getRow()), 10, govOldPersonDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

    private void resfGovHealthAnswer(List<GovOldPersonDto> govOldPersonDtos) {

        if (govOldPersonDtos == null || govOldPersonDtos.size() < 1) {
            return;
        }
        List<String> oldIds = new ArrayList<>();
        for (GovOldPersonDto govOldPersonDto : govOldPersonDtos) {
            oldIds.add(govOldPersonDto.getOldId());
        }

        GovHealthAnswerValueDto govHealthAnswerValueDto = new GovHealthAnswerValueDto();
        govHealthAnswerValueDto.setPersonIds(oldIds.toArray(new String[oldIds.size()]));
        govHealthAnswerValueDto.setCaId(govOldPersonDtos.get(0).getCaId());
        List<GovHealthAnswerValueDto> govHealthAnswerValueDtos
                = govHealthAnswerValueInnerServiceSMOImpl.queryGovHealthAnswerValues(govHealthAnswerValueDto);

        List<GovHealthAnswerValueDto> tmpGovHealthAnswerValueDtos = null;
        for (GovOldPersonDto govOldPersonDto : govOldPersonDtos) {
            tmpGovHealthAnswerValueDtos = new ArrayList<>();
            for (GovHealthAnswerValueDto govHealthAnswerValue : govHealthAnswerValueDtos) {
                if (govHealthAnswerValue.getPersonId().equals(govOldPersonDto.getOldId())) {
                    tmpGovHealthAnswerValueDtos.add(govHealthAnswerValue);
                }
            }
            govOldPersonDto.setGovHealthAnswerValueDtos(tmpGovHealthAnswerValueDtos);
        }


    }

    /**
     *
     *
     * @param  govOldPersonDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> queryGovOldAccountDetail(GovOldPersonDto govOldPersonDto) {

        JSONObject reqJson = new JSONObject();
        reqJson.put("govCommunityId",govOldPersonDto.getGovCommunityId());
        reqJson.put("objId",govOldPersonDto.getOldId());
        reqJson.put("acctType",GovOldPersonDto.ACCT_TYPE);
        reqJson.put("extUserSpecCd",GovOldPersonDto.EXT_USER_SPEC_CD);
        reqJson.put("objType",GovOldPersonDto.OBJ_TYPE);
//        reqJson.put("detailType",govOldPersonDto.getDetailType());
//        reqJson.put("orderId",govOldPersonDto.getOrderId());
        reqJson.put("page",govOldPersonDto.getPage());
        reqJson.put("row",govOldPersonDto.getRow());
        ResponseEntity<String> responseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.GET_ACCOUNT_DETAIL_MALL, HttpMethod.GET);
        if (responseEntity != null) {
            if (responseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(responseEntity.getBody());
            }
        }
        return responseEntity;
    }
    /**
     *
     *
     * @param
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> queryAccountAmount() {

        JSONObject oldReqJson = new JSONObject();
        oldReqJson.put("acctType",GovOldPersonDto.ACCT_TYPE);
        oldReqJson.put("extUserSpecCd",GovOldPersonDto.EXT_USER_SPEC_CD);
        oldReqJson.put("page","1");
        oldReqJson.put("row","10");

        ResponseEntity<String> oldResponseEntity = RestTemplateFactory.restOutMallTemplate(oldReqJson, outRestTemplate, MallConstant.GET_ACCOUNT_MALL, HttpMethod.GET);
        if (oldResponseEntity != null) {
            if (oldResponseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(oldResponseEntity.getBody());
            }
        }
        JSONArray oldAmountArray = JSONArray.parseArray(oldResponseEntity.getBody().toString());
        String oldAmount = oldAmountArray.getJSONObject(0).getString("amount");


        JSONObject volunReqJson = new JSONObject();
        volunReqJson.put("acctType", GovVolunteerDto.ACCT_TYPE);
        volunReqJson.put("extUserSpecCd",GovVolunteerDto.EXT_USER_SPEC_CD);
        volunReqJson.put("page","1");
        volunReqJson.put("row","10");

        ResponseEntity<String> volunResponseEntity = RestTemplateFactory.restOutMallTemplate(volunReqJson, outRestTemplate, MallConstant.GET_ACCOUNT_MALL, HttpMethod.GET);
        if (volunResponseEntity != null) {
            if (volunResponseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(volunResponseEntity.getBody());
            }
        }
        JSONArray volunAmountArray = JSONArray.parseArray(volunResponseEntity.getBody().toString());
        String volunAmount = volunAmountArray.getJSONObject(0).getString("amount");

        double outOldAmount = Double.parseDouble(oldAmount);
        double outaVolunAmount = Double.parseDouble(volunAmount);
        double amout = 0.00;
        BigDecimal amountBig = new BigDecimal(outOldAmount);
        amout = amountBig.add(new BigDecimal(outaVolunAmount)).doubleValue();

        JSONObject returAmount = new JSONObject();
        returAmount.put("outOldAmount",outOldAmount);
        returAmount.put("outaVolunAmount",outaVolunAmount);
        returAmount.put("amout",amout);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(returAmount.toJSONString(), HttpStatus.OK);
        return responseEntity;
    }

    /**
     *
     *
     * @param
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> queryStoreOrderCart(int page,int row) {

        JSONObject productReqJson = new JSONObject();
        productReqJson.put("cartType",CART_TYPE_PRODUCT);
        productReqJson.put("page","1");
        productReqJson.put("row","1");

        ResponseEntity<String>productResponseEntity = RestTemplateFactory.restOutMallTemplate(productReqJson, outRestTemplate, MallConstant.GET_STORE_ORDER_MALL, HttpMethod.GET);
        if (productResponseEntity != null) {
            if (productResponseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(productResponseEntity.getBody());
            }
        }
        JSONObject productObject = JSONArray.parseObject(productResponseEntity.getBody().toString());
        String product = productObject.getString("total");


        JSONObject servReqJson = new JSONObject();
        servReqJson.put("cartType",CART_TYPE_SERV);
        servReqJson.put("page","1");
        servReqJson.put("row","1");

        ResponseEntity<String> servResponseEntity = RestTemplateFactory.restOutMallTemplate(servReqJson, outRestTemplate, MallConstant.GET_STORE_ORDER_MALL, HttpMethod.GET);
        if (servResponseEntity != null) {
            if (servResponseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(servResponseEntity.getBody());
            }
        }
        JSONObject servAmountObject = JSONArray.parseObject(servResponseEntity.getBody().toString());
        String serv = servAmountObject.getString("total");

        JSONObject reqJson = new JSONObject();
        reqJson.put("page",page);
        reqJson.put("row",row);

        ResponseEntity<String> allResponseEntity = RestTemplateFactory.restOutMallTemplate(reqJson, outRestTemplate, MallConstant.GET_STORE_ORDER_MALL, HttpMethod.GET);
        if (allResponseEntity != null) {
            if (allResponseEntity.getStatusCode() != HttpStatus.OK) {
                throw new IllegalArgumentException(allResponseEntity.getBody());
            }
        }

        JSONObject returAmount = new JSONObject();
        returAmount.put("productTotal",product);
        returAmount.put("servTotal",serv);
        returAmount.put( "storeOrderCart",  JSONArray.parseObject(allResponseEntity.getBody().toString()));

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(returAmount.toJSONString(), HttpStatus.OK);
        return responseEntity;
    }
}
