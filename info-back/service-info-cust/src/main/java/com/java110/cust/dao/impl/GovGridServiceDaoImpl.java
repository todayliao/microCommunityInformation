package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovGridServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 网格人员服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govGridServiceDaoImpl")
//@Transactional
public class GovGridServiceDaoImpl extends BaseServiceDao implements IGovGridServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovGridServiceDaoImpl.class);





    /**
     * 保存网格人员信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovGridInfo(Map info) throws DAOException {
        logger.debug("保存网格人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govGridServiceDaoImpl.saveGovGridInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存网格人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询网格人员信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovGridInfo(Map info) throws DAOException {
        logger.debug("查询网格人员信息 入参 info : {}",info);

        List<Map> businessGovGridInfos = sqlSessionTemplate.selectList("govGridServiceDaoImpl.getGovGridInfo",info);

        return businessGovGridInfos;
    }


    /**
     * 修改网格人员信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovGridInfo(Map info) throws DAOException {
        logger.debug("修改网格人员信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govGridServiceDaoImpl.updateGovGridInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改网格人员信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询网格人员数量
     * @param info 网格人员信息
     * @return 网格人员数量
     */
    @Override
    public int queryGovGridsCount(Map info) {
        logger.debug("查询网格人员数据 入参 info : {}",info);

        List<Map> businessGovGridInfos = sqlSessionTemplate.selectList("govGridServiceDaoImpl.queryGovGridsCount", info);
        if (businessGovGridInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovGridInfos.get(0).get("count").toString());
    }


}
