package com.java110.cust.bmo.govGridType.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govGridType.IUpdateGovGridTypeBMO;
import com.java110.dto.govCommunityArea.GovCommunityAreaDto;
import com.java110.dto.uOrg.UOrgDto;
import com.java110.intf.assets.IGovCommunityAreaInnerServiceSMO;
import com.java110.intf.cust.IUOrgInnerServiceSMO;
import com.java110.po.govGridType.GovGridTypePo;
import com.java110.po.uOrg.UOrgPo;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovGridTypeInnerServiceSMO;

import java.util.List;

@Service("updateGovGridTypeBMOImpl")
public class UpdateGovGridTypeBMOImpl implements IUpdateGovGridTypeBMO {

    @Autowired
    private IGovGridTypeInnerServiceSMO govGridTypeInnerServiceSMOImpl;
    @Autowired
    private IGovCommunityAreaInnerServiceSMO govCommunityAreaInnerServiceSMOImpl;
    @Autowired
    private IUOrgInnerServiceSMO uOrgInnerServiceSMOImpl;
    /**
     *
     *
     * @param govGridTypePo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(GovGridTypePo govGridTypePo,String storeId) {


       // extracted(govGridTypePo, storeId);
        int flag = govGridTypeInnerServiceSMOImpl.updateGovGridType(govGridTypePo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    private void extracted(GovGridTypePo govGridTypePo, String storeId) {
        //查询区域
        GovCommunityAreaDto govCommunityAreaDto = new GovCommunityAreaDto();
        govCommunityAreaDto.setCaId(govGridTypePo.getCaId());
        List<GovCommunityAreaDto> govCommunityAreaDtos = govCommunityAreaInnerServiceSMOImpl.queryGovCommunityAreas(govCommunityAreaDto);
        if (govCommunityAreaDtos == null || govCommunityAreaDtos.size() < 1) {

            throw new IllegalArgumentException("查询对应区域失败");
        }
        UOrgDto uOrgDto = new UOrgDto();
        uOrgDto.setOrgLevel("1");
        uOrgDto.setStoreId(storeId);
        List<UOrgDto> uOrgDtos = uOrgInnerServiceSMOImpl.queryUOrgs(uOrgDto);
        if (uOrgDtos == null || uOrgDtos.size() < 1) {
            throw new IllegalArgumentException("查询一级目录失败,请检查配置");
        }
        //保存2级目录
        UOrgPo uOrgPo = new UOrgPo();
        uOrgPo.setOrgId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_orgId));
        uOrgPo.setOrgLevel("2");
        uOrgPo.setbId("-1");
        uOrgPo.setStoreId(storeId);
        uOrgPo.setOrgName(govCommunityAreaDtos.get(0).getCaName());
        uOrgPo.setParentOrgId(uOrgDtos.get(0).getOrgId());
        uOrgPo.setAllowOperation("T");
        uOrgPo.setDescription("系统添加" + govCommunityAreaDtos.get(0).getCaName());
        uOrgPo.setBelongCommunityId(govGridTypePo.getCaId());

        UOrgDto uOrgTooDto = new UOrgDto();
        uOrgTooDto.setOrgName(govCommunityAreaDtos.get(0).getCaName());
        uOrgTooDto.setOrgLevel("2");
        uOrgTooDto.setParentOrgId(uOrgDtos.get(0).getOrgId());
        uOrgTooDto.setBelongCommunityId(govGridTypePo.getCaId());
        List<UOrgDto> uOrgTooDtos  =uOrgInnerServiceSMOImpl.queryUOrgs(uOrgTooDto);
        if(uOrgTooDtos == null || uOrgTooDtos.size() < 1){
            int flag = uOrgInnerServiceSMOImpl.saveUOrg(uOrgPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存默认2级菜单失败");
            }
        }else {
            uOrgPo.setOrgId(uOrgTooDtos.get(0).getOrgId());
        }

        UOrgDto uOrgThreeDto = new UOrgDto();
        uOrgThreeDto.setOrgId(govGridTypePo.getOrgId());
        List<UOrgDto> uOrgThreeDtos = uOrgInnerServiceSMOImpl.queryUOrgs(uOrgThreeDto);
        if (uOrgThreeDtos == null || uOrgThreeDtos.size() < 1) {
            //保存3级目录
            UOrgPo uOrgThreePo = new UOrgPo();
            uOrgThreePo.setOrgId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_orgId));
            uOrgThreePo.setOrgLevel("3");
            uOrgThreePo.setbId("-1");
            uOrgThreePo.setStoreId(storeId);
            uOrgThreePo.setOrgName(govGridTypePo.getTypeName());
            uOrgThreePo.setParentOrgId(uOrgPo.getOrgId());
            uOrgThreePo.setAllowOperation("T");
            uOrgThreePo.setDescription("系统添加" + govGridTypePo.getTypeName());
            uOrgThreePo.setBelongCommunityId(uOrgPo.getOrgId());
            int flag = uOrgInnerServiceSMOImpl.saveUOrg(uOrgPo);
            if (flag < 1) {
                throw new IllegalArgumentException("保存默认2级菜单失败");
            }
        }else {
            UOrgPo uOrgThreePo = new UOrgPo();
            uOrgThreePo.setOrgId(govGridTypePo.getOrgId());
            uOrgThreePo.setOrgName(govGridTypePo.getTypeName());
            uOrgThreePo.setParentOrgId(uOrgPo.getOrgId());
            uOrgThreePo.setAllowOperation("T");
            uOrgThreePo.setBelongCommunityId(uOrgPo.getOrgId());
            uOrgInnerServiceSMOImpl.updateUOrg(uOrgThreePo);
        }
    }

}
