package com.java110.cust.bmo.UserV1;

import com.java110.po.UserV1.UserV1Po;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveUserV1BMO {


    /**
     * 添加用戶管理
     * add by wuxw
     * @param UserV1Po
     * @return
     */
    ResponseEntity<String> save(UserV1Po UserV1Po);


}
