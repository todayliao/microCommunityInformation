package com.java110.cust.smo.impl;


import com.java110.cust.dao.IMachineStaffShowServiceDao;
import com.java110.intf.cust.IMachineStaffShowInnerServiceSMO;
import com.java110.dto.machineStaffShow.MachineStaffShowDto;
import com.java110.po.machineStaffShow.MachineStaffShowPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 摄像头员工关系内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class MachineStaffShowInnerServiceSMOImpl extends BaseServiceSMO implements IMachineStaffShowInnerServiceSMO {

    @Autowired
    private IMachineStaffShowServiceDao machineStaffShowServiceDaoImpl;


    @Override
    public int saveMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo) {
        int saveFlag = 1;
        machineStaffShowServiceDaoImpl.saveMachineStaffShowInfo(BeanConvertUtil.beanCovertMap(machineStaffShowPo));
        return saveFlag;
    }

     @Override
    public int updateMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo) {
        int saveFlag = 1;
         machineStaffShowServiceDaoImpl.updateMachineStaffShowInfo(BeanConvertUtil.beanCovertMap(machineStaffShowPo));
        return saveFlag;
    }

     @Override
    public int deleteMachineStaffShow(@RequestBody  MachineStaffShowPo machineStaffShowPo) {
        int saveFlag = 1;
        machineStaffShowPo.setStatusCd("1");
        machineStaffShowServiceDaoImpl.updateMachineStaffShowInfo(BeanConvertUtil.beanCovertMap(machineStaffShowPo));
        return saveFlag;
    }

    @Override
    public List<MachineStaffShowDto> queryMachineStaffShows(@RequestBody  MachineStaffShowDto machineStaffShowDto) {

        //校验是否传了 分页信息

        int page = machineStaffShowDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            machineStaffShowDto.setPage((page - 1) * machineStaffShowDto.getRow());
        }

        List<MachineStaffShowDto> machineStaffShows = BeanConvertUtil.covertBeanList(machineStaffShowServiceDaoImpl.getMachineStaffShowInfo(BeanConvertUtil.beanCovertMap(machineStaffShowDto)), MachineStaffShowDto.class);

        return machineStaffShows;
    }


    @Override
    public int queryMachineStaffShowsCount(@RequestBody MachineStaffShowDto machineStaffShowDto) {
        return machineStaffShowServiceDaoImpl.queryMachineStaffShowsCount(BeanConvertUtil.beanCovertMap(machineStaffShowDto));    }

    public IMachineStaffShowServiceDao getMachineStaffShowServiceDaoImpl() {
        return machineStaffShowServiceDaoImpl;
    }

    public void setMachineStaffShowServiceDaoImpl(IMachineStaffShowServiceDao machineStaffShowServiceDaoImpl) {
        this.machineStaffShowServiceDaoImpl = machineStaffShowServiceDaoImpl;
    }
}
