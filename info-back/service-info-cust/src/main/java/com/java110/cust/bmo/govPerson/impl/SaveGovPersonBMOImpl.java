package com.java110.cust.bmo.govPerson.impl;

import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.cust.bmo.govPerson.ISaveGovPersonBMO;
import com.java110.dto.govCompanyPerson.GovCompanyPersonDto;
import com.java110.intf.cust.IGovCompanyPersonInnerServiceSMO;
import com.java110.intf.cust.IGovPersonInnerServiceSMO;
import com.java110.intf.gov.IGovPersonLabelRelInnerServiceSMO;
import com.java110.po.govCompanyPerson.GovCompanyPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import com.java110.po.govPersonLabelRel.GovPersonLabelRelPo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("saveGovPersonBMOImpl")
public class SaveGovPersonBMOImpl implements ISaveGovPersonBMO {

    @Autowired
    private IGovPersonInnerServiceSMO govPersonInnerServiceSMOImpl;
    @Autowired
    private IGovCompanyPersonInnerServiceSMO govCompanyPersonInnerServiceSMOImpl;
    @Autowired
    private IGovPersonLabelRelInnerServiceSMO govPersonLabelRelInnerServiceSMOImpl;
    /**
     * 添加小区信息
     *
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> save(GovPersonPo govPersonPo) {

        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        govPersonPo.setDatasourceType("999999");
        int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    /**
     * 添加小区信息
     *
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> saveGovPetition(GovPersonPo govPersonPo) {
        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        govPersonPo.setDatasourceType("999999");
        int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);
        if (flag > 0) {
            GovPersonLabelRelPo labelRelPo = new GovPersonLabelRelPo();
            labelRelPo.setGovPersonId(govPersonPo.getGovPersonId());
            labelRelPo.setCaId(govPersonPo.getCaId());
            labelRelPo.setLabelRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_labelRelId));
            labelRelPo.setLabelCd("6008");//信访人员
            flag = govPersonLabelRelInnerServiceSMOImpl.saveGovPersonLabelRel(labelRelPo);
            if (flag < 1) {
                return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "信访人员标签保存失败");
            }
            return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }
        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

    /**
     * 添加小区信息
     *
     * @param govPersonPo
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> saveGovPersonCompany(GovPersonPo govPersonPo) {

        govPersonPo.setGovPersonId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_govPersonId));
        int flag = govPersonInnerServiceSMOImpl.saveGovPerson(govPersonPo);

        if (flag > 0) {

            //处理员工企业关系信息
            GovCompanyPersonPo govCompanyPersonPo = new GovCompanyPersonPo();
            govCompanyPersonPo.setRelId(GenerateCodeFactory.getGeneratorId(GenerateCodeFactory.CODE_PREFIX_relId));
            govCompanyPersonPo.setGovCompanyId(govPersonPo.getGovCompanyId());
            govCompanyPersonPo.setGovPersonId(govPersonPo.getGovPersonId());
            govCompanyPersonPo.setCaId(govPersonPo.getCaId());
            govCompanyPersonPo.setState(govPersonPo.getState());
            govCompanyPersonPo.setRelCd(govPersonPo.getRelCd());
            govCompanyPersonPo.setGovOrgName(govPersonPo.getGovOrgName());
            flag = govCompanyPersonInnerServiceSMOImpl.saveGovCompanyPerson(govCompanyPersonPo);


        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
