package com.java110.cust.bmo.govHealthTitle.impl;

import com.java110.cust.bmo.govHealthTitle.IGetGovHealthTitleBMO;
import com.java110.dto.govHealthTerm.GovHealthTermDto;
import com.java110.dto.govHealthTitleValue.GovHealthTitleValueDto;
import com.java110.intf.cust.IGovHealthTermInnerServiceSMO;
import com.java110.intf.cust.IGovHealthTitleValueInnerServiceSMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthTitleInnerServiceSMO;
import com.java110.dto.govHealthTitle.GovHealthTitleDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthTitleBMOImpl")
public class GetGovHealthTitleBMOImpl implements IGetGovHealthTitleBMO {

    @Autowired
    private IGovHealthTitleInnerServiceSMO govHealthTitleInnerServiceSMOImpl;
    @Autowired
    private IGovHealthTitleValueInnerServiceSMO govHealthTitleValueInnerServiceSMOImpl;
    @Autowired
    private IGovHealthTermInnerServiceSMO govHealthTermInnerServiceSMOImpl;

    /**
     * @param govHealthTitleDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthTitleDto govHealthTitleDto) {


        int count = govHealthTitleInnerServiceSMOImpl.queryGovHealthTitlesCount( govHealthTitleDto );

        List<GovHealthTitleDto> govHealthTitleDtos = null;
        if (count > 0) {
            govHealthTitleDtos = govHealthTitleInnerServiceSMOImpl.queryGovHealthTitles( govHealthTitleDto );
            refreshTitileValues( govHealthTitleDtos );
        } else {
            govHealthTitleDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo( (int) Math.ceil( (double) count / (double) govHealthTitleDto.getRow() ), count, govHealthTitleDtos );

        ResponseEntity<String> responseEntity = new ResponseEntity<String>( resultVo.toString(), HttpStatus.OK );

        return responseEntity;
    }

    /**
     * @param govHealthTitleDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> getTermInfo(GovHealthTitleDto govHealthTitleDto) {


        int count = govHealthTitleInnerServiceSMOImpl.queryGovHealthTitlesCount( govHealthTitleDto );

        List<GovHealthTitleDto> govHealthTitleDtos = null;
        List<GovHealthTermDto> GovHealthTermDtos = null;
        if (count > 0) {
            govHealthTitleDtos = govHealthTitleInnerServiceSMOImpl.queryGovHealthTitles( govHealthTitleDto );
            GovHealthTermDtos =  refreshTitile( govHealthTitleDtos );
        } else {
            GovHealthTermDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo( (int) Math.ceil( (double) count / (double) govHealthTitleDto.getRow() ), count, GovHealthTermDtos );

        ResponseEntity<String> responseEntity = new ResponseEntity<String>( resultVo.toString(), HttpStatus.OK );

        return responseEntity;
    }

    private void refreshTitileValues(List<GovHealthTitleDto> govHealthTitleDtos) {

        if (govHealthTitleDtos == null || govHealthTitleDtos.size() < 1) {
            return;
        }

        List<String> titleIds = new ArrayList<>();
        for (GovHealthTitleDto govHealthTitleDto : govHealthTitleDtos) {
            titleIds.add( govHealthTitleDto.getTitleId() );
        }

        GovHealthTitleValueDto govHealthTitleValueDto = new GovHealthTitleValueDto();
        govHealthTitleValueDto.setTitleIds( titleIds.toArray( new String[titleIds.size()] ) );
        govHealthTitleValueDto.setCaId( govHealthTitleDtos.get( 0 ).getCaId() );
        List<GovHealthTitleValueDto> govHealthTitleValueDtos
                = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValues( govHealthTitleValueDto );

        List<GovHealthTitleValueDto> tmpTitleValueDtos = null;
        for (GovHealthTitleDto reportInfoTitleDto : govHealthTitleDtos) {
            tmpTitleValueDtos = new ArrayList<>();
            for (GovHealthTitleValueDto reportInfoTitleValueDto1 : govHealthTitleValueDtos) {
                if (reportInfoTitleDto.getTitleId().equals( reportInfoTitleValueDto1.getTitleId() )) {
                    tmpTitleValueDtos.add( reportInfoTitleValueDto1 );
                }
            }
            reportInfoTitleDto.setGovHealthTitleValueDtos( tmpTitleValueDtos );
        }
    }

    private List<GovHealthTermDto> refreshTitile(List<GovHealthTitleDto> govHealthTitleDtos) {

        if (govHealthTitleDtos == null || govHealthTitleDtos.size() < 1) {
            return null;
        }

        List<String> titleIds = new ArrayList<>();
        List<String> termIds = new ArrayList<>();
        for (GovHealthTitleDto govHealthTitleDto : govHealthTitleDtos) {
            titleIds.add( govHealthTitleDto.getTitleId() );
            termIds.add( govHealthTitleDto.getTermId() );
        }

        GovHealthTermDto govHealthTermDto = new GovHealthTermDto();
        govHealthTermDto.setCaId( govHealthTitleDtos.get( 0 ).getCaId() );
        govHealthTermDto.setTermIds( termIds.toArray( new String[termIds.size()] ) );
        List<GovHealthTermDto> govHealthTermDtos = govHealthTermInnerServiceSMOImpl.queryGovHealthTerms( govHealthTermDto );

        GovHealthTitleValueDto govHealthTitleValueDto = new GovHealthTitleValueDto();
        govHealthTitleValueDto.setTitleIds( titleIds.toArray( new String[titleIds.size()] ) );
        govHealthTitleValueDto.setCaId( govHealthTitleDtos.get( 0 ).getCaId() );
        List<GovHealthTitleValueDto> govHealthTitleValueDtos
                = govHealthTitleValueInnerServiceSMOImpl.queryGovHealthTitleValues( govHealthTitleValueDto );

        List<GovHealthTitleValueDto> tmpTitleValueDtos = null;
        for (GovHealthTitleDto reportInfoTitleDto : govHealthTitleDtos) {
            tmpTitleValueDtos = new ArrayList<>();
            for (GovHealthTitleValueDto reportInfoTitleValueDto1 : govHealthTitleValueDtos) {
                if (reportInfoTitleDto.getTitleId().equals( reportInfoTitleValueDto1.getTitleId() )) {
                    tmpTitleValueDtos.add( reportInfoTitleValueDto1 );
                }
            }
            reportInfoTitleDto.setGovHealthTitleValueDtos( tmpTitleValueDtos );
        }

        List<GovHealthTitleDto> tmpHealthTitleDtos = null;
        for (GovHealthTermDto govHealthTerm : govHealthTermDtos) {
            tmpHealthTitleDtos = new ArrayList<>();
            for (GovHealthTitleDto TitleDto : govHealthTitleDtos) {
                if (TitleDto.getTermId().equals( govHealthTerm.getTermId() )) {
                    tmpHealthTitleDtos.add( TitleDto );
                }
            }
            govHealthTerm.setGovHealthTitleDtoList( tmpHealthTitleDtos );
        }
        return govHealthTermDtos;
    }

}
