package com.java110.cust.bmo.govPersonDieRecord;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govPersonDieRecord.GovPersonDieRecordDto;
public interface IGetGovPersonDieRecordBMO {


    /**
     * 查询临终送别
     * add by wuxw
     * @param  govPersonDieRecordDto
     * @return
     */
    ResponseEntity<String> get(GovPersonDieRecordDto govPersonDieRecordDto);


}
