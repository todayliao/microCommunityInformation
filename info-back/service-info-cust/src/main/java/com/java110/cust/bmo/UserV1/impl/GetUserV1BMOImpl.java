package com.java110.cust.bmo.UserV1.impl;

import com.java110.cust.bmo.UserV1.IGetUserV1BMO;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUserV1InnerServiceSMO;
import com.java110.dto.UserV1.UserV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getUserV1BMOImpl")
public class GetUserV1BMOImpl implements IGetUserV1BMO {

    @Autowired
    private IUserV1InnerServiceSMO UserV1InnerServiceSMOImpl;

    /**
     *
     *
     * @param  UserV1Dto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(UserV1Dto UserV1Dto) {


        int count = UserV1InnerServiceSMOImpl.queryUserV1sCount(UserV1Dto);

        List<UserV1Dto> UserV1Dtos = null;
        if (count > 0) {
            UserV1Dtos = UserV1InnerServiceSMOImpl.queryUserV1s(UserV1Dto);
        } else {
            UserV1Dtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) UserV1Dto.getRow()), count, UserV1Dtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
