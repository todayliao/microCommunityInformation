package com.java110.cust.dao.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.utils.constant.ResponseConstant;
import com.java110.utils.exception.DAOException;
import com.java110.utils.util.DateUtil;
import com.java110.core.base.dao.BaseServiceDao;
import com.java110.cust.dao.IGovFollowupSurveyServiceDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 随访登记服务 与数据库交互
 * Created by wuxw on 2017/4/5.
 */
@Service("govFollowupSurveyServiceDaoImpl")
//@Transactional
public class GovFollowupSurveyServiceDaoImpl extends BaseServiceDao implements IGovFollowupSurveyServiceDao {

    private static Logger logger = LoggerFactory.getLogger(GovFollowupSurveyServiceDaoImpl.class);





    /**
     * 保存随访登记信息 到 instance
     * @param info   bId 信息
     * @throws DAOException DAO异常
     */
    @Override
    public void saveGovFollowupSurveyInfo(Map info) throws DAOException {
        logger.debug("保存随访登记信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.insert("govFollowupSurveyServiceDaoImpl.saveGovFollowupSurveyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"保存随访登记信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }


    /**
     * 查询随访登记信息（instance）
     * @param info bId 信息
     * @return List<Map>
     * @throws DAOException DAO异常
     */
    @Override
    public List<Map> getGovFollowupSurveyInfo(Map info) throws DAOException {
        logger.debug("查询随访登记信息 入参 info : {}",info);

        List<Map> businessGovFollowupSurveyInfos = sqlSessionTemplate.selectList("govFollowupSurveyServiceDaoImpl.getGovFollowupSurveyInfo",info);

        return businessGovFollowupSurveyInfos;
    }


    /**
     * 修改随访登记信息
     * @param info 修改信息
     * @throws DAOException DAO异常
     */
    @Override
    public void updateGovFollowupSurveyInfo(Map info) throws DAOException {
        logger.debug("修改随访登记信息Instance 入参 info : {}",info);

        int saveFlag = sqlSessionTemplate.update("govFollowupSurveyServiceDaoImpl.updateGovFollowupSurveyInfo",info);

        if(saveFlag < 1){
            throw new DAOException(ResponseConstant.RESULT_PARAM_ERROR,"修改随访登记信息Instance数据失败："+ JSONObject.toJSONString(info));
        }
    }

     /**
     * 查询随访登记数量
     * @param info 随访登记信息
     * @return 随访登记数量
     */
    @Override
    public int queryGovFollowupSurveysCount(Map info) {
        logger.debug("查询随访登记数据 入参 info : {}",info);

        List<Map> businessGovFollowupSurveyInfos = sqlSessionTemplate.selectList("govFollowupSurveyServiceDaoImpl.queryGovFollowupSurveysCount", info);
        if (businessGovFollowupSurveyInfos.size() < 1) {
            return 0;
        }

        return Integer.parseInt(businessGovFollowupSurveyInfos.get(0).get("count").toString());
    }


}
