package com.java110.cust.bmo.uOrg;

import com.java110.po.uOrg.UOrgPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveUOrgBMO {


    /**
     * 添加组织管理
     * add by wuxw
     * @param uOrgPo
     * @return
     */
    ResponseEntity<String> save(UOrgPo uOrgPo);


}
