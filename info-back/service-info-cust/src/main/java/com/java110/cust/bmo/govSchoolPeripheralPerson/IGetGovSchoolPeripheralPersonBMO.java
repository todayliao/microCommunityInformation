package com.java110.cust.bmo.govSchoolPeripheralPerson;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govSchoolPeripheralPerson.GovSchoolPeripheralPersonDto;
public interface IGetGovSchoolPeripheralPersonBMO {


    /**
     * 查询校园周边重点人员
     * add by wuxw
     * @param  govSchoolPeripheralPersonDto
     * @return
     */
    ResponseEntity<String> get(GovSchoolPeripheralPersonDto govSchoolPeripheralPersonDto);


}
