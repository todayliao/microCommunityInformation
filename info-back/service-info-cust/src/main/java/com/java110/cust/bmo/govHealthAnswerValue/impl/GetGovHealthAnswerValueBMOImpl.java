package com.java110.cust.bmo.govHealthAnswerValue.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.govHealthAnswerValue.IGetGovHealthAnswerValueBMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IGovHealthAnswerValueInnerServiceSMO;
import com.java110.dto.govHealthAnswerValue.GovHealthAnswerValueDto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import org.springframework.http.HttpStatus;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service("getGovHealthAnswerValueBMOImpl")
public class GetGovHealthAnswerValueBMOImpl implements IGetGovHealthAnswerValueBMO {

    @Autowired
    private IGovHealthAnswerValueInnerServiceSMO govHealthAnswerValueInnerServiceSMOImpl;

    /**
     *
     *
     * @param  govHealthAnswerValueDto
     * @return 订单服务能够接受的报文
     */
    public ResponseEntity<String> get(GovHealthAnswerValueDto govHealthAnswerValueDto) {


        int count = govHealthAnswerValueInnerServiceSMOImpl.queryGovHealthAnswerValuesCount(govHealthAnswerValueDto);

        List<GovHealthAnswerValueDto> govHealthAnswerValueDtos = null;
        if (count > 0) {
            govHealthAnswerValueDtos = govHealthAnswerValueInnerServiceSMOImpl.queryGovHealthAnswerValues(govHealthAnswerValueDto);
        } else {
            govHealthAnswerValueDtos = new ArrayList<>();
        }

        ResultVo resultVo = new ResultVo((int) Math.ceil((double) count / (double) govHealthAnswerValueDto.getRow()), count, govHealthAnswerValueDtos);

        ResponseEntity<String> responseEntity = new ResponseEntity<String>(resultVo.toString(), HttpStatus.OK);

        return responseEntity;
    }

}
