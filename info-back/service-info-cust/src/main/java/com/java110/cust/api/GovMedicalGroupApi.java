package com.java110.cust.api;

import com.alibaba.fastjson.JSONObject;
import com.java110.dto.govMedicalGroup.GovMedicalGroupDto;
import com.java110.po.govMedicalGroup.GovMedicalGroupPo;
import com.java110.cust.bmo.govMedicalGroup.IDeleteGovMedicalGroupBMO;
import com.java110.cust.bmo.govMedicalGroup.IGetGovMedicalGroupBMO;
import com.java110.cust.bmo.govMedicalGroup.ISaveGovMedicalGroupBMO;
import com.java110.cust.bmo.govMedicalGroup.IUpdateGovMedicalGroupBMO;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/govMedicalGroup")
public class GovMedicalGroupApi {

    @Autowired
    private ISaveGovMedicalGroupBMO saveGovMedicalGroupBMOImpl;
    @Autowired
    private IUpdateGovMedicalGroupBMO updateGovMedicalGroupBMOImpl;
    @Autowired
    private IDeleteGovMedicalGroupBMO deleteGovMedicalGroupBMOImpl;

    @Autowired
    private IGetGovMedicalGroupBMO getGovMedicalGroupBMOImpl;

    /**
     * 微信保存消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGroup/saveGovMedicalGroup
     * @path /app/govMedicalGroup/saveGovMedicalGroup
     */
    @RequestMapping(value = "/saveGovMedicalGroup", method = RequestMethod.POST)
    public ResponseEntity<String> saveGovMedicalGroup(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "hospitalId", "请求报文中未包含hospitalId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");


        GovMedicalGroupPo govMedicalGroupPo = BeanConvertUtil.covertBean(reqJson, GovMedicalGroupPo.class);
        return saveGovMedicalGroupBMOImpl.save(govMedicalGroupPo);
    }

    /**
     * 微信修改消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGroup/updateGovMedicalGroup
     * @path /app/govMedicalGroup/updateGovMedicalGroup
     */
    @RequestMapping(value = "/updateGovMedicalGroup", method = RequestMethod.POST)
    public ResponseEntity<String> updateGovMedicalGroup(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "groupId", "请求报文中未包含groupId");
        Assert.hasKeyAndValue(reqJson, "caId", "请求报文中未包含caId");
        Assert.hasKeyAndValue(reqJson, "hospitalId", "请求报文中未包含hospitalId");
        Assert.hasKeyAndValue(reqJson, "name", "请求报文中未包含name");

        GovMedicalGroupPo govMedicalGroupPo = BeanConvertUtil.covertBean(reqJson, GovMedicalGroupPo.class);
        return updateGovMedicalGroupBMOImpl.update(govMedicalGroupPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param reqJson
     * @return
     * @serviceCode /govMedicalGroup/deleteGovMedicalGroup
     * @path /app/govMedicalGroup/deleteGovMedicalGroup
     */
    @RequestMapping(value = "/deleteGovMedicalGroup", method = RequestMethod.POST)
    public ResponseEntity<String> deleteGovMedicalGroup(@RequestBody JSONObject reqJson) {

        Assert.hasKeyAndValue(reqJson, "groupId", "groupId不能为空");


        GovMedicalGroupPo govMedicalGroupPo = BeanConvertUtil.covertBean(reqJson, GovMedicalGroupPo.class);
        return deleteGovMedicalGroupBMOImpl.delete(govMedicalGroupPo);
    }

    /**
     * 微信删除消息模板
     *
     * @param caId 小区ID
     * @return
     * @serviceCode /govMedicalGroup/queryGovMedicalGroup
     * @path /app/govMedicalGroup/queryGovMedicalGroup
     */
    @RequestMapping(value = "/queryGovMedicalGroup", method = RequestMethod.GET)
    public ResponseEntity<String> queryGovMedicalGroup(@RequestParam(value = "caId") String caId,
                                                       @RequestParam(value = "name",required = false) String name,
                                                       @RequestParam(value = "companyName",required = false) String companyName,
                                                       @RequestParam(value = "page") int page,
                                                       @RequestParam(value = "row") int row) {
        GovMedicalGroupDto govMedicalGroupDto = new GovMedicalGroupDto();
        govMedicalGroupDto.setPage(page);
        govMedicalGroupDto.setRow(row);
        govMedicalGroupDto.setCaId(caId);
        govMedicalGroupDto.setName(name);
        govMedicalGroupDto.setCompanyName(companyName);
        return getGovMedicalGroupBMOImpl.get(govMedicalGroupDto);
    }
}
