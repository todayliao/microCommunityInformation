package com.java110.cust.bmo.uOrgCommunityV1.impl;

import com.alibaba.fastjson.JSONObject;
import com.java110.cust.bmo.uOrgCommunityV1.IUpdateUOrgCommunityV1BMO;
import com.java110.core.annotation.Java110Transactional;
import com.java110.core.factory.GenerateCodeFactory;
import com.java110.po.uOrgCommunityV1.UOrgCommunityV1Po;
import com.java110.vo.ResultVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import com.java110.core.context.DataFlowContext;
import com.java110.intf.cust.IUOrgCommunityV1InnerServiceSMO;
import com.java110.dto.uOrgCommunityV1.UOrgCommunityV1Dto;
import com.java110.utils.constant.BusinessTypeConstant;
import com.java110.utils.constant.CommonConstant;
import com.java110.utils.util.Assert;
import com.java110.utils.util.BeanConvertUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.java110.po.store.StorePo;
import java.util.List;

@Service("updateUOrgCommunityV1BMOImpl")
public class UpdateUOrgCommunityV1BMOImpl implements IUpdateUOrgCommunityV1BMO {

    @Autowired
    private IUOrgCommunityV1InnerServiceSMO uOrgCommunityV1InnerServiceSMOImpl;

    /**
     *
     *
     * @param uOrgCommunityV1Po
     * @return 订单服务能够接受的报文
     */
    @Java110Transactional
    public ResponseEntity<String> update(UOrgCommunityV1Po uOrgCommunityV1Po) {

        int flag = uOrgCommunityV1InnerServiceSMOImpl.updateUOrgCommunityV1(uOrgCommunityV1Po);

        if (flag > 0) {
        return ResultVo.createResponseEntity(ResultVo.CODE_OK, "保存成功");
        }

        return ResultVo.createResponseEntity(ResultVo.CODE_ERROR, "保存失败");
    }

}
