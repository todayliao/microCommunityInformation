package com.java110.cust.smo.impl;


import com.java110.cust.dao.IGovOldPersonTypeServiceDao;
import com.java110.intf.cust.IGovOldPersonTypeInnerServiceSMO;
import com.java110.dto.govOldPersonType.GovOldPersonTypeDto;
import com.java110.po.govOldPersonType.GovOldPersonTypePo;
import com.java110.utils.util.BeanConvertUtil;
import com.java110.core.base.smo.BaseServiceSMO;
import com.java110.dto.user.UserDto;
import com.java110.dto.PageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName FloorInnerServiceSMOImpl
 * @Description 老人类型内部服务实现类
 * @Author wuxw
 * @Date 2019/4/24 9:20
 * @Version 1.0
 * add by wuxw 2019/4/24
 **/
@RestController
public class GovOldPersonTypeInnerServiceSMOImpl extends BaseServiceSMO implements IGovOldPersonTypeInnerServiceSMO {

    @Autowired
    private IGovOldPersonTypeServiceDao govOldPersonTypeServiceDaoImpl;


    @Override
    public int saveGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo) {
        int saveFlag = 1;
        govOldPersonTypeServiceDaoImpl.saveGovOldPersonTypeInfo(BeanConvertUtil.beanCovertMap(govOldPersonTypePo));
        return saveFlag;
    }

     @Override
    public int updateGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo) {
        int saveFlag = 1;
         govOldPersonTypeServiceDaoImpl.updateGovOldPersonTypeInfo(BeanConvertUtil.beanCovertMap(govOldPersonTypePo));
        return saveFlag;
    }

     @Override
    public int deleteGovOldPersonType(@RequestBody  GovOldPersonTypePo govOldPersonTypePo) {
        int saveFlag = 1;
        govOldPersonTypePo.setStatusCd("1");
        govOldPersonTypeServiceDaoImpl.updateGovOldPersonTypeInfo(BeanConvertUtil.beanCovertMap(govOldPersonTypePo));
        return saveFlag;
    }

    @Override
    public List<GovOldPersonTypeDto> queryGovOldPersonTypes(@RequestBody  GovOldPersonTypeDto govOldPersonTypeDto) {

        //校验是否传了 分页信息

        int page = govOldPersonTypeDto.getPage();

        if (page != PageDto.DEFAULT_PAGE) {
            govOldPersonTypeDto.setPage((page - 1) * govOldPersonTypeDto.getRow());
        }

        List<GovOldPersonTypeDto> govOldPersonTypes = BeanConvertUtil.covertBeanList(govOldPersonTypeServiceDaoImpl.getGovOldPersonTypeInfo(BeanConvertUtil.beanCovertMap(govOldPersonTypeDto)), GovOldPersonTypeDto.class);

        return govOldPersonTypes;
    }


    @Override
    public int queryGovOldPersonTypesCount(@RequestBody GovOldPersonTypeDto govOldPersonTypeDto) {
        return govOldPersonTypeServiceDaoImpl.queryGovOldPersonTypesCount(BeanConvertUtil.beanCovertMap(govOldPersonTypeDto));    }

    public IGovOldPersonTypeServiceDao getGovOldPersonTypeServiceDaoImpl() {
        return govOldPersonTypeServiceDaoImpl;
    }

    public void setGovOldPersonTypeServiceDaoImpl(IGovOldPersonTypeServiceDao govOldPersonTypeServiceDaoImpl) {
        this.govOldPersonTypeServiceDaoImpl = govOldPersonTypeServiceDaoImpl;
    }
}
