package com.java110.cust.bmo.govOldPerson;

import com.alibaba.fastjson.JSONObject;
import com.java110.po.govOldPerson.GovOldPersonPo;
import com.java110.po.govPerson.GovPersonPo;
import org.springframework.http.ResponseEntity;
import com.java110.po.store.StorePo;
public interface ISaveGovOldPersonBMO {


    /**
     * 添加老人管理
     * add by wuxw
     * @param govOldPersonPo
     * @return
     */
    ResponseEntity<String> save(GovOldPersonPo govOldPersonPo, GovPersonPo govPersonPo, JSONObject reqJson);


}
