package com.java110.cust.bmo.govFollowupSurvey;
import org.springframework.http.ResponseEntity;
import com.java110.po.govFollowupSurvey.GovFollowupSurveyPo;

public interface IDeleteGovFollowupSurveyBMO {


    /**
     * 修改随访登记
     * add by wuxw
     * @param govFollowupSurveyPo
     * @return
     */
    ResponseEntity<String> delete(GovFollowupSurveyPo govFollowupSurveyPo);


}
