package com.java110.cust.bmo.govSymptomType;
import org.springframework.http.ResponseEntity;
import com.java110.dto.govSymptomType.GovSymptomTypeDto;
public interface IGetGovSymptomTypeBMO {


    /**
     * 查询症状类型
     * add by wuxw
     * @param  govSymptomTypeDto
     * @return
     */
    ResponseEntity<String> get(GovSymptomTypeDto govSymptomTypeDto);


}
