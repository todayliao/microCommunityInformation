/*
 * Copyright 2017-2020 吴学文 and java110 team.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.java110.utils.constant;

import com.java110.utils.cache.MappingCache;
import com.java110.utils.util.StringUtil;

/**
 * HC 物联网常量类
 * 接口协议地址： https://gitee.com/java110/MicroCommunityThings/blob/master/back/docs/api.md
 *
 * @desc add by 吴学文 9:49
 */
public class MallConstant {

    public static final String MALL_DOMAIN = "MALL"; // 物联网域
    public static final String MALL_URL = "MALL_URL"; // 物联网域

    public static final String COMMON_DOMAIN = "DOMAIN.COMMON"; // 物联网域
    public static final String SIP_URL = "SIP_URL"; // 物联网域
    public static final String VIDEO_URL = "VIDEO_SIP_URL"; // 物联网域

    public static final String MALL_APP_ID = "APP_ID"; // 物联网域
    public static final String MALL_APP_SECRET = "APP_SECRET"; // 物联网域
    public static final String MALL_IS_SEND = "IS_SEND"; // 是否下发商城

    private static final String DEFAULT_MALL_URL = "http://proxy.homecommunity.cn:9024/";

    private static final String DEFAULT_SIP_URL = "http://112.124.21.207:1985/";
    private static final String DEFAULT_VIDEO_SIP_URL = "webrtc://112.124.21.207/live/";

    private static final String DEFAULT_APP_ID = "e86a6a373c354927bea5fd21a0bec617";
    private static final String DEFAULT_APP_SECRET = "ead9a2f67f96e2b8ed2fe38cc9709463";

    public static final String GET_TOKEN_URL = "/extApi/auth/getAccessToken?appId=APP_ID&appSecret=APP_SECRET";

    //添加、修改 小区
    public static final String ADD_COMMUNITY_MALL = "/communityMall/saveCommunityMall";
    public static final String EDIT_COMMUNITY_MALL = "/communityMall/updateCommunityMall";
    public static final String DELETE_COMMUNITY_MALL = "/communityMall/deleteCommunityMall";
    //添加、修改 设备
    public static final String ADD_MACHINE_MALL = "/communityMall/saveMachine";
    public static final String EDIT_MACHINEMALL = "/communityMall/updateMachine";
    public static final String DELETE_MACHINE_MALL = "/communityMall/deleteMachine";

    //添加、修改、老人
    public static final String ADD_OLD_PERSON_MALL = "/govOldPerson/saveGovOldPerson";
    public static final String EDIT_OLD_PERSON_MALL = "/govOldPerson/updateGovOldPerson";
    public static final String DELETE_OLD_PERSON_MALL = "/govOldPerson/deleteGovOldPerson";
    //添加、修改、网格人员
    public static final String ADD_VOLUNTEER_MALL = "/govVolunteer/saveGovVolunteer";
    public static final String EDIT_VOLUNTEER_MALL = "/govVolunteer/updateGovVolunteer";
    public static final String DELETE_VOLUNTEER_MALL = "/govVolunteer/deleteGovVolunteer";
    //查询账户明细
    public static final String GET_ACCOUNT_DETAIL_MALL = "/accountDetailMall/queryAccountDetail";
    //查询账户总数
    public static final String GET_ACCOUNT_MALL = "/accountAmountMall/queryAccountAmount";
    //查询订单
    public static final String GET_STORE_ORDER_MALL = "/storeOrderMall/queryStoreOrderCart";

    //crt 接口
    public static final String SIP_ACTION = "/api/v1/gb28181";


    public static String getUrl(String param) {
        String url = MappingCache.getValue(MALL_DOMAIN, MallConstant.MALL_URL);

        if (StringUtil.isEmpty(url)) {
            return DEFAULT_MALL_URL + param;
        }

        return url + param;
    }
    public static String getSipUrl(String param) {
        String url = MappingCache.getValue(COMMON_DOMAIN, MallConstant.SIP_URL);

        if (StringUtil.isEmpty(url)) {
            return DEFAULT_SIP_URL + param;
        }

        return url + param;
    }
    public static String getVideoSipUrl(String param) {
        String url = MappingCache.getValue(COMMON_DOMAIN, MallConstant.VIDEO_URL);

        if (StringUtil.isEmpty(url)) {
            return DEFAULT_VIDEO_SIP_URL + param;
        }

        return url + param;
    }

    public static String getAppId() {
        String appId = MappingCache.getValue(MALL_DOMAIN, MallConstant.MALL_APP_ID);

        if (StringUtil.isEmpty(appId)) {
            return DEFAULT_APP_ID;
        }

        return appId;
    }

    public static String getAppSecret() {
        String appSecret = MappingCache.getValue(MALL_DOMAIN, MallConstant.MALL_APP_SECRET);

        if (StringUtil.isEmpty(appSecret)) {
            return DEFAULT_APP_SECRET;
        }

        return appSecret;
    }
}
