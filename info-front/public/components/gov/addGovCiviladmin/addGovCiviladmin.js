(function (vc) {

    vc.extends({
        data: {
            addGovActivitiesInfo: {
                title: '',
                typeCd: '',
                context: '',
                startTime: '',
                state: '11000',
                endTime: '',
                govActivitiesTypes:[],
            }
        },
        _initMethod: function () {
            $that._initNoticeInfo();
            $that._queryAddGovActivitiesTypes(1,50);
        },
        _initEvent: function () {
            vc.on('addGovActivities', 'openAddGovActivitiesModal', function () {
            });
        },
        methods: {
            addGovActivitiesValidate() {
                return vc.validate.validate({
                    addGovActivitiesInfo: vc.component.addGovActivitiesInfo
                }, {
                    'addGovActivitiesInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "标题太长"
                        },
                    ],
                    'addGovActivitiesInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "文章类型太长"
                        },
                    ],
                   
                    'addGovActivitiesInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "内容不能为空"
                        }
                    ],
                    'addGovActivitiesInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        }
                    ],
                    'addGovActivitiesInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        }
                    ]




                });
            },
            saveGovActivitiesInfo: function () {
                if (!vc.component.addGovActivitiesValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovActivitiesInfo.caId = vc.getCurrentCommunity().caId;


                vc.http.apiPost(
                    '/govCiviladmin/saveGovCiviladmin',
                    JSON.stringify(vc.component.addGovActivitiesInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivitiesModel').modal('hide');
                            vc.component.clearAddGovActivitiesInfo();
                            vc.emit('govActivitiesManage', 'listGovActivities', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            closeActivitiesInfo: function () {
                vc.emit('govActivitiesManage', 'listGovActivities', {});

            },
            _initNoticeInfo: function () {
                vc.component.addGovActivitiesInfo.startTime = vc.dateTimeFormat(new Date().getTime());
                $('.noticeStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true

                });
                $('.noticeStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".noticeStartTime").val();
                        vc.component.addGovActivitiesInfo.startTime = value;
                        let start = Date.parse(new Date(vc.component.addGovActivitiesInfo.startTime))
                        let end = Date.parse(new Date(vc.component.addGovActivitiesInfo.endTime))
                        if (end != 0 && start - end >= 0) {
                            vc.toast("开始时间必须小于结束时间")
                            vc.component.addGovActivitiesInfo.startTime = '';
                        }
                    });
                $('.noticeEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.noticeEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".noticeEndTime").val();
                        vc.component.addGovActivitiesInfo.endTime = value;
                        let start = Date.parse(new Date(vc.component.addGovActivitiesInfo.startTime))
                        let end = Date.parse(new Date(vc.component.addGovActivitiesInfo.endTime))
                        if (start - end >= 0) {
                            vc.toast("结束时间必须大于开始时间")
                            vc.component.addGovActivitiesInfo.endTime = '';
                        }
                    });
                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote,files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addGovActivitiesInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendAddFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _queryAddGovActivitiesTypes: function (_page, _rows) {

                var param = {
                    params: {
                       page : _page,
                        row : _rows,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCiviladminType/queryGovCiviladminType',
                    param,
                    function (json, res) {
                        var _govActivitiesTypeManageInfo = JSON.parse(json);
                        vc.component.addGovActivitiesInfo.govActivitiesTypes = _govActivitiesTypeManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovActivitiesInfo: function () {
                vc.component.addGovActivitiesInfo = {
                    title: '',
                    typeCd: '',
                    context: '',
                    startTime: '',
                    endTime: '',
                    state: '11000',
                    govActivitiesTypes:[],

                };
            }
        }
    });

})(window.vc);
