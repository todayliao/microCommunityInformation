(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovSpecialFollowInfo: {
                followId: '',
                specialPersonId: '',
                name: '',
                caId: '',
                specialStartTime: '',
                specialReason: '',
                isWork: '',
                workPerson: '',
                workPersonTel: '',
                workAddress: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovSpecialFollowInfo.specialStartTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovSpecialFollow', 'openAddGovSpecialFollowModal', function (_parma) {
                $that.addGovSpecialFollowInfo.specialPersonId = _parma.specialPersonId;
                $that.addGovSpecialFollowInfo.name = _parma.name;
                $('#addGovSpecialFollowModel').modal('show');
            });
        },
        methods: {
            addGovSpecialFollowValidate() {
                return vc.validate.validate({
                    addGovSpecialFollowInfo: vc.component.addGovSpecialFollowInfo
                }, {
                    'addGovSpecialFollowInfo.specialPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "特殊人员ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "特殊人员ID不能超过30"
                        },
                    ],
                    'addGovSpecialFollowInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "特殊人员名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "特殊人员名称不能超过64"
                        },
                    ],
                    'addGovSpecialFollowInfo.specialStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "登记时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "登记时间不能超过时间类型"
                        },
                    ],
                    'addGovSpecialFollowInfo.specialReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "近况说明不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "近况说明不能超过128"
                        },
                    ],
                    'addGovSpecialFollowInfo.isWork': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否有工作不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否有工作不能超过64"
                        },
                    ],
                    'addGovSpecialFollowInfo.workPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "负责人不能超过128"
                        },
                    ],
                    'addGovSpecialFollowInfo.workPersonTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "负责人联系电话不能超过11"
                        },
                    ],
                    'addGovSpecialFollowInfo.workAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "工作地址不能超过128"
                        },
                    ],

                });
            },
            saveGovSpecialFollowInfo: function () {
                if (!vc.component.addGovSpecialFollowValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovSpecialFollowInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovSpecialFollowInfo);
                    $('#addGovSpecialFollowModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govSpecialFollow/saveGovSpecialFollow',
                    JSON.stringify(vc.component.addGovSpecialFollowInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovSpecialFollowModel').modal('hide');
                            vc.component.clearAddGovSpecialFollowInfo();
                            vc.emit('govSpecialFollowManage', 'listGovSpecialFollow', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovSpecialFollowInfo: function () {
                vc.component.addGovSpecialFollowInfo = {
                    followId: '',
                    specialPersonId: '',
                    name: '',
                    caId: '',
                    specialStartTime: '',
                    specialReason: '',
                    isWork: '',
                    workPerson: '',
                    workPersonTel: '',
                    workAddress: '',
                    ramark: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
