(function (vc, vm) {

    vc.extends({
        data: {
            editGovCommunityLocationInfo: {
                locationId: '',
                govCommunityId: '',
                name: '',
                caId:''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovCommunityLocation', 'openEditGovCommunityLocationModal', function (_params) {
                vc.component.refreshEditGovCommunityLocationInfo();
                $('#editGovCommunityLocationModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCommunityLocationInfo);
                vc.component.editGovCommunityLocationInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovCommunityLocationValidate: function () {
                return vc.validate.validate({
                    editGovCommunityLocationInfo: vc.component.editGovCommunityLocationInfo
                }, {
                    'editGovCommunityLocationInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'editGovCommunityLocationInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "位置名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "位置名称超长"
                        },
                    ],
                    'editGovCommunityLocationInfo.locationId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "编号不能为空"
                        }]

                });
            },
            editGovCommunityLocation: function () {
                if (!vc.component.editGovCommunityLocationValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCommunityLocation/updateGovCommunityLocation',
                    JSON.stringify(vc.component.editGovCommunityLocationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCommunityLocationModel').modal('hide');
                            vc.emit('govCommunityLocationManage', 'listGovCommunityLocation', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovCommunityLocationInfo: function () {
                vc.component.editGovCommunityLocationInfo = {
                    locationId: '',
                    govCommunityId: '',
                    name: '',
                    caId:''
                }
            }
        }
    });

})(window.vc, window.vc.component);
