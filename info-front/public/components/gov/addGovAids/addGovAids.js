(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovAidsInfo: {
                aidsId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                aidsStartTime: '',
                aidsReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovAidsInfo.aidsStartTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovAids', 'openAddGovAidsModal', function () {
                $('#addGovAidsModel').modal('show');
            });
        },
        methods: {
            addGovAidsValidate() {
                return vc.validate.validate({
                    addGovAidsInfo: vc.component.addGovAidsInfo
                }, {
                    'addGovAidsInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "艾滋病者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "艾滋病者名称不能超过64"
                        },
                    ],
                    'addGovAidsInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovAidsInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovAidsInfo.aidsStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'addGovAidsInfo.aidsReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "感染原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "感染原因不能超过128"
                        },
                    ],
                    'addGovAidsInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'addGovAidsInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ],

                });
            },
            saveGovAidsInfo: function () {
                if (!vc.component.addGovAidsValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addGovAidsInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovAidsInfo);
                    $('#addGovAidsModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govAids/saveGovAids',
                    JSON.stringify(vc.component.addGovAidsInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovAidsModel').modal('hide');
                            vc.component.clearAddGovAidsInfo();
                            vc.emit('govAidsManage', 'listGovAids', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovAidsInfo: function () {
                vc.component.addGovAidsInfo = {
                    aidsId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    aidsStartTime: '',
                    aidsReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
