(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovOldPersonTypeInfo: {
                typeId: '',
                caId: '',
                typeName: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovOldPersonType', 'openAddGovOldPersonTypeModal', function () {
                $('#addGovOldPersonTypeModel').modal('show');
            });
        },
        methods: {
            addGovOldPersonTypeValidate() {
                return vc.validate.validate({
                    addGovOldPersonTypeInfo: vc.component.addGovOldPersonTypeInfo
                }, {
                   
                    'addGovOldPersonTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ],
                    'addGovOldPersonTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        }
                    ]



                });
            },
            saveGovOldPersonTypeInfo: function () {
                if (!vc.component.addGovOldPersonTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovOldPersonTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovOldPersonTypeInfo);
                    $('#addGovOldPersonTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govOldPersonType/saveGovOldPersonType',
                    JSON.stringify(vc.component.addGovOldPersonTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovOldPersonTypeModel').modal('hide');
                            vc.component.clearAddGovOldPersonTypeInfo();
                            vc.emit('govOldPersonTypeManage', 'listGovOldPersonType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovOldPersonTypeInfo: function () {
                vc.component.addGovOldPersonTypeInfo = {
                    typeId: '',
                    caId: '',
                    typeName: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
