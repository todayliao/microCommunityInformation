(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGovActivityInfo:{
                actId:'',
                typeId:'',
                caId:'',
                actName:'',
                actTime:'',
                actAddress:'',
                personCount:'',
                context:'',
                contactName:'',
                contactLink:''
            }
        },
         _initMethod:function(){
            vc.component._initAddActTime();
         },
         _initEvent:function(){
            vc.on('addGovActivity','openAddGovActivityModal',function(){
                $('#addGovActivityModel').modal('show');
            });
        },
        methods:{
            addGovActivityValidate(){
                return vc.validate.validate({
                    addGovActivityInfo:vc.component.addGovActivityInfo
                },{
                    'addGovActivityInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'addGovActivityInfo.actName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"活动名称超长"
                        },
                    ],
'addGovActivityInfo.actTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"活动时间超长"
                        },
                    ],
'addGovActivityInfo.actAddress':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动地点不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"512",
                            errInfo:"活动地点超长"
                        },
                    ],
'addGovActivityInfo.personCount':[
{
                            limit:"required",
                            param:"",
                            errInfo:"人数不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"20",
                            errInfo:"人数超长"
                        },
                    ],
'addGovActivityInfo.context':[
{
                            limit:"required",
                            param:"",
                            errInfo:"活动内容不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"2048",
                            errInfo:"活动内容太长"
                        },
                    ],
'addGovActivityInfo.contactName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"联系人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"联系人名称太长"
                        },
                    ],
'addGovActivityInfo.contactLink':[
{
                            limit:"required",
                            param:"",
                            errInfo:"联系电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"联系电话太长"
                        },
                    ],




                });
            },
            saveGovActivityInfo:function(){
                vc.component.addGovActivityInfo.caId = vc.getCurrentCommunity().caId;
                if(!vc.component.addGovActivityValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }
                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGovActivityInfo);
                    $('#addGovActivityModel').modal('hide');
                    return ;
                }

                vc.http.apiPost(
                    '/govActivity/saveGovActivity',
                    JSON.stringify(vc.component.addGovActivityInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovActivityModel').modal('hide');
                            vc.component.clearAddGovActivityInfo();
                            vc.emit('govActivityManage','listGovActivity',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
            
            _initAddActTime: function () {
                vc.component.addGovActivityInfo.actTime = vc.dateTimeFormat(new Date().getTime());
                $('.addActTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.addActTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".addActTime").val();
                        vc.component.addGovActivityInfo.actTime = value;
                    });
            },
            clearAddGovActivityInfo:function(){
                vc.component.addGovActivityInfo = {
                                            caId:'',
                                            actName:'',
                                            typeId:'',
                                            actTime:'',
                                            actAddress:'',
                                            personCount:'',
                                            context:'',
                                            contactName:'',
                                            contactLink:''
                                        };
            }
        }
    });

})(window.vc);
