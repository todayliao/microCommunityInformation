(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCommunityAreaInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCommunityArea', 'openDeleteGovCommunityAreaModal', function (_params) {

                vc.component.deleteGovCommunityAreaInfo = _params;
                $('#deleteGovCommunityAreaModel').modal('show');

            });
        },
        methods: {
            deleteGovCommunityArea: function () {
                //vc.component.deleteGovCommunityAreaInfo.communityId = vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govCommunityArea/deleteGovCommunityArea',
                    JSON.stringify(vc.component.deleteGovCommunityAreaInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCommunityAreaModel').modal('hide');
                            vc.emit('govCommunityAreaManage', 'listGovCommunityArea', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCommunityAreaModel: function () {
                $('#deleteGovCommunityAreaModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
