(function (vc, vm) {

    vc.extends({
        data: {
            reviewGovVolunteerInfo: {
                volunteerId: '',
                state: '',
                reviewComm: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('reviewGovVolunteer', 'openreviewGovVolunteerModal', function (_params) {
                vc.component.refreshreviewGovVolunteerInfo();
                $('#reviewGovVolunteerModel').modal('show');
                vc.copyObject(_params._govVolunteer, vc.component.reviewGovVolunteerInfo);
                $that.reviewGovVolunteerInfo.reviewComm='';
            });
        },
        methods: {
            reviewGovVolunteerValidate: function () {
                return vc.validate.validate({
                    reviewGovVolunteerInfo: vc.component.reviewGovVolunteerInfo
                }, {
                    'reviewGovVolunteerInfo.volunteerId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "志愿者ID不能超过30"
                        },
                    ],
                    'reviewGovVolunteerInfo.state': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "审核状态不能为空"
                        }
                    ],
                    'reviewGovVolunteerInfo.reviewComm': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "审核意见不能为空"
                        }
                    ]

                });
            },
            reviewGovVolunteer: function () {
                if (!vc.component.reviewGovVolunteerValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govVolunteer/reviewGovVolunteer',
                    JSON.stringify(vc.component.reviewGovVolunteerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#reviewGovVolunteerModel').modal('hide');
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshreviewGovVolunteerInfo: function () {
                vc.component.reviewGovVolunteerInfo = {
                    volunteerId: '',
                    state: '',
                    reviewComm: ''
                }
            }
        }
    });

})(window.vc, window.vc.component);
