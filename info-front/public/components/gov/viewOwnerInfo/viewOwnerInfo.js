(function (vc) {
    vc.extends({
        data: {
            viewOwnerInfo: {
                orgId: '',
                govPersonId: '',
                owners: []
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('ownerInfo', 'switch', function (_param) {
                vc.component._refreshviewGovOwnerMemberInfo();
                console.log(_param);
                vc.component._loadOwnerInfo(_param.govPersonId);
            });
        },
        methods: {
            _loadOwnerInfo: function (_personId) {
                var param = {
                    params: {
                        page: 1,
                        row: 10,
                        caId: vc.getCurrentCommunity().caId,
                        govPersonId: _personId
                    }
                };
                //发送get请求
                vc.http.apiGet('/govOwner/getGovOwnerPersonRel',
                    param,
                    function (json) {
                        var _labelInfo = JSON.parse(json);
                        $that.viewOwnerInfo.owners = _labelInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _refreshviewGovOwnerMemberInfo: function () {
                $that.viewOwnerInfo = {
                    orgId: '',
                    govPersonId: '',
                    owners: []
                }
            }
        }

    });
})(window.vc);
