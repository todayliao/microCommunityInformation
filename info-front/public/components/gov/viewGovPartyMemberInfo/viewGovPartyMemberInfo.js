/**
    党员管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovPartyMemberInfo:{
                index:0,
                flowComponent:'viewGovPartyMemberInfo',
                caId:'',
personId:'',
orgId:'',
orgName:'',
govTypeId:'',
memberFlag:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovPartyMemberInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovPartyMemberInfo','chooseGovPartyMember',function(_app){
                vc.copyObject(_app, vc.component.viewGovPartyMemberInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovPartyMemberInfo);
            });

            vc.on('viewGovPartyMemberInfo', 'onIndex', function(_index){
                vc.component.viewGovPartyMemberInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovPartyMemberInfoModel(){
                vc.emit('chooseGovPartyMember','openChooseGovPartyMemberModel',{});
            },
            _openAddGovPartyMemberInfoModel(){
                vc.emit('addGovPartyMember','openAddGovPartyMemberModal',{});
            },
            _loadGovPartyMemberInfoData:function(){

            }
        }
    });

})(window.vc);
