(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMajorEvents:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMajorEventsInfo:{
                govMajorEventss:[],
                _currentGovMajorEventsName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMajorEvents','openChooseGovMajorEventsModel',function(_param){
                $('#chooseGovMajorEventsModel').modal('show');
                vc.component._refreshChooseGovMajorEventsInfo();
                vc.component._loadAllGovMajorEventsInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMajorEventsInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMajorEvents/queryGovMajorEvents',
                             param,
                             function(json){
                                var _govMajorEventsInfo = JSON.parse(json);
                                vc.component.chooseGovMajorEventsInfo.govMajorEventss = _govMajorEventsInfo.govMajorEventss;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMajorEvents:function(_govMajorEvents){
                if(_govMajorEvents.hasOwnProperty('name')){
                     _govMajorEvents.govMajorEventsName = _govMajorEvents.name;
                }
                vc.emit($props.emitChooseGovMajorEvents,'chooseGovMajorEvents',_govMajorEvents);
                vc.emit($props.emitLoadData,'listGovMajorEventsData',{
                    govMajorEventsId:_govMajorEvents.govMajorEventsId
                });
                $('#chooseGovMajorEventsModel').modal('hide');
            },
            queryGovMajorEventss:function(){
                vc.component._loadAllGovMajorEventsInfo(1,10,vc.component.chooseGovMajorEventsInfo._currentGovMajorEventsName);
            },
            _refreshChooseGovMajorEventsInfo:function(){
                vc.component.chooseGovMajorEventsInfo._currentGovMajorEventsName = "";
            }
        }

    });
})(window.vc);
