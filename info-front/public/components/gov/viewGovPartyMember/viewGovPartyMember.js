(function (vc) {
    vc.extends({
        data: {
            viewGovPartyMemberInfo: {
                govMembers: [],
                orgId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('viewGovPartyMember', 'openviewGovPartyMemberModel', function (_param) {
                vc.component._refreshviewGovPartyMemberInfo();
                //vc.component._loadAllGovRoomInfo(1, 10, '');
            });
            vc.on('viewGovPartyMember', 'selectPartyOrgs', function (_param) {
                console.log(_param);
                $that.viewGovPartyMemberInfo.orgId = _param;
            });
            vc.on('meetingListChooseGovPartyMemberOrg', 'chooseGovPartyMemberOrg', function (_param) {
                $that.viewGovPartyMemberInfo.govMembers.push(_param);
                vc.emit('viewGovPartyMember', 'page_event', $that.viewGovPartyMemberInfo.govMembers);
            });
            
        },
        methods: {
            _loadAllPartyMemberInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        name: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingMemberRel/queryGovMeetingMemberRel',
                    param,
                    function (json) {
                        var _govRoomInfo = JSON.parse(json);
                        vc.component.viewGovPartyMemberInfo.govRooms = _govRoomInfo.govRooms;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseGovPartyMemberOrg: function () {
                let _caId = vc.getCurrentCommunity().caId;
                //let _orgId = $that.viewGovPartyMemberInfo.orgId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                
                var param ={
                    caId: _caId
                }
                vc.emit('chooseGovPartyMemberOrg', 'openchooseGovPartyMemberOrgModel', param);
            },
            queryvPartyMember: function () {
                vc.component._loadAllPartyMemberInfo(1, 10, vc.component.viewGovPartyMemberInfo._currentGovRoomName);
            },
            _openDeleteGovPartyMemberOrg: function (_govMember) {

                let _tmpGovMembers = [];
                $that.viewGovPartyMemberInfo.govMembers.forEach(item => {
                    if (item.govMemberId != _govMember.govMemberId) {
                        _tmpGovMembers.push(item);
                    }
                });
                $that.viewGovPartyMemberInfo.govMembers = _tmpGovMembers;
                vc.emit('viewGovPartyMember', 'page_event', $that.viewGovPartyMemberInfo.govMembers);
            },
            _refreshviewGovPartyMemberInfo: function () {
                vc.component.viewGovPartyMemberInfo._currentGovRoomName = "";
                $that.viewGovPartyMemberInfo.govMembers = [];
            }
        }

    });
})(window.vc);
