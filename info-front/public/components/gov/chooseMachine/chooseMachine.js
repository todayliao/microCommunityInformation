(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseMachine: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseMachineInfo: {
                machines: [],
                _currentMachineName: '',
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseMachine', 'openChooseMachineModel', function (_param) {
                $('#chooseMachineModel').modal('show');
                vc.component._refreshChooseMachineInfo();
                vc.component._loadAllMachineInfo(1, 10, '');
            });
        },
        methods: {
            _loadAllMachineInfo: function (_page, _row, _name) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        caId: vc.getCurrentCommunity().caId,
                        machineName: _name
                    }
                };

                //发送get请求
                vc.http.apiGet('/machine/queryMachine',
                    param,
                    function (json) {
                        var _machineInfo = JSON.parse(json);
                        vc.component.chooseMachineInfo.machines = _machineInfo.data;
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseMachine: function (_machine) {
                vc.emit($props.emitChooseMachine, 'chooseMachine', _machine);
                $('#chooseMachineModel').modal('hide');
            },
            queryMachines: function () {
                vc.component._loadAllMachineInfo(1, 10, vc.component.chooseMachineInfo._currentMachineName);
            },
            _refreshChooseMachineInfo: function () {
                vc.component.chooseMachineInfo._currentMachineName = "";
            }
        }

    });
})(window.vc);
