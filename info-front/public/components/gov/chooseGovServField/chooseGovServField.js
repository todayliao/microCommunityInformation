(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovServField:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovServFieldInfo:{
                govServFields:[],
                _currentGovServFieldName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovServField','openChooseGovServFieldModel',function(_param){
                $('#chooseGovServFieldModel').modal('show');
                vc.component._refreshChooseGovServFieldInfo();
                vc.component._loadAllGovServFieldInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovServFieldInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govServField/queryGovServField',
                             param,
                             function(json){
                                var _govServFieldInfo = JSON.parse(json);
                                vc.component.chooseGovServFieldInfo.govServFields = _govServFieldInfo.govServFields;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovServField:function(_govServField){
                if(_govServField.hasOwnProperty('name')){
                     _govServField.govServFieldName = _govServField.name;
                }
                vc.emit($props.emitChooseGovServField,'chooseGovServField',_govServField);
                vc.emit($props.emitLoadData,'listGovServFieldData',{
                    govServFieldId:_govServField.govServFieldId
                });
                $('#chooseGovServFieldModel').modal('hide');
            },
            queryGovServFields:function(){
                vc.component._loadAllGovServFieldInfo(1,10,vc.component.chooseGovServFieldInfo._currentGovServFieldName);
            },
            _refreshChooseGovServFieldInfo:function(){
                vc.component.chooseGovServFieldInfo._currentGovServFieldName = "";
            }
        }

    });
})(window.vc);
