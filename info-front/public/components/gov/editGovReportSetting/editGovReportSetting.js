(function (vc, vm) {

    vc.extends({
        data: {
            editGovReportSettingInfo: {
                settingId: '',
                reportTypeName: '',
                reportWay: '',
                returnVisitFlag: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovReportSetting', 'openEditGovReportSettingModal', function (_params) {
                vc.component.refreshEditGovReportSettingInfo();
                $('#editGovReportSettingModel').modal('show');
                vc.copyObject(_params, vc.component.editGovReportSettingInfo);
                vc.component.editGovReportSettingInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovReportSettingValidate: function () {
                return vc.validate.validate({
                    editGovReportSettingInfo: vc.component.editGovReportSettingInfo
                }, {
                    'editGovReportSettingInfo.reportTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "类型名称超长"
                        },
                    ],
                    'editGovReportSettingInfo.reportWay': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "派单方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "派单方式超长"
                        },
                    ],
                    'editGovReportSettingInfo.returnVisitFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "回访标识不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "回访标识超长"
                        },
                    ],
                    'editGovReportSettingInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovReportSettingInfo.settingId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "设置编码不能为空"
                        }]

                });
            },
            editGovReportSetting: function () {
                if (!vc.component.editGovReportSettingValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    'govReportSetting.updateGovReportSetting',
                    JSON.stringify(vc.component.editGovReportSettingInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovReportSettingModel').modal('hide');
                            vc.emit('govReportSettingManage', 'listGovReportSetting', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovReportSettingInfo: function () {
                vc.component.editGovReportSettingInfo = {
                    settingId: '',
                    reportTypeName: '',
                    reportWay: '',
                    returnVisitFlag: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
