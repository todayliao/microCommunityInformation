(function (vc, vm) {

    vc.extends({
        data: {
            editGovWorkTypeInfo: {
                govTypeId: '',
                workTypeName: '',
                state: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovWorkType', 'openEditGovWorkTypeModal', function (_params) {
                vc.component.refreshEditGovWorkTypeInfo();
                $('#editGovWorkTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovWorkTypeInfo);
            });
        },
        methods: {
            editGovWorkTypeValidate: function () {
                return vc.validate.validate({
                    editGovWorkTypeInfo: vc.component.editGovWorkTypeInfo
                }, {
                    'editGovWorkTypeInfo.workTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职务名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "职务名称超长"
                        },
                    ],
                    'editGovWorkTypeInfo.state': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "状态超长"
                        },
                    ],
                    'editGovWorkTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "备注太长"
                        },
                    ],
                    'editGovWorkTypeInfo.govTypeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党内职务ID不能为空"
                        }]

                });
            },
            editGovWorkType: function () {
                if (!vc.component.editGovWorkTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govWorkType/updateGovWorkType',
                    JSON.stringify(vc.component.editGovWorkTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovWorkTypeModel').modal('hide');
                            vc.emit('govWorkTypeManage', 'listGovWorkType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovWorkTypeInfo: function () {
                vc.component.editGovWorkTypeInfo = {
                    govTypeId: '',
                    workTypeName: '',
                    state: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
