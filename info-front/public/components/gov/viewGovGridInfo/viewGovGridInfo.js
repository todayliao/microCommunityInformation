/**
    网格人员 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovGridInfo:{
                index:0,
                flowComponent:'viewGovGridInfo',
                govGridId:'',
govPersonId:'',
govTypeId:'',
caId:'',
personName:'',
personTel:'',
ramark:'',
statusCd:'',
datasourceType:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovGridInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovGridInfo','chooseGovGrid',function(_app){
                vc.copyObject(_app, vc.component.viewGovGridInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovGridInfo);
            });

            vc.on('viewGovGridInfo', 'onIndex', function(_index){
                vc.component.viewGovGridInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovGridInfoModel(){
                vc.emit('chooseGovGrid','openChooseGovGridModel',{});
            },
            _openAddGovGridInfoModel(){
                vc.emit('addGovGrid','openAddGovGridModal',{});
            },
            _loadGovGridInfoData:function(){

            }
        }
    });

})(window.vc);
