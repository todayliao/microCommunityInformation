(function (vc, vm) {

    vc.extends({
        data: {
            editGovCommunityAreaInfo: {
                caId: '',
                caCode: '',
                caName: '',
                caSpace: '',
                areaCode: '',
                caAddress: '',
                person: '',
                personLink: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovCommunityArea', 'openEditGovCommunityAreaModal', function (_params) {
                vc.component.refreshEditGovCommunityAreaInfo();
                $('#editGovCommunityAreaModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCommunityAreaInfo);
                //vc.component.editGovCommunityAreaInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovCommunityAreaValidate: function () {
                return vc.validate.validate({
                    editGovCommunityAreaInfo: vc.component.editGovCommunityAreaInfo
                }, {
                    'editGovCommunityAreaInfo.caCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "区域编号超长"
                        },
                    ],
                    'editGovCommunityAreaInfo.caName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "区域名称超长"
                        },
                    ],
                    'editGovCommunityAreaInfo.caSpace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域面积不能为空"
                        }
                    ],
                    'editGovCommunityAreaInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "城市区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域名称超长"
                        },
                    ],
                    'editGovCommunityAreaInfo.caAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "256",
                            errInfo: "区域地址超长"
                        },
                    ],
                    'editGovCommunityAreaInfo.person': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'editGovCommunityAreaInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "责任人电话格式错误"
                        },
                    ],
                    'editGovCommunityAreaInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],
                    'editGovCommunityAreaInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域管理ID不能为空"
                        }]

                });
            },
            editGovCommunityArea: function () {
                if (!vc.component.editGovCommunityAreaValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCommunityArea/updateGovCommunityArea',
                    JSON.stringify(vc.component.editGovCommunityAreaInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCommunityAreaModel').modal('hide');
                            vc.emit('govCommunityAreaManage', 'listGovCommunityArea', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovCommunityAreaInfo: function () {
                vc.component.editGovCommunityAreaInfo = {
                    caId: '',
                    caCode: '',
                    caName: '',
                    caSpace: '',
                    areaCode: '',
                    caAddress: '',
                    person: '',
                    personLink: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
