(function(vc){

    vc.extends({
        propTypes: {
               callBackListener:vc.propTypes.string, //父组件名称
               callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            addGuideSubscribeInfo:{
                wgsId:'',
                caId:'',
                wgId:'',
                person:'',
                link:'',
                startTime:'',
                endTime:'',
                state:'',

            }
        },
         _initMethod:function(){
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGuideSubscribeInfo.startTime = _value;
            });
            vc.initDateTime('addEndTime', function (_value) {
                $that.addGuideSubscribeInfo.endTime = _value;
            });
         },
         _initEvent:function(){
            vc.on('addGuideSubscribe','openAddGuideSubscribeModal',function(){
                $('#addGuideSubscribeModel').modal('show');
            });
        },
        methods:{
            addGuideSubscribeValidate(){
                return vc.validate.validate({
                    addGuideSubscribeInfo:vc.component.addGuideSubscribeInfo
                },{
                    'addGuideSubscribeInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'addGuideSubscribeInfo.wgId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项id不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"事项id超长"
                        },
                    ],
'addGuideSubscribeInfo.person':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"咨询人名称太长"
                        },
                    ],
'addGuideSubscribeInfo.link':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"咨询电话太长"
                        },
                    ],
'addGuideSubscribeInfo.startTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"开始时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"开始时间超长"
                        },
                    ],
'addGuideSubscribeInfo.endTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"结束时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"结束时间超长"
                        },
                    ],
'addGuideSubscribeInfo.state':[
{
                            limit:"required",
                            param:"",
                            errInfo:"预约状态不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"12",
                            errInfo:"预约状态超长"
                        },
                    ],




                });
            },
            saveGuideSubscribeInfo:function(){
                vc.component.addGuideSubscribeInfo.caId = vc.getCurrentCommunity().caId;
                if(!vc.component.addGuideSubscribeValidate()){
                    vc.toast(vc.validate.errInfo);

                    return ;
                }

                //不提交数据将数据 回调给侦听处理
                if(vc.notNull($props.callBackListener)){
                    vc.emit($props.callBackListener,$props.callBackFunction,vc.component.addGuideSubscribeInfo);
                    $('#addGuideSubscribeModel').modal('hide');
                    return ;
                }

                vc.http.apiPost(
                    '/govGuideSubscribe/saveGovGuideSubscribe',
                    JSON.stringify(vc.component.addGuideSubscribeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGuideSubscribeModel').modal('hide');
                            vc.component.clearAddGuideSubscribeInfo();
                            vc.emit('guideSubscribeManage','listGuideSubscribe',{});

                            return ;
                        }
                        vc.message(_json.msg);

                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);

                     });
            },
            clearAddGuideSubscribeInfo:function(){
                vc.component.addGuideSubscribeInfo = {
                                            caId:'',
wgId:'',
person:'',
link:'',
startTime:'',
endTime:'',
state:'',

                                        };
            }
        }
    });

})(window.vc);
