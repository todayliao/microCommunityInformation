(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMajorEventsInfo: {
                govEventsId: '',
                caId: '',
                eventsName: '',
                happenTime: '',
                levelCd: '',
                levelCds: [],
                typeId: '',
                govEventsTypes: [],
                areaCode: '',
                eventsAddress: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            $that._listAddGovEventsTypes();
            vc.initDate('happenTimes', function (_value) {
                $that.addGovMajorEventsInfo.happenTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovMajorEvents', 'openAddGovMajorEventsModal', function () {
                $('#addGovMajorEventsModel').modal('show');

                vc.getDict('gov_major_events', "level_cd", function (_data) {
                    vc.component.addGovMajorEventsInfo.levelCds = _data;
                });
            });
        },
        methods: {
            addGovMajorEventsValidate() {
                return vc.validate.validate({
                    addGovMajorEventsInfo: vc.component.addGovMajorEventsInfo
                }, {
                   
                    'addGovMajorEventsInfo.eventsName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "名称不能超过64"
                        },
                    ],
                    'addGovMajorEventsInfo.happenTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生时间不能超过时间类型"
                        },
                    ],
                    'addGovMajorEventsInfo.levelCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "事件分级不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "事件分级不能超过30"
                        },
                    ],
                    'addGovMajorEventsInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "事件类型Id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "事件类型Id不能超过102"
                        },
                    ],
                    'addGovMajorEventsInfo.eventsAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "详细地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "详细地址不能超过1024"
                        },
                    ],
                    'addGovMajorEventsInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],




                });
            },
            saveGovMajorEventsInfo: function () {
                if (!vc.component.addGovMajorEventsValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovMajorEventsInfo.caId = vc.getCurrentCommunity().caId;
                vc.component.addGovMajorEventsInfo.areaCode = vc.component.addGovMajorEventsInfo.eventsAddress;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMajorEventsInfo);
                    $('#addGovMajorEventsModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMajorEvents/saveGovMajorEvents',
                    JSON.stringify(vc.component.addGovMajorEventsInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMajorEventsModel').modal('hide');
                            vc.component.clearAddGovMajorEventsInfo();
                            vc.emit('govMajorEventsManage', 'listGovMajorEvents', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovEventsTypes: function () {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govEventsType/queryGovEventsType',
                    param,
                    function (json, res) {
                        var _govEventsTypeManageInfo = JSON.parse(json);
                        vc.component.addGovMajorEventsInfo.govEventsTypes = _govEventsTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovMajorEventsInfo: function () {
                vc.component.addGovMajorEventsInfo = {
                    govEventsId: '',
                    caId: '',
                    eventsName: '',
                    happenTime: '',
                    levelCd: '',
                    levelCds: [],
                    typeId: '',
                    govEventsTypes: [],
                    areaCode: '',
                    eventsAddress: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
