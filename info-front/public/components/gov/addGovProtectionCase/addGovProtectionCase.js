(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovProtectionCaseInfo: {
                objType: '',
                objId: '',
                objName: '',
                caId: '',
                govCommunityId: '',
                caseName: '',
                caseCode: '',
                areaCode: '',
                happenedTime: '',
                happenedPlace: '',
                caseType: '',
                idType: '',
                idCard: '',
                personName: '',
                isSolved: '',
                personNum: '',
                fleeingNum: '',
                arrestsNum: '',
                detectionContent: '',
                caseContent: '',
                areas: [],
                provs: [],
                citys: [],
                selectProv: '',
                selectCity: '',
                selectArea: '',
                

            }
        },
        _initMethod: function () {
            vc.initDateTime('addHappenedTime', function (_value) {
                $that.addGovProtectionCaseInfo.happenedTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovProtectionCase', 'openAddGovProtectionCaseModal', function () {
                vc.component._initArea('101', '0');
                $('#addGovProtectionCaseModel').modal('show');
            });
        },
        methods: {
            addGovProtectionCaseValidate() {
                return vc.validate.validate({
                    addGovProtectionCaseInfo: vc.component.addGovProtectionCaseInfo
                }, {
                   
                    'addGovProtectionCaseInfo.objType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象类型（1道路；2校园周边）不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "对象类型（1道路；2校园周边）不能超过2"
                        },
                    ],
                    'addGovProtectionCaseInfo.objId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象Id不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "对象Id不能超过30"
                        },
                    ],
                    'addGovProtectionCaseInfo.objName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "60",
                            errInfo: "对象名称不能超过60"
                        },
                    ],
                    'addGovProtectionCaseInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'addGovProtectionCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件编号不能超过100"
                        },
                    ],
                    'addGovProtectionCaseInfo.caseCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件名称不能超过100"
                        },
                    ],
                    'addGovProtectionCaseInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发案地不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "8",
                            errInfo: "发案地不能超过8"
                        },
                    ],
                    'addGovProtectionCaseInfo.happenedTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'addGovProtectionCaseInfo.happenedPlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "发生地点不能超过200"
                        },
                    ],
                    'addGovProtectionCaseInfo.caseType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件性质不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "案件性质不能超过12"
                        },
                    ],
                    'addGovProtectionCaseInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "主犯（嫌疑人）证件类型不能超过12"
                        },
                    ],
                    'addGovProtectionCaseInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）证件号码不能超过64"
                        },
                    ],
                    'addGovProtectionCaseInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）姓名不能超过64"
                        },
                    ],
                    'addGovProtectionCaseInfo.isSolved': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否破案不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "是否破案不能超过2"
                        },
                    ],
                    'addGovProtectionCaseInfo.personNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "作案人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "作案人数不能超过3"
                        },
                    ],
                    'addGovProtectionCaseInfo.fleeingNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在逃人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "在逃人数不能超过3"
                        },
                    ],
                    'addGovProtectionCaseInfo.arrestsNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "抓捕人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "抓捕人数不能超过3"
                        },
                    ],
                    'addGovProtectionCaseInfo.detectionContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件侦破情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案件侦破情况不能超过长文本"
                        },
                    ],
                    'addGovProtectionCaseInfo.caseContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案（事）件情况不能超过长文本"
                        },
                    ]
                });
            },
            saveGovProtectionCaseInfo: function () {
                vc.component.addGovProtectionCaseInfo.caId = vc.getCurrentCommunity().caId;
                if (!vc.component.addGovProtectionCaseValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovProtectionCaseInfo);
                    $('#addGovProtectionCaseModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govProtectionCase/saveGovProtectionCase',
                    JSON.stringify(vc.component.addGovProtectionCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovProtectionCaseModel').modal('hide');
                            vc.component.clearAddGovProtectionCaseInfo();
                            vc.emit('govProtectionCaseManage', 'listGovProtectionCase', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            getObjName: function (_objId) {
               let _objType = vc.component.addGovProtectionCaseInfo.objType;
                if(_objType =="1"){
                    vc.component.govProtectionCaseManageInfo.govRoadProtections.forEach(_item => {
                        if (_item.roadProtectionId == _objId) {
                            vc.component.addGovProtectionCaseInfo.objName = _item.roadName
                        }
                    });
                }else if(_objType =="2"){
                    vc.component.govProtectionCaseManageInfo.govSchools.forEach(_item => {
                        if (_item.schoolId == _objId) {
                            vc.component.addGovProtectionCaseInfo.objName = _item.schoolName
                        }
                    });
                }
            },
            getProv: function (_prov) {
                vc.component._initArea('202', _prov);
            },
            getCity: function (_city) {
                vc.component._initArea('303', _city);
            },
            getArea: function (_area) {
                vc.component.addGovProtectionCaseInfo.areaCode = _area;
            },
            _initArea: function (_areaLevel, _parentAreaCode) { //加载区域
                var _param = {
                    params: {
                        areaLevel: _areaLevel,
                        parentAreaCode: _parentAreaCode
                    }
                };
                 //发送get请求
                vc.http.apiGet('/cityArea/getAreas',
                    _param,
                    function (json, res) {
                            var _tmpAreas = JSON.parse(json);
                            if (_areaLevel == '101') {
                                vc.component.addGovProtectionCaseInfo.provs = _tmpAreas.data.areas;
                                vc.component.addGovProtectionCaseInfo.citys = [];
                                vc.component.addGovProtectionCaseInfo.areas = [];
                            } else if (_areaLevel == '202') {
                                vc.component.addGovProtectionCaseInfo.citys = _tmpAreas.data.areas;
                                vc.component.addGovProtectionCaseInfo.areas = [];
                            } else {
                                vc.component.addGovProtectionCaseInfo.areas = _tmpAreas.data.areas;
                            }
                            return;
                        //vc.component.$emit('errorInfoEvent',json);
                    }, function (errInfo, error) {
                        console.log('请求失败处理', errInfo, error);
                        vc.toast("查询地区失败");
                    });
            },
            clearAddGovProtectionCaseInfo: function () {
                vc.component.addGovProtectionCaseInfo = {
                    objType: '',
                    objId: '',
                    objName: '',
                    caId: '',
                    govCommunityId: '',
                    caseName: '',
                    caseCode: '',
                    areaCode: '',
                    happenedTime: '',
                    happenedPlace: '',
                    caseType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: '',
                    areas: [],
                    provs: [],
                    citys: [],
                    selectProv: '',
                    selectCity: '',
                    selectArea: '',
                };
            }
        }
    });

})(window.vc);
