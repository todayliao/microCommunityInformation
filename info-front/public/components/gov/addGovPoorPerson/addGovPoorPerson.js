(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPoorPersonInfo: {
                govPersonId: '',
                lableCd: '6009',
                lableName: '贫困人员',
                lableType: 'P',
                caId: '',
                personName: ''

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovPoorPerson', 'openaddGovPoorPersonModal', function () {
                $('#addGovPoorPersonModel').modal('show');
            });

            vc.on('addGovPoorPerson','chooseGovPerson', function (_param) {
                $that.addGovPoorPersonInfo.govPersonId = _param.govPersonId;
                $that.addGovPoorPersonInfo.personName = _param.personName;
            });
        },
        methods: {
            addGovPoorPersonValidate() {
                return vc.validate.validate({
                    addGovPoorPersonInfo: vc.component.addGovPoorPersonInfo
                }, {
                    'addGovPoorPersonInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人员名称不能为空"
                        }
                    ]
                });
            },
            saveaGovPoorPersonInfo: function () {
                if (!vc.component.addGovPoorPersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addGovPoorPersonInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPoorPersonInfo);
                    $('#addGovPoorPersonModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govPersonLabelRel/savePersonLabelRel',
                    JSON.stringify(vc.component.addGovPoorPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPoorPersonModel').modal('hide');
                            vc.component.clearaddGovPoorPersonInfo();
                            vc.emit('govPoorPersonManage', 'listGovPerson', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _openAddChooseGovPersonModel: function () {
                let _caId = vc.getCurrentCommunity().caId;

                vc.emit('chooseGovPerson', 'openChooseGovPersonModel', _caId);
            },
            _closeAddGovPoorPerson: function () {
                $that.clearaddGovPoorPersonInfo();
                vc.emit('govPoorPersonManage', 'listGovPerson', {});
            },
            clearaddGovPoorPersonInfo: function () {
                vc.component.addGovPoorPersonInfo = {
                    govPersonId: '',
                    lableCd: '6009',
                    lableName: '贫困人员',
                    lableType: 'P',
                    caId: '',
                    personName: ''

                };
            }
        }
    });

})(window.vc);
