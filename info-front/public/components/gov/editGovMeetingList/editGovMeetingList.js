(function (vc, vm) {

    vc.extends({
        data: {
            editGovMeetingListInfo: {
                govMeetingId: '',
                caId: '',
                typeId: '',
                orgId: '',
                govMemberId: '',
                memberName: '',
                meetingName: '',
                meetingTime: '',
                meetingAddress: '',
                meetingBrief: '',
                govMembers: [],
                govMeetingTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
            $that._listEditGovMeetingTypes();
            $that._listEditGovPartyOrgs();
        },
        _initEvent: function () {
            vc.on('editGovMeetingList', 'openEditGovMeetingListModal', function (_params) {
                vc.component.refreshEditGovMeetingListInfo();
                $('#editGovMeetingListModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.editGovMeetingListInfo);
                vc.component.editGovMeetingListInfo.govMembers=_params.govMeetingMemberRelDtos;
                vc.component.editGovMeetingListInfo.caId = vc.getCurrentCommunity().caId;

                vc.emit('viewEditGovPartyMember', 'openviewEditGovPartyMemberModel', $that.editGovMeetingListInfo.govMembers);
            });
            vc.on('meetingListChooseGovPartyMember', 'chooseGovPartyMember', function (_param) {
                $that.editGovMeetingListInfo.govMemberId = _param.govMemberId;
                $that.editGovMeetingListInfo.memberName = _param.memberName;
            });
            vc.on('viewEditGovPartyMember', 'page_event', function (_param) {
                $that.editGovMeetingListInfo.govMembers = _param;
            });
        },
        methods: {
            editGovMeetingListValidate: function () {
                return vc.validate.validate({
                    editGovMeetingListInfo: vc.component.editGovMeetingListInfo
                }, {
                    'editGovMeetingListInfo.govMeetingId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议ID不能超过30"
                        },
                    ],
                    'editGovMeetingListInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovMeetingListInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议类型不能超过30"
                        },
                    ],
                    'editGovMeetingListInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织部门不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织部门不能超过30"
                        },
                    ],
                    'editGovMeetingListInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主持人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "主持人不能超过30"
                        },
                    ],
                    'editGovMeetingListInfo.meetingName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "会议名称不能超过64"
                        },
                    ],
                    'editGovMeetingListInfo.meetingTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "会议时间不能超过时间类型"
                        },
                    ],
                    'editGovMeetingListInfo.meetingAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "会议地点不能超过512"
                        },
                    ],
                    'editGovMeetingListInfo.meetingBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议简介不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议简介不能超过30"
                        },
                    ]

                });
            },
            editGovMeetingList: function () {
                if (!vc.component.editGovMeetingListValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govMeetingList/updateGovMeetingList',
                    JSON.stringify(vc.component.editGovMeetingListInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMeetingListModel').modal('hide');
                            vc.component.refreshEditGovMeetingListInfo();
                            vc.emit('govMeetingListManage', 'listGovMeetingList', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovMeetingTypes: function () {
                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingType/queryGovMeetingType',
                    param,
                    function (json, res) {
                        var _govMeetingTypeManageInfo = JSON.parse(json);
                        vc.component.editGovMeetingListInfo.govMeetingTypes = _govMeetingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listEditGovPartyOrgs: function () {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.editGovMeetingListInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openEditChooseGovPartyMember: function (_editGovMeetingListInfo) {
                let _caId = vc.getCurrentCommunity().caId;
                if (!_caId) {
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('chooseGovPartyMember', 'openChooseGovPartyMemberModel', _editGovMeetingListInfo);
            },
            _setEditviewGovPartyMember: function (_orgId) {
                let _caId = vc.getCurrentCommunity().caId;
                if (!_caId) {
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('viewEditGovPartyMember', 'selectPartyOrgs', _orgId);
            },
            _clearEditGovMeetingListModel: function () {
                $that.refreshEditGovMeetingListInfo();
                vc.emit('govMeetingListManage', 'listGovMeetingList', {});
            },
            refreshEditGovMeetingListInfo: function () {
                let _govMembers = vc.component.editGovMeetingListInfo.govMembers;
                let _govMeetingTypes = vc.component.editGovMeetingListInfo.govMeetingTypes;
                let _govPartyOrgs = vc.component.editGovMeetingListInfo.govPartyOrgs;
                vc.component.editGovMeetingListInfo = {
                    govMeetingId: '',
                    caId: '',
                    typeId: '',
                    orgId: '',
                    govMemberId: '',
                    memberName: '',
                    meetingName: '',
                    meetingTime: '',
                    meetingAddress: '',
                    meetingBrief: '',
                    govMembers: _govMembers,
                    govMeetingTypes: _govMeetingTypes,
                    govPartyOrgs: _govPartyOrgs
                }
            }
        }
    });

})(window.vc, window.vc.component);
