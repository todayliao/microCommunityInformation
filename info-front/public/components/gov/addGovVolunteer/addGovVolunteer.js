(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovVolunteerInfo: {
                volunteerId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                student: '',
                company: '',
                volWork: '',
                edu: '',
                goodAtSkills: [],
                freeTimes: [],
                freeTime: [],
                ramark: '',
                politicalOutlooks: [],
                edus: [],
                goodAtSkillss: [],
                volunteerServ: [],
                volunteerServs: [],
                persons: {
                    idType: '1',
                    idCard: '',
                    politicalOutlook: '',
                    nativePlace: '',
                    nation: '',
                    personSex: '',

                }
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovVolunteer', 'openAddGovVolunteerModal', function () {

                vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                    vc.component.addGovVolunteerInfo.politicalOutlooks = _data;
                });
                vc.getDict('gov_volunteer', "edu", function (_data) {
                    vc.component.addGovVolunteerInfo.edus = _data;
                });
                vc.getDict('gov_volunteer', "good_at_skills", function (_data) {
                    vc.component.addGovVolunteerInfo.goodAtSkillss = _data;
                });
                vc.getDict('gov_volunteer', "free_time", function (_data) {
                    vc.component.addGovVolunteerInfo.freeTimes = _data;
                });
                $that._listAddGovServFields();
                $('#addGovVolunteerModel').modal('show');
            });
        },
        methods: {
            addGovVolunteerValidate() {
                return vc.validate.validate({
                    addGovVolunteerInfo: vc.component.addGovVolunteerInfo
                }, {
                    'addGovVolunteerInfo.persons.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证不能为空"
                        }
                    ],
                    'addGovVolunteerInfo.persons.politicalOutlook': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政治面貌不能为空"
                        }
                    ],
                    'addGovVolunteerInfo.persons.nativePlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "籍贯不能为空"
                        }
                    ],
                    'addGovVolunteerInfo.persons.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'addGovVolunteerInfo.persons.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'addGovVolunteerInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "志愿者名称不能超过64"
                        },
                    ],
                    'addGovVolunteerInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovVolunteerInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovVolunteerInfo.student': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否学生不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否学生不能超过12"
                        },
                    ],
                    'addGovVolunteerInfo.company': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校或工作单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "学校或工作单位不能超过128"
                        },
                    ],
                    'addGovVolunteerInfo.volWork': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "从业情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "从业情况不能超过128"
                        },
                    ],
                    'addGovVolunteerInfo.edu': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学历不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "学历不能超过12"
                        },
                    ],
                    'addGovVolunteerInfo.goodAtSkills': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "擅长技能不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "擅长技能不能超过512"
                        },
                    ],
                    'addGovVolunteerInfo.freeTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "空闲时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "空闲时间不能超过64"
                        },
                    ]




                });
            },
            saveGovVolunteerInfo: function () {
                if (!vc.component.addGovVolunteerValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                if (vc.component.addGovVolunteerInfo.goodAtSkills.length < 1) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.addGovVolunteerInfo.goodAtSkills.length > 3) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.addGovVolunteerInfo.volunteerServ.length > 3) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }
                if (vc.component.addGovVolunteerInfo.volunteerServ.length < 1) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }


                vc.component.addGovVolunteerInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovVolunteerInfo);
                    $('#addGovVolunteerModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govVolunteer/saveGovVolunteer',
                    JSON.stringify(vc.component.addGovVolunteerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovVolunteerModel').modal('hide');
                            vc.component.clearAddGovVolunteerInfo();
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _chooseSingle: function (__goodAtSkills) {
                console.log(__goodAtSkills);
            },
            _listAddGovServFields: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.addGovVolunteerInfo.volunteerServs = _govServFieldManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovVolunteerInfo: function () {
                vc.component.addGovVolunteerInfo = {
                    volunteerId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    student: '',
                    company: '',
                    volWork: '',
                    edu: '',
                    goodAtSkills: [],
                    freeTimes: [],
                    freeTime: [],
                    ramark: '',
                    politicalOutlooks: [],
                    edus: [],
                    goodAtSkillss: [],
                    volunteerServs: [],
                    persons: {
                        idType: '1',
                        idCard: '',
                        politicalOutlook: '',
                        nativePlace: '',
                        nation: '',
                        personSex: '',

                    }

                };
            }
        }
    });

})(window.vc);
