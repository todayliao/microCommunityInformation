(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivity:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivityInfo:{
                govActivitys:[],
                _currentGovActivityName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivity','openChooseGovActivityModel',function(_param){
                $('#chooseGovActivityModel').modal('show');
                vc.component._refreshChooseGovActivityInfo();
                vc.component._loadAllGovActivityInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivityInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govActivity.listGovActivitys',
                             param,
                             function(json){
                                var _govActivityInfo = JSON.parse(json);
                                vc.component.chooseGovActivityInfo.govActivitys = _govActivityInfo.govActivitys;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivity:function(_govActivity){
                if(_govActivity.hasOwnProperty('name')){
                     _govActivity.govActivityName = _govActivity.name;
                }
                vc.emit($props.emitChooseGovActivity,'chooseGovActivity',_govActivity);
                vc.emit($props.emitLoadData,'listGovActivityData',{
                    govActivityId:_govActivity.govActivityId
                });
                $('#chooseGovActivityModel').modal('hide');
            },
            queryGovActivitys:function(){
                vc.component._loadAllGovActivityInfo(1,10,vc.component.chooseGovActivityInfo._currentGovActivityName);
            },
            _refreshChooseGovActivityInfo:function(){
                vc.component.chooseGovActivityInfo._currentGovActivityName = "";
            }
        }

    });
})(window.vc);
