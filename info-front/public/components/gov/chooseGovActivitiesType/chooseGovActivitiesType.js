(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivitiesType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivitiesTypeInfo:{
                govActivitiesTypes:[],
                _currentGovActivitiesTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivitiesType','openChooseGovActivitiesTypeModel',function(_param){
                $('#chooseGovActivitiesTypeModel').modal('show');
                vc.component._refreshChooseGovActivitiesTypeInfo();
                vc.component._loadAllGovActivitiesTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivitiesTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govActivitiesType.listGovActivitiesTypes',
                             param,
                             function(json){
                                var _govActivitiesTypeInfo = JSON.parse(json);
                                vc.component.chooseGovActivitiesTypeInfo.govActivitiesTypes = _govActivitiesTypeInfo.govActivitiesTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivitiesType:function(_govActivitiesType){
                if(_govActivitiesType.hasOwnProperty('name')){
                     _govActivitiesType.govActivitiesTypeName = _govActivitiesType.name;
                }
                vc.emit($props.emitChooseGovActivitiesType,'chooseGovActivitiesType',_govActivitiesType);
                vc.emit($props.emitLoadData,'listGovActivitiesTypeData',{
                    govActivitiesTypeId:_govActivitiesType.govActivitiesTypeId
                });
                $('#chooseGovActivitiesTypeModel').modal('hide');
            },
            queryGovActivitiesTypes:function(){
                vc.component._loadAllGovActivitiesTypeInfo(1,10,vc.component.chooseGovActivitiesTypeInfo._currentGovActivitiesTypeName);
            },
            _refreshChooseGovActivitiesTypeInfo:function(){
                vc.component.chooseGovActivitiesTypeInfo._currentGovActivitiesTypeName = "";
            }
        }

    });
})(window.vc);
