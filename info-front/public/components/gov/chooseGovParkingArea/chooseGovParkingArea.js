(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovParkingArea:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovParkingAreaInfo:{
                govParkingAreas:[],
                _currentGovParkingAreaName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovParkingArea','openChooseGovParkingAreaModel',function(_param){
                $('#chooseGovParkingAreaModel').modal('show');
                vc.component._refreshChooseGovParkingAreaInfo();
                vc.component._loadAllGovParkingAreaInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovParkingAreaInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govParkingArea.listGovParkingAreas',
                             param,
                             function(json){
                                var _govParkingAreaInfo = JSON.parse(json);
                                vc.component.chooseGovParkingAreaInfo.govParkingAreas = _govParkingAreaInfo.govParkingAreas;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovParkingArea:function(_govParkingArea){
                if(_govParkingArea.hasOwnProperty('name')){
                     _govParkingArea.govParkingAreaName = _govParkingArea.name;
                }
                vc.emit($props.emitChooseGovParkingArea,'chooseGovParkingArea',_govParkingArea);
                vc.emit($props.emitLoadData,'listGovParkingAreaData',{
                    govParkingAreaId:_govParkingArea.govParkingAreaId
                });
                $('#chooseGovParkingAreaModel').modal('hide');
            },
            queryGovParkingAreas:function(){
                vc.component._loadAllGovParkingAreaInfo(1,10,vc.component.chooseGovParkingAreaInfo._currentGovParkingAreaName);
            },
            _refreshChooseGovParkingAreaInfo:function(){
                vc.component.chooseGovParkingAreaInfo._currentGovParkingAreaName = "";
            }
        }

    });
})(window.vc);
