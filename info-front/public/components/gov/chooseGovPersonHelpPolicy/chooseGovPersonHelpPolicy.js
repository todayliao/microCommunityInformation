(function (vc) {
    vc.extends({
        data: {
            chooseGovPersonHelpPolicyInfo: {
                addGovHelpPolicyListInfo: {},
                govPersons: [],
                personNameLike: '',
                idCard: '',
                labelCd: '',
                _caId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPersonHelpPolicy', 'openchooseGovPersonHelpPolicyModel', function (_param) {
                $that.chooseGovPersonHelpPolicyInfo.addGovHelpPolicyListInfo = _param.addGovHelpPolicyListInfo;
                $that.chooseGovPersonHelpPolicyInfo._caId = _param.addGovHelpPolicyListInfo.caId;
                $that.chooseGovPersonHelpPolicyInfo.labelCd = _param.labelCd;
                $('#chooseGovPersonHelpPolicyModel').modal('show');
                vc.component._refreshchooseGovPersonHelpPolicyInfo();
                vc.component._loadAllGovPersonInfo(1, 10, '','',$that.chooseGovPersonHelpPolicyInfo._caId,$that.chooseGovPersonHelpPolicyInfo.labelCd);
            });
        },
        methods: {
            _loadAllGovPersonInfo: function (_page, _row, _name,_idCard,_caId,_labelCd) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        personNameLike: _name,
                        caId:_caId,
                        idCard: _idCard,
                        labelCd: _labelCd
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/getGovPersonHelpPolicy',
                    param,
                    function (json) {
                        var _govPersonInfo = JSON.parse(json);
                        vc.component.chooseGovPersonHelpPolicyInfo.total = _govPersonInfo.total;
                        vc.component.chooseGovPersonHelpPolicyInfo.records = _govPersonInfo.records;
                        vc.component.chooseGovPersonHelpPolicyInfo.govPersons = _govPersonInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.chooseGovPersonHelpPolicyInfo.records,
                            currentPage: _page
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPersonHelpPolicy: function (_govPerson) {
                if($that.chooseGovPersonHelpPolicyInfo.labelCd == '6009'){
                    $that.chooseGovPersonHelpPolicyInfo.addGovHelpPolicyListInfo.poorPersonId = _govPerson.govPersonId;
                    $that.chooseGovPersonHelpPolicyInfo.addGovHelpPolicyListInfo.poorPersonName = _govPerson.personName

                }else if($that.chooseGovPersonHelpPolicyInfo.labelCd == '6010'){
                    $that.chooseGovPersonHelpPolicyInfo.addGovHelpPolicyListInfo.cadrePersonId = _govPerson.govPersonId;
                    $that.chooseGovPersonHelpPolicyInfo.addGovHelpPolicyListInfo.cadrePersonName = _govPerson.personName
                }
                $('#chooseGovPersonHelpPolicyModel').modal('hide');
            },
            queryGovPersons: function () {
                vc.component._loadAllGovPersonInfo(1, 10, vc.component.chooseGovPersonHelpPolicyInfo.personNameLike,vc.component.chooseGovPersonHelpPolicyInfo.idCard,$that.chooseGovPersonHelpPolicyInfo._caId);
            },
            _refreshchooseGovPersonHelpPolicyInfo: function () {
                vc.component.chooseGovPersonHelpPolicyInfo.personNameLike = "";
                vc.component.chooseGovPersonHelpPolicyInfo.idCard = "";

            }
        }

    });
})(window.vc);
