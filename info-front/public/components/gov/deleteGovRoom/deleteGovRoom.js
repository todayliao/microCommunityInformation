(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovRoomInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovRoom', 'openDeleteGovRoomModal', function (_params) {

                vc.component.deleteGovRoomInfo = _params;
                $('#deleteGovRoomModel').modal('show');

            });
        },
        methods: {
            deleteGovRoom: function () {
                vc.http.apiPost(
                    '/govRoom/deleteGovRoom',
                    JSON.stringify(vc.component.deleteGovRoomInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovRoomModel').modal('hide');
                            vc.emit('govRoomManage', 'listGovRoom', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovRoomModel: function () {
                $('#deleteGovRoomModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
