(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovEventsTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovEventsType', 'openDeleteGovEventsTypeModal', function (_params) {

                vc.component.deleteGovEventsTypeInfo = _params;
                $('#deleteGovEventsTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovEventsType: function () {
                vc.component.deleteGovEventsTypeInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govEventsType/deleteGovEventsType',
                    JSON.stringify(vc.component.deleteGovEventsTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovEventsTypeModel').modal('hide');
                            vc.emit('govEventsTypeManage', 'listGovEventsType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovEventsTypeModel: function () {
                $('#deleteGovEventsTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
