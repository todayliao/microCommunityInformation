(function (vc, vm) {

    vc.extends({
        data: {
            editGovVolunteerInfo: {
                volunteerId: '',
                govPersonId:'',
                name: '',
                caId: '',
                address: '',
                tel: '',
                student: '',
                company: '',
                volWork: '',
                edu: '',
                goodAtSkills: [],
                freeTimes: [],
                freeTime: [],
                ramark: '',
                politicalOutlooks: [],
                edus: [],
                goodAtSkillss: [],
                volunteerServ: [],
                volunteerServs: [],
                persons: {
                    idType: '1',
                    idCard: '',
                    politicalOutlook: '',
                    nativePlace: '',
                    nation: '',
                    personSex: '',

                }
            }
        },
        _initMethod: function () {
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.editGovVolunteerInfo.politicalOutlooks = _data;
            });
            vc.getDict('gov_volunteer', "edu", function (_data) {
                vc.component.editGovVolunteerInfo.edus = _data;
            });
            vc.getDict('gov_volunteer', "good_at_skills", function (_data) {
                vc.component.editGovVolunteerInfo.goodAtSkillss = _data;
            });
            vc.getDict('gov_volunteer', "free_time", function (_data) {
                vc.component.editGovVolunteerInfo.freeTimes = _data;
            });
            $that._listEditGovServFields();
        },
        _initEvent: function () {
            vc.on('editGovVolunteer', 'openEditGovVolunteerModal', function (_params) {
                vc.component.refreshEditGovVolunteerInfo();
                $('#editGovVolunteerModel').modal('show');
                console.log(_params);
                vc.copyObject(_params, vc.component.editGovVolunteerInfo);
                vc.copyObject(_params, vc.component.editGovVolunteerInfo.persons);
                vc.component.editGovVolunteerInfo.goodAtSkills = JSON.parse(_params.goodAtSkills);
                vc.component.editGovVolunteerInfo.freeTime = JSON.parse(_params.freeTime);
                vc.component.editGovVolunteerInfo.volunteerServ = JSON.parse(_params.volunteerServ);

                vc.component.editGovVolunteerInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovVolunteerValidate: function () {
                return vc.validate.validate({
                    editGovVolunteerInfo: vc.component.editGovVolunteerInfo
                }, {
                    'editGovVolunteerInfo.volunteerId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "志愿者ID不能超过30"
                        },
                    ],
                    'editGovVolunteerInfo.govPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "人口ID不能为空"
                        }
                    ],
                    'editGovVolunteerInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "志愿者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "志愿者名称不能超过64"
                        },
                    ],
                    'editGovVolunteerInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovVolunteerInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovVolunteerInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovVolunteerInfo.student': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否学生不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否学生不能超过12"
                        },
                    ],
                    'editGovVolunteerInfo.company': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学校或工作单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "学校或工作单位不能超过128"
                        },
                    ],
                    'editGovVolunteerInfo.volWork': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "从业情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "从业情况不能超过128"
                        },
                    ],
                    'editGovVolunteerInfo.edu': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "学历不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "学历不能超过12"
                        },
                    ],
                    'editGovVolunteerInfo.goodAtSkills': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "擅长技能不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "擅长技能不能超过512"
                        },
                    ],
                    'editGovVolunteerInfo.freeTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "空闲时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "空闲时间不能超过64"
                        },
                    ]

                });
            },
            editGovVolunteer: function () {
                if (!vc.component.editGovVolunteerValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                console.log(vc.component.editGovVolunteerInfo.volunteerServ);

                if (vc.component.editGovVolunteerInfo.goodAtSkills.length < 1) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.editGovVolunteerInfo.goodAtSkills.length > 3) {
                    vc.toast("擅长技能请选择1-3种");
                    return;
                }
                if (vc.component.editGovVolunteerInfo.volunteerServ.length > 3) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }
                if (vc.component.editGovVolunteerInfo.volunteerServ.length < 1) {
                    vc.toast("服务领域请选择1-3种");

                    return;
                }

                vc.http.apiPost(
                    '/govVolunteer/updateGovVolunteer',
                    JSON.stringify(vc.component.editGovVolunteerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovVolunteerModel').modal('hide');
                            vc.emit('govVolunteerManage', 'listGovVolunteer', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovServFields: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.editGovVolunteerInfo.volunteerServs = _govServFieldManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovVolunteerInfo: function () {
                let _goodAtSkillss = $that.editGovVolunteerInfo.goodAtSkillss;
                let _freeTimes = $that.editGovVolunteerInfo.freeTimes;
                let _politicalOutlooks = $that.editGovVolunteerInfo.politicalOutlooks;
                let _edus = $that.editGovVolunteerInfo.edus;
                let _volunteerServs = $that.editGovVolunteerInfo.volunteerServs;
                vc.component.editGovVolunteerInfo = {
                    volunteerId: '',
                    govPersonId:'',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    student: '',
                    company: '',
                    volWork: '',
                    edu: '',
                    goodAtSkills: [],
                    freeTimes: _freeTimes,
                    freeTime: [],
                    ramark: '',
                    politicalOutlooks: _politicalOutlooks,
                    edus: _edus,
                    goodAtSkillss: _goodAtSkillss,
                    volunteerServ: [],
                    volunteerServs: _volunteerServs,
                    persons: {
                        idType: '1',
                        idCard: '',
                        politicalOutlook: '',
                        nativePlace: '',
                        nation: '',
                        personSex: '',
    
                    }
                }
            }
        }
    });

})(window.vc, window.vc.component);
