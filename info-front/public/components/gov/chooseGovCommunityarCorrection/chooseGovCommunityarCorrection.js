(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovCommunityarCorrection:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovCommunityarCorrectionInfo:{
                govCommunityarCorrections:[],
                _currentGovCommunityarCorrectionName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovCommunityarCorrection','openChooseGovCommunityarCorrectionModel',function(_param){
                $('#chooseGovCommunityarCorrectionModel').modal('show');
                vc.component._refreshChooseGovCommunityarCorrectionInfo();
                vc.component._loadAllGovCommunityarCorrectionInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovCommunityarCorrectionInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govCommunityarCorrection/queryGovCommunityarCorrection',
                             param,
                             function(json){
                                var _govCommunityarCorrectionInfo = JSON.parse(json);
                                vc.component.chooseGovCommunityarCorrectionInfo.govCommunityarCorrections = _govCommunityarCorrectionInfo.govCommunityarCorrections;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovCommunityarCorrection:function(_govCommunityarCorrection){
                if(_govCommunityarCorrection.hasOwnProperty('name')){
                     _govCommunityarCorrection.govCommunityarCorrectionName = _govCommunityarCorrection.name;
                }
                vc.emit($props.emitChooseGovCommunityarCorrection,'chooseGovCommunityarCorrection',_govCommunityarCorrection);
                vc.emit($props.emitLoadData,'listGovCommunityarCorrectionData',{
                    govCommunityarCorrectionId:_govCommunityarCorrection.govCommunityarCorrectionId
                });
                $('#chooseGovCommunityarCorrectionModel').modal('hide');
            },
            queryGovCommunityarCorrections:function(){
                vc.component._loadAllGovCommunityarCorrectionInfo(1,10,vc.component.chooseGovCommunityarCorrectionInfo._currentGovCommunityarCorrectionName);
            },
            _refreshChooseGovCommunityarCorrectionInfo:function(){
                vc.component.chooseGovCommunityarCorrectionInfo._currentGovCommunityarCorrectionName = "";
            }
        }

    });
})(window.vc);
