(function(vc,vm){

    vc.extends({
        data:{
            deleteGovSchoolPeripheralPersonInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovSchoolPeripheralPerson','openDeleteGovSchoolPeripheralPersonModal',function(_params){

                vc.component.deleteGovSchoolPeripheralPersonInfo = _params;
                $('#deleteGovSchoolPeripheralPersonModel').modal('show');

            });
        },
        methods:{
            deleteGovSchoolPeripheralPerson:function(){
                vc.component.deleteGovSchoolPeripheralPersonInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govSchoolPeripheralPerson/deleteGovSchoolPeripheralPerson',
                    JSON.stringify(vc.component.deleteGovSchoolPeripheralPersonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovSchoolPeripheralPersonModel').modal('hide');
                            vc.emit('govSchoolPeripheralPersonManage','listGovSchoolPeripheralPerson',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovSchoolPeripheralPersonModel:function(){
                $('#deleteGovSchoolPeripheralPersonModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
