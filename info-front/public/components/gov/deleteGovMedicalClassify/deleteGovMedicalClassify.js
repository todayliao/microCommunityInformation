(function(vc,vm){

    vc.extends({
        data:{
            deleteGovMedicalClassifyInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovMedicalClassify','openDeleteGovMedicalClassifyModal',function(_params){

                vc.component.deleteGovMedicalClassifyInfo = _params;
                $('#deleteGovMedicalClassifyModel').modal('show');

            });
        },
        methods:{
            deleteGovMedicalClassify:function(){
                vc.component.deleteGovMedicalClassifyInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMedicalClassify/deleteGovMedicalClassify',
                    JSON.stringify(vc.component.deleteGovMedicalClassifyInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMedicalClassifyModel').modal('hide');
                            vc.emit('govMedicalClassifyManage','listGovMedicalClassify',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovMedicalClassifyModel:function(){
                $('#deleteGovMedicalClassifyModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
