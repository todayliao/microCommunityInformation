(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMentalDisorders:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMentalDisordersInfo:{
                govMentalDisorderss:[],
                _currentGovMentalDisordersName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMentalDisorders','openChooseGovMentalDisordersModel',function(_param){
                $('#chooseGovMentalDisordersModel').modal('show');
                vc.component._refreshChooseGovMentalDisordersInfo();
                vc.component._loadAllGovMentalDisordersInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMentalDisordersInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMentalDisorders/queryGovMentalDisorders',
                             param,
                             function(json){
                                var _govMentalDisordersInfo = JSON.parse(json);
                                vc.component.chooseGovMentalDisordersInfo.govMentalDisorderss = _govMentalDisordersInfo.govMentalDisorderss;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMentalDisorders:function(_govMentalDisorders){
                if(_govMentalDisorders.hasOwnProperty('name')){
                     _govMentalDisorders.govMentalDisordersName = _govMentalDisorders.name;
                }
                vc.emit($props.emitChooseGovMentalDisorders,'chooseGovMentalDisorders',_govMentalDisorders);
                vc.emit($props.emitLoadData,'listGovMentalDisordersData',{
                    govMentalDisordersId:_govMentalDisorders.govMentalDisordersId
                });
                $('#chooseGovMentalDisordersModel').modal('hide');
            },
            queryGovMentalDisorderss:function(){
                vc.component._loadAllGovMentalDisordersInfo(1,10,vc.component.chooseGovMentalDisordersInfo._currentGovMentalDisordersName);
            },
            _refreshChooseGovMentalDisordersInfo:function(){
                vc.component.chooseGovMentalDisordersInfo._currentGovMentalDisordersName = "";
            }
        }

    });
})(window.vc);
