(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovAreaRenovationInfo: {
                govRenovationId: '',
                caId: '',
                typeId: '',
                govAreaTypes: [],
                securityKey: '',
                securityProblem: '',
                securityRange: '',
                leadCompany: '',
                leadParticipate: '',
                name: '',
                tel: '',
                startTime: '',
                endTime: '',
                crackCriminal: '',
                crackSecurity: '',
                leadRamark: '',

            }
        },
        _initMethod: function () {
            $that._listAddGovAreaTypes();
        },
        _initEvent: function () {
            vc.on('addGovAreaRenovation', 'openAddGovAreaRenovationModal', function () {
                $('#addGovAreaRenovationModel').modal('show');
            });
        },
        methods: {
            addGovAreaRenovationValidate() {
                return vc.validate.validate({
                    addGovAreaRenovationInfo: vc.component.addGovAreaRenovationInfo
                }, {
                   
                    'addGovAreaRenovationInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "涉及区域类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "涉及区域类型ID不能超过30"
                        },
                    ],
                    'addGovAreaRenovationInfo.securityKey': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安重点地区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "治安重点地区不能超过64"
                        },
                    ],
                    'addGovAreaRenovationInfo.securityProblem': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "治安突出问题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "治安突出问题不能超过64"
                        },
                    ],
                    'addGovAreaRenovationInfo.securityRange': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "涉及区域范围不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "涉及区域范围不能超过64"
                        },
                    ],
                    'addGovAreaRenovationInfo.leadCompany': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治牵头单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "整治牵头单位不能超过102"
                        },
                    ],
                    'addGovAreaRenovationInfo.leadParticipate': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治参与单位不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "102",
                            errInfo: "整治参与单位不能超过102"
                        },
                    ],
                    'addGovAreaRenovationInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人姓名不能超过64"
                        },
                    ],
                    'addGovAreaRenovationInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人联系方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "负责人联系方式不能超过11"
                        },
                    ],
                    'addGovAreaRenovationInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整改开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "整改开始时间不能超过时间类型"
                        },
                    ],
                    'addGovAreaRenovationInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整改结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "整改结束时间不能超过时间类型"
                        },
                    ],
                    'addGovAreaRenovationInfo.crackCriminal': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "破获刑事案件数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "破获刑事案件数不能超过11"
                        },
                    ],
                    'addGovAreaRenovationInfo.crackSecurity': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "查处治安案件数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "查处治安案件数不能超过11"
                        },
                    ],
                    'addGovAreaRenovationInfo.leadRamark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "整治情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "整治情况不能超过1024"
                        },
                    ],




                });
            },
            saveGovAreaRenovationInfo: function () {
                if (!vc.component.addGovAreaRenovationValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovAreaRenovationInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovAreaRenovationInfo);
                    $('#addGovAreaRenovationModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govAreaRenovation/saveGovAreaRenovation',
                    JSON.stringify(vc.component.addGovAreaRenovationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovAreaRenovationModel').modal('hide');
                            vc.component.clearAddGovAreaRenovationInfo();
                            vc.emit('govAreaRenovationManage', 'listGovAreaRenovation', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovAreaTypes: function () {
                var param = {
                    params: {
                        caId:vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govAreaType/queryGovAreaType',
                    param,
                    function (json, res) {
                        var _govAreaTypeManageInfo = JSON.parse(json);
                        vc.component.addGovAreaRenovationInfo.govAreaTypes = _govAreaTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovAreaRenovationInfo: function () {
                vc.component.addGovAreaRenovationInfo = {
                    govRenovationId: '',
                    caId: '',
                    typeId: '',
                    govAreaTypes: [],
                    securityKey: '',
                    securityProblem: '',
                    securityRange: '',
                    leadCompany: '',
                    leadParticipate: '',
                    name: '',
                    tel: '',
                    startTime: '',
                    endTime: '',
                    crackCriminal: '',
                    crackSecurity: '',
                    leadRamark: '',

                };
            }
        }
    });

})(window.vc);
