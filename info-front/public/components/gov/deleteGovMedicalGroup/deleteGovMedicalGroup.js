(function(vc,vm){

    vc.extends({
        data:{
            deleteGovMedicalGroupInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovMedicalGroup','openDeleteGovMedicalGroupModal',function(_params){

                vc.component.deleteGovMedicalGroupInfo = _params;
                $('#deleteGovMedicalGroupModel').modal('show');

            });
        },
        methods:{
            deleteGovMedicalGroup:function(){
                vc.component.deleteGovMedicalGroupInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMedicalGroup/deleteGovMedicalGroup',
                    JSON.stringify(vc.component.deleteGovMedicalGroupInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMedicalGroupModel').modal('hide');
                            vc.emit('govMedicalGroupManage','listGovMedicalGroup',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovMedicalGroupModel:function(){
                $('#deleteGovMedicalGroupModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
