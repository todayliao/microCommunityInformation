(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovHealthInfo: {
                healthId: '',
                caId: '',
                name: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovHealth', 'openAddGovHealthModal', function () {
                $('#addGovHealthModel').modal('show');
            });
        },
        methods: {
            addGovHealthValidate() {
                return vc.validate.validate({
                    addGovHealthInfo: vc.component.addGovHealthInfo
                }, {
                    'addGovHealthInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "项目名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "项目名称不能超过64"
                        },
                    ]




                });
            },
            saveGovHealthInfo: function () {
                if (!vc.component.addGovHealthValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovHealthInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovHealthInfo);
                    $('#addGovHealthModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govHealth/saveGovHealth',
                    JSON.stringify(vc.component.addGovHealthInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovHealthModel').modal('hide');
                            vc.component.clearAddGovHealthInfo();
                            vc.emit('govHealthManage', 'listGovHealth', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovHealthInfo: function () {
                vc.component.addGovHealthInfo = {
                    healthId: '',
                    caId: '',
                    name: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
