(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovRoadProtection:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovRoadProtectionInfo:{
                govRoadProtections:[],
                _currentGovRoadProtectionName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovRoadProtection','openChooseGovRoadProtectionModel',function(_param){
                $('#chooseGovRoadProtectionModel').modal('show');
                vc.component._refreshChooseGovRoadProtectionInfo();
                vc.component._loadAllGovRoadProtectionInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovRoadProtectionInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govRoadProtection/queryGovRoadProtection',
                             param,
                             function(json){
                                var _govRoadProtectionInfo = JSON.parse(json);
                                vc.component.chooseGovRoadProtectionInfo.govRoadProtections = _govRoadProtectionInfo.govRoadProtections;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovRoadProtection:function(_govRoadProtection){
                if(_govRoadProtection.hasOwnProperty('name')){
                     _govRoadProtection.govRoadProtectionName = _govRoadProtection.name;
                }
                vc.emit($props.emitChooseGovRoadProtection,'chooseGovRoadProtection',_govRoadProtection);
                vc.emit($props.emitLoadData,'listGovRoadProtectionData',{
                    govRoadProtectionId:_govRoadProtection.govRoadProtectionId
                });
                $('#chooseGovRoadProtectionModel').modal('hide');
            },
            queryGovRoadProtections:function(){
                vc.component._loadAllGovRoadProtectionInfo(1,10,vc.component.chooseGovRoadProtectionInfo._currentGovRoadProtectionName);
            },
            _refreshChooseGovRoadProtectionInfo:function(){
                vc.component.chooseGovRoadProtectionInfo._currentGovRoadProtectionName = "";
            }
        }

    });
})(window.vc);
