(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addPerGovActivitiesInfo: {
                perActivitiesId: '',
                title: '',
                typeCd: '',
                headerImg: '',
                caId: '',
                context: '',
                userId: '',
                userName: '',
                startTime: '',
                endTime: '',
                activityTime: '',
                state: '',
                oldPersons:[],
                statusCd: '',
            }
        },
        _initMethod: function () {
            $that._initSummernoteInfo();
        },
        _initEvent: function () {
            vc.on('addPerGovActivities', 'openAddPerGovActivitiesModal', function () {
                $('#addPerGovActivitiesModel').modal('show');
            });
            vc.on('viewGovOldBirthPerson', 'page_event', function (_param) {
                $that.addPerGovActivitiesInfo.oldPersons = _param;
            });
        },
        methods: {
            addPerGovActivitiesValidate() {
                return vc.validate.validate({
                    addPerGovActivitiesInfo: vc.component.addPerGovActivitiesInfo
                }, {
                    'addPerGovActivitiesInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "记录标题不能超过200"
                        },
                    ],
                    'addPerGovActivitiesInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "记录内容不能超过长文本"
                        },
                    ],

                    'addPerGovActivitiesInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'addPerGovActivitiesInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                    'addPerGovActivitiesInfo.activityTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "活动时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "活动时间不能超过时间类型"
                        },
                    ]
                });
            },
            savePerGovActivitiesInfo: function () {
                if (!vc.component.addPerGovActivitiesValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }
                vc.component.addPerGovActivitiesInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addPerGovActivitiesInfo);
                    $('#addPerGovActivitiesModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/perGovActivities/savePerGovActivities',
                    JSON.stringify(vc.component.addPerGovActivitiesInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addPerGovActivitiesModel').modal('hide');
                            vc.component.clearAddPerGovActivitiesInfo();
                            vc.emit('perGovActivitiesManage', 'listPerGovActivities', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(errInfo);
                    });
            },
            _initSummernoteInfo: function () {
                vc.initDateTime('addStartTime', function (_value) {
                    $that.addPerGovActivitiesInfo.startTime = _value;
                });
                vc.initDateTime('addEndTime', function (_value) {
                    $that.addPerGovActivitiesInfo.endTime = _value;
                });
                vc.initDateTime('addActivityTime', function (_value) {
                    $that.addPerGovActivitiesInfo.activityTime = _value;
                });

                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入公告内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote,files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addPerGovActivitiesInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendAddFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _clearAddPerGovActivitiesInfo: function () {
                $that.clearAddPerGovActivitiesInfo();
                vc.emit('perGovActivitiesManage', 'listPerGovActivities', {});
            },
            clearAddPerGovActivitiesInfo: function () {
                vc.component.addPerGovActivitiesInfo = {
                    perActivitiesId: '',
                    title: '',
                    typeCd: '',
                    headerImg: '',
                    caId: '',
                    context: '',
                    userId: '',
                    userName: '',
                    startTime: '',
                    endTime: '',
                    activityTime: '',
                    state: '',
                    statusCd: '',

                };
            }
        }
    });

})(window.vc);
