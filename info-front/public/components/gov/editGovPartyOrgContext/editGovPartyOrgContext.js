(function (vc, vm) {

    vc.extends({
        data: {
            editGovPartyOrgContextInfo: {
                orgId: '',
                orgId: '',
                context: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovPartyOrgContext', 'openEditGovPartyOrgContextModal', function (_params) {
                vc.component.refreshEditGovPartyOrgContextInfo();
                $('#editGovPartyOrgContextModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPartyOrgContextInfo);
            });
        },
        methods: {
            editGovPartyOrgContextValidate: function () {
                return vc.validate.validate({
                    editGovPartyOrgContextInfo: vc.component.editGovPartyOrgContextInfo
                }, {
                    'editGovPartyOrgContextInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'editGovPartyOrgContextInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "简介不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "简介超长"
                        },
                    ],
                    'editGovPartyOrgContextInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党组织ID不能为空"
                        }]

                });
            },
            editGovPartyOrgContext: function () {
                if (!vc.component.editGovPartyOrgContextValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govPartyOrgContext/updateGovPartyOrgContext',
                    JSON.stringify(vc.component.editGovPartyOrgContextInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPartyOrgContextModel').modal('hide');
                            vc.emit('govPartyOrgContextManage', 'listGovPartyOrgContext', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovPartyOrgContextInfo: function () {
                vc.component.editGovPartyOrgContextInfo = {
                    orgId: '',
                    orgId: '',
                    context: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
