(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovOwnerInfo: {
                govOwnerId: '',
                caId: '',
                roomId: '',
                roomNum: '',
                ownerNum: '',
                ownerName: '',
                ownerType: '',
                idCard: '',
                ownerTel: '',
                ownerAddress: '',
                ramark: '',
                govCommunityAreas: [],
                govRooms: []
            }
        },
        _initMethod: function () {
            vc.component.addGovOwnerInfo.caId = vc.getCurrentCommunity().caId;
            $that._listAddGovCommunityAreas();
            $that._listAddGovRooms(vc.component.addGovOwnerInfo.caId);
        },
        _initEvent: function () {
            vc.on('addGovOwner', 'openAddGovOwnerModal', function () {
                $('#addGovOwnerModel').modal('show');
            });
        },
        methods: {
            addGovOwnerValidate() {
                return vc.validate.validate({
                    addGovOwnerInfo: vc.component.addGovOwnerInfo
                }, {
                    'addGovOwnerInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addGovOwnerInfo.roomId': [
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "房屋编号id超长"
                        },
                    ],
                    'addGovOwnerInfo.roomNum': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "所属房屋超长"
                        },
                    ],
                    'addGovOwnerInfo.ownerNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "户号超长"
                        },
                    ],
                    'addGovOwnerInfo.ownerName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户主不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "户主超长"
                        },
                    ],
                    'addGovOwnerInfo.ownerType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "6",
                            errInfo: "户别超长"
                        },
                    ],
                    'addGovOwnerInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "身份证格式错误"
                        },
                    ],
                    'addGovOwnerInfo.ownerTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户主电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "户主电话太长"
                        },
                    ],
                    'addGovOwnerInfo.ownerAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "户籍地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "户籍地址超长"
                        },
                    ],
                    'addGovOwnerInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovOwnerInfo: function () {
                console.log(vc.component.addGovOwnerInfo);
                if (!vc.component.addGovOwnerValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovOwnerInfo);
                    $('#addGovOwnerModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govOwner/saveGovOwner',
                    JSON.stringify(vc.component.addGovOwnerInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovOwnerModel').modal('hide');
                            vc.component.clearAddGovOwnerInfo();
                            vc.emit('govOwnerManage', 'listGovOwner', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.addGovOwnerInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.addGovOwnerInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.addGovOwnerInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovRooms: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId:_caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govRoom/queryGovRoom',
                    param,
                    function (json, res) {
                        var _govRoomManageInfo = JSON.parse(json);
                        vc.component.addGovOwnerInfo.total = _govRoomManageInfo.total;
                        vc.component.addGovOwnerInfo.records = _govRoomManageInfo.records;
                        vc.component.addGovOwnerInfo.govRooms = _govRoomManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _setAddRoomNums:function(_roomId){
                $that.addGovOwnerInfo.govRooms.forEach(item => {
                    if (item.govRoomId == _roomId) {
                        $that.addGovOwnerInfo.roomNum = item.roomNum;
                    }
                });
            },
            clearAddGovOwnerInfo: function () {
                vc.component.addGovOwnerInfo = {
                    caId: '',
                    roomId: '',
                    roomNum: '',
                    ownerNum: '',
                    ownerName: '',
                    ownerType: '',
                    idCard: '',
                    ownerTel: '',
                    ownerAddress: '',
                    ramark: '',
                    govCommunityAreas: [],
                    govRooms: []
                };
            }
        }
    });

})(window.vc);
