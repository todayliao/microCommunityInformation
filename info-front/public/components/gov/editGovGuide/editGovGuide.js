(function(vc,vm){

    vc.extends({
        data:{
            editGovGuideInfo:{
                wgId:'',
                caId:'',
                guideType:'',
                guideName:'',
                flow:'',
                data:'',
                person:'',
                link:'',
                state:'',
                subscribe:'',

            }
        },
         _initMethod:function(){
            $that._initEditGovGuideInfoFlow();
         },
         _initEvent:function(){
             vc.on('editGovGuide','openEditGovGuideModal',function(_params){
                vc.component.refreshEditGovGuideInfo();
                $('#editGovGuideModel').modal('show');
                vc.copyObject(_params, vc.component.editGovGuideInfo );
                $(".editGovGuideInfoFlow").summernote('code', vc.component.editGovGuideInfo.flow);
            });
        },
        methods:{
            editGovGuideValidate:function(){
                        return vc.validate.validate({
                            editGovGuideInfo:vc.component.editGovGuideInfo
                        },{
'editGovGuideInfo.guideType':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项类型不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"事项类型ID超长"
                        },
                    ],
'editGovGuideInfo.guideName':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"事项名称超长"
                        },
                    ],
'editGovGuideInfo.flow':[
{
                            limit:"required",
                            param:"",
                            errInfo:"办事流程不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"4000",
                            errInfo:"办事流程超长"
                        },
                    ],
'editGovGuideInfo.data':[
{
                            limit:"required",
                            param:"",
                            errInfo:"办事材料不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"4000",
                            errInfo:"办事材料超长"
                        },
                    ],
'editGovGuideInfo.person':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"咨询人名称太长"
                        },
                    ],
'editGovGuideInfo.link':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"咨询电话太长"
                        },
                    ],
'editGovGuideInfo.state':[
{
                            limit:"required",
                            param:"",
                            errInfo:"对外公示不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"对外公示ID超长"
                        },
                    ],
'editGovGuideInfo.subscribe':[
{
                            limit:"required",
                            param:"",
                            errInfo:"是否开启预约不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"上级组织ID超长"
                        },
                    ],
'editGovGuideInfo.wgId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"流程ID不能为空"
                        }]

                        });
             },
            editGovGuide:function(){
                if(!vc.component.editGovGuideValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govWorkGuide/updateGovWorkGuide',
                    JSON.stringify(vc.component.editGovGuideInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovGuideModel').modal('hide');
                             vc.emit('govGuideManage','listGovGuide',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            refreshEditGovGuideInfo:function(){
                vc.component.editGovGuideInfo= {
                  wgId:'',
                    caId:'',
                    guideType:'',
                    guideName:'',
                    flow:'',
                    data:'',
                    person:'',
                    link:'',
                    state:'',
                    subscribe:'',

                }
            },
            _initEditGovGuideInfoFlow: function () {
                let $summernote = $('.editGovGuideInfoFlow').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入办事流程',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendEditFile($summernote, files);
                        },
                        onChange: function (contexts, $editable) {
                            $that.editGovGuideInfo.flow = contexts;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            }
        }
    });

})(window.vc,window.vc.component);
