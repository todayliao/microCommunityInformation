/**
    重特大事件 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMajorEventsInfo:{
                index:0,
                flowComponent:'viewGovMajorEventsInfo',
                govEventsId:'',
caId:'',
eventsName:'',
happenTime:'',
levelCd:'',
victimName:'',
typeId:'',
areaCode:'',
eventsAddress:'',
statusCd:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMajorEventsInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMajorEventsInfo','chooseGovMajorEvents',function(_app){
                vc.copyObject(_app, vc.component.viewGovMajorEventsInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMajorEventsInfo);
            });

            vc.on('viewGovMajorEventsInfo', 'onIndex', function(_index){
                vc.component.viewGovMajorEventsInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMajorEventsInfoModel(){
                vc.emit('chooseGovMajorEvents','openChooseGovMajorEventsModel',{});
            },
            _openAddGovMajorEventsInfoModel(){
                vc.emit('addGovMajorEvents','openAddGovMajorEventsModal',{});
            },
            _loadGovMajorEventsInfoData:function(){

            }
        }
    });

})(window.vc);
