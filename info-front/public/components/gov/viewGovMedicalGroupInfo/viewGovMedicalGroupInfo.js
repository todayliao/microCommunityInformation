/**
    医疗团队 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMedicalGroupInfo:{
                index:0,
                flowComponent:'viewGovMedicalGroupInfo',
                groupId:'',
caId:'',
hospitalId:'',
name:'',
seq:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMedicalGroupInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMedicalGroupInfo','chooseGovMedicalGroup',function(_app){
                vc.copyObject(_app, vc.component.viewGovMedicalGroupInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMedicalGroupInfo);
            });

            vc.on('viewGovMedicalGroupInfo', 'onIndex', function(_index){
                vc.component.viewGovMedicalGroupInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMedicalGroupInfoModel(){
                vc.emit('chooseGovMedicalGroup','openChooseGovMedicalGroupModel',{});
            },
            _openAddGovMedicalGroupInfoModel(){
                vc.emit('addGovMedicalGroup','openAddGovMedicalGroupModal',{});
            },
            _loadGovMedicalGroupInfoData:function(){

            }
        }
    });

})(window.vc);
