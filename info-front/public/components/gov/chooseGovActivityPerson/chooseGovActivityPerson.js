(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovActivityPerson:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovActivityPersonInfo:{
                govActivityPersons:[],
                _currentGovActivityPersonName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovActivityPerson','openChooseGovActivityPersonModel',function(_param){
                $('#chooseGovActivityPersonModel').modal('show');
                vc.component._refreshChooseGovActivityPersonInfo();
                vc.component._loadAllGovActivityPersonInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovActivityPersonInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govActivityPerson.listGovActivityPersons',
                             param,
                             function(json){
                                var _govActivityPersonInfo = JSON.parse(json);
                                vc.component.chooseGovActivityPersonInfo.govActivityPersons = _govActivityPersonInfo.govActivityPersons;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovActivityPerson:function(_govActivityPerson){
                if(_govActivityPerson.hasOwnProperty('name')){
                     _govActivityPerson.govActivityPersonName = _govActivityPerson.name;
                }
                vc.emit($props.emitChooseGovActivityPerson,'chooseGovActivityPerson',_govActivityPerson);
                vc.emit($props.emitLoadData,'listGovActivityPersonData',{
                    govActivityPersonId:_govActivityPerson.govActivityPersonId
                });
                $('#chooseGovActivityPersonModel').modal('hide');
            },
            queryGovActivityPersons:function(){
                vc.component._loadAllGovActivityPersonInfo(1,10,vc.component.chooseGovActivityPersonInfo._currentGovActivityPersonName);
            },
            _refreshChooseGovActivityPersonInfo:function(){
                vc.component.chooseGovActivityPersonInfo._currentGovActivityPersonName = "";
            }
        }

    });
})(window.vc);
