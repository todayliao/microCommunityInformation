(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovPartyOrgContext:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovPartyOrgContextInfo:{
                govPartyOrgContexts:[],
                _currentGovPartyOrgContextName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovPartyOrgContext','openChooseGovPartyOrgContextModel',function(_param){
                $('#chooseGovPartyOrgContextModel').modal('show');
                vc.component._refreshChooseGovPartyOrgContextInfo();
                vc.component._loadAllGovPartyOrgContextInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovPartyOrgContextInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govPartyOrgContext.listGovPartyOrgContexts',
                             param,
                             function(json){
                                var _govPartyOrgContextInfo = JSON.parse(json);
                                vc.component.chooseGovPartyOrgContextInfo.govPartyOrgContexts = _govPartyOrgContextInfo.govPartyOrgContexts;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovPartyOrgContext:function(_govPartyOrgContext){
                if(_govPartyOrgContext.hasOwnProperty('name')){
                     _govPartyOrgContext.govPartyOrgContextName = _govPartyOrgContext.name;
                }
                vc.emit($props.emitChooseGovPartyOrgContext,'chooseGovPartyOrgContext',_govPartyOrgContext);
                vc.emit($props.emitLoadData,'listGovPartyOrgContextData',{
                    govPartyOrgContextId:_govPartyOrgContext.govPartyOrgContextId
                });
                $('#chooseGovPartyOrgContextModel').modal('hide');
            },
            queryGovPartyOrgContexts:function(){
                vc.component._loadAllGovPartyOrgContextInfo(1,10,vc.component.chooseGovPartyOrgContextInfo._currentGovPartyOrgContextName);
            },
            _refreshChooseGovPartyOrgContextInfo:function(){
                vc.component.chooseGovPartyOrgContextInfo._currentGovPartyOrgContextName = "";
            }
        }

    });
})(window.vc);
