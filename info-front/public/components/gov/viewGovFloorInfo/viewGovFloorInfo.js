/**
    建筑物管理 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovFloorInfo: {
                index: 0,
                flowComponent: 'viewGovFloorInfo',
                caId: '',
                govCommunityId: '',
                floorNum: '',
                floorName: '',
                floorType: '',
                floorArea: '',
                layerCount: '',
                unitCount: '',
                floorUse: '',
                personName: '',
                personLink: '',
                floorIcon: '',
                remark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovFloorInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovFloorInfo', 'chooseGovFloor', function (_app) {
                vc.copyObject(_app, vc.component.viewGovFloorInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovFloorInfo);
            });

            vc.on('viewGovFloorInfo', 'onIndex', function (_index) {
                vc.component.viewGovFloorInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovFloorInfoModel() {
                vc.emit('chooseGovFloor', 'openChooseGovFloorModel', {});
            },
            _openAddGovFloorInfoModel() {
                vc.emit('addGovFloor', 'openAddGovFloorModal', {});
            },
            _loadGovFloorInfoData: function () {

            }
        }
    });

})(window.vc);
