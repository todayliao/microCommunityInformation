(function(vc,vm){

    vc.extends({
        data:{
            deleteGovSpecialFollowInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovSpecialFollow','openDeleteGovSpecialFollowModal',function(_params){

                vc.component.deleteGovSpecialFollowInfo = _params;
                $('#deleteGovSpecialFollowModel').modal('show');

            });
        },
        methods:{
            deleteGovSpecialFollow:function(){
                vc.component.deleteGovSpecialFollowInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govSpecialFollow/deleteGovSpecialFollow',
                    JSON.stringify(vc.component.deleteGovSpecialFollowInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovSpecialFollowModel').modal('hide');
                            vc.emit('govSpecialFollowManage','listGovSpecialFollow',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovSpecialFollowModel:function(){
                $('#deleteGovSpecialFollowModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
