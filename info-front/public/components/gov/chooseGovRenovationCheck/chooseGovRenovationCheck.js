(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovRenovationCheck:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovRenovationCheckInfo:{
                govRenovationChecks:[],
                _currentGovRenovationCheckName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovRenovationCheck','openChooseGovRenovationCheckModel',function(_param){
                $('#chooseGovRenovationCheckModel').modal('show');
                vc.component._refreshChooseGovRenovationCheckInfo();
                vc.component._loadAllGovRenovationCheckInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovRenovationCheckInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govRenovationCheck/queryGovRenovationCheck',
                             param,
                             function(json){
                                var _govRenovationCheckInfo = JSON.parse(json);
                                vc.component.chooseGovRenovationCheckInfo.govRenovationChecks = _govRenovationCheckInfo.govRenovationChecks;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovRenovationCheck:function(_govRenovationCheck){
                if(_govRenovationCheck.hasOwnProperty('name')){
                     _govRenovationCheck.govRenovationCheckName = _govRenovationCheck.name;
                }
                vc.emit($props.emitChooseGovRenovationCheck,'chooseGovRenovationCheck',_govRenovationCheck);
                vc.emit($props.emitLoadData,'listGovRenovationCheckData',{
                    govRenovationCheckId:_govRenovationCheck.govRenovationCheckId
                });
                $('#chooseGovRenovationCheckModel').modal('hide');
            },
            queryGovRenovationChecks:function(){
                vc.component._loadAllGovRenovationCheckInfo(1,10,vc.component.chooseGovRenovationCheckInfo._currentGovRenovationCheckName);
            },
            _refreshChooseGovRenovationCheckInfo:function(){
                vc.component.chooseGovRenovationCheckInfo._currentGovRenovationCheckName = "";
            }
        }

    });
})(window.vc);
