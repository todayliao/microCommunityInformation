/**
    老人类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovOldPersonTypeInfo:{
                index:0,
                flowComponent:'viewGovOldPersonTypeInfo',
                typeId:'',
caId:'',
typeName:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovOldPersonTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovOldPersonTypeInfo','chooseGovOldPersonType',function(_app){
                vc.copyObject(_app, vc.component.viewGovOldPersonTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovOldPersonTypeInfo);
            });

            vc.on('viewGovOldPersonTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovOldPersonTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovOldPersonTypeInfoModel(){
                vc.emit('chooseGovOldPersonType','openChooseGovOldPersonTypeModel',{});
            },
            _openAddGovOldPersonTypeInfoModel(){
                vc.emit('addGovOldPersonType','openAddGovOldPersonTypeModal',{});
            },
            _loadGovOldPersonTypeInfoData:function(){

            }
        }
    });

})(window.vc);
