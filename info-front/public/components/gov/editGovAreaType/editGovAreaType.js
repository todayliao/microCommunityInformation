(function (vc, vm) {

    vc.extends({
        data: {
            editGovAreaTypeInfo: {
                typeId: '',
                caId: '',
                name: '',
                isShow: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovAreaType', 'openEditGovAreaTypeModal', function (_params) {
                vc.component.refreshEditGovAreaTypeInfo();
                $('#editGovAreaTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovAreaTypeInfo);
                vc.component.editGovAreaTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovAreaTypeValidate: function () {
                return vc.validate.validate({
                    editGovAreaTypeInfo: vc.component.editGovAreaTypeInfo
                }, {
                    'editGovAreaTypeInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "涉及区域类型ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "涉及区域类型ID不能超过30"
                        },
                    ],
                    'editGovAreaTypeInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域ID不能超过30"
                        },
                    ],
                    'editGovAreaTypeInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "区域类型名称不能超过64"
                        },
                    ],
                    'editGovAreaTypeInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否显示不能超过64"
                        },
                    ],
                    'editGovAreaTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]

                });
            },
            editGovAreaType: function () {
                if (!vc.component.editGovAreaTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govAreaType/updateGovAreaType',
                    JSON.stringify(vc.component.editGovAreaTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovAreaTypeModel').modal('hide');
                            vc.emit('govAreaTypeManage', 'listGovAreaType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovAreaTypeInfo: function () {
                vc.component.editGovAreaTypeInfo = {
                    typeId: '',
                    caId: '',
                    name: '',
                    isShow: '',
                    ramark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
