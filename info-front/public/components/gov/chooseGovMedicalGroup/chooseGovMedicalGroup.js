(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMedicalGroup:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMedicalGroupInfo:{
                govMedicalGroups:[],
                _currentGovMedicalGroupName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMedicalGroup','openChooseGovMedicalGroupModel',function(_param){
                $('#chooseGovMedicalGroupModel').modal('show');
                vc.component._refreshChooseGovMedicalGroupInfo();
                vc.component._loadAllGovMedicalGroupInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMedicalGroupInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMedicalGroup/queryGovMedicalGroup',
                             param,
                             function(json){
                                var _govMedicalGroupInfo = JSON.parse(json);
                                vc.component.chooseGovMedicalGroupInfo.govMedicalGroups = _govMedicalGroupInfo.govMedicalGroups;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMedicalGroup:function(_govMedicalGroup){
                if(_govMedicalGroup.hasOwnProperty('name')){
                     _govMedicalGroup.govMedicalGroupName = _govMedicalGroup.name;
                }
                vc.emit($props.emitChooseGovMedicalGroup,'chooseGovMedicalGroup',_govMedicalGroup);
                vc.emit($props.emitLoadData,'listGovMedicalGroupData',{
                    govMedicalGroupId:_govMedicalGroup.govMedicalGroupId
                });
                $('#chooseGovMedicalGroupModel').modal('hide');
            },
            queryGovMedicalGroups:function(){
                vc.component._loadAllGovMedicalGroupInfo(1,10,vc.component.chooseGovMedicalGroupInfo._currentGovMedicalGroupName);
            },
            _refreshChooseGovMedicalGroupInfo:function(){
                vc.component.chooseGovMedicalGroupInfo._currentGovMedicalGroupName = "";
            }
        }

    });
})(window.vc);
