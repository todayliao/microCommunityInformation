(function(vc,vm){

    vc.extends({
        data:{
            deleteGovParkingAreaInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovParkingArea','openDeleteGovParkingAreaModal',function(_params){

                vc.component.deleteGovParkingAreaInfo = _params;
                $('#deleteGovParkingAreaModel').modal('show');

            });
        },
        methods:{
            deleteGovParkingArea:function(){
                vc.component.deleteGovParkingAreaInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/parkingArea/deleteParkingArea',
                    JSON.stringify(vc.component.deleteGovParkingAreaInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovParkingAreaModel').modal('hide');
                            vc.emit('govParkingAreaManage','listGovParkingArea',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovParkingAreaModel:function(){
                $('#deleteGovParkingAreaModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
