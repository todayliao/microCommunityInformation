(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCompanyInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCompany', 'openDeleteGovCompanyModal', function (_params) {

                vc.component.deleteGovCompanyInfo = _params;
                $('#deleteGovCompanyModel').modal('show');

            });
        },
        methods: {
            deleteGovCompany: function () {
                vc.http.apiPost(
                    '/govCompany/deleteGovCompany',
                    JSON.stringify(vc.component.deleteGovCompanyInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCompanyModel').modal('hide');
                            vc.emit('govCompanyManage', 'listGovCompany', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCompanyModel: function () {
                $('#deleteGovCompanyModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
