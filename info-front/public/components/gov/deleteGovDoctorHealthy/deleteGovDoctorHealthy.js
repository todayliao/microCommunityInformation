(function(vc,vm){

    vc.extends({
        data:{
            deleteGovDoctorHealthyInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovDoctorHealthy','openDeleteGovDoctorHealthyModal',function(_params){

                vc.component.deleteGovDoctorHealthyInfo = _params;
                $('#deleteGovDoctorHealthyModel').modal('show');

            });
        },
        methods:{
            deleteGovDoctorHealthy:function(){
                vc.component.deleteGovDoctorHealthyInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govDoctorHealthy/deleteGovDoctorHealthy',
                    JSON.stringify(vc.component.deleteGovDoctorHealthyInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovDoctorHealthyModel').modal('hide');
                            vc.emit('govDoctorHealthyManage','listGovDoctorHealthy',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovDoctorHealthyModel:function(){
                $('#deleteGovDoctorHealthyModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
