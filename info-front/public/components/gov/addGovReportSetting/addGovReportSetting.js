(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovReportSettingInfo: {
                settingId: '',
                reportTypeName: '',
                reportWay: '',
                caId: '',
                returnVisitFlag: '',
                remark: '',

            }
        },
        _initMethod: function () {
            vc.component.addGovReportSettingInfo.caId = vc.getCurrentCommunity().caId;
        },
        _initEvent: function () {
            vc.on('addGovReportSetting', 'openAddGovReportSettingModal', function () {
                $('#addGovReportSettingModel').modal('show');
            });
        },
        methods: {
            addGovReportSettingValidate() {
                return vc.validate.validate({
                    addGovReportSettingInfo: vc.component.addGovReportSettingInfo
                }, {
                    'addGovReportSettingInfo.reportTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "类型名称超长"
                        },
                    ],
                    'addGovReportSettingInfo.reportWay': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "派单方式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "派单方式超长"
                        },
                    ],
                    'addGovReportSettingInfo.returnVisitFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "回访标识不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "回访标识超长"
                        },
                    ]




                });
            },
            saveGovReportSettingInfo: function () {
                if (!vc.component.addGovReportSettingValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovReportSettingInfo);
                    $('#addGovReportSettingModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govReportSetting/saveGovReportSetting',
                    JSON.stringify(vc.component.addGovReportSettingInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovReportSettingModel').modal('hide');
                            vc.component.clearAddGovReportSettingInfo();
                            vc.emit('govReportSettingManage', 'listGovReportSetting', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovReportSettingInfo: function () {
                vc.component.addGovReportSettingInfo = {
                    reportTypeName: '',
                    reportWay: '',
                    returnVisitFlag: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
