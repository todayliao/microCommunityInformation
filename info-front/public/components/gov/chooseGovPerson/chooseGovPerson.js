(function (vc) {
    vc.extends({
        propTypes: {
            emitChooseGovPerson: vc.propTypes.string,
            emitLoadData: vc.propTypes.string
        },
        data: {
            chooseGovPersonInfo: {
                govPersons: [],
                personNameLike: '',
                _idCard: '',
                _personTel: '',
                _caId: ''
            }
        },
        _initMethod: function () {
        },
        _initEvent: function () {
            vc.on('chooseGovPerson', 'openChooseGovPersonModel', function (_param) {
                $that.chooseGovPersonInfo._caId = _param;
                $('#chooseGovPersonModel').modal('show');
                vc.component._refreshChooseGovPersonInfo();
                vc.component._loadAllGovPersonInfo(1, 10, '','','',$that.chooseGovPersonInfo._caId);
            });
        },
        methods: {
            _loadAllGovPersonInfo: function (_page, _row,_name, _idCard, _tel,_caId) {
                var param = {
                    params: {
                        page: _page,
                        row: _row,
                        personNameLike: _name,
                        caId:_caId,
                        idCard: _idCard,
                        personTel: _tel,
                        caId:_caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPerson/queryGovPerson',
                    param,
                    function (json) {
                        var _govPersonInfo = JSON.parse(json);
                        vc.component.chooseGovPersonInfo.total = _govPersonInfo.total;
                        vc.component.chooseGovPersonInfo.records = _govPersonInfo.records;
                        vc.component.chooseGovPersonInfo.govPersons = _govPersonInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.chooseGovPersonInfo.records,
                            currentPage: _page
                        });
                    }, function () {
                        console.log('请求失败处理');
                    }
                );
            },
            chooseGovPerson: function (_govPerson) {
                vc.emit($props.emitChooseGovPerson, 'chooseGovPerson', _govPerson);
                $('#chooseGovPersonModel').modal('hide');
            },
            queryGovPersons: function () {
                vc.component._loadAllGovPersonInfo(1, 10, vc.component.chooseGovPersonInfo.personNameLike,
                    vc.component.chooseGovPersonInfo._idCard,
                    vc.component.chooseGovPersonInfo._personTel,
                    $that.chooseGovPersonInfo._caId);
            },
            _refreshChooseGovPersonInfo: function () {
                vc.component.chooseGovPersonInfo.personNameLike = "";
                vc.component.chooseGovPersonInfo._idCard = "";
                vc.component.chooseGovPersonInfo._personTel = "";
            }
        }

    });
})(window.vc);
