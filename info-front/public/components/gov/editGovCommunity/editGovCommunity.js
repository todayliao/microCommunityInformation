(function (vc, vm) {

    vc.extends({
        data: {
            editGovCommunityInfo: {
                govCommunityId: '',
                caId: '',
                communityName: '',
                propertyType: '',
                managerName: '',
                personName: '',
                personLink: '',
                communityIcon: '',
                oldCommunityIcon: '',
                communitySecure: '',
                ramark: '',
                topic: '',
                mapX: '',
                mapY: '',
                govCommunityAreas: []

            }
        },
        _initMethod: function () {
            $that._listEditGovCommunityAreas();
           
        },
        _initEvent: function () {
            vc.on('editGovCommunity', 'openEditGovCommunityModal', function (_params) {
                vc.component.refreshEditGovCommunityInfo();
                $('#editGovCommunityModel').modal('show');
                vc.copyObject(_params, vc.component.editGovCommunityInfo);
                let _GovCommunitys = [];
                _GovCommunitys.push(vc.component.editGovCommunityInfo.communityIcon);
                vc.emit('editGovCommunityCover','uploadImage', 'notifyPhotos',_GovCommunitys);
                var map = new AMap.Map("containeredit", {
                    zoom:14, //初始化地图层级
                    center: [vc.component.editGovCommunityInfo.mapX, vc.component.editGovCommunityInfo.mapY],
                    resizeEnable: true
                });
                 // 同时引入工具条插件，比例尺插件和鹰眼插件
                 AMap.plugin([
                    'AMap.ToolBar',
                    'AMap.Geolocation',
                ], function(){
                    // 在图面添加工具条控件，工具条控件集成了缩放、平移、定位等功能按钮在内的组合控件
                    map.addControl(new AMap.ToolBar());
                    // 在图面添加定位控件，用来获取和展示用户主机所在的经纬度位置
                    map.addControl(new AMap.Geolocation());
                });
                //为地图注册click事件获取鼠标点击出的经纬度坐标
                map.on('click', function(e) {
                    vc.message("成功获取坐标。");
                    vc.component.editGovCommunityInfo.mapX = e.lnglat.getLng() 
                    vc.component.editGovCommunityInfo.mapY = e.lnglat.getLat()
                });
            });
            vc.on("editGovCommunity", "notifyUploadCoverImage", function (_param) {
                if(_param.length > 0){
                    vc.component.editGovCommunityInfo.communityIcon = _param[0];
                }else{
                    vc.component.editGovCommunityInfo.communityIcon = '';
                }
                
            });
        },
        methods: {
            editGovCommunityValidate: function () {
                return vc.validate.validate({
                    editGovCommunityInfo: vc.component.editGovCommunityInfo
                }, {
                    'editGovCommunityInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "建筑分类名称超长"
                        },
                    ],
                    'editGovCommunityInfo.communityName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "小区名称超长"
                        },
                    ],
                    'editGovCommunityInfo.propertyType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "物业状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "物业状态超长"
                        },
                    ],
                    'editGovCommunityInfo.managerName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "管理组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "管理组织名称超长"
                        },
                    ],
                    'editGovCommunityInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'editGovCommunityInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "30",
                            errInfo: "联系电话格式错误"
                        },
                    ],
                    'editGovCommunityInfo.communityIcon': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区图片不能为空"
                        }
                    ],
                    'editGovCommunityInfo.communitySecure': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区秘钥不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区秘钥超长"
                        },
                    ],
                    'editGovCommunityInfo.mapX': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "坐标X位置不能为空"
                        }
                    ],
                    'editGovCommunityInfo.mapY': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "坐标Y位置不能为空"
                        }
                    ],
                    'editGovCommunityInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],
                    'editGovCommunityInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区信息ID不能为空"
                        }],
                    'editGovCommunityInfo.topic': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区主题不能为空"
                        }]

                });
            },
            editGovCommunity: function () {
                if (!vc.component.editGovCommunityValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCommunity/updateGovCommunity',
                    JSON.stringify(vc.component.editGovCommunityInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovCommunityModel').modal('hide');
                            vc.emit('govCommunityManage', 'listGovCommunity', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.editGovCommunityInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.editGovCommunityInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.editGovCommunityInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovCommunityInfo: function () {
                let _govCommunityAreas = $that.editGovCommunityInfo.govCommunityAreas;
                vc.component.editGovCommunityInfo = {
                    govCommunityId: '',
                    caId: '',
                    communityName: '',
                    propertyType: '',
                    managerName: '',
                    personName: '',
                    personLink: '',
                    communityIcon: '',
                    oldCommunityIcon: '',
                    communitySecure: '',
                    ramark: '',
                    topic: '',
                    mapX: '',
                    mapY: '',
                    govCommunityAreas: _govCommunityAreas
                }
            }
        }
    });

})(window.vc, window.vc.component);
