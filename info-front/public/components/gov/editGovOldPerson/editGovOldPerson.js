(function (vc, vm) {

    vc.extends({
        data: {
            editGovOldPersonInfo: {
                oldId: '',
                caId: '',
                govCommunityId: '',
                govPersonId: '',
                personName: '',
                personTel: '',
                personAge: '',
                contactPerson: '',
                contactTel: '',
                servPerson: '',
                servTel: '',
                birthday: '',
                ramark: '',
                idCard: '',
                personSex: '',
                nation: '',
                maritalStatus: 'Y',
                timeAmount: '0',
                typeId: '',
                govCommunitys: [],
                govOldPersonTypes: []
            }
        },
        _initMethod: function () {
            vc.initDate('editBirthday', function (_value) {
                $that.editGovOldPersonInfo.birthday = _value;
            });
            $that._editGovCommunitys(vc.getCurrentCommunity().caId);
            $that._editGovOldPersonTypes();
        },
        _initEvent: function () {
            vc.on('editGovOldPerson', 'openEditGovOldPersonModal', function (_params) {
                vc.component.refreshEditGovOldPersonInfo();
                $('#editGovOldPersonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovOldPersonInfo);
                vc.component.editGovOldPersonInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovOldPersonValidate: function () {
                return vc.validate.validate({
                    editGovOldPersonInfo: vc.component.editGovOldPersonInfo
                }, {

                    'editGovOldPersonInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区ID不能超过30"
                        },
                    ],
                    'editGovOldPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.personAge': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "年龄不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "年龄不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.contactPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "家人联系人不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.contactTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "家人联系电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.servPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "服务人不能超过64"
                        },
                    ],
                    'editGovOldPersonInfo.servTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "服务人电话不能超过11"
                        },
                    ],
                    'editGovOldPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证号码不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'editGovOldPersonInfo.timeAmount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "初始时间不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "初始时间格式错误"
                        },
                    ]


                });
            },
            _editGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovOldPersonInfo.total = _govCommunityManageInfo.total;
                        vc.component.editGovOldPersonInfo.records = _govCommunityManageInfo.records;
                        vc.component.editGovOldPersonInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _editGovOldPersonTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.editGovOldPersonInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.editGovOldPersonInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.editGovOldPersonInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            editGovOldPerson: function () {
                if (!vc.component.editGovOldPersonValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govOldPerson/updateGovOldPerson',
                    JSON.stringify(vc.component.editGovOldPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovOldPersonModel').modal('hide');
                            vc.emit('govOldPersonManage', 'listGovOldPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovOldPersonInfo: function () {
                let _govCommunitys = vc.component.editGovOldPersonInfo.govCommunitys;
                let _govOldPersonTypes = vc.component.editGovOldPersonInfo.govOldPersonTypes;
                vc.component.editGovOldPersonInfo = {
                    oldId: '',
                    caId: '',
                    govCommunityId: '',
                    govPersonId: '',
                    personName: '',
                    personTel: '',
                    personAge: '',
                    contactPerson: '',
                    contactTel: '',
                    servPerson: '',
                    servTel: '',
                    ramark: '',
                    idCard: '',
                    personSex: '',
                    nation: '',
                    maritalStatus: 'Y',
                    timeAmount: '0',
                    typeId: '',
                    govCommunitys: _govCommunitys,
                    govOldPersonTypes: _govOldPersonTypes
                }
            }
        }
    });

})(window.vc, window.vc.component);
