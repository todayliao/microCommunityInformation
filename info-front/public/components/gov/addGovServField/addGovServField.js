(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovServFieldInfo: {
                servId: '',
                caId: '',
                name: '',
                isShow: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovServField', 'openAddGovServFieldModal', function () {
                $('#addGovServFieldModel').modal('show');
            });
        },
        methods: {
            addGovServFieldValidate() {
                return vc.validate.validate({
                    addGovServFieldInfo: vc.component.addGovServFieldInfo
                }, {
                    'addGovServFieldInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "领域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "领域名称不能超过64"
                        },
                    ],
                    'addGovServFieldInfo.isShow': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否显示不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "是否显示不能超过64"
                        },
                    ]



                });
            },
            saveGovServFieldInfo: function () {
                if (!vc.component.addGovServFieldValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovServFieldInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovServFieldInfo);
                    $('#addGovServFieldModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govServField/saveGovServField',
                    JSON.stringify(vc.component.addGovServFieldInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovServFieldModel').modal('hide');
                            vc.component.clearAddGovServFieldInfo();
                            vc.emit('govServFieldManage', 'listGovServField', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovServFieldInfo: function () {
                vc.component.addGovServFieldInfo = {
                    servId: '',
                    caId: '',
                    name: '',
                    isShow: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
