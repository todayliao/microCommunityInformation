(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovGrid:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovGridInfo:{
                govGrids:[],
                _currentGovGridName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovGrid','openChooseGovGridModel',function(_param){
                $('#chooseGovGridModel').modal('show');
                vc.component._refreshChooseGovGridInfo();
                vc.component._loadAllGovGridInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovGridInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govGrid.listGovGrids',
                             param,
                             function(json){
                                var _govGridInfo = JSON.parse(json);
                                vc.component.chooseGovGridInfo.govGrids = _govGridInfo.govGrids;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovGrid:function(_govGrid){
                if(_govGrid.hasOwnProperty('name')){
                     _govGrid.govGridName = _govGrid.name;
                }
                vc.emit($props.emitChooseGovGrid,'chooseGovGrid',_govGrid);
                vc.emit($props.emitLoadData,'listGovGridData',{
                    govGridId:_govGrid.govGridId
                });
                $('#chooseGovGridModel').modal('hide');
            },
            queryGovGrids:function(){
                vc.component._loadAllGovGridInfo(1,10,vc.component.chooseGovGridInfo._currentGovGridName);
            },
            _refreshChooseGovGridInfo:function(){
                vc.component.chooseGovGridInfo._currentGovGridName = "";
            }
        }

    });
})(window.vc);
