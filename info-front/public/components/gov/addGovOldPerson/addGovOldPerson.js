(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovOldPersonInfo: {
                oldId: '',
                caId: '',
                govCommunityId: '',
                personName: '',
                personTel: '',
                personAge: '',
                contactPerson: '',
                contactTel: '',
                servPerson: '',
                servTel: '',
                ramark: '',
                idCard: '',
                birthday: '',
                personSex: '',
                nation: '',
                maritalStatus: 'Y',
                timeAmount: '0',
                typeId: '',
                govCommunitys: [],
                govOldPersonTypes: []

            }
        },
        _initMethod: function () {
            vc.initDate('addBirthday', function (_value) {
                $that.addGovOldPersonInfo.birthday = _value;
            });
            $that._addGovCommunitys(vc.getCurrentCommunity().caId);
            $that._addGovOldPersonTypes();
        },
        _initEvent: function () {
            vc.on('addGovOldPerson', 'openAddGovOldPersonModal', function () {
                $('#addGovOldPersonModel').modal('show');
            });
        },
        methods: {
            addGovOldPersonValidate() {
                return vc.validate.validate({
                    addGovOldPersonInfo: vc.component.addGovOldPersonInfo
                }, {
                    'addGovOldPersonInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "小区不能超过30"
                        },
                    ],
                    'addGovOldPersonInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证号码不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.birthday': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "生日不能为空"
                        },
                        {
                            limit: "date",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'addGovOldPersonInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'addGovOldPersonInfo.personAge': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "年龄不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "年龄不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.contactPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "家人联系人不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.contactTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "家人联系电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "家人联系电话不能超过11"
                        },
                    ],
                    'addGovOldPersonInfo.servPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "服务人不能超过64"
                        },
                    ],
                    'addGovOldPersonInfo.servTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服务人电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "服务人电话不能超过11"
                        },
                    ]
                    ,
                    'addGovOldPersonInfo.timeAmount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "初始时间不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "初始时间格式错误"
                        },
                    ]



                });
            },
            saveGovOldPersonInfo: function () {
                if (!vc.component.addGovOldPersonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovOldPersonInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovOldPersonInfo);
                    $('#addGovOldPersonModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govOldPerson/saveGovOldPerson',
                    JSON.stringify(vc.component.addGovOldPersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovOldPersonModel').modal('hide');
                            vc.component.clearAddGovOldPersonInfo();
                            vc.emit('govOldPersonManage', 'listGovOldPerson', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _addGovOldPersonTypes: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                    param,
                    function (json, res) {
                        var _govOldPersonTypeManageInfo = JSON.parse(json);
                        vc.component.addGovOldPersonInfo.total = _govOldPersonTypeManageInfo.total;
                        vc.component.addGovOldPersonInfo.records = _govOldPersonTypeManageInfo.records;
                        vc.component.addGovOldPersonInfo.govOldPersonTypes = _govOldPersonTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _addGovCommunitys: function (_caId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: _caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovOldPersonInfo.total = _govCommunityManageInfo.total;
                        vc.component.addGovOldPersonInfo.records = _govCommunityManageInfo.records;
                        vc.component.addGovOldPersonInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovOldPersonInfo: function () {
                vc.component.addGovOldPersonInfo = {
                    oldId: '',
                    caId: '',
                    govCommunityId: '',
                    personName: '',
                    personTel: '',
                    personAge: '',
                    birthday: '',
                    contactPerson: '',
                    contactTel: '',
                    servPerson: '',
                    servTel: '',
                    ramark: '',
                    idCard: '',
                    personSex: '',
                    nation: '',
                    maritalStatus: 'Y',
                    timeAmount: '0',
                    typeId: '',
                    govCommunitys: [],
                    govOldPersonTypes: []


                };
            }
        }
    });

})(window.vc);
