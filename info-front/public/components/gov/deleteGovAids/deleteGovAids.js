(function(vc,vm){

    vc.extends({
        data:{
            deleteGovAidsInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovAids','openDeleteGovAidsModal',function(_params){

                vc.component.deleteGovAidsInfo = _params;
                $('#deleteGovAidsModel').modal('show');

            });
        },
        methods:{
            deleteGovAids:function(){
                vc.component.deleteGovAidsInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govAids/deleteGovAids',
                    JSON.stringify(vc.component.deleteGovAidsInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovAidsModel').modal('hide');
                            vc.emit('govAidsManage','listGovAids',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovAidsModel:function(){
                $('#deleteGovAidsModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
