(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovMeetingListInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovMeetingList', 'openDeleteGovMeetingListModal', function (_params) {
               
                vc.component.deleteGovMeetingListInfo = _params;
                $('#deleteGovMeetingListModel').modal('show');

            });
        },
        methods: {
            deleteGovMeetingList: function () {
                vc.component.deleteGovMeetingListInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMeetingList/deleteGovMeetingList',
                    JSON.stringify(vc.component.deleteGovMeetingListInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMeetingListModel').modal('hide');
                            vc.emit('govMeetingListManage', 'listGovMeetingList', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovMeetingListModel: function () {
                $('#deleteGovMeetingListModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
