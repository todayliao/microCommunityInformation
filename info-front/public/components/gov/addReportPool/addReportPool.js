(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addReportPoolInfo: {
                reportId: '',
                caId: '',
                reportType: '',
                reportName: '',
                tel: '',
                context: '',
                appointmentTime: '',
                govReportSettings:[]

            }
        },
        _initMethod: function () {
            vc.component.addReportPoolInfo.caId = vc.getCurrentCommunity().caId;
            $that._addGovReportSettings();
            vc.initDateTime('addAppointmentTime', function (_value) {
                $that.addReportPoolInfo.appointmentTime = _value;
            });

        },
        _initEvent: function () {
            vc.on('addReportPool', 'openAddReportPoolModal', function () {
                $('#addReportPoolModel').modal('show');
            });
        },
        methods: {
            addReportPoolValidate() {
                return vc.validate.validate({
                    addReportPoolInfo: vc.component.addReportPoolInfo
                }, {
                    'addReportPoolInfo.caId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "所属区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "所属区域超长"
                        },
                    ],
                    'addReportPoolInfo.reportType': [
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "所报事类型超长"
                        },
                    ],
                    'addReportPoolInfo.reportName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "户号超长"
                        },
                    ],
                    'addReportPoolInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "13",
                            errInfo: "手机号超长"
                        },
                    ],
                    'addReportPoolInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "报事内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2000",
                            errInfo: "报事内容超长"
                        },
                    ],
                    'addReportPoolInfo.appointmentTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "预约时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveReportPoolInfo: function () {
                if (!vc.component.addReportPoolValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addReportPoolInfo);
                    $('#addReportPoolModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/reportPool/saveTelReportPool',
                    JSON.stringify(vc.component.addReportPoolInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addReportPoolModel').modal('hide');
                            vc.component.clearAddReportPoolInfo();
                            vc.emit('reportPoolManage', 'listReportPool', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _addGovReportSettings: function () {


                var param = {
                    params: {
                        page:1,
                        row: 50,
                        caId: vc.component.addReportPoolInfo.caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govReportSetting/queryGovReportSetting',
                    param,
                    function (json, res) {
                        var _govReportSettingManageInfo = JSON.parse(json);
                        vc.component.addReportPoolInfo.total = _govReportSettingManageInfo.total;
                        vc.component.addReportPoolInfo.records = _govReportSettingManageInfo.records;
                        vc.component.addReportPoolInfo.govReportSettings = _govReportSettingManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddReportPoolInfo: function () {
                let _govReportSettings = vc.component.addReportPoolInfo.govReportSettings;
                vc.component.addReportPoolInfo = {
                    caId: '',
                    reportType: '',
                    reportName: '',
                    tel: '',
                    context: '',
                    appointmentTime: '',
                    govReportSettings:_govReportSettings
                };
            }
        }
    });

})(window.vc);
