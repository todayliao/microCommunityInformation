(function(vc,vm){

    vc.extends({
        data:{
            deleteGovGuideInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovGuide','openDeleteGovGuideModal',function(_params){

                vc.component.deleteGovGuideInfo = _params;
                $('#deleteGovGuideModel').modal('show');

            });
        },
        methods:{
            deleteGovGuide:function(){
                vc.component.deleteGovGuideInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govWorkGuide/deleteGovWorkGuide',
                    JSON.stringify(vc.component.deleteGovGuideInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovGuideModel').modal('hide');
                            vc.emit('govGuideManage','listGovGuide',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovGuideModel:function(){
                $('#deleteGovGuideModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
