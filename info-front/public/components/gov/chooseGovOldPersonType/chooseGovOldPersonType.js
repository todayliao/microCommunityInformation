(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovOldPersonType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovOldPersonTypeInfo:{
                govOldPersonTypes:[],
                _currentGovOldPersonTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovOldPersonType','openChooseGovOldPersonTypeModel',function(_param){
                $('#chooseGovOldPersonTypeModel').modal('show');
                vc.component._refreshChooseGovOldPersonTypeInfo();
                vc.component._loadAllGovOldPersonTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovOldPersonTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govOldPersonType/queryGovOldPersonType',
                             param,
                             function(json){
                                var _govOldPersonTypeInfo = JSON.parse(json);
                                vc.component.chooseGovOldPersonTypeInfo.govOldPersonTypes = _govOldPersonTypeInfo.govOldPersonTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovOldPersonType:function(_govOldPersonType){
                if(_govOldPersonType.hasOwnProperty('name')){
                     _govOldPersonType.govOldPersonTypeName = _govOldPersonType.name;
                }
                vc.emit($props.emitChooseGovOldPersonType,'chooseGovOldPersonType',_govOldPersonType);
                vc.emit($props.emitLoadData,'listGovOldPersonTypeData',{
                    govOldPersonTypeId:_govOldPersonType.govOldPersonTypeId
                });
                $('#chooseGovOldPersonTypeModel').modal('hide');
            },
            queryGovOldPersonTypes:function(){
                vc.component._loadAllGovOldPersonTypeInfo(1,10,vc.component.chooseGovOldPersonTypeInfo._currentGovOldPersonTypeName);
            },
            _refreshChooseGovOldPersonTypeInfo:function(){
                vc.component.chooseGovOldPersonTypeInfo._currentGovOldPersonTypeName = "";
            }
        }

    });
})(window.vc);
