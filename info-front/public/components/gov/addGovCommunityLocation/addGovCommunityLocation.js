(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCommunityLocationInfo: {
                locationId: '',
                govCommunityId: '',
                name: '',
                govCommunitys: [],
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCommunityLocation', 'openAddGovCommunityLocationModal', function () {
                $that.clearAddGovCommunityLocationInfo();
                $that._listAddFloorGovCommunitys();
                $('#addGovCommunityLocationModel').modal('show');
            });
        },
        methods: {
            addGovCommunityLocationValidate() {
                return vc.validate.validate({
                    addGovCommunityLocationInfo: vc.component.addGovCommunityLocationInfo
                }, {
                    'addGovCommunityLocationInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'addGovCommunityLocationInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "位置名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "位置名称超长"
                        },
                    ],




                });
            },
            saveGovCommunityLocationInfo: function () {
                if (!vc.component.addGovCommunityLocationValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovCommunityLocationInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCommunityLocationInfo);
                    $('#addGovCommunityLocationModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govCommunityLocation/saveGovCommunityLocation',
                    JSON.stringify(vc.component.addGovCommunityLocationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCommunityLocationModel').modal('hide');
                            vc.component.clearAddGovCommunityLocationInfo();
                            vc.emit('govCommunityLocationManage', 'listGovCommunityLocation', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovCommunityLocationInfo: function () {
                vc.component.addGovCommunityLocationInfo = {
                    govCommunityId: '',
                    name: '',
                    govCommunitys: [],
                };
            },
            _listAddFloorGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovCommunityLocationInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
        }
    });

})(window.vc);
