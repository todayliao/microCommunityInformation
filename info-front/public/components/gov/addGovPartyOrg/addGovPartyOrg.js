(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovPartyOrgInfo: {
                orgId: '',
                orgName: '',
                preOrgId: '-1',
                preOrgName: '无',
                orgCode: '',
                orgSimpleName: '',
                personName: '',
                personTel: '',
                address: '',
                orgLevel: '',
                orgFlag: '',
                context: '',
                communityOrArea: '',
                ramark: '',
                listGovPartyOrgs: [],
                communityOrAreas: []

            }
        },
        _initMethod: function () {
            $that._listAddGovPartyOrgs();
            $that._initAddGovPartyOrg();
        },
        _initEvent: function () {
            vc.on('addGovPartyOrg', 'openAddGovPartyOrgModal', function () {
                $('#addGovPartyOrgModel').modal('show');
            });
        },
        methods: {
            addGovPartyOrgValidate() {
                return vc.validate.validate({
                    addGovPartyOrgInfo: vc.component.addGovPartyOrgInfo
                }, {
                    'addGovPartyOrgInfo.orgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "组织名称超长"
                        },
                    ],
                    'addGovPartyOrgInfo.preOrgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "上级组织不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "上级组织ID超长"
                        },
                    ],
                    'addGovPartyOrgInfo.preOrgName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "上级组织名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "上级组织名称超长"
                        },
                    ],
                    'addGovPartyOrgInfo.orgCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "组织编号超长"
                        },
                    ],
                    'addGovPartyOrgInfo.orgSimpleName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "名称简称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "32",
                            errInfo: "名称简称超长"
                        },
                    ],
                    'addGovPartyOrgInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "负责人超长"
                        },
                    ],
                    'addGovPartyOrgInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "负责电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "负责电话格式错误"
                        },
                    ],
                    'addGovPartyOrgInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "地址超长"
                        },
                    ],
                    'addGovPartyOrgInfo.orgLevel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党组织级别不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "党组织级别超长"
                        },
                    ],
                    'addGovPartyOrgInfo.orgFlag': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否建立党组织不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "是否建立党组织超长"
                        },
                    ],
                    'addGovPartyOrgInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注太长"
                        },
                    ],
                    'addGovPartyOrgInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "党组织简介不能为空"
                        }
                    ],




                });
            },
            saveGovPartyOrgInfo: function () {
                if (!vc.component.addGovPartyOrgValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovPartyOrgInfo);
                    $('#addGovPartyOrgModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govPartyOrg/saveGovPartyOrg',
                    JSON.stringify(vc.component.addGovPartyOrgInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovPartyOrgModel').modal('hide');
                            vc.component.clearAddGovPartyOrgInfo();
                            vc.emit('govPartyOrgManage', 'listGovPartyOrg', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _closeAddGovPartyOrgModel: function () {
                $that.clearAddGovPartyOrgInfo();
                vc.emit('govPartyOrgManage', 'listGovPartyOrg', {});
            },
            _listAddGovPartyOrgs: function () {
                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        preOrgId: '-1'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.addGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.addGovPartyOrgInfo.listGovPartyOrgs = _govPartyOrgManageInfo.data;
                      
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddCommunityOrArea: function(_orgLevel){
                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                if('1001' == _orgLevel){
                    //发送get请求
                    vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.addGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.addGovPartyOrgInfo.communityOrAreas = _govPartyOrgManageInfo.data;
                    
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                    );
                    $that.addGovPartyOrgInfo.preOrgId='-1';
                    $that.addGovPartyOrgInfo.preOrgName='无';
                }else if('2002' == _orgLevel){
                    //发送get请求
                    vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovPartyOrgInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.addGovPartyOrgInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.addGovPartyOrgInfo.communityOrAreas = _govPartyOrgManageInfo.data;
                    
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                    );
                }

                console.log($that.addGovPartyOrgInfo.communityOrArea);
            },
            _setAddPreOrgName:function(_preOrgId){
                $that.addGovPartyOrgInfo.listGovPartyOrgs.forEach(item => {
                    if (item.orgId == _preOrgId) {
                        $that.addGovPartyOrgInfo.preOrgName = item.orgName;
                    }
                });
            },
            _initAddGovPartyOrg: function () {
                let $summernote = $('.addPartOrgSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入党组织简介',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote, files[0]);
                        },
                        onChange: function (contexts, $editable) {
                            $that.addGovPartyOrgInfo.context = contexts;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendAddFile: function ($summernote,files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            clearAddGovPartyOrgInfo: function () {
                vc.component.addGovPartyOrgInfo = {
                    orgName: '',
                    preOrgId: '',
                    preOrgName: '',
                    orgCode: '',
                    orgSimpleName: '',
                    personName: '',
                    personTel: '',
                    address: '',
                    orgLevel: '',
                    orgFlag: '',
                    context: '',
                    communityOrArea: '',
                    ramark: '',
                    listGovPartyOrgs: [],
                    communityOrAreas: []

                };
            }
        }
    });

})(window.vc);
