/**
    医疗分级 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMedicalGradeInfo:{
                index:0,
                flowComponent:'viewGovMedicalGradeInfo',
                medicalGradeId:'',
medicalClassifyId:'',
caId:'',
gradeName:'',
gradeType:'',
seq:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMedicalGradeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMedicalGradeInfo','chooseGovMedicalGrade',function(_app){
                vc.copyObject(_app, vc.component.viewGovMedicalGradeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMedicalGradeInfo);
            });

            vc.on('viewGovMedicalGradeInfo', 'onIndex', function(_index){
                vc.component.viewGovMedicalGradeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMedicalGradeInfoModel(){
                vc.emit('chooseGovMedicalGrade','openChooseGovMedicalGradeModel',{});
            },
            _openAddGovMedicalGradeInfoModel(){
                vc.emit('addGovMedicalGrade','openAddGovMedicalGradeModal',{});
            },
            _loadGovMedicalGradeInfoData:function(){

            }
        }
    });

})(window.vc);
