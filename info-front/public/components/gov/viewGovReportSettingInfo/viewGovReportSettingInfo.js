/**
    报事设置 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovReportSettingInfo:{
                index:0,
                flowComponent:'viewGovReportSettingInfo',
                reportTypeName:'',
reportWay:'',
returnVisitFlag:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovReportSettingInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovReportSettingInfo','chooseGovReportSetting',function(_app){
                vc.copyObject(_app, vc.component.viewGovReportSettingInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovReportSettingInfo);
            });

            vc.on('viewGovReportSettingInfo', 'onIndex', function(_index){
                vc.component.viewGovReportSettingInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovReportSettingInfoModel(){
                vc.emit('chooseGovReportSetting','openChooseGovReportSettingModel',{});
            },
            _openAddGovReportSettingInfoModel(){
                vc.emit('addGovReportSetting','openAddGovReportSettingModal',{});
            },
            _loadGovReportSettingInfoData:function(){

            }
        }
    });

})(window.vc);
