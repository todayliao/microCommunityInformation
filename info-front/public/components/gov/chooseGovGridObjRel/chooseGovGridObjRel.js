(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovGridObjRel:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovGridObjRelInfo:{
                govGridObjRels:[],
                _currentGovGridObjRelName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovGridObjRel','openChooseGovGridObjRelModel',function(_param){
                $('#chooseGovGridObjRelModel').modal('show');
                vc.component._refreshChooseGovGridObjRelInfo();
                vc.component._loadAllGovGridObjRelInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovGridObjRelInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govGridObjRel/queryGovGridObjRel',
                             param,
                             function(json){
                                var _govGridObjRelInfo = JSON.parse(json);
                                vc.component.chooseGovGridObjRelInfo.govGridObjRels = _govGridObjRelInfo.govGridObjRels;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovGridObjRel:function(_govGridObjRel){
                if(_govGridObjRel.hasOwnProperty('name')){
                     _govGridObjRel.govGridObjRelName = _govGridObjRel.name;
                }
                vc.emit($props.emitChooseGovGridObjRel,'chooseGovGridObjRel',_govGridObjRel);
                vc.emit($props.emitLoadData,'listGovGridObjRelData',{
                    govGridObjRelId:_govGridObjRel.govGridObjRelId
                });
                $('#chooseGovGridObjRelModel').modal('hide');
            },
            queryGovGridObjRels:function(){
                vc.component._loadAllGovGridObjRelInfo(1,10,vc.component.chooseGovGridObjRelInfo._currentGovGridObjRelName);
            },
            _refreshChooseGovGridObjRelInfo:function(){
                vc.component.chooseGovGridObjRelInfo._currentGovGridObjRelName = "";
            }
        }

    });
})(window.vc);
