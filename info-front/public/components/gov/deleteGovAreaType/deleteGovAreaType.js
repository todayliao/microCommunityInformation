(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovAreaTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovAreaType', 'openDeleteGovAreaTypeModal', function (_params) {

                vc.component.deleteGovAreaTypeInfo = _params;
                $('#deleteGovAreaTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovAreaType: function () {
                vc.component.deleteGovAreaTypeInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govAreaType/deleteGovAreaType',
                    JSON.stringify(vc.component.deleteGovAreaTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovAreaTypeModel').modal('hide');
                            vc.emit('govAreaTypeManage', 'listGovAreaType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovAreaTypeModel: function () {
                $('#deleteGovAreaTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
