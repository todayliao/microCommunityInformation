(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovServFieldInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovServField', 'openDeleteGovServFieldModal', function (_params) {

                vc.component.deleteGovServFieldInfo = _params;
                $('#deleteGovServFieldModel').modal('show');

            });
        },
        methods: {
            deleteGovServField: function () {
                vc.component.deleteGovServFieldInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govServField/deleteGovServField',
                    JSON.stringify(vc.component.deleteGovServFieldInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovServFieldModel').modal('hide');
                            vc.emit('govServFieldManage', 'listGovServField', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovServFieldModel: function () {
                $('#deleteGovServFieldModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
