/**
    活动类型管理 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovActivityTypeInfo:{
                index:0,
                flowComponent:'viewGovActivityTypeInfo',
                caId:'',
typeName:'',
ramark:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovActivityTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovActivityTypeInfo','chooseGovActivityType',function(_app){
                vc.copyObject(_app, vc.component.viewGovActivityTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovActivityTypeInfo);
            });

            vc.on('viewGovActivityTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovActivityTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovActivityTypeInfoModel(){
                vc.emit('chooseGovActivityType','openChooseGovActivityTypeModel',{});
            },
            _openAddGovActivityTypeInfoModel(){
                vc.emit('addGovActivityType','openAddGovActivityTypeModal',{});
            },
            _loadGovActivityTypeInfoData:function(){

            }
        }
    });

})(window.vc);
