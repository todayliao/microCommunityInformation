(function (vc, vm) {
    vc.extends({
        data: {
            editAdvertInfo: {
                caId: '',
                advertId: '',
                adName: '',
                adTypeCd: '',
                state: '',
                seq: '',
                advertType: '',
                pageUrl: '',
                startTime: '',
                endTime: '',
                photos: [],
                viewType: '',
                vedioName: ''
            }
        },
        _initMethod: function () {
            vc.component._initEditAdvertDateInfo();
        },
        _initEvent: function () {
            vc.on('editAdvert', 'openEditAdvertModal', function (_params) {
                vc.component.refreshEditAdvertInfo();
                $('#editAdvertModel').modal('show');
                vc.copyObject(_params, vc.component.editAdvertInfo);
                vc.component._loadAdvertItem();
                //查询 广告属性
            });

            vc.on("editAdvert", "notifyUploadImage", function (_param) {
                vc.component.editAdvertInfo.photos = _param;
            });
            vc.on("editAdvert", "notifyUploadVedio", function (_param) {
                vc.component.editAdvertInfo.vedioName = _param.realFileName;
            });
        },
        methods: {
            _initEditAdvertDateInfo: function () {
                vc.component.editAdvertInfo.startTime = vc.dateTimeFormat(new Date().getTime());
                $('.editAdvertStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.editAdvertStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".editAdvertStartTime").val();
                        vc.component.editAdvertInfo.startTime = value;
                    });
                $('.editAdvertEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.editAdvertEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".editAdvertEndTime").val();
                        vc.component.editAdvertInfo.endTime = value;
                    });
            },
            editAdvertValidate: function () {
                return vc.validate.validate({
                    editAdvertInfo: vc.component.editAdvertInfo
                }, {
                    'editAdvertInfo.adName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "广告名称不能为空"
                        },
                        {
                            limit: "maxin",
                            param: "1,200",
                            errInfo: "广告名称不能超过200位"
                        },
                    ],
                    'editAdvertInfo.adTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "广告类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "广告类型不能为空"
                        },
                    ],
                    'editAdvertInfo.advertType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发布类型不能为空"
                        }
                    ],
                    'editAdvertInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "播放顺序不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "播放顺序不是有效的数字"
                        },
                    ],
                    'editAdvertInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "投放时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'editAdvertInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "dateTime",
                            param: "",
                            errInfo: "不是有效的时间格式"
                        },
                    ],
                    'editAdvertInfo.advertId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "广告ID不能为空"
                        }]
                });
            },
            editAdvert: function () {
                if (!vc.component.editAdvertValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                if (vc.component.editAdvertInfo.viewType == '8888') {
                    vc.component.editAdvertInfo.vedioName = '';
                } else {
                    vc.component.editAdvertInfo.photos = [];
                }
                vc.http.apiPost('/govAdvert/updateGovAdvert',
                    JSON.stringify(vc.component.editAdvertInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        if (res.status == 200) {
                            //关闭model
                            $('#editAdvertModel').modal('hide');
                            vc.emit('advertManage', 'listAdvert', {});
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            _loadAdvertItem: function () {
                var param = {
                    params: {
                        page : 1,
                        row : 50,
                        advertId: vc.component.editAdvertInfo.advertId,
                        caId : vc.getCurrentCommunity().caId
                    }
                };
                vc.http.apiGet('/govAdvertItem/queryGovAdvertItem',
                    param,
                    function (json, res) {
                        var _advertItemInfo = JSON.parse(json);
                        vc.component._freshPhotoOrVedio(_advertItemInfo.data);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _freshPhotoOrVedio: function (_advertItems) {
                var _photos = [];
                var _photos2 = [];
                //判断属性中是否有照片
                _advertItems.forEach(function (_item) {
                        vc.component.editAdvertInfo.viewType = _item.itemTypeCd;
                        if (_item.itemTypeCd == '8888') {
                            _photos.push(_item.url);
                        } else {
                            vc.component.editAdvertInfo.vedioName = _item.url;
                            _photos2.push(_item.url);
                            
                        }
                    }
                );
                vc.emit('editAdvert', 'uploadImage', 'notifyPhotos', _photos);
                vc.emit('editAdvert', 'uploadVedio', 'notifyVedio', _photos2);
            },
            refreshEditAdvertInfo: function () {
                vc.component.editAdvertInfo = {
                    caId: '',
                    advertId: '',
                    adName: '',
                    adTypeCd: '',
                    state: '',
                    seq: '',
                    advertType: '',
                    pageUrl: '',
                    startTime: '',
                    endTime: '',
                    photos: [],
                    viewType: '',
                    vedioName: ''
                }
            }
        }
    });
})(window.vc, window.vc.component);
