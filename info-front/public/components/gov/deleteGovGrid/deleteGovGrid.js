(function(vc,vm){

    vc.extends({
        data:{
            deleteGovGridInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovGrid','openDeleteGovGridModal',function(_params){

                vc.component.deleteGovGridInfo = _params;
                $('#deleteGovGridModel').modal('show');

            });
        },
        methods:{
            deleteGovGrid:function(){
                vc.component.deleteGovGridInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/govGrid/deleteGovGrid',
                    JSON.stringify(vc.component.deleteGovGridInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovGridModel').modal('hide');
                            vc.emit('govGridManage','listGovGrid',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovGridModel:function(){
                $('#deleteGovGridModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
