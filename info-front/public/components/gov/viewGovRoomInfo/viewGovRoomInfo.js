/**
    房屋管理 组件
**/
(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            viewGovRoomInfo: {
                index: 0,
                flowComponent: 'viewGovRoomInfo',
                caId: '',
                govCommunityId: '',
                govFloorId: '',
                roomNum: '',
                roomType: '',
                roomArea: '',
                layer: '',
                roomAddress: '',
                roomRight: '',
                ownerId: '',
                ownerName: '',
                ownerTel: '',
                isConservati: '',
                isSettle: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovRoomInfoData();
        },
        _initEvent: function () {
            vc.on('viewGovRoomInfo', 'chooseGovRoom', function (_app) {
                vc.copyObject(_app, vc.component.viewGovRoomInfo);
                vc.emit($props.callBackListener, $props.callBackFunction, vc.component.viewGovRoomInfo);
            });

            vc.on('viewGovRoomInfo', 'onIndex', function (_index) {
                vc.component.viewGovRoomInfo.index = _index;
            });

        },
        methods: {

            _openSelectGovRoomInfoModel() {
                vc.emit('chooseGovRoom', 'openChooseGovRoomModel', {});
            },
            _openAddGovRoomInfoModel() {
                vc.emit('addGovRoom', 'openAddGovRoomModal', {});
            },
            _loadGovRoomInfoData: function () {

            }
        }
    });

})(window.vc);
