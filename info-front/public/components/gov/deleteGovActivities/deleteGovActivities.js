(function(vc,vm){

    vc.extends({
        data:{
            deleteGovActivitiesInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovActivities','openDeleteGovActivitiesModal',function(_params){

                vc.component.deleteGovActivitiesInfo = _params;
                $('#deleteGovActivitiesModel').modal('show');

            });
        },
        methods:{
            deleteGovActivities:function(){
                vc.component.deleteGovActivitiesInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govActivities/deleteGovActivities',
                    JSON.stringify(vc.component.deleteGovActivitiesInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovActivitiesModel').modal('hide');
                            vc.emit('govActivitiesManage','listGovActivities',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovActivitiesModel:function(){
                $('#deleteGovActivitiesModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
