(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCadrePersonInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCadrePerson', 'openDeleteGovPersonModal', function (_params) {

                vc.component.deleteGovCadrePersonInfo = _params;
                $('#deleteGovPersonModel').modal('show');

            });
        },
        methods: {
            deleteGovPerson: function () {
                vc.http.apiPost(
                    '/govPersonLabelRel/deleteGovPersonLabelRel',
                    JSON.stringify(vc.component.deleteGovCadrePersonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPersonModel').modal('hide');
                            vc.emit('govCadrePersonManage', 'listGovPerson', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovPersonModel: function () {
                $('#deleteGovPersonModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
