(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovDrug:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovDrugInfo:{
                govDrugs:[],
                _currentGovDrugName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovDrug','openChooseGovDrugModel',function(_param){
                $('#chooseGovDrugModel').modal('show');
                vc.component._refreshChooseGovDrugInfo();
                vc.component._loadAllGovDrugInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovDrugInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govDrug/queryGovDrug',
                             param,
                             function(json){
                                var _govDrugInfo = JSON.parse(json);
                                vc.component.chooseGovDrugInfo.govDrugs = _govDrugInfo.govDrugs;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovDrug:function(_govDrug){
                if(_govDrug.hasOwnProperty('name')){
                     _govDrug.govDrugName = _govDrug.name;
                }
                vc.emit($props.emitChooseGovDrug,'chooseGovDrug',_govDrug);
                vc.emit($props.emitLoadData,'listGovDrugData',{
                    govDrugId:_govDrug.govDrugId
                });
                $('#chooseGovDrugModel').modal('hide');
            },
            queryGovDrugs:function(){
                vc.component._loadAllGovDrugInfo(1,10,vc.component.chooseGovDrugInfo._currentGovDrugName);
            },
            _refreshChooseGovDrugInfo:function(){
                vc.component.chooseGovDrugInfo._currentGovDrugName = "";
            }
        }

    });
})(window.vc);
