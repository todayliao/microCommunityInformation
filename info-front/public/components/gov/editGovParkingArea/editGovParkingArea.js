(function (vc, vm) {

    vc.extends({
        data: {
            editGovParkingAreaInfo: {
                paId: '',
                govCommunityId: '',
                num: '',
                parkingCount: '',
                typeCd: '',
                remark: '',
                govCommunitys:[]
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovParkingArea', 'openEditGovParkingAreaModal', function (_params) {
                vc.component.refreshEditGovParkingAreaInfo();
                $('#editGovParkingAreaModel').modal('show');
                $that._listEditGovCommunitys();
                vc.copyObject(_params, vc.component.editGovParkingAreaInfo);
                vc.component.editGovParkingAreaInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods: {
            editGovParkingAreaValidate: function () {
                return vc.validate.validate({
                    editGovParkingAreaInfo: vc.component.editGovParkingAreaInfo
                }, {
                    'editGovParkingAreaInfo.govCommunityId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "小区不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "小区超长"
                        },
                    ],
                    'editGovParkingAreaInfo.num': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "停车场编号超长"
                        },
                    ],
                    'editGovParkingAreaInfo.parkingCount': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "车位数量不能为空"
                        },
                        {
                            limit: "num",
                            param: "100",
                            errInfo: "车位数量不是有效数字"
                        },
                    ],
                    'editGovParkingAreaInfo.typeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "停车场类型不能为空"
                        },
                        {
                            limit: "num",
                            param: "100",
                            errInfo: "停车场类型不是有效数字"
                        },
                    ],
                    'editGovParkingAreaInfo.remark': [
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "备注内容不能超过200"
                        },
                    ],
                    'editGovParkingAreaInfo.paId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "编号不能为空"
                        }]

                });
            },
            editGovParkingArea: function () {
                if (!vc.component.editGovParkingAreaValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/parkingArea/updateParkingArea',
                    JSON.stringify(vc.component.editGovParkingAreaInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovParkingAreaModel').modal('hide');
                            vc.emit('govParkingAreaManage', 'listGovParkingArea', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _listEditGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.editGovParkingAreaInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovParkingAreaInfo: function () {
                vc.component.editGovParkingAreaInfo = {
                    paId: '',
                    govCommunityId: '',
                    num: '',
                    parkingCount: '',
                    typeCd: '',
                    remark: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
