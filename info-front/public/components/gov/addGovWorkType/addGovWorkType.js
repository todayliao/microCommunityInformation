(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovWorkTypeInfo: {
                govTypeId: '',
                workTypeName: '',
                state: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovWorkType', 'openAddGovWorkTypeModal', function () {
                $('#addGovWorkTypeModel').modal('show');
            });
        },
        methods: {
            addGovWorkTypeValidate() {
                return vc.validate.validate({
                    addGovWorkTypeInfo: vc.component.addGovWorkTypeInfo
                }, {
                    'addGovWorkTypeInfo.workTypeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "职务名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "职务名称超长"
                        },
                    ],
                    'addGovWorkTypeInfo.state': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "状态超长"
                        },
                    ],
                    'addGovWorkTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "备注太长"
                        },
                    ],




                });
            },
            saveGovWorkTypeInfo: function () {
                if (!vc.component.addGovWorkTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovWorkTypeInfo);
                    $('#addGovWorkTypeModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govWorkType/saveGovWorkType',
                    JSON.stringify(vc.component.addGovWorkTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovWorkTypeModel').modal('hide');
                            vc.component.clearAddGovWorkTypeInfo();
                            vc.emit('govWorkTypeManage', 'listGovWorkType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovWorkTypeInfo: function () {
                vc.component.addGovWorkTypeInfo = {
                    workTypeName: '',
                    state: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
