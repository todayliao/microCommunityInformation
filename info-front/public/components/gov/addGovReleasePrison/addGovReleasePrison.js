(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovReleasePrisonInfo: {
                releasePrisonId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                releaseTime: '',
                sentenceStartTime: '',
                sentenceEndTime: '',
                sentenceReason: '',
                ramark: '',

            }
        },
        _initMethod: function () {
            vc.initDateTime('addStartTime', function (_value) {
                $that.addGovReleasePrisonInfo.sentenceStartTime = _value;
            });
            vc.initDateTime('addEndTime', function (_value) {
                $that.addGovReleasePrisonInfo.sentenceEndTime = _value;
            });

        },
        _initEvent: function () {
            vc.on('addGovReleasePrison', 'openAddGovReleasePrisonModal', function () {
                $('#addGovReleasePrisonModel').modal('show');
            });
        },
        methods: {
            addGovReleasePrisonValidate() {
                return vc.validate.validate({
                    addGovReleasePrisonInfo: vc.component.addGovReleasePrisonInfo
                }, {

                    'addGovReleasePrisonInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "刑满释放者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "刑满释放者名称不能超过64"
                        },
                    ],
                    'addGovReleasePrisonInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'addGovReleasePrisonInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'addGovReleasePrisonInfo.releaseTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服刑周期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "36",
                            errInfo: "服刑周期不能超过36"
                        },
                    ],
                    'addGovReleasePrisonInfo.sentenceStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服刑开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "服刑开始时间不能超过时间类型"
                        },
                    ],
                    'addGovReleasePrisonInfo.sentenceEndTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服刑结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "服刑结束时间不能超过时间类型"
                        },
                    ],
                    'addGovReleasePrisonInfo.sentenceReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "服刑原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "服刑原因不能超过128"
                        },
                    ]


                });
            },
            saveGovReleasePrisonInfo: function () {
                if (!vc.component.addGovReleasePrisonValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovReleasePrisonInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovReleasePrisonInfo);
                    $('#addGovReleasePrisonModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govReleasePrison/saveGovReleasePrison',
                    JSON.stringify(vc.component.addGovReleasePrisonInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovReleasePrisonModel').modal('hide');
                            vc.component.clearAddGovReleasePrisonInfo();
                            vc.emit('govReleasePrisonManage', 'listGovReleasePrison', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovReleasePrisonInfo: function () {
                vc.component.addGovReleasePrisonInfo = {
                    releasePrisonId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    releaseTime: '',
                    sentenceStartTime: '',
                    sentenceEndTime: '',
                    sentenceReason: '',
                    ramark: '',
                };
            }
        }
    });

})(window.vc);
