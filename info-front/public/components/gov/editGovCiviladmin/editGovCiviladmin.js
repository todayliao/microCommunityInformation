(function (vc, vm) {

    vc.extends({
        data: {
            editGovActivitiesInfo: {
                civiladminId: '',
                title: '',
                typeCd: '',
                context: '',
                startTime: '',
                endTime: '',
                state: '',
                govActivitiesTypes: []

            }
        },
        _initMethod: function () {
            $that._initEditNoticeInfo();
            $that._queryEditGovActivitiesTypes(1,50);
        },
        _initEvent: function () {
            vc.on('editGovActivities', 'openEditGovActivitiesModal', function (_params) {
                vc.component.refreshEditGovActivitiesInfo();
                
                vc.copyObject(_params, vc.component.editGovActivitiesInfo);
                vc.component.editGovActivitiesInfo.caId = vc.getCurrentCommunity().caId;
                $(".eidtSummernote").summernote('code', vc.component.editGovActivitiesInfo.context);
            });
        },
        methods: {
            editGovActivitiesValidate: function () {
                return vc.validate.validate({
                    editGovActivitiesInfo: vc.component.editGovActivitiesInfo
                }, {
                    'editGovActivitiesInfo.civiladminId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主键不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "类别名称名称太长"
                        },
                    ],
                    'editGovActivitiesInfo.title': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "标题不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "标题太长"
                        },
                    ],
                    'editGovActivitiesInfo.typeCd': [
                        {
                            limit: "maxLength",
                            param: "50",
                            errInfo: "类型太长"
                        },
                    ],
                    'editGovActivitiesInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "内容不能为空"
                        }
                    ],
                    'editGovActivitiesInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        }
                    ],
                    'editGovActivitiesInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        }
                    ]

                });
            },
            editGovActivities: function () {
                console.log(vc.component.editGovActivitiesInfo);

                if (!vc.component.editGovActivitiesValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govCiviladmin/updateGovCiviladmin',
                    JSON.stringify(vc.component.editGovActivitiesInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovActivitiesModel').modal('hide');
                            vc.emit('govActivitiesManage', 'listGovActivities', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            closeEditActivitiesInfo: function () {
                vc.emit('govActivitiesManage', 'listGovActivities', {});

            },
            _initEditNoticeInfo: function () {
                $('.noticeStartTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true

                });
                $('.noticeStartTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".noticeStartTime").val();
                        vc.component.editGovActivitiesInfo.startTime = value;
                        let start = Date.parse(new Date(vc.component.editGovActivitiesInfo.startTime))
                        let end = Date.parse(new Date(vc.component.editGovActivitiesInfo.endTime))
                        if (end != 0 && start - end >= 0) {
                            vc.toast("开始时间必须小于结束时间")
                            vc.component.editGovActivitiesInfo.startTime = '';
                        }
                    });
                $('.noticeEndTime').datetimepicker({
                    language: 'zh-CN',
                    fontAwesome: 'fa',
                    format: 'yyyy-mm-dd hh:ii:ss',
                    initTime: true,
                    initialDate: new Date(),
                    autoClose: 1,
                    todayBtn: true
                });
                $('.noticeEndTime').datetimepicker()
                    .on('changeDate', function (ev) {
                        var value = $(".noticeEndTime").val();
                        vc.component.editGovActivitiesInfo.endTime = value;
                        let start = Date.parse(new Date(vc.component.editGovActivitiesInfo.startTime))
                        let end = Date.parse(new Date(vc.component.editGovActivitiesInfo.endTime))
                        if (start - end >= 0) {
                            vc.toast("结束时间必须大于开始时间")
                            vc.component.editGovActivitiesInfo.endTime = '';
                        }
                    });
                var $summernote = $('.eidtSummernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendEditFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.editGovActivitiesInfo.context = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });
            },
            sendEditFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _queryEditGovActivitiesTypes: function (_page, _rows) {

                var param = {
                    params: {
                       page : _page,
                        row : _rows,
                        caId : vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCiviladminType/queryGovCiviladminType',
                    param,
                    function (json, res) {
                        var _govActivitiesTypeManageInfo = JSON.parse(json);
                        vc.component.editGovActivitiesInfo.govActivitiesTypes = _govActivitiesTypeManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            refreshEditGovActivitiesInfo: function () {
                let _govActivitiesTypes = vc.component.editGovActivitiesInfo.govActivitiesTypes;
                vc.component.editGovActivitiesInfo = {
                    civiladminId: '',
                    title: '',
                    typeCd: '',
                    context: '',
                    startTime: '',
                    endTime: '',
                    state: '',
                    govActivitiesTypes: _govActivitiesTypes

                }
            }
        }
    });

})(window.vc, window.vc.component);
