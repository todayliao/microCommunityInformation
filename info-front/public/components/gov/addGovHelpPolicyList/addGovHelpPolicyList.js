(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovHelpPolicyListInfo: {
                govHelpListId: '',
                listName: '',
                caId: '',
                govHelpId: '',
                helpName: '',
                poorPersonId: '',
                poorPersonName: '',
                cadrePersonId: '',
                cadrePersonName: '',
                startTime: '',
                endTime: '',
                listBrief: '',
                govHelpPolicys: []
            }
        },
        _initMethod: function () {
            vc.initDateTime('startTimes', function (_value) {
                $that.addGovHelpPolicyListInfo.startTime = _value;
            });
            vc.initDateTime('endTimes', function (_value) {
                $that.addGovHelpPolicyListInfo.endTime = _value;
            });
            $that._initNoticeInfo();
            $that._listAddGovHelpPolicys();
        },
        _initEvent: function () {
            vc.on('addGovHelpPolicyList', 'openAddGovHelpPolicyListModal', function () {
                $('#addGovHelpPolicyListModel').modal('show');
            });
        },
        methods: {
            addGovHelpPolicyListValidate() {
                return vc.validate.validate({
                    addGovHelpPolicyListInfo: vc.component.addGovHelpPolicyListInfo
                }, {
                    'addGovHelpPolicyListInfo.listName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "记录名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "250",
                            errInfo: "记录名称不能超过250"
                        },
                    ],
                    'addGovHelpPolicyListInfo.govHelpId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策ID不能超过64"
                        },
                    ],
                    'addGovHelpPolicyListInfo.helpName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "政策名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "政策名称不能超过64"
                        },
                    ],
                    'addGovHelpPolicyListInfo.poorPersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "贫困人员ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "贫困人员ID不能超过30"
                        },
                    ],
                    'addGovHelpPolicyListInfo.poorPersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "贫困人员不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "贫困人员不能超过64"
                        },
                    ],
                    'addGovHelpPolicyListInfo.cadrePersonId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "扶贫干部ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "扶贫干部ID不能超过30"
                        },
                    ],
                    'addGovHelpPolicyListInfo.cadrePersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "扶贫干部不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "扶贫干部不能超过64"
                        },
                    ],
                    'addGovHelpPolicyListInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "帮扶开始时间不能超过时间类型"
                        },
                    ],
                    'addGovHelpPolicyListInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "帮扶结束时间不能超过时间类型"
                        },
                    ],
                    'addGovHelpPolicyListInfo.listBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "帮扶内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "帮扶内容不能超过1024"
                        },
                    ]




                });
            },
            saveGovHelpPolicyListInfo: function () {
                if (!vc.component.addGovHelpPolicyListValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovHelpPolicyListInfo);
                    $('#addGovHelpPolicyListModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govHelpPolicyList/saveGovHelpPolicyList',
                    JSON.stringify(vc.component.addGovHelpPolicyListInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovHelpPolicyListModel').modal('hide');
                            vc.component.clearAddGovHelpPolicyListInfo();
                            vc.emit('govHelpPolicyListManage', 'listGovHelpPolicyList', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovHelpPolicys: function () {
                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govHelpPolicy/queryGovHelpPolicy',
                    param,
                    function (json, res) {
                        var _govHelpPolicyManageInfo = JSON.parse(json);
                        vc.component.addGovHelpPolicyListInfo.govHelpPolicys = _govHelpPolicyManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _initNoticeInfo: function () {

                var $summernote = $('.summernote').summernote({
                    lang: 'zh-CN',
                    height: 300,
                    placeholder: '必填，请输入政策内容',
                    callbacks: {
                        onImageUpload: function (files, editor, $editable) {
                            $that.sendAddFile($summernote, files[0]);
                        },
                        onChange: function (contents, $editable) {
                            vc.component.addGovHelpPolicyListInfo.listBrief = contents;
                        }
                    },
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'italic', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['height', ['height']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                        ['help', ['help']]
                    ],
                });

            },
            sendAddFile: function ($summernote, files) {
                console.log('上传图片', files);

                var param = new FormData();
                param.append("uploadFile", files);
                vc.http.uploadImage(
                    param,
                    {
                        emulateJSON: true,
                        //添加请求头
                        headers: {
                            "Content-Type": "multipart/form-data"
                        }
                    },
                    function (json, res) {
                        if (res.status == 200) {
                            var data = JSON.parse(json);
                            $summernote.summernote('insertImage', data.data);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });

            },
            _openAddchooseGovPersonPolicyModel: function () {
                vc.component.addGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                var param = {
                    addGovHelpPolicyListInfo: $that.addGovHelpPolicyListInfo,
                    labelCd: '6009'
                }
                vc.emit('chooseGovPersonHelpPolicy', 'openchooseGovPersonHelpPolicyModel', param);
            },
            _openAddchooseGovPersonHelpModel: function () {
                vc.component.addGovHelpPolicyListInfo.caId = vc.getCurrentCommunity().caId;
                var param = {
                    addGovHelpPolicyListInfo: $that.addGovHelpPolicyListInfo,
                    labelCd: '6010'
                }
                vc.emit('chooseGovPersonHelpPolicy', 'openchooseGovPersonHelpPolicyModel', param);
            },
            setAddHelpName: function(_govHelpId){
                $that.addGovHelpPolicyListInfo.govHelpPolicys.forEach(element => {
                    if(element.govHelpId == _govHelpId){
                        $that.addGovHelpPolicyListInfo.helpName = element.helpName;
                    }
                });
            },
            closeAddGovHelpPolicyList: function () {
                $that.clearAddGovHelpPolicyListInfo();
                vc.emit('govHelpPolicyListManage', 'listGovHelpPolicyList', {});

            },
            clearAddGovHelpPolicyListInfo: function () {
                vc.component.addGovHelpPolicyListInfo = {
                    govHelpListId: '',
                    listName: '',
                    caId: '',
                    govHelpId: '',
                    helpName: '',
                    poorPersonId: '',
                    poorPersonName: '',
                    cadrePersonId: '',
                    cadrePersonName: '',
                    startTime: '',
                    endTime: '',
                    listBrief: '',
                    govHelpPolicys: []

                };
            }
        }
    });

})(window.vc);
