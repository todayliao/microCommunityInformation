(function(vc,vm){

    vc.extends({
        data:{
            deleteGovGridObjRelInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovGridObjRel','openDeleteGovGridObjRelModal',function(_params){

                vc.component.deleteGovGridObjRelInfo = _params;
                $('#deleteGovGridObjRelModel').modal('show');

            });
        },
        methods:{
            deleteGovGridObjRel:function(){
                vc.component.deleteGovGridObjRelInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govGridObjRel/deleteGovGridObjRel',
                    JSON.stringify(vc.component.deleteGovGridObjRelInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovGridObjRelModel').modal('hide');
                            vc.emit('govGridObjRelManage','listGovGridObjRel',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovGridObjRelModel:function(){
                $('#deleteGovGridObjRelModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
