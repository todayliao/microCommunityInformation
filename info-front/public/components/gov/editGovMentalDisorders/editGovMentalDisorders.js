(function (vc, vm) {

    vc.extends({
        data: {
            editGovMentalDisordersInfo: {
                disordersId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                disordersStartTime: '',
                disordersReason: '',
                emergencyPerson: '',
                emergencyTel: '',
                ramark: '',
                statusCd: '',

            }
        },
        _initMethod: function () {
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovMentalDisordersInfo.disordersStartTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovMentalDisorders', 'openEditGovMentalDisordersModal', function (_params) {
                vc.component.refreshEditGovMentalDisordersInfo();
                $('#editGovMentalDisordersModel').modal('show');
                vc.copyObject(_params, vc.component.editGovMentalDisordersInfo);
                vc.component.editGovMentalDisordersInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovMentalDisordersValidate: function () {
                return vc.validate.validate({
                    editGovMentalDisordersInfo: vc.component.editGovMentalDisordersInfo
                }, {
                    'editGovMentalDisordersInfo.name': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍者名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "障碍者名称不能超过64"
                        },
                    ],
                    'editGovMentalDisordersInfo.address': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "住址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "住址不能超过512"
                        },
                    ],
                    'editGovMentalDisordersInfo.tel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "手机号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "手机号不能超过11"
                        },
                    ],
                    'editGovMentalDisordersInfo.disordersStartTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "障碍开始时间不能超过时间类型"
                        },
                    ],
                    'editGovMentalDisordersInfo.disordersReason': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍原因不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "障碍原因不能超过128"
                        },
                    ],
                    'editGovMentalDisordersInfo.emergencyPerson': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "紧急联系人不能超过64"
                        },
                    ],
                    'editGovMentalDisordersInfo.emergencyTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "紧急联系电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "紧急联系电话不能超过11"
                        },
                    ],
                    'editGovMentalDisordersInfo.disordersId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "障碍者ID不能为空"
                        }]

                });
            },
            editGovMentalDisorders: function () {
                if (!vc.component.editGovMentalDisordersValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }

                vc.http.apiPost(
                    '/govMentalDisorders/updateGovMentalDisorders',
                    JSON.stringify(vc.component.editGovMentalDisordersInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovMentalDisordersModel').modal('hide');
                            vc.emit('govMentalDisordersManage', 'listGovMentalDisorders', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovMentalDisordersInfo: function () {
                vc.component.editGovMentalDisordersInfo = {
                    disordersId: '',
                    disordersId: '',
                    name: '',
                    caId: '',
                    address: '',
                    tel: '',
                    disordersStartTime: '',
                    disordersReason: '',
                    emergencyPerson: '',
                    emergencyTel: '',
                    ramark: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
