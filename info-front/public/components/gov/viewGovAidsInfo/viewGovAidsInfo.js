/**
    艾滋病者 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovAidsInfo:{
                index:0,
                flowComponent:'viewGovAidsInfo',
                aidsId:'',
name:'',
caId:'',
address:'',
tel:'',
aidsStartTime:'',
aidsReason:'',
emergencyPerson:'',
emergencyTel:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovAidsInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovAidsInfo','chooseGovAids',function(_app){
                vc.copyObject(_app, vc.component.viewGovAidsInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovAidsInfo);
            });

            vc.on('viewGovAidsInfo', 'onIndex', function(_index){
                vc.component.viewGovAidsInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovAidsInfoModel(){
                vc.emit('chooseGovAids','openChooseGovAidsModel',{});
            },
            _openAddGovAidsInfoModel(){
                vc.emit('addGovAids','openAddGovAidsModal',{});
            },
            _loadGovAidsInfoData:function(){

            }
        }
    });

})(window.vc);
