/**
    特殊人员跟进记录 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovSpecialFollowInfo:{
                index:0,
                flowComponent:'viewGovSpecialFollowInfo',
                followId:'',
specialPersonId:'',
name:'',
caId:'',
specialStartTime:'',
specialReason:'',
isWork:'',
workPerson:'',
workPersonTel:'',
workAddress:'',
ramark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovSpecialFollowInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovSpecialFollowInfo','chooseGovSpecialFollow',function(_app){
                vc.copyObject(_app, vc.component.viewGovSpecialFollowInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovSpecialFollowInfo);
            });

            vc.on('viewGovSpecialFollowInfo', 'onIndex', function(_index){
                vc.component.viewGovSpecialFollowInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovSpecialFollowInfoModel(){
                vc.emit('chooseGovSpecialFollow','openChooseGovSpecialFollowModel',{});
            },
            _openAddGovSpecialFollowInfoModel(){
                vc.emit('addGovSpecialFollow','openAddGovSpecialFollowModal',{});
            },
            _loadGovSpecialFollowInfoData:function(){

            }
        }
    });

})(window.vc);
