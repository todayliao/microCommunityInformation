(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovMeetingTypeInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovMeetingType', 'openDeleteGovMeetingTypeModal', function (_params) {

                vc.component.deleteGovMeetingTypeInfo = _params;
                $('#deleteGovMeetingTypeModel').modal('show');

            });
        },
        methods: {
            deleteGovMeetingType: function () {
                vc.component.deleteGovMeetingTypeInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMeetingType/deleteGovMeetingType',
                    JSON.stringify(vc.component.deleteGovMeetingTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMeetingTypeModel').modal('hide');
                            vc.emit('govMeetingTypeManage', 'listGovMeetingType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovMeetingTypeModel: function () {
                $('#deleteGovMeetingTypeModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
