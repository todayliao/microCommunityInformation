(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovEventsTypeInfo: {
                typeId: '',
                caId: '',
                typeName: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovEventsType', 'openAddGovEventsTypeModal', function () {
                $('#addGovEventsTypeModel').modal('show');
            });
        },
        methods: {
            addGovEventsTypeValidate() {
                return vc.validate.validate({
                    addGovEventsTypeInfo: vc.component.addGovEventsTypeInfo
                }, {
                    'addGovEventsTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "类型名称不能超过64"
                        },
                    ],
                    'addGovEventsTypeInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ],




                });
            },
            saveGovEventsTypeInfo: function () {
                if (!vc.component.addGovEventsTypeValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovEventsTypeInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovEventsTypeInfo);
                    $('#addGovEventsTypeModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govEventsType/saveGovEventsType',
                    JSON.stringify(vc.component.addGovEventsTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovEventsTypeModel').modal('hide');
                            vc.component.clearAddGovEventsTypeInfo();
                            vc.emit('govEventsTypeManage', 'listGovEventsType', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovEventsTypeInfo: function () {
                vc.component.addGovEventsTypeInfo = {
                    typeId: '',
                    caId: '',
                    typeName: '',
                    ramark: '',

                };
            }
        }
    });

})(window.vc);
