/**
    随访登记 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovFollowupSurveyInfo:{
                index:0,
                flowComponent:'viewGovFollowupSurveyInfo',
                surveyId:'',
surveyTime:'',
govPersonId:'',
surveyWay:'',
symptoms:'',
caId:'',
lifeStyleGuide:'',
drugCompliance:'',
adrs:'',
surveyType:'',
medication:'',
referralReason:'',
referralDepartment:'',
surveyAdvice:'',
surveyConclusion:'',
nextSurveyTime:'',
surveyDoctor:'',
surveyDoctorId:'',
remark:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovFollowupSurveyInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovFollowupSurveyInfo','chooseGovFollowupSurvey',function(_app){
                vc.copyObject(_app, vc.component.viewGovFollowupSurveyInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovFollowupSurveyInfo);
            });

            vc.on('viewGovFollowupSurveyInfo', 'onIndex', function(_index){
                vc.component.viewGovFollowupSurveyInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovFollowupSurveyInfoModel(){
                vc.emit('chooseGovFollowupSurvey','openChooseGovFollowupSurveyModel',{});
            },
            _openAddGovFollowupSurveyInfoModel(){
                vc.emit('addGovFollowupSurvey','openAddGovFollowupSurveyModal',{});
            },
            _loadGovFollowupSurveyInfoData:function(){

            }
        }
    });

})(window.vc);
