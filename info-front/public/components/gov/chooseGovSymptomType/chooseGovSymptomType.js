(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovSymptomType:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovSymptomTypeInfo:{
                govSymptomTypes:[],
                _currentGovSymptomTypeName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovSymptomType','openChooseGovSymptomTypeModel',function(_param){
                $('#chooseGovSymptomTypeModel').modal('show');
                vc.component._refreshChooseGovSymptomTypeInfo();
                vc.component._loadAllGovSymptomTypeInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovSymptomTypeInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govSymptomType/queryGovSymptomType',
                             param,
                             function(json){
                                var _govSymptomTypeInfo = JSON.parse(json);
                                vc.component.chooseGovSymptomTypeInfo.govSymptomTypes = _govSymptomTypeInfo.govSymptomTypes;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovSymptomType:function(_govSymptomType){
                if(_govSymptomType.hasOwnProperty('name')){
                     _govSymptomType.govSymptomTypeName = _govSymptomType.name;
                }
                vc.emit($props.emitChooseGovSymptomType,'chooseGovSymptomType',_govSymptomType);
                vc.emit($props.emitLoadData,'listGovSymptomTypeData',{
                    govSymptomTypeId:_govSymptomType.govSymptomTypeId
                });
                $('#chooseGovSymptomTypeModel').modal('hide');
            },
            queryGovSymptomTypes:function(){
                vc.component._loadAllGovSymptomTypeInfo(1,10,vc.component.chooseGovSymptomTypeInfo._currentGovSymptomTypeName);
            },
            _refreshChooseGovSymptomTypeInfo:function(){
                vc.component.chooseGovSymptomTypeInfo._currentGovSymptomTypeName = "";
            }
        }

    });
})(window.vc);
