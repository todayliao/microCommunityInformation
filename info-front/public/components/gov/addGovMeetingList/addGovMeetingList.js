(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovMeetingListInfo: {
                govMeetingId: '',
                caId: '',
                typeId: '',
                orgId: '',
                govMemberId: '',
                memberName:'',
                meetingName: '',
                meetingTime: '',
                meetingAddress: '',
                meetingBrief: '',
                govMembers:[],
                govMeetingTypes: [],
                govPartyOrgs: []
            }
        },
        _initMethod: function () {
            $that._listAddGovMeetingTypes();
            $that._listAddGovPartyOrgs();
            vc.initDateTime('meetingTime', function (_value) {
                $that.addGovMeetingListInfo.meetingTime = _value;
            });
        },
        _initEvent: function () {
            vc.on('addGovMeetingList', 'openAddGovMeetingListModal', function () {
               
                $('#addGovMeetingListModel').modal('show');
            });

            vc.on('viewGovPartyMember', 'page_event', function (_param) {
                $that.addGovMeetingListInfo.govMembers = _param;
            });
        },
        methods: {
            addGovMeetingListValidate() {
                return vc.validate.validate({
                    addGovMeetingListInfo: vc.component.addGovMeetingListInfo
                }, {
                    'addGovMeetingListInfo.typeId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议类型不能超过30"
                        },
                    ],
                    'addGovMeetingListInfo.orgId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "组织部门不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "组织部门不能超过30"
                        },
                    ],
                    'addGovMeetingListInfo.govMemberId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主持人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "主持人不能超过30"
                        },
                    ],
                    'addGovMeetingListInfo.meetingName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "会议名称不能超过64"
                        },
                    ],
                    'addGovMeetingListInfo.meetingTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "会议时间不能超过时间类型"
                        },
                    ],
                    'addGovMeetingListInfo.meetingAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "会议地点不能超过512"
                        },
                    ],
                    'addGovMeetingListInfo.meetingBrief': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "会议简介不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "会议简介不能超过30"
                        },
                    ]

                });
            },
            saveGovMeetingListInfo: function () {
                if (!vc.component.addGovMeetingListValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovMeetingListInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovMeetingListInfo);
                    $('#addGovMeetingListModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govMeetingList/saveGovMeetingList',
                    JSON.stringify(vc.component.addGovMeetingListInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovMeetingListModel').modal('hide');
                            vc.component.clearAddGovMeetingListInfo();
                            vc.emit('govMeetingListManage', 'listGovMeetingList', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovMeetingTypes: function () {
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govMeetingType/queryGovMeetingType',
                    param,
                    function (json, res) {
                        var _govMeetingTypeManageInfo = JSON.parse(json);
                        vc.component.addGovMeetingListInfo.govMeetingTypes = _govMeetingTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovPartyOrgs: function () {

                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.addGovMeetingListInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddChooseGovPartyMember: function (_addGovMeetingListInfo) {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('chooseGovPartyMember', 'openChooseGovPartyMemberModel', _addGovMeetingListInfo);
            },
            _setAddviewGovPartyMember: function (_orgId) {
                let _caId = vc.getCurrentCommunity().caId;
                if(!_caId){
                    vc.toast("请先选择区域！");
                    return;
                }
                vc.emit('viewGovPartyMember', 'selectPartyOrgs', _orgId);
            },
            _clearAddGovMeetingListModel: function () {
                $that.clearAddGovMeetingListInfo();
                vc.emit('govMeetingListManage', 'listGovMeetingList', {});
            },
            clearAddGovMeetingListInfo: function () {
                vc.component.addGovMeetingListInfo = {
                    govMeetingId: '',
                    caId: '',
                    typeId: '',
                    orgId: '',
                    govMemberId: '',
                    memberName:'',
                    meetingName: '',
                    meetingTime: '',
                    meetingAddress: '',
                    meetingBrief: '',
                    govMembers:[],
                    govMeetingTypes: [],
                    govPartyOrgs: []
                };
            }
        }
    });

})(window.vc);
