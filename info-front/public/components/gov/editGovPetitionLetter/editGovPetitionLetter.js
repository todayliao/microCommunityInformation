(function (vc, vm) {

    vc.extends({
        data: {
            editGovPetitionLetterInfo: {
                petitionLetterId: '',
                caId: '',
                govCommunityId: '',
                letterCode: '',
                queryCode: '',
                petitionTime: '',
                petitionPersonName: '',
                petitionPersonId: '',
                personLink: '',
                idCard: '',
                petitionType: '',
                petitionForm: '',
                petitionPurpose: '',
                context: '',
                state: '',
                imgUrl: '',
                remark: '',
                dealDepartmentId: '',
                dealDepartmentName: '',
                dealUserName: '',
                dealUserId: '',
                dealOpinion: '',
                receiveUserId: '',
                receiveUserName: '',
                receiveDepartmentId: '',
                receiveDepartmentName: '',
                statusCd: '',
            }
        },
        _initMethod: function () {
            $('.editPetitionTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true
            });
            $('.editPetitionTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editPetitionTime").val();
                    vc.component.editGovPetitionLetterInfo.petitionTime = value;
                });
        },
        _initEvent: function () {
            vc.on('editGovPetitionLetter', 'openEditGovPetitionLetterModal', function (_params) {
                vc.component.refreshEditGovPetitionLetterInfo();
                $('#editGovPetitionLetterModel').modal('show');
                console.log("_params",_params);
                vc.copyObject(_params, vc.component.editGovPetitionLetterInfo);
                vc.component.editGovPetitionLetterInfo.caId = vc.getCurrentCommunity().caId;
                var photos = [];
                photos.push(vc.component.editGovPetitionLetterInfo.imgUrl);
                
                vc.emit('editImgUrl', 'uploadImage', 'notifyPhotos', photos);
            });
            vc.on("editImgUrl", "notifyUploadImage", function (_param) {
                if (!vc.isEmpty(_param) && _param.length > 0) {
                    vc.component.editGovPetitionLetterInfo.imgUrl = _param[0];
                } else {
                    vc.component.editGovPetitionLetterInfo.imgUrl = '';
                }
            });
            vc.on('openChooseGovPetitionPerson', 'chooseGovPetitionPerson', function (_param) {
                vc.component.editGovPetitionLetterInfo.petitionPersonName = _param.personName;
                vc.component.editGovPetitionLetterInfo.petitionPersonId = _param.petitionPersonId;
                vc.component.editGovPetitionLetterInfo.personLink = _param.personTel;
                vc.component.editGovPetitionLetterInfo.idCard = _param.personName;
            });
        },
        methods: {
            editGovPetitionLetterValidate: function () {
                return vc.validate.validate({
                    editGovPetitionLetterInfo: vc.component.editGovPetitionLetterInfo
                }, {
                    'editGovPetitionLetterInfo.letterCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访件号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访件号不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.queryCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "查询码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "查询码不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.petitionTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "信访日期不能超过时间类型"
                        },
                    ],
                    'editGovPetitionLetterInfo.petitionPersonName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访人名称不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人电话不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "信访人电话不能超过11"
                        },
                    ],
                    'editGovPetitionLetterInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访人身份证不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "20",
                            errInfo: "信访人身份证不能超过20"
                        },
                    ],
                    'editGovPetitionLetterInfo.petitionType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访类型不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.petitionForm': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访形式不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访形式不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.petitionPurpose': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访目的不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "信访目的不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.context': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "信访内容不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "信访内容不能超过长文本"
                        },
                    ],
                    'editGovPetitionLetterInfo.imgUrl': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "照片地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "512",
                            errInfo: "照片地址不能超过512"
                        },
                    ],
                    'editGovPetitionLetterInfo.receiveUserName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "接访人姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "接访人姓名不能超过30"
                        },
                    ],
                    'editGovPetitionLetterInfo.receiveDepartmentName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "接访单位名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "接访单位名称不能超过30"
                        },
                    ]
                });
            },
            closeEditActivitiesInfo: function () {
                vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});
            },
            editGovPetitionLetter: function () {
                if (!vc.component.editGovPetitionLetterValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/govPetitionLetter/updateGovPetitionLetter',
                    JSON.stringify(vc.component.editGovPetitionLetterInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPetitionLetterModel').modal('hide');
                            vc.emit('govPetitionLetterManage', 'listGovPetitionLetter', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            _openEditChoosePetitionPersonModel: function () {
                vc.emit( 'chooseGovPetitionPerson', 'openChooseGovPetitionPersonModel', {});
            },
            refreshEditGovPetitionLetterInfo: function () {
                vc.component.editGovPetitionLetterInfo = {
                    petitionLetterId: '',
                    petitionLetterId: '',
                    caId: '',
                    govCommunityId: '',
                    letterCode: '',
                    queryCode: '',
                    petitionTime: '',
                    petitionPersonName: '',
                    petitionPersonId: '',
                    personLink: '',
                    idCard: '',
                    petitionType: '',
                    petitionForm: '',
                    petitionPurpose: '',
                    context: '',
                    state: '',
                    imgUrl: '',
                    remark: '',
                    dealDepartmentId: '',
                    dealDepartmentName: '',
                    dealUserName: '',
                    dealUserId: '',
                    dealOpinion: '',
                    receiveUserId: '',
                    receiveUserName: '',
                    receiveDepartmentId: '',
                    receiveDepartmentName: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
