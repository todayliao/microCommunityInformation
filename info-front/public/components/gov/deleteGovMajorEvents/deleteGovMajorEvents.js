(function(vc,vm){

    vc.extends({
        data:{
            deleteGovMajorEventsInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovMajorEvents','openDeleteGovMajorEventsModal',function(_params){

                vc.component.deleteGovMajorEventsInfo = _params;
                $('#deleteGovMajorEventsModel').modal('show');

            });
        },
        methods:{
            deleteGovMajorEvents:function(){
                vc.component.deleteGovMajorEventsInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMajorEvents/deleteGovMajorEvents',
                    JSON.stringify(vc.component.deleteGovMajorEventsInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMajorEventsModel').modal('hide');
                            vc.emit('govMajorEventsManage','listGovMajorEvents',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovMajorEventsModel:function(){
                $('#deleteGovMajorEventsModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
