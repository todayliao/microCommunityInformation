(function(vc,vm){

    vc.extends({
        data:{
            deleteGovSymptomTypeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovSymptomType','openDeleteGovSymptomTypeModal',function(_params){

                vc.component.deleteGovSymptomTypeInfo = _params;
                $('#deleteGovSymptomTypeModel').modal('show');

            });
        },
        methods:{
            deleteGovSymptomType:function(){
                vc.component.deleteGovSymptomTypeInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govSymptomType/deleteGovSymptomType',
                    JSON.stringify(vc.component.deleteGovSymptomTypeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovSymptomTypeModel').modal('hide');
                            vc.emit('govSymptomTypeManage','listGovSymptomType',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovSymptomTypeModel:function(){
                $('#deleteGovSymptomTypeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
