(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovVolunteerServInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovVolunteerServ', 'openDeleteGovVolunteerServModal', function (_params) {

                vc.component.deleteGovVolunteerServInfo = _params;
                $('#deleteGovVolunteerServModel').modal('show');

            });
        },
        methods: {
            deleteGovVolunteerServ: function () {
                vc.component.deleteGovVolunteerServInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govVolunteerServ/deleteGovVolunteerServ',
                    JSON.stringify(vc.component.deleteGovVolunteerServInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovVolunteerServModel').modal('hide');
                            vc.emit('govVolunteerServManage', 'listGovVolunteerServ', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovVolunteerServModel: function () {
                $('#deleteGovVolunteerServModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
