(function(vc,vm){

    vc.extends({
        data:{
            deleteGovMedicalGradeInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovMedicalGrade','openDeleteGovMedicalGradeModal',function(_params){

                vc.component.deleteGovMedicalGradeInfo = _params;
                $('#deleteGovMedicalGradeModel').modal('show');

            });
        },
        methods:{
            deleteGovMedicalGrade:function(){
                vc.component.deleteGovMedicalGradeInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govMedicalGrade/deleteGovMedicalGrade',
                    JSON.stringify(vc.component.deleteGovMedicalGradeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovMedicalGradeModel').modal('hide');
                            vc.emit('govMedicalGradeManage','listGovMedicalGrade',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovMedicalGradeModel:function(){
                $('#deleteGovMedicalGradeModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
