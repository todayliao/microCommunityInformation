(function(vc,vm){

    vc.extends({
        data:{
            deleteReportPoolInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteReportPool','openDeleteReportPoolModal',function(_params){

                vc.component.deleteReportPoolInfo = _params;
                $('#deleteReportPoolModel').modal('show');

            });
        },
        methods:{
            deleteReportPool:function(){
                vc.component.deleteReportPoolInfo.communityId=vc.getCurrentCommunity().communityId;
                vc.http.apiPost(
                    '/reportPool/deleteReportPool',
                    JSON.stringify(vc.component.deleteReportPoolInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteReportPoolModel').modal('hide');
                            vc.emit('reportPoolManage','listReportPool',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteReportPoolModel:function(){
                $('#deleteReportPoolModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
