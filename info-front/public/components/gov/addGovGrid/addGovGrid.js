(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovGridInfo: {
                govGridId: '',
                govTypeId: '',
                caId: '',
                personName: '',
                personTel: '',
                ramark: '',
                govGridTypes: [],
                idCard: '',
                personSex: '',
                nation:'',
                maritalStatus:''
            }
        },
        _initMethod: function () {
            $that._addGovGridTypes();
        },
        _initEvent: function () {
            vc.on('addGovGrid', 'openAddGovGridModal', function () {
                $('#addGovGridModel').modal('show');
            });
        },
        methods: {
            addGovGridValidate() {
                return vc.validate.validate({
                    addGovGridInfo: vc.component.addGovGridInfo
                }, {

                    'addGovGridInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "姓名不能超过64"
                        },
                    ],
                    'addGovGridInfo.personTel': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "电话格式错误"
                        },
                    ],
                    'addGovGridInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "身份证不能为空"
                        }
                    ],
                    'addGovGridInfo.personSex': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "性别不能为空"
                        }
                    ],
                    'addGovGridInfo.nation': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "民族不能为空"
                        }
                    ],
                    'addGovGridInfo.maritalStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "婚姻状态不能为空"
                        }
                    ],
                    'addGovGridInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "备注不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "备注不能超过1024"
                        },
                    ]


                });
            },
            saveGovGridInfo: function () {
                if (!vc.component.addGovGridValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovGridInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovGridInfo);
                    $('#addGovGridModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govGrid/saveGovGrid',
                    JSON.stringify(vc.component.addGovGridInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovGridModel').modal('hide');
                            vc.component.clearAddGovGridInfo();
                            vc.emit('govGridManage', 'listGovGrid', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _addGovGridTypes: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govGridType/queryGovGridType',
                    param,
                    function (json, res) {
                        var _govGridTypeManageInfo = JSON.parse(json);
                        vc.component.addGovGridInfo.govGridTypes = _govGridTypeManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            clearAddGovGridInfo: function () {
                vc.component.addGovGridInfo = {
                    govGridId: '',
                    govPersonId: '',
                    govTypeId: '',
                    caId: '',
                    personName: '',
                    personTel: '',
                    ramark: '',
                    govGridTypes: [],
                    idCard: '',
                    personSex: '',
                    nation:'',
                    maritalStatus:''

                };
            }
        }
    });

})(window.vc);
