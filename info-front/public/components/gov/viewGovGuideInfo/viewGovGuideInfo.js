/**
    办事流程 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovGuideInfo:{
                index:0,
                flowComponent:'viewGovGuideInfo',
                caId:'',
guideType:'',
guideName:'',
flow:'',
data:'',
person:'',
link:'',
state:'',
subscribe:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovGuideInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovGuideInfo','chooseGovGuide',function(_app){
                vc.copyObject(_app, vc.component.viewGovGuideInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovGuideInfo);
            });

            vc.on('viewGovGuideInfo', 'onIndex', function(_index){
                vc.component.viewGovGuideInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovGuideInfoModel(){
                vc.emit('chooseGovGuide','openChooseGovGuideModel',{});
            },
            _openAddGovGuideInfoModel(){
                vc.emit('addGovGuide','openAddGovGuideModal',{});
            },
            _loadGovGuideInfoData:function(){

            }
        }
    });

})(window.vc);
