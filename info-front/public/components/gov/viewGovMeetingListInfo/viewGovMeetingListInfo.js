/**
    会议列表 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovMeetingListInfo:{
                index:0,
                flowComponent:'viewGovMeetingListInfo',
                govMeetingId:'',
caId:'',
typeId:'',
orgId:'',
govMemberId:'',
meetingName:'',
meetingTime:'',
meetingAddress:'',
meetingBrief:'',
meetingPoints:'',
meetingContent:'',
meetingImg:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovMeetingListInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovMeetingListInfo','chooseGovMeetingList',function(_app){
                vc.copyObject(_app, vc.component.viewGovMeetingListInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovMeetingListInfo);
            });

            vc.on('viewGovMeetingListInfo', 'onIndex', function(_index){
                vc.component.viewGovMeetingListInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovMeetingListInfoModel(){
                vc.emit('chooseGovMeetingList','openChooseGovMeetingListModel',{});
            },
            _openAddGovMeetingListInfoModel(){
                vc.emit('addGovMeetingList','openAddGovMeetingListModal',{});
            },
            _loadGovMeetingListInfoData:function(){

            }
        }
    });

})(window.vc);
