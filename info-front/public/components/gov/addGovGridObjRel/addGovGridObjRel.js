(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovGridObjRelInfo: {
                govGridObjId: '',
                govGridId: '',
                objTypeCd: '',
                objTypeCds: [],
                caId: '',
                objId: '',
                objName: '',
                startTime: '',
                endTime: '',
                workStatus: '',
                ramark: '',
                govCommunitys: [],
                govFloors: [],
                communityId: '',
                floorId: ''
            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovGridObjRel', 'openAddGovGridObjRelModal', function (param) {
                $that.addGovGridObjRelInfo.govGridId = param;
                $('#addGovGridObjRelModel').modal('show');
                vc.getDict('gov_grid_obj_rel', "obj_type_cd", function (_data) {
                    vc.component.addGovGridObjRelInfo.objTypeCds = _data;
                });
                $that._listAddGovCommunitys();
                vc.initDate('startTimes', function (_value) {
                    $that.addGovGridObjRelInfo.startTime = _value;
                });
                vc.initDate('endTimes', function (_value) {
                    $that.addGovGridObjRelInfo.endTime = _value;
                });
            });
        },
        methods: {
            addGovGridObjRelValidate() {
                return vc.validate.validate({
                    addGovGridObjRelInfo: vc.component.addGovGridObjRelInfo
                }, {
                    'addGovGridObjRelInfo.govGridId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "网格员编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "网格员编号不能超过30"
                        },
                    ],
                    'addGovGridObjRelInfo.objTypeCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象类型编码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "对象类型编码不能超过11"
                        },
                    ],
                    'addGovGridObjRelInfo.objId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "对象ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "对象ID不能超过30"
                        },
                    ],
                    'addGovGridObjRelInfo.startTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "开始时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "开始时间不能超过时间类型"
                        },
                    ],
                    'addGovGridObjRelInfo.endTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "结束时间不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "结束时间不能超过时间类型"
                        },
                    ],
                    'addGovGridObjRelInfo.workStatus': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "工作状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "工作状态不能超过11"
                        },
                    ]


                });
            },
            saveGovGridObjRelInfo: function () {
                if (!vc.component.addGovGridObjRelValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                vc.component.addGovGridObjRelInfo.caId = vc.getCurrentCommunity().caId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovGridObjRelInfo);
                    $('#addGovGridObjRelModel').modal('hide');
                    return;
                }
                vc.http.apiPost(
                    '/govGridObjRel/saveGovGridObjRel',
                    JSON.stringify(vc.component.addGovGridObjRelInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovGridObjRelModel').modal('hide');
                            vc.component.clearAddGovGridObjRelInfo();
                            vc.emit('govGridObjRelManage', 'listGovGridObjRel', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            _listAddGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.addGovGridObjRelInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listAddGovFloors: function (_GovCommunityId) {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        govCommunityId: _GovCommunityId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govFloor/queryGovFloor',
                    param,
                    function (json, res) {
                        var _govFloorManageInfo = JSON.parse(json);
                        vc.component.addGovGridObjRelInfo.govFloors = _govFloorManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            selectObj: function (_objTypeCd) {
                if (!_objTypeCd) {
                    return;
                }
                $that.addGovGridObjRelInfo.objId = '';
                if (_objTypeCd == '9001') {
                    $that.addGovGridObjRelInfo.objId = vc.getCurrentCommunity().caId;
                    $that.addGovGridObjRelInfo.objName = vc.getCurrentCommunity().caName;
                }
            },
            selecFloor: function (_communityId) {
                if (!_communityId) {
                    return;
                }
                $that.addGovGridObjRelInfo.objId = '';
                $that.addGovGridObjRelInfo.objName = '';
                if ($that.addGovGridObjRelInfo.objTypeCd == '9002') {

                    $that.addGovGridObjRelInfo.objId = _communityId;
                    $that.addGovGridObjRelInfo.govCommunitys.forEach(_item => {
                        if (_item.govCommunityId == _communityId) {
                            $that.addGovGridObjRelInfo.objName = _item.communityName;
                        }
                    });


                } else if ($that.addGovGridObjRelInfo.objTypeCd == '9003') {
                    $that._listAddGovFloors(_communityId);
                }
            },
            setObj: function (_floorId) {
                if (!_floorId) {
                    return;
                }
                $that.addGovGridObjRelInfo.objId = '';
                $that.addGovGridObjRelInfo.objId = _floorId;
                $that.addGovGridObjRelInfo.govFloors.forEach(_item => {
                    if (_item.govFloorId == _floorId) {
                        $that.addGovGridObjRelInfo.objName = _item.floorName;
                    }
                });
            },
            clearAddGovGridObjRelInfo: function () {
                vc.component.addGovGridObjRelInfo = {
                    govGridObjId: '',
                    govGridId: '',
                    objTypeCd: '',
                    caId: '',
                    objId: '',
                    objName: '',
                    startTime: '',
                    endTime: '',
                    workStatus: '',
                    ramark: '',
                    govCommunitys: [],
                    govFloors: [],
                    communityId: '',
                    floorId: ''
                };
            }
        }
    });

})(window.vc);
