(function(vc,vm){

    vc.extends({
        data:{
            deleteGovCommunityarCorrectionInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovCommunityarCorrection','openDeleteGovCommunityarCorrectionModal',function(_params){

                vc.component.deleteGovCommunityarCorrectionInfo = _params;
                $('#deleteGovCommunityarCorrectionModel').modal('show');

            });
        },
        methods:{
            deleteGovCommunityarCorrection:function(){
                vc.component.deleteGovCommunityarCorrectionInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govCommunityarCorrection/deleteGovCommunityarCorrection',
                    JSON.stringify(vc.component.deleteGovCommunityarCorrectionInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCommunityarCorrectionModel').modal('hide');
                            vc.emit('govCommunityarCorrectionManage','listGovCommunityarCorrection',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovCommunityarCorrectionModel:function(){
                $('#deleteGovCommunityarCorrectionModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
