/**
    安全事件 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovProtectionCaseInfo:{
                index:0,
                flowComponent:'viewGovProtectionCaseInfo',
                caseId:'',
objType:'',
objId:'',
objName:'',
caId:'',
govCommunityId:'',
caseName:'',
caseCode:'',
areaCode:'',
happenedTime:'',
happenedPlace:'',
caseType:'',
idType:'',
idCard:'',
personName:'',
isSolved:'',
personNum:'',
fleeingNum:'',
arrestsNum:'',
detectionContent:'',
caseContent:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovProtectionCaseInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovProtectionCaseInfo','chooseGovProtectionCase',function(_app){
                vc.copyObject(_app, vc.component.viewGovProtectionCaseInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovProtectionCaseInfo);
            });

            vc.on('viewGovProtectionCaseInfo', 'onIndex', function(_index){
                vc.component.viewGovProtectionCaseInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovProtectionCaseInfoModel(){
                vc.emit('chooseGovProtectionCase','openChooseGovProtectionCaseModel',{});
            },
            _openAddGovProtectionCaseInfoModel(){
                vc.emit('addGovProtectionCase','openAddGovProtectionCaseModal',{});
            },
            _loadGovProtectionCaseInfoData:function(){

            }
        }
    });

})(window.vc);
