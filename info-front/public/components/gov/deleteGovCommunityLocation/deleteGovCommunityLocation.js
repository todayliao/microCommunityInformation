(function (vc, vm) {

    vc.extends({
        data: {
            deleteGovCommunityLocationInfo: {

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('deleteGovCommunityLocation', 'openDeleteGovCommunityLocationModal', function (_params) {

                vc.component.deleteGovCommunityLocationInfo = _params;
                $('#deleteGovCommunityLocationModel').modal('show');

            });
        },
        methods: {
            deleteGovCommunityLocation: function () {
                vc.component.deleteGovCommunityLocationInfo.caId = vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govCommunityLocation/deleteGovCommunityLocation',
                    JSON.stringify(vc.component.deleteGovCommunityLocationInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovCommunityLocationModel').modal('hide');
                            vc.emit('govCommunityLocationManage', 'listGovCommunityLocation', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.message(json);

                    });
            },
            closeDeleteGovCommunityLocationModel: function () {
                $('#deleteGovCommunityLocationModel').modal('hide');
            }
        }
    });

})(window.vc, window.vc.component);
