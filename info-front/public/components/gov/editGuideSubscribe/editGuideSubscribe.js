(function(vc,vm){

    vc.extends({
        data:{
            editGuideSubscribeInfo:{
                wgsId:'',
                caId:'',
                wgId:'',
                person:'',
                link:'',
                startTime:'',
                endTime:'',
                state:'',

            }
        },
         _initMethod:function(){
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGuideSubscribeInfo.startTime = value;
                });
            $('.editEndTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true
            });
            $('.editEndTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editEndTime").val();
                    vc.component.editGuideSubscribeInfo.endTime = value;
                });
         },
         _initEvent:function(){
             vc.on('editGuideSubscribe','openEditGuideSubscribeModal',function(_params){
                vc.component.refreshEditGuideSubscribeInfo();
                $('#editGuideSubscribeModel').modal('show');
                vc.copyObject(_params, vc.component.editGuideSubscribeInfo );
                vc.component.editGuideSubscribeInfo.communityId = vc.getCurrentCommunity().communityId;
            });
        },
        methods:{
            editGuideSubscribeValidate:function(){
                        return vc.validate.validate({
                            editGuideSubscribeInfo:vc.component.editGuideSubscribeInfo
                        },{
                            'editGuideSubscribeInfo.caId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"所属区域不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"30",
                            errInfo:"所属区域超长"
                        },
                    ],
'editGuideSubscribeInfo.wgId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"事项id不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"事项id超长"
                        },
                    ],
'editGuideSubscribeInfo.person':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询人不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"咨询人名称太长"
                        },
                    ],
'editGuideSubscribeInfo.link':[
{
                            limit:"required",
                            param:"",
                            errInfo:"咨询电话不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"咨询电话太长"
                        },
                    ],
'editGuideSubscribeInfo.startTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"开始时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"开始时间超长"
                        },
                    ],
'editGuideSubscribeInfo.endTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"结束时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"结束时间超长"
                        },
                    ],
'editGuideSubscribeInfo.state':[
{
                            limit:"required",
                            param:"",
                            errInfo:"预约状态不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"12",
                            errInfo:"预约状态超长"
                        },
                    ],
'editGuideSubscribeInfo.wgsId':[
{
                            limit:"required",
                            param:"",
                            errInfo:"报名ID不能为空"
                        }]

                        });
             },
            editGuideSubscribe:function(){
                if(!vc.component.editGuideSubscribeValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govGuideSubscribe/updateGovGuideSubscribe',
                    JSON.stringify(vc.component.editGuideSubscribeInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGuideSubscribeModel').modal('hide');
                             vc.emit('guideSubscribeManage','listGuideSubscribe',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            refreshEditGuideSubscribeInfo:function(){
                vc.component.editGuideSubscribeInfo= {
                  wgsId:'',
caId:'',
wgId:'',
person:'',
link:'',
startTime:'',
endTime:'',
state:'',

                }
            }
        }
    });

})(window.vc,window.vc.component);
