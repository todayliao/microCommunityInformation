/**
    帮扶记录 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovHelpPolicyListInfo:{
                index:0,
                flowComponent:'viewGovHelpPolicyListInfo',
                govHelpListId:'',
caId:'',
helpId:'',
helpName:'',
poorPersonId:'',
poorPersonName:'',
cadrePersonId:'',
cadrePersonName:'',
startTime:'',
endTime:'',
listBrief:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovHelpPolicyListInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovHelpPolicyListInfo','chooseGovHelpPolicyList',function(_app){
                vc.copyObject(_app, vc.component.viewGovHelpPolicyListInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovHelpPolicyListInfo);
            });

            vc.on('viewGovHelpPolicyListInfo', 'onIndex', function(_index){
                vc.component.viewGovHelpPolicyListInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovHelpPolicyListInfoModel(){
                vc.emit('chooseGovHelpPolicyList','openChooseGovHelpPolicyListModel',{});
            },
            _openAddGovHelpPolicyListInfoModel(){
                vc.emit('addGovHelpPolicyList','openAddGovHelpPolicyListModal',{});
            },
            _loadGovHelpPolicyListInfoData:function(){

            }
        }
    });

})(window.vc);
