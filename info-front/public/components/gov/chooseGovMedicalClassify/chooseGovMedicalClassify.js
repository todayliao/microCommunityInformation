(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovMedicalClassify:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovMedicalClassifyInfo:{
                total: 0,
                records: 1,
                govMedicalClassifys:[],
                _currentGovMedicalClassifyName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovMedicalClassify','openChooseGovMedicalClassifyModel',function(_param){
                $('#chooseGovMedicalClassifyModel').modal('show');
                vc.component._refreshChooseGovMedicalClassifyInfo();
                vc.component._loadAllGovMedicalClassifyInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovMedicalClassifyInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        caId:vc.getCurrentCommunity().caId,
                        classifyName:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('/govMedicalClassify/queryGovMedicalClassify',
                             param,
                             function(json){
                                var _govMedicalClassifyInfo = JSON.parse(json);
                                vc.component.chooseGovMedicalClassifyInfo.govMedicalClassifys = _govMedicalClassifyInfo.data;
                                vc.component.chooseGovMedicalClassifyInfo.total = _govMedicalClassifyInfo.total;
                                vc.component.chooseGovMedicalClassifyInfo.records = _govMedicalClassifyInfo.records;
                                vc.emit('pagination', 'init', {
                                    total: vc.component.chooseGovMedicalClassifyInfo.records,
                                    currentPage: _page
                                });
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovMedicalClassify:function(_govMedicalClassify){
                vc.emit($props.emitChooseGovMedicalClassify,'chooseGovMedicalClassify',_govMedicalClassify);
                $('#chooseGovMedicalClassifyModel').modal('hide');
            },
            queryGovMedicalClassifys:function(){
                vc.component._loadAllGovMedicalClassifyInfo(1,10,vc.component.chooseGovMedicalClassifyInfo._currentGovMedicalClassifyName);
            },
            _refreshChooseGovMedicalClassifyInfo:function(){
                vc.component.chooseGovMedicalClassifyInfo._currentGovMedicalClassifyName = "";
            }
        }

    });
})(window.vc);
