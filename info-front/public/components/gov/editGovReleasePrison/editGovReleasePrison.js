(function(vc,vm){

    vc.extends({
        data:{
            editGovReleasePrisonInfo:{
                releasePrisonId:'',
                name:'',
                caId:'',
                address:'',
                tel:'',
                releaseTime:'',
                sentenceStartTime:'',
                sentenceEndTime:'',
                sentenceReason:'',
                ramark:'',

            }
        },
         _initMethod:function(){
            $('.editStartTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true

            });
            $('.editStartTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editStartTime").val();
                    vc.component.editGovReleasePrisonInfo.sentenceStartTime = value;
                });
            $('.editEndTime').datetimepicker({
                language: 'zh-CN',
                fontAwesome: 'fa',
                format: 'yyyy-mm-dd hh:ii:ss',
                initTime: true,
                initialDate: new Date(),
                autoClose: 1,
                todayBtn: true
            });
            $('.editEndTime').datetimepicker()
                .on('changeDate', function (ev) {
                    var value = $(".editEndTime").val();
                    vc.component.editGovReleasePrisonInfo.sentenceEndTime = value;
                });
         },
         _initEvent:function(){
             vc.on('editGovReleasePrison','openEditGovReleasePrisonModal',function(_params){
                vc.component.refreshEditGovReleasePrisonInfo();
                $('#editGovReleasePrisonModel').modal('show');
                vc.copyObject(_params, vc.component.editGovReleasePrisonInfo );
                vc.component.editGovReleasePrisonInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods:{
            editGovReleasePrisonValidate:function(){
                        return vc.validate.validate({
                            editGovReleasePrisonInfo:vc.component.editGovReleasePrisonInfo
                        },{
'editGovReleasePrisonInfo.name':[
{
                            limit:"required",
                            param:"",
                            errInfo:"刑满释放者名称不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"64",
                            errInfo:"刑满释放者名称不能超过64"
                        },
                    ],
'editGovReleasePrisonInfo.address':[
{
                            limit:"required",
                            param:"",
                            errInfo:"住址不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"512",
                            errInfo:"住址不能超过512"
                        },
                    ],
'editGovReleasePrisonInfo.tel':[
{
                            limit:"required",
                            param:"",
                            errInfo:"手机号不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"11",
                            errInfo:"手机号不能超过11"
                        },
                    ],
'editGovReleasePrisonInfo.releaseTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"服刑周期不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"36",
                            errInfo:"服刑周期不能超过36"
                        },
                    ],
'editGovReleasePrisonInfo.sentenceStartTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"服刑开始时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"时间类型",
                            errInfo:"服刑开始时间不能超过时间类型"
                        },
                    ],
'editGovReleasePrisonInfo.sentenceEndTime':[
{
                            limit:"required",
                            param:"",
                            errInfo:"服刑结束时间不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"时间类型",
                            errInfo:"服刑结束时间不能超过时间类型"
                        },
                    ],
'editGovReleasePrisonInfo.sentenceReason':[
{
                            limit:"required",
                            param:"",
                            errInfo:"服刑原因不能为空"
                        },
 {
                            limit:"maxLength",
                            param:"128",
                            errInfo:"服刑原因不能超过128"
                        },
                    ]
                        });
             },
            editGovReleasePrison:function(){
                if(!vc.component.editGovReleasePrisonValidate()){
                    vc.toast(vc.validate.errInfo);
                    return ;
                }

                vc.http.apiPost(
                    '/govReleasePrison/updateGovReleasePrison',
                    JSON.stringify(vc.component.editGovReleasePrisonInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovReleasePrisonModel').modal('hide');
                             vc.emit('govReleasePrisonManage','listGovReleasePrison',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');

                        vc.message(errInfo);
                     });
            },
            refreshEditGovReleasePrisonInfo:function(){
                vc.component.editGovReleasePrisonInfo= {
                  releasePrisonId:'',
releasePrisonId:'',
name:'',
caId:'',
address:'',
tel:'',
releaseTime:'',
sentenceStartTime:'',
sentenceEndTime:'',
sentenceReason:'',
ramark:'',

                }
            }
        }
    });

})(window.vc,window.vc.component);
