(function(vc){
    vc.extends({
        propTypes: {
           emitChooseGovVolunteerServ:vc.propTypes.string,
           emitLoadData:vc.propTypes.string
        },
        data:{
            chooseGovVolunteerServInfo:{
                govVolunteerServs:[],
                _currentGovVolunteerServName:'',
            }
        },
        _initMethod:function(){
        },
        _initEvent:function(){
            vc.on('chooseGovVolunteerServ','openChooseGovVolunteerServModel',function(_param){
                $('#chooseGovVolunteerServModel').modal('show');
                vc.component._refreshChooseGovVolunteerServInfo();
                vc.component._loadAllGovVolunteerServInfo(1,10,'');
            });
        },
        methods:{
            _loadAllGovVolunteerServInfo:function(_page,_row,_name){
                var param = {
                    params:{
                        page:_page,
                        row:_row,
                        communityId:vc.getCurrentCommunity().communityId,
                        name:_name
                    }
                };

                //发送get请求
               vc.http.apiGet('govVolunteerServ.listGovVolunteerServs',
                             param,
                             function(json){
                                var _govVolunteerServInfo = JSON.parse(json);
                                vc.component.chooseGovVolunteerServInfo.govVolunteerServs = _govVolunteerServInfo.govVolunteerServs;
                             },function(){
                                console.log('请求失败处理');
                             }
                           );
            },
            chooseGovVolunteerServ:function(_govVolunteerServ){
                if(_govVolunteerServ.hasOwnProperty('name')){
                     _govVolunteerServ.govVolunteerServName = _govVolunteerServ.name;
                }
                vc.emit($props.emitChooseGovVolunteerServ,'chooseGovVolunteerServ',_govVolunteerServ);
                vc.emit($props.emitLoadData,'listGovVolunteerServData',{
                    govVolunteerServId:_govVolunteerServ.govVolunteerServId
                });
                $('#chooseGovVolunteerServModel').modal('hide');
            },
            queryGovVolunteerServs:function(){
                vc.component._loadAllGovVolunteerServInfo(1,10,vc.component.chooseGovVolunteerServInfo._currentGovVolunteerServName);
            },
            _refreshChooseGovVolunteerServInfo:function(){
                vc.component.chooseGovVolunteerServInfo._currentGovVolunteerServName = "";
            }
        }

    });
})(window.vc);
