/**
    帮扶政策 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovHelpPolicyInfo:{
                index:0,
                flowComponent:'viewGovHelpPolicyInfo',
                govHelpId:'',
caId:'',
helpName:'',
helpTime:'',
helpBrief:'',
statusCd:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovHelpPolicyInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovHelpPolicyInfo','chooseGovHelpPolicy',function(_app){
                vc.copyObject(_app, vc.component.viewGovHelpPolicyInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovHelpPolicyInfo);
            });

            vc.on('viewGovHelpPolicyInfo', 'onIndex', function(_index){
                vc.component.viewGovHelpPolicyInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovHelpPolicyInfoModel(){
                vc.emit('chooseGovHelpPolicy','openChooseGovHelpPolicyModel',{});
            },
            _openAddGovHelpPolicyInfoModel(){
                vc.emit('addGovHelpPolicy','openAddGovHelpPolicyModal',{});
            },
            _loadGovHelpPolicyInfoData:function(){

            }
        }
    });

})(window.vc);
