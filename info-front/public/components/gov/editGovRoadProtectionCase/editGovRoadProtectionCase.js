(function (vc, vm) {

    vc.extends({
        data: {
            editGovRoadProtectionCaseInfo: {
                roadCaseId: '',
                roadProtectionId: '',
                caId: '',
                caseName: '',
                caseCode: '',
                happenedTime: '',
                happenedPlace: '',
                idType: '',
                idCard: '',
                personName: '',
                isSolved: '',
                personNum: '',
                fleeingNum: '',
                arrestsNum: '',
                detectionContent: '',
                caseContent: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovRoadProtectionCase', 'openEditGovRoadProtectionCaseModal', function (_params) {
                vc.component.refreshEditGovRoadProtectionCaseInfo();
                $('#editGovRoadProtectionCaseModel').modal('show');
                vc.copyObject(_params, vc.component.editGovRoadProtectionCaseInfo);
                vc.component.editGovRoadProtectionCaseInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovRoadProtectionCaseValidate: function () {
                return vc.validate.validate({
                    editGovRoadProtectionCaseInfo: vc.component.editGovRoadProtectionCaseInfo
                }, {
                    'editGovRoadProtectionCaseInfo.roadCaseId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "路按事件ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "路按事件ID不能超过30"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.roadProtectionId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "护路护线ID不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "护路护线ID不能超过30"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.caseName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件编号不能超过100"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.caseCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "案（事）件名称不能超过100"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.happenedTime': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生日期不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "时间类型",
                            errInfo: "发生日期不能超过时间类型"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.happenedPlace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "发生地点不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "200",
                            errInfo: "发生地点不能超过200"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.idType': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件类型不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "12",
                            errInfo: "主犯（嫌疑人）证件类型不能超过12"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.idCard': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）证件号码不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）证件号码不能超过64"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.personName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "主犯（嫌疑人）姓名不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "主犯（嫌疑人）姓名不能超过64"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.isSolved': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "是否破案不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "是否破案不能超过2"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.personNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "作案人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "作案人数不能超过3"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.fleeingNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "在逃人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "在逃人数不能超过3"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.arrestsNum': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "抓捕人数不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "3",
                            errInfo: "抓捕人数不能超过3"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.detectionContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案件侦破情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案件侦破情况不能超过长文本"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.caseContent': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "案（事）件情况不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "长文本",
                            errInfo: "案（事）件情况不能超过长文本"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.statusCd': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "数据状态不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "2",
                            errInfo: "数据状态不能超过2"
                        },
                    ],
                    'editGovRoadProtectionCaseInfo.roadCaseId': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "路案事件ID不能为空"
                        }]

                });
            },
            editGovRoadProtectionCase: function () {
                if (!vc.component.editGovRoadProtectionCaseValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/govRoadProtectionCase/updateGovRoadProtectionCase',
                    JSON.stringify(vc.component.editGovRoadProtectionCaseInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovRoadProtectionCaseModel').modal('hide');
                            vc.emit('govRoadProtectionCaseManage', 'listGovRoadProtectionCase', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovRoadProtectionCaseInfo: function () {
                vc.component.editGovRoadProtectionCaseInfo = {
                    roadCaseId: '',
                    roadProtectionId: '',
                    caId: '',
                    caseName: '',
                    caseCode: '',
                    happenedTime: '',
                    happenedPlace: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: '',
                    statusCd: '',
                }
            }
        }
    });

})(window.vc, window.vc.component);
