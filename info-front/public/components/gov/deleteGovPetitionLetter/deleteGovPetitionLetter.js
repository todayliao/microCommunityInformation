(function(vc,vm){

    vc.extends({
        data:{
            deleteGovPetitionLetterInfo:{

            }
        },
         _initMethod:function(){

         },
         _initEvent:function(){
             vc.on('deleteGovPetitionLetter','openDeleteGovPetitionLetterModal',function(_params){

                vc.component.deleteGovPetitionLetterInfo = _params;
                $('#deleteGovPetitionLetterModel').modal('show');

            });
        },
        methods:{
            deleteGovPetitionLetter:function(){
                vc.component.deleteGovPetitionLetterInfo.caId=vc.getCurrentCommunity().caId;
                vc.http.apiPost(
                    '/govPetitionLetter/deleteGovPetitionLetter',
                    JSON.stringify(vc.component.deleteGovPetitionLetterInfo),
                    {
                        emulateJSON:true
                     },
                     function(json,res){
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#deleteGovPetitionLetterModel').modal('hide');
                            vc.emit('govPetitionLetterManage','listGovPetitionLetter',{});
                            return ;
                        }
                        vc.message(_json.msg);
                     },
                     function(errInfo,error){
                        console.log('请求失败处理');
                        vc.message(json);

                     });
            },
            closeDeleteGovPetitionLetterModel:function(){
                $('#deleteGovPetitionLetterModel').modal('hide');
            }
        }
    });

})(window.vc,window.vc.component);
