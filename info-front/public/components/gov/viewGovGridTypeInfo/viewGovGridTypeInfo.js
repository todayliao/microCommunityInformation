/**
    网格类型 组件
**/
(function(vc){

    vc.extends({
        propTypes: {
           callBackListener:vc.propTypes.string, //父组件名称
           callBackFunction:vc.propTypes.string //父组件监听方法
        },
        data:{
            viewGovGridTypeInfo:{
                index:0,
                flowComponent:'viewGovGridTypeInfo',
                govTypeId:'',
caId:'',
typeName:'',
ramark:'',
statusCd:'',
datasourceType:'',

            }
        },
        _initMethod:function(){
            //根据请求参数查询 查询 业主信息
            vc.component._loadGovGridTypeInfoData();
        },
        _initEvent:function(){
            vc.on('viewGovGridTypeInfo','chooseGovGridType',function(_app){
                vc.copyObject(_app, vc.component.viewGovGridTypeInfo);
                vc.emit($props.callBackListener,$props.callBackFunction,vc.component.viewGovGridTypeInfo);
            });

            vc.on('viewGovGridTypeInfo', 'onIndex', function(_index){
                vc.component.viewGovGridTypeInfo.index = _index;
            });

        },
        methods:{

            _openSelectGovGridTypeInfoModel(){
                vc.emit('chooseGovGridType','openChooseGovGridTypeModel',{});
            },
            _openAddGovGridTypeInfoModel(){
                vc.emit('addGovGridType','openAddGovGridTypeModal',{});
            },
            _loadGovGridTypeInfoData:function(){

            }
        }
    });

})(window.vc);
