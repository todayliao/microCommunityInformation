(function (vc, vm) {

    vc.extends({
        data: {
            editGovPetitionTypeInfo: {
                typeId: '',
                typeId: '',
                typeName: '',
                typeDesc: '',
                seq: '',
                caId: '',
                statusCd: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('editGovPetitionType', 'openEditGovPetitionTypeModal', function (_params) {
                vc.component.refreshEditGovPetitionTypeInfo();
                $('#editGovPetitionTypeModel').modal('show');
                vc.copyObject(_params, vc.component.editGovPetitionTypeInfo);
                vc.component.editGovPetitionTypeInfo.caId = vc.getCurrentCommunity().caId;
            });
        },
        methods: {
            editGovPetitionTypeValidate: function () {
                return vc.validate.validate({
                    editGovPetitionTypeInfo: vc.component.editGovPetitionTypeInfo
                }, {
                
                    'editGovPetitionTypeInfo.typeName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "100",
                            errInfo: "类型名称不能超过100"
                        },
                    ],
                    'editGovPetitionTypeInfo.typeDesc': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "类型描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "500",
                            errInfo: "类型描述不能超过500"
                        },
                    ],
                    'editGovPetitionTypeInfo.seq': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "顺序不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "11",
                            errInfo: "顺序不能超过11"
                        },
                    ]
                });
            },
            editGovPetitionType: function () {
                if (!vc.component.editGovPetitionTypeValidate()) {
                    vc.toast(vc.validate.errInfo);
                    return;
                }
                vc.http.apiPost(
                    '/govPetitionType/updateGovPetitionType',
                    JSON.stringify(vc.component.editGovPetitionTypeInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#editGovPetitionTypeModel').modal('hide');
                            vc.emit('govPetitionTypeManage', 'listGovPetitionType', {});
                            return;
                        }
                        vc.message(_json.msg);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);
                    });
            },
            refreshEditGovPetitionTypeInfo: function () {
                vc.component.editGovPetitionTypeInfo = {
                    typeId: '',
                    typeId: '',
                    typeName: '',
                    typeDesc: '',
                    seq: '',
                    caId: '',
                    statusCd: '',

                }
            }
        }
    });

})(window.vc, window.vc.component);
