(function (vc) {

    vc.extends({
        propTypes: {
            callBackListener: vc.propTypes.string, //父组件名称
            callBackFunction: vc.propTypes.string //父组件监听方法
        },
        data: {
            addGovCommunityAreaInfo: {
                caId: '',
                caCode: '',
                caName: '',
                caSpace: '',
                areaCode: '',
                caAddress: '',
                person: '',
                personLink: '',
                ramark: '',

            }
        },
        _initMethod: function () {

        },
        _initEvent: function () {
            vc.on('addGovCommunityArea', 'openAddGovCommunityAreaModal', function () {
                $('#addGovCommunityAreaModel').modal('show');
            });
        },
        methods: {
            addGovCommunityAreaValidate() {
                return vc.validate.validate({
                    addGovCommunityAreaInfo: vc.component.addGovCommunityAreaInfo
                }, {
                    'addGovCommunityAreaInfo.caCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域编号不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "区域编号超长"
                        },
                    ],
                    'addGovCommunityAreaInfo.caName': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域名称不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "128",
                            errInfo: "区域名称超长"
                        },
                    ],
                    'addGovCommunityAreaInfo.caSpace': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域面积不能为空"
                        },
                        {
                            limit: "num",
                            param: "",
                            errInfo: "区域面积必须是数字"
                        },
                    ],
                    'addGovCommunityAreaInfo.areaCode': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "城市区域不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "30",
                            errInfo: "区域名称超长"
                        },
                    ],
                    'addGovCommunityAreaInfo.caAddress': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "区域地址不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "256",
                            errInfo: "区域地址超长"
                        },
                    ],
                    'addGovCommunityAreaInfo.person': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "64",
                            errInfo: "责任人超长"
                        },
                    ],
                    'addGovCommunityAreaInfo.personLink': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "责任人电话不能为空"
                        },
                        {
                            limit: "phone",
                            param: "",
                            errInfo: "责任人电话格式错误"
                        },
                    ],
                    'addGovCommunityAreaInfo.ramark': [
                        {
                            limit: "required",
                            param: "",
                            errInfo: "描述不能为空"
                        },
                        {
                            limit: "maxLength",
                            param: "1024",
                            errInfo: "描述太长"
                        },
                    ],




                });
            },
            saveGovCommunityAreaInfo: function () {
                if (!vc.component.addGovCommunityAreaValidate()) {
                    vc.toast(vc.validate.errInfo);

                    return;
                }

                //vc.component.addGovCommunityAreaInfo.communityId = vc.getCurrentCommunity().communityId;
                //不提交数据将数据 回调给侦听处理
                if (vc.notNull($props.callBackListener)) {
                    vc.emit($props.callBackListener, $props.callBackFunction, vc.component.addGovCommunityAreaInfo);
                    $('#addGovCommunityAreaModel').modal('hide');
                    return;
                }

                vc.http.apiPost(
                    '/govCommunityArea/saveGovCommunityArea',
                    JSON.stringify(vc.component.addGovCommunityAreaInfo),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        let _json = JSON.parse(json);
                        if (_json.code == 0) {
                            //关闭model
                            $('#addGovCommunityAreaModel').modal('hide');
                            vc.component.clearAddGovCommunityAreaInfo();
                            vc.emit('govCommunityAreaManage', 'listGovCommunityArea', {});

                            return;
                        }
                        vc.message(_json.msg);

                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');

                        vc.message(errInfo);

                    });
            },
            clearAddGovCommunityAreaInfo: function () {
                vc.component.addGovCommunityAreaInfo = {
                    caCode: '',
                    caName: '',
                    caSpace: '',
                    areaCode: '',
                    caAddress: '',
                    person: '',
                    personLink: '',
                    remark: '',

                };
            }
        }
    });

})(window.vc);
