/**
    志愿人员 组件
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            viewGovVolunteerInfo: {
                volunteerId: '',
                govPersonId: '',
                name: '',
                caId: '',
                address: '',
                tel: '',
                student: '',
                company: '',
                volWork: '',
                edu: '',
                stateName: '',
                reviewComm: '',
                goodAtSkills: [],
                freeTimes: [],
                freeTime: [],
                ramark: '',
                politicalOutlooks: [],
                edus: [],
                goodAtSkillss: [],
                volunteerServ: [],
                volunteerServs: [],
                persons: {
                    idType: '1',
                    idCard: '',
                    politicalOutlook: '',
                    nativePlace: '',
                    nation: '',
                    personSex: '',

                }

            },
            govOldAccountDetail: {
                acctId:'',
                acctName: '',
                amount: '',
                createTime: '',
                govOldAccountDetails: [],
                conditions: {
                    detailType: '',
                    orderId: ''
                }
            }
        },
        _initMethod: function () {

            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.viewGovVolunteerInfo.politicalOutlooks = _data;
            });
            vc.getDict('gov_volunteer', "edu", function (_data) {
                vc.component.viewGovVolunteerInfo.edus = _data;
            });
            vc.getDict('gov_volunteer', "good_at_skills", function (_data) {
                vc.component.viewGovVolunteerInfo.goodAtSkillss = _data;
            });
            vc.getDict('gov_volunteer', "free_time", function (_data) {
                vc.component.viewGovVolunteerInfo.freeTimes = _data;
            });
            let _volunteerId = vc.getParam("volunteerId");
            $that.viewGovVolunteerInfo.volunteerId = _volunteerId;
            $that._listViewGovServFields();
            //根据请求参数查询 查询 业主信息
            vc.component._listGovVolunteers(_volunteerId);
            $that._queryGovVolunteerAccountDetail(DEFAULT_PAGE,DEFAULT_ROWS,_volunteerId);
        },
        _initEvent: function () {
            vc.on('viewGovVolunteerInfo', 'chooseGovVolunteer', function (_app) {
                vc.copyObject(_app, vc.component.viewGovVolunteerInfo);
            });

            vc.on('viewGovVolunteerInfo', 'onIndex', function (_index) {
                vc.component.viewGovVolunteerInfo.index = _index;
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._queryGovVolunteerAccountDetail(_currentPage, DEFAULT_ROWS, $that.viewGovVolunteerInfo.volunteerId);
            });
        },
        methods: {

            _openSelectGovVolunteerInfoModel() {
                vc.emit('chooseGovVolunteer', 'openChooseGovVolunteerModel', {});
            },
            _openAddGovVolunteerInfoModel() {
                vc.emit('addGovVolunteer', 'openAddGovVolunteerModal', {});
            },
            _listGovVolunteers: function (_volunteerId) {

                var param = {
                    params: {
                        caId: vc.getCurrentCommunity().caId,
                        volunteerId: _volunteerId,
                        page: 1,
                        row: 10
                    }
                };

                //发送get请求
                vc.http.apiGet('/govVolunteer/queryGovVolunteer',
                    param,
                    function (json, res) {
                        var _govVolunteerManageInfo = JSON.parse(json);
                        vc.copyObject(_govVolunteerManageInfo.data[0], vc.component.viewGovVolunteerInfo);
                        vc.copyObject(_govVolunteerManageInfo.data[0], vc.component.viewGovVolunteerInfo.persons);
                        console.log(vc.component.viewGovVolunteerInfo);

                        vc.component.viewGovVolunteerInfo.edu = $that._getspecEduName(vc.component.viewGovVolunteerInfo.edu);
                        vc.component.viewGovVolunteerInfo.goodAtSkills = $that._getspecGoodName(vc.component.viewGovVolunteerInfo.goodAtSkills);
                        vc.component.viewGovVolunteerInfo.freeTime = $that._getspecFreeName(vc.component.viewGovVolunteerInfo.freeTime);
                        vc.component.viewGovVolunteerInfo.volunteerServ = $that._getspecServName(vc.component.viewGovVolunteerInfo.volunteerServ);
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listViewGovServFields: function () {
                var param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                        isShow: 'Y'
                    }
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.viewGovVolunteerInfo.volunteerServs = _govServFieldManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryGovVolunteerAccountDetail: function (_page,_row,_volunteerId) {
                var param = {
                    params: {
                        volunteerId: _volunteerId,
                        caId: vc.getCurrentCommunity().caId,
                        page: _page,
                        row: _row,
                        detailType: $that.govOldAccountDetail.conditions.detailType,
                        orderId: $that.govOldAccountDetail.conditions.orderId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govVolunteer/getVolunteerAccountDetail',
                    param,
                    function (json, res) {
                        vc.component.govOldAccountDetail.govOldAccountDetails = [];
                        var _govOldPersonManageInfo = JSON.parse(json);
                        vc.copyObject(_govOldPersonManageInfo.data[0], vc.component.govOldAccountDetail);
                        vc.component.govOldAccountDetail.govOldAccountDetails = _govOldPersonManageInfo.data[0].accountDetailDtos.data;
                        vc.emit('pagination', 'init', {
                            total: _govOldPersonManageInfo.data[0].accountDetailDtos.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryAccountDetailMethod: function () {
                _queryGovOldAccountDetail($that.viewGovOldPersonInfo.oldId);
            },
            _getspecEduName: function (_edu) {
                let _retutest = '';
                console.log($that.viewGovVolunteerInfo);
                console.log(_edu);
                $that.viewGovVolunteerInfo.edus.forEach(_item => {
                    if (_item.statusCd == _edu) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecGoodName: function (_str) {
                let _retutest = '';
                let _tempGoodAtSkills = JSON.parse(_str);
                $that.viewGovVolunteerInfo.goodAtSkillss.forEach(_item => {
                    _tempGoodAtSkills.forEach(_tempItem => {
                        if (_item.statusCd == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecFreeName: function (_str) {
                let _retutest = '';
                let _tempFreeTime = JSON.parse(_str);
                $that.viewGovVolunteerInfo.freeTimes.forEach(_item => {
                    _tempFreeTime.forEach(_tempItem => {
                        if (_item.statusCd == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _getspecServName: function (_str) {
                let _retutest = '';
                let _tempServ = JSON.parse(_str);
                $that.viewGovVolunteerInfo.volunteerServs.forEach(_item => {
                    _tempServ.forEach(_tempItem => {
                        if (_item.servId == _tempItem) {
                            _retutest = _item.name + ',' + _retutest;
                        }
                    })
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _goBack: function () {
                vc.goBack();
            }
        }
    });

}) (window.vc);
