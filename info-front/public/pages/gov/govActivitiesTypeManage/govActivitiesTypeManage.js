/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govActivitiesTypeManageInfo: {
                govActivitiesTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeCd: '',
                conditions: {
                    caId : vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovActivitiesTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govActivitiesTypeManage', 'listGovActivitiesType', function (_param) {
                vc.component._listGovActivitiesTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovActivitiesTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovActivitiesTypes: function (_page, _rows) {

                vc.component.govActivitiesTypeManageInfo.conditions.page = _page;
                vc.component.govActivitiesTypeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govActivitiesTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govActivitiesType/queryGovActivitiesType',
                    param,
                    function (json, res) {
                        var _govActivitiesTypeManageInfo = JSON.parse(json);
                        vc.component.govActivitiesTypeManageInfo.total = _govActivitiesTypeManageInfo.total;
                        vc.component.govActivitiesTypeManageInfo.records = _govActivitiesTypeManageInfo.records;
                        vc.component.govActivitiesTypeManageInfo.govActivitiesTypes = _govActivitiesTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govActivitiesTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovActivitiesTypeModal: function () {
                vc.emit('addGovActivitiesType', 'openAddGovActivitiesTypeModal', {});
            },
            _openEditGovActivitiesTypeModel: function (_govActivitiesType) {
                vc.emit('editGovActivitiesType', 'openEditGovActivitiesTypeModal', _govActivitiesType);
            },
            _openDeleteGovActivitiesTypeModel: function (_govActivitiesType) {
                vc.emit('deleteGovActivitiesType', 'openDeleteGovActivitiesTypeModal', _govActivitiesType);
            },
            _queryGovActivitiesTypeMethod: function () {
                vc.component._listGovActivitiesTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govActivitiesTypeManageInfo.moreCondition) {
                    vc.component.govActivitiesTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govActivitiesTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
