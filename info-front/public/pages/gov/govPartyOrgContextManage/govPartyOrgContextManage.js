/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPartyOrgContextManageInfo: {
                govPartyOrgContexts: [],
                total: 0,
                records: 1,
                moreCondition: false,
                orgId: '',
                conditions: {
                    orgId: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPartyOrgContexts(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPartyOrgContextManage', 'listGovPartyOrgContext', function (_param) {
                vc.component._listGovPartyOrgContexts(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPartyOrgContexts(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPartyOrgContexts: function (_page, _rows) {

                vc.component.govPartyOrgContextManageInfo.conditions.page = _page;
                vc.component.govPartyOrgContextManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPartyOrgContextManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrgContext/queryGovPartyOrgContext',
                    param,
                    function (json, res) {
                        var _govPartyOrgContextManageInfo = JSON.parse(json);
                        vc.component.govPartyOrgContextManageInfo.total = _govPartyOrgContextManageInfo.total;
                        vc.component.govPartyOrgContextManageInfo.records = _govPartyOrgContextManageInfo.records;
                        vc.component.govPartyOrgContextManageInfo.govPartyOrgContexts = _govPartyOrgContextManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPartyOrgContextManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPartyOrgContextModal: function () {
                vc.emit('addGovPartyOrgContext', 'openAddGovPartyOrgContextModal', {});
            },
            _openEditGovPartyOrgContextModel: function (_govPartyOrgContext) {
                vc.emit('editGovPartyOrgContext', 'openEditGovPartyOrgContextModal', _govPartyOrgContext);
            },
            _openDeleteGovPartyOrgContextModel: function (_govPartyOrgContext) {
                vc.emit('deleteGovPartyOrgContext', 'openDeleteGovPartyOrgContextModal', _govPartyOrgContext);
            },
            _queryGovPartyOrgContextMethod: function () {
                vc.component._listGovPartyOrgContexts(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPartyOrgContextManageInfo.moreCondition) {
                    vc.component.govPartyOrgContextManageInfo.moreCondition = false;
                } else {
                    vc.component.govPartyOrgContextManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
