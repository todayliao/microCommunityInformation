/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPersonManageInfo: {
                govPersons: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govPersonId: '',
                personTypeIs: 'govPersonInfo',
                govCommunityAreas: [],
                govCompanys:[],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    personType: '8008',
                    personSex: '',
                    idCard: '',
                    govCompanyId: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCompanys(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPersonManage', 'listGovPerson', function (_param) {
                vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPersons(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPersons: function (_page, _rows) {

                vc.component.govPersonManageInfo.conditions.page = _page;
                vc.component.govPersonManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPersonManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPersonCompany/queryGovPersonCompany',
                    param,
                    function (json, res) {
                        var _govPersonManageInfo = JSON.parse(json);
                        vc.component.govPersonManageInfo.total = _govPersonManageInfo.total;
                        vc.component.govPersonManageInfo.records = _govPersonManageInfo.records;
                        vc.component.govPersonManageInfo.govPersons = _govPersonManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPersonManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCompanys: function (_page, _rows) {

               
                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId,
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCompany/queryGovCompany',
                    param,
                    function (json, res) {
                        var _govCompanyManageInfo = JSON.parse(json);
                        vc.component.govPersonManageInfo.total = _govCompanyManageInfo.total;
                        vc.component.govPersonManageInfo.records = _govCompanyManageInfo.records;
                        vc.component.govPersonManageInfo.govCompanys = _govCompanyManageInfo.data;

                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govPersonManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govPersonManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govPersonManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPersonModal: function () {
                let _personType = $that.govPersonManageInfo.conditions.personType;
                vc.emit('addGovPerson', 'openAddGovPersonModal', {_personType});
            },
            _openEditGovPersonModel: function (_govPerson) {
                vc.emit('editGovPerson', 'openEditGovPersonModal', _govPerson);
            },
            _openDeleteGovPersonModel: function (_govPerson) {
                vc.emit('deleteGovPerson', 'openDeleteGovPersonModal', _govPerson);
            },
            _queryGovPersonMethod: function () {
                vc.component._listGovPersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPersonManageInfo.moreCondition) {
                    vc.component.govPersonManageInfo.moreCondition = false;
                } else {
                    vc.component.govPersonManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
