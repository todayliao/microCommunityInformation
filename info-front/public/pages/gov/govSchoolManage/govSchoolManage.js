/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govSchoolManageInfo: {
                govSchools: [],
                schoolTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                schoolId: '',
                conditions: {
                    schoolId: '',
                    caId: '',
                    schoolName: '',
                    schoolAddress: '',
                    schoolType: '',
                    areaCode: '',
                    studentNum: '',
                    schoolmaster: '',
                    masterTel: '',
                    partSafetyLeader: '',
                    partLeaderTel: '',
                    safetyLeader: '',
                    leaderTel: '',
                    securityLeader: '',
                    securityLeaderTel: '',
                    securityPersonnelNum: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovSchools(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.getDict('gov_school', "school_type", function (_data) {
                vc.component.govSchoolManageInfo.schoolTypes = _data;
            });
        },
        _initEvent: function () {

            vc.on('govSchoolManage', 'listGovSchool', function (_param) {
                vc.component._listGovSchools(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovSchools(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovSchools: function (_page, _rows) {
                vc.component.govSchoolManageInfo.conditions.page = _page;
                vc.component.govSchoolManageInfo.conditions.row = _rows;
                vc.component.govSchoolManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govSchoolManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/govSchool/queryGovSchool',
                    param,
                    function (json, res) {
                        var _govSchoolManageInfo = JSON.parse(json);
                        vc.component.govSchoolManageInfo.total = _govSchoolManageInfo.total;
                        vc.component.govSchoolManageInfo.records = _govSchoolManageInfo.records;
                        vc.component.govSchoolManageInfo.govSchools = _govSchoolManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govSchoolManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSchoolTypes: function (_value) {
                let _retutest = '';
                $that.govSchoolManageInfo.schoolTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _openAddGovSchoolModal: function () {
                vc.emit('addGovSchool', 'openAddGovSchoolModal', {});
            },
            _openEditGovSchoolModel: function (_govSchool) {
                vc.emit('editGovSchool', 'openEditGovSchoolModal', _govSchool);
            },
            _openDeleteGovSchoolModel: function (_govSchool) {
                vc.emit('deleteGovSchool', 'openDeleteGovSchoolModal', _govSchool);
            },
            _queryGovSchoolMethod: function () {
                vc.component._listGovSchools(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govSchoolManageInfo.moreCondition) {
                    vc.component.govSchoolManageInfo.moreCondition = false;
                } else {
                    vc.component.govSchoolManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
