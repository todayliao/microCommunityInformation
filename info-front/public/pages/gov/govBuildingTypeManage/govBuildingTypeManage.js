/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govBuildingTypeManageInfo: {
                govBuildingTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeId: '',
                conditions: {
                    typeName: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovBuildingTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govBuildingTypeManage', 'listGovBuildingType', function (_param) {
                vc.component._listGovBuildingTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovBuildingTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovBuildingTypes: function (_page, _rows) {

                vc.component.govBuildingTypeManageInfo.conditions.page = _page;
                vc.component.govBuildingTypeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govBuildingTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govBuildingType/queryGovBuildingType',
                    param,
                    function (json, res) {
                        var _govBuildingTypeManageInfo = JSON.parse(json);
                        vc.component.govBuildingTypeManageInfo.total = _govBuildingTypeManageInfo.total;
                        vc.component.govBuildingTypeManageInfo.records = _govBuildingTypeManageInfo.records;
                        vc.component.govBuildingTypeManageInfo.govBuildingTypes = _govBuildingTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govBuildingTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovBuildingTypeModal: function () {
                vc.emit('addGovBuildingType', 'openAddGovBuildingTypeModal', {});
            },
            _openEditGovBuildingTypeModel: function (_govBuildingType) {
                vc.emit('editGovBuildingType', 'openEditGovBuildingTypeModal', _govBuildingType);
            },
            _openDeleteGovBuildingTypeModel: function (_govBuildingType) {
                vc.emit('deleteGovBuildingType', 'openDeleteGovBuildingTypeModal', _govBuildingType);
            },
            _queryGovBuildingTypeMethod: function () {
                vc.component._listGovBuildingTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govBuildingTypeManageInfo.moreCondition) {
                    vc.component.govBuildingTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govBuildingTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
