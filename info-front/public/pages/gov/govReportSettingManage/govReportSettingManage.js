/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govReportSettingManageInfo: {
                govReportSettings: [],
                total: 0,
                records: 1,
                moreCondition: false,
                settingId: '',
                govCommunityAreas: [],
                conditions: {
                    reportTypeName: '',
                    reportWay: '',
                    returnVisitFlag: '',
                    caId: vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovReportSettings(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._queryGovCommunityAreas();
        },
        _initEvent: function () {

            vc.on('govReportSettingManage', 'listGovReportSetting', function (_param) {
                vc.component._listGovReportSettings(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovReportSettings(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovReportSettings: function (_page, _rows) {

                vc.component.govReportSettingManageInfo.conditions.page = _page;
                vc.component.govReportSettingManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govReportSettingManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govReportSetting/queryGovReportSetting',
                    param,
                    function (json, res) {
                        var _govReportSettingManageInfo = JSON.parse(json);
                        vc.component.govReportSettingManageInfo.total = _govReportSettingManageInfo.total;
                        vc.component.govReportSettingManageInfo.records = _govReportSettingManageInfo.records;
                        vc.component.govReportSettingManageInfo.govReportSettings = _govReportSettingManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govReportSettingManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _queryGovCommunityAreas: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govCommunityArea/queryGovCommunityArea',
                    param,
                    function (json, res) {
                        var _govCommunityAreaManageInfo = JSON.parse(json);
                        vc.component.govReportSettingManageInfo.total = _govCommunityAreaManageInfo.total;
                        vc.component.govReportSettingManageInfo.records = _govCommunityAreaManageInfo.records;
                        vc.component.govReportSettingManageInfo.govCommunityAreas = _govCommunityAreaManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovReportSettingModal: function () {
                vc.emit('addGovReportSetting', 'openAddGovReportSettingModal', {});
            },
            _openEditGovReportSettingModel: function (_govReportSetting) {
                vc.emit('editGovReportSetting', 'openEditGovReportSettingModal', _govReportSetting);
            },
            _openDeleteGovReportSettingModel: function (_govReportSetting) {
                vc.emit('deleteGovReportSetting', 'openDeleteGovReportSettingModal', _govReportSetting);
            },
            _queryGovReportSettingMethod: function () {
                vc.component._listGovReportSettings(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govReportSettingManageInfo.moreCondition) {
                    vc.component.govReportSettingManageInfo.moreCondition = false;
                } else {
                    vc.component.govReportSettingManageInfo.moreCondition = true;
                }
            },
            _openGovReportTypeUser: function (_govReportSetting) {
                //跳转到浏览器
                vc.jumpToPage('/admin.html#/pages/gov/govReportTypeUserManage?reportType='+_govReportSetting.reportType+'&reportTypeName='+_govReportSetting.reportTypeName);
           },
           

        }
    });
})(window.vc);
