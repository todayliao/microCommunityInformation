/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govProtectionCaseManageInfo: {
                govSchools:[],
                govProtectionCases: [],
                govRoadProtections: [],
                idTypes:[],
                caseTypes:[],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caseId: '',
                    objType: '',
                    objId: '',
                    caId: '',
                    caseName: '',
                    caseCode: '',
                    areaCode: '',
                    happenedTime: '',
                    happenedPlace: '',
                    caseType: '',
                    idType: '',
                    idCard: '',
                    personName: '',
                    isSolved: '',
                    personNum: '',
                    fleeingNum: '',
                    arrestsNum: '',
                    detectionContent: '',
                    caseContent: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovRoadProtections();
            vc.component._listGovSchools();
            vc.getDict('gov_road_protection_case', "case_type", function (_data) {
                vc.component.govProtectionCaseManageInfo.caseTypes = _data;
            });
            vc.getDict('gov_road_protection_case', "id_type", function (_data) {
                vc.component.govProtectionCaseManageInfo.idTypes = _data;
            });
            vc.initDate('qHappenedTime', function (_value) {
                $that.govProtectionCaseManageInfo.conditions.happenedTime = _value;
            });
        },
        _initEvent: function () {

            vc.on('govProtectionCaseManage', 'listGovProtectionCase', function (_param) {
                vc.component._listGovProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovProtectionCases(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            
            _listGovProtectionCases: function (_page, _rows) {

                vc.component.govProtectionCaseManageInfo.conditions.page = _page;
                vc.component.govProtectionCaseManageInfo.conditions.row = _rows;
                vc.component.govProtectionCaseManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govProtectionCaseManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govProtectionCase/queryGovProtectionCase',
                    param,
                    function (json, res) {
                        var _govProtectionCaseManageInfo = JSON.parse(json);
                        vc.component.govProtectionCaseManageInfo.total = _govProtectionCaseManageInfo.total;
                        vc.component.govProtectionCaseManageInfo.records = _govProtectionCaseManageInfo.records;
                        vc.component.govProtectionCaseManageInfo.govProtectionCases = _govProtectionCaseManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govProtectionCaseManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovSchools: function () {
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page :1,
                        row :50}
                };
                //发送get请求
                vc.http.apiGet('/govSchool/queryGovSchool',
                    param,
                    function (json, res) {
                        var _govSchoolManageInfo = JSON.parse(json);
                        vc.component.govProtectionCaseManageInfo.govSchools = _govSchoolManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovRoadProtections: function () {
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page :1,
                        row :50
                    }
                };
                //发送get请求
                vc.http.apiGet('/govRoadProtection/queryGovRoadProtection',
                    param,
                    function (json, res) {
                        var _govRoadProtectionManageInfo = JSON.parse(json);
                        vc.component.govProtectionCaseManageInfo.govRoadProtections = _govRoadProtectionManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getIdType: function (_value) { 
                let _retutest = '';
                $that.govProtectionCaseManageInfo.idTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _getCaseType: function (_value) {
                let _retutest = '';
                $that.govProtectionCaseManageInfo.caseTypes.forEach(_item => {
                    if (_item.statusCd == _value) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _openAddGovProtectionCaseModal: function () {
                vc.emit('addGovProtectionCase', 'openAddGovProtectionCaseModal', {});
            },
            _openEditGovProtectionCaseModel: function (_govProtectionCase) {
                vc.emit('editGovProtectionCase', 'openEditGovProtectionCaseModal', _govProtectionCase);
            },
            _openDeleteGovProtectionCaseModel: function (_govProtectionCase) {
                vc.emit('deleteGovProtectionCase', 'openDeleteGovProtectionCaseModal', _govProtectionCase);
            },
            _queryGovProtectionCaseMethod: function () {
                vc.component._listGovProtectionCases(DEFAULT_PAGE, DEFAULT_ROWS);

            }
        }
    });
})(window.vc);
