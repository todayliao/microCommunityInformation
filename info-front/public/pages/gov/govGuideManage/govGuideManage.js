/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govGuideManageInfo:{
                govGuides:[],
                total:0,
                records:1,
                moreCondition:false,
                wgId:'',
                conditions:{
                    caId:'',
                    guideName:'',
                    person:'',
                    link:'',
                }
            }
        },
        _initMethod:function(){
            vc.component._listGovGuides(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            
            vc.on('govGuideManage','listGovGuide',function(_param){
                  vc.component._listGovGuides(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovGuides(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovGuides:function(_page, _rows){
                vc.component.govGuideManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                vc.component.govGuideManageInfo.conditions.page = _page;
                vc.component.govGuideManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.govGuideManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govWorkGuide/queryGovWorkGuide',
                             param,
                             function(json,res){
                                var _govGuideManageInfo=JSON.parse(json);
                                vc.component.govGuideManageInfo.total = _govGuideManageInfo.total;
                                vc.component.govGuideManageInfo.records = _govGuideManageInfo.records;
                                vc.component.govGuideManageInfo.govGuides = _govGuideManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govGuideManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovGuideModal:function(){
                vc.emit('addGovGuide','openAddGovGuideModal',{});
            },
            _openEditGovGuideModel:function(_govGuide){
                vc.emit('editGovGuide','openEditGovGuideModal',_govGuide);
            },
            _openDeleteGovGuideModel:function(_govGuide){
                vc.emit('deleteGovGuide','openDeleteGovGuideModal',_govGuide);
            },
            _queryGovGuideMethod:function(){
                vc.component._listGovGuides(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govGuideManageInfo.moreCondition){
                    vc.component.govGuideManageInfo.moreCondition = false;
                }else{
                    vc.component.govGuideManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
