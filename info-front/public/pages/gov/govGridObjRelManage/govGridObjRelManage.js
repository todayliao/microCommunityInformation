/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govGridObjRelManageInfo: {
                govGridObjRels: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                govGridId: '',
                typeName: '',
                personName: '',
                conditions: {
                    govGridId: '',
                    caId: ''
                }
            }
        },
        _initMethod: function () {
            $that.govGridObjRelManageInfo.conditions.govGridId = vc.getParam('govGridId');
            $that.govGridObjRelManageInfo.govGridId = vc.getParam('govGridId');
            $that.govGridObjRelManageInfo.typeName = vc.getParam('typeName');
            $that.govGridObjRelManageInfo.personName = vc.getParam('personName');
            vc.component._listGovGridObjRels(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govGridObjRelManage', 'listGovGridObjRel', function (_param) {
                vc.component._listGovGridObjRels(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovGridObjRels(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovGridObjRels: function (_page, _rows) {

                vc.component.govGridObjRelManageInfo.conditions.page = _page;
                vc.component.govGridObjRelManageInfo.conditions.row = _rows;
                vc.component.govGridObjRelManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govGridObjRelManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govGridObjRel/queryGovGridObjRel',
                    param,
                    function (json, res) {
                        var _govGridObjRelManageInfo = JSON.parse(json);
                        vc.component.govGridObjRelManageInfo.total = _govGridObjRelManageInfo.total;
                        vc.component.govGridObjRelManageInfo.records = _govGridObjRelManageInfo.records;
                        vc.component.govGridObjRelManageInfo.govGridObjRels = _govGridObjRelManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govGridObjRelManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovGridObjRelModal: function () {
                vc.emit('addGovGridObjRel', 'openAddGovGridObjRelModal', $that.govGridObjRelManageInfo.govGridId);
            },
            _openEditGovGridObjRelModel: function (_govGridObjRel) {
                vc.emit('editGovGridObjRel', 'openEditGovGridObjRelModal', _govGridObjRel);
            },
            _openDeleteGovGridObjRelModel: function (_govGridObjRel) {
                vc.emit('deleteGovGridObjRel', 'openDeleteGovGridObjRelModal', _govGridObjRel);
            },
            _queryGovGridObjRelMethod: function () {
                vc.component._listGovGridObjRels(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _getspecName: function (_objTypeCd) {
                let _retutest = '';
                $that.govGridObjRelManageInfo.objTypeCds.forEach(_item => {
                    if (_item.statusCd == _objTypeCd) {
                        _retutest = _item.name;
                    }
                });
                return _retutest;
            },
            _goBack: function () {
                vc.goBack();
            },
            _moreCondition: function () {
                if (vc.component.govGridObjRelManageInfo.moreCondition) {
                    vc.component.govGridObjRelManageInfo.moreCondition = false;
                } else {
                    vc.component.govGridObjRelManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
