/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            perGovActivitiesManageInfo: {
                perGovActivitiess: [],
                total: 0,
                records: 1,
                moreCondition: false,
                perActivitiesId: '',
                componentShow: 'perGovActivitiesManage',
                conditions: {
                    perActivitiesId: '',
                    title: '',
                    caId: '',
                    context: '',
                    userId: '',
                    userName: '',
                    startTime: '',
                    endTime: '',
                    activityTime: '',
                    state: '',
                    statusCd: '',

                }
            }
        },
        _initMethod: function () {
            vc.initDate('qActivityTime', function (_value) {
                $that.perGovActivitiesManageInfo.conditions.activityTime = _value;
            });
            vc.component._listPerGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('perGovActivitiesManage', 'listPerGovActivities', function (_param) {
                $that.perGovActivitiesManageInfo.componentShow ='perGovActivitiesManage';
                vc.component._listPerGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listPerGovActivitiess(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listPerGovActivitiess: function (_page, _rows) {
                vc.component.perGovActivitiesManageInfo.conditions.page = _page;
                vc.component.perGovActivitiesManageInfo.conditions.row = _rows;
                vc.component.perGovActivitiesManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.perGovActivitiesManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/perGovActivities/queryPerGovActivities',
                    param,
                    function (json, res) {
                        var _perGovActivitiesManageInfo = JSON.parse(json);
                        vc.component.perGovActivitiesManageInfo.total = _perGovActivitiesManageInfo.total;
                        vc.component.perGovActivitiesManageInfo.records = _perGovActivitiesManageInfo.records;
                        vc.component.perGovActivitiesManageInfo.perGovActivitiess = _perGovActivitiesManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.perGovActivitiesManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddPerGovActivitiesModal: function () {
                $that.perGovActivitiesManageInfo.componentShow = 'addPreBirthList';
            },
            _goBack:function(){
                vc.goBack();
            },
            _openEditPerGovActivitiesModel: function (_perGovActivities) {
                $that.perGovActivitiesManageInfo.componentShow = 'editPreBirthList';
            },
            _openDeletePerGovActivitiesModel: function (_perGovActivities) {
                vc.emit('deletePerGovActivities', 'openDeletePerGovActivitiesModal', _perGovActivities);
            },
            _openGovOldBirthModel: function (_perGovActivities) {
                vc.jumpToPage('/admin.html#/pages/gov/viewPerGovActivitiesInfo?perActivitiesId=' + _perGovActivities.perActivitiesId);
            },
            _queryPerGovActivitiesMethod: function () {
                vc.component._listPerGovActivitiess(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.perGovActivitiesManageInfo.moreCondition) {
                    vc.component.perGovActivitiesManageInfo.moreCondition = false;
                } else {
                    vc.component.perGovActivitiesManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
