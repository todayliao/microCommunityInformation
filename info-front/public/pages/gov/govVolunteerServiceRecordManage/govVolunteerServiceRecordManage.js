/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govVolunteerServiceRecordManageInfo: {
                govVolunteerServiceRecords: [],
                total: 0,
                records: 1,
                moreCondition: false,
                serviceRecordId: '',
                componentShow: 'govVolunteerServiceRecordManage',
                conditions: {
                    serviceRecordId: '',
                    personName: '',
                    volunteerName: '',
                    tel: '',
                    caId: '',
                    title: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovVolunteerServiceRecords(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {
            vc.on('govVolunteerServiceRecordManage', 'listGovVolunteerServiceRecord', function (_param) {
                $that.govVolunteerServiceRecordManageInfo.componentShow ='govVolunteerServiceRecordManage';
                vc.component._listGovVolunteerServiceRecords(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovVolunteerServiceRecords(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovVolunteerServiceRecords: function (_page, _rows) {

                vc.component.govVolunteerServiceRecordManageInfo.conditions.page = _page;
                vc.component.govVolunteerServiceRecordManageInfo.conditions.row = _rows;
                vc.component.govVolunteerServiceRecordManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govVolunteerServiceRecordManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govVolunteerServiceRecord/queryGovVolunteerServiceRecord',
                    param,
                    function (json, res) {
                        var _govVolunteerServiceRecordManageInfo = JSON.parse(json);
                        vc.component.govVolunteerServiceRecordManageInfo.total = _govVolunteerServiceRecordManageInfo.total;
                        vc.component.govVolunteerServiceRecordManageInfo.records = _govVolunteerServiceRecordManageInfo.records;
                        vc.component.govVolunteerServiceRecordManageInfo.govVolunteerServiceRecords = _govVolunteerServiceRecordManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govVolunteerServiceRecordManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovVolunteerServiceRecordModal: function () {
                //vc.emit('addGovVolunteerServiceRecord', 'openAddGovVolunteerServiceRecordModal', {});
                $that.govVolunteerServiceRecordManageInfo.componentShow = 'addGovMeetingList';
            },
            _openEditGovVolunteerServiceRecordModel: function (_govVolunteerServiceRecord) {
                vc.emit('editGovVolunteerServiceRecord', 'openEditGovVolunteerServiceRecordModal', _govVolunteerServiceRecord);
            },
            _openDeleteGovVolunteerServiceRecordModel: function (_govVolunteerServiceRecord) {
                vc.emit('deleteGovVolunteerServiceRecord', 'openDeleteGovVolunteerServiceRecordModal', _govVolunteerServiceRecord);
            },
            _queryGovVolunteerServiceRecordMethod: function () {
                vc.component._listGovVolunteerServiceRecords(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govVolunteerServiceRecordManageInfo.moreCondition) {
                    vc.component.govVolunteerServiceRecordManageInfo.moreCondition = false;
                } else {
                    vc.component.govVolunteerServiceRecordManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
