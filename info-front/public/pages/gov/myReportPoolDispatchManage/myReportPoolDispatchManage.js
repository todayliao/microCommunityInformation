/**
 入驻小区
 **/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            myReportPoolDispatchInfo: {
                pools: [],
                total: 0,
                records: 1,
                moreCondition: false,
                repairName: '',
                currentRepairId: '',
                states: [],
                conditions: {
                    repairType: '',
                    state: '',
                    reportId:'',
                    reportName:'',
                    tel:'',

                }
            }
        },
        _initMethod: function () {
            vc.getDict('gov_report_pool', "state", function (_data) {
                vc.component.myReportPoolDispatchInfo.states = _data;
            });
            vc.component._lismyReportPoolDispatchs(DEFAULT_PAGE, DEFAULT_ROWS);
    
        },
        _initEvent: function () {
            vc.on('myReportPoolDispatch', 'listOwnerRepair', function (_param) {
                vc.component._lismyReportPoolDispatchs(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._lismyReportPoolDispatchs(_currentPage, DEFAULT_ROWS);
            });
            vc.on('myReportPoolDispatch', 'notifyData', function (_param) {
                vc.component._closeRepairDispatchOrder(_param);
            });
        },
        methods: {
            _lismyReportPoolDispatchs: function (_page, _rows) {
                vc.component.myReportPoolDispatchInfo.conditions.page = _page;
                vc.component.myReportPoolDispatchInfo.conditions.row = _rows;
                vc.component.myReportPoolDispatchInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.myReportPoolDispatchInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/reportPool/quseryReportFinishStaffs',
                    param,
                    function (json, res) {
                        var _myReportPoolDispatchInfo = JSON.parse(json);
                        vc.component.myReportPoolDispatchInfo.total = _myReportPoolDispatchInfo.total;
                        vc.component.myReportPoolDispatchInfo.records = _myReportPoolDispatchInfo.records;
                        vc.component.myReportPoolDispatchInfo.pools = _myReportPoolDispatchInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.myReportPoolDispatchInfo.records,
                            dataCount: vc.component.myReportPoolDispatchInfo.total,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _moreCondition: function () {
                if (vc.component.myReportPoolDispatchInfo.moreCondition) {
                    vc.component.myReportPoolDispatchInfo.moreCondition = false;
                } else {
                    vc.component.myReportPoolDispatchInfo.moreCondition = true;
                }
            },
            _openDetail: function (_pool) {
                vc.jumpToPage('/admin.html#/pages/gov/reportPoolDetail?reportId=' + _pool.reportId+"&ruId="+_pool.ruId)
            
            },
            _closeRepairDispatchOrder: function (_orderInfo) {
                var _repairDispatchParam = {
                    repairId: vc.component.myReportPoolDispatchInfo.currentRepairId,
                    context: _orderInfo.remark,
                    caId: vc.getCurrentCommunity().caId
                };
                if (_orderInfo.state == '1100') {
                    _repairDispatchParam.state = '10002';
                } else {
                    _repairDispatchParam.state = '10003';
                }
                vc.http.post(
                    'myReportPoolDispatch',
                    'closeOrder',
                    JSON.stringify(_repairDispatchParam),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
                        if (res.status == 200) {
                            //关闭model
                            vc.component._listHousekeepingPools(DEFAULT_PAGE, DEFAULT_ROWS);
                            return;
                        }
                        vc.toast(json);
                    },
                    function (errInfo, error) {
                        console.log('请求失败处理');
                        vc.toast(errInfo);
                    });
            },
            _openDispatchRepairDetail: function (_ownerRepair) {
                vc.emit('ownerRepairDetail', 'openOwnerRepairDetailModal', _ownerRepair);
            },
            _queryHousekeepingPoolMethod: function () {
                vc.component._lismyReportPoolDispatchs(DEFAULT_PAGE, DEFAULT_ROWS);
            }
        }
    });
})(window.vc);
