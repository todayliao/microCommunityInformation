/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govAreaTypeManageInfo: {
                govAreaTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    name: '',
                    isShow: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovAreaTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govAreaTypeManage', 'listGovAreaType', function (_param) {
                vc.component._listGovAreaTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovAreaTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovAreaTypes: function (_page, _rows) {

                vc.component.govAreaTypeManageInfo.conditions.page = _page;
                vc.component.govAreaTypeManageInfo.conditions.row = _rows;
                vc.component.govAreaTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govAreaTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govAreaType/queryGovAreaType',
                    param,
                    function (json, res) {
                        var _govAreaTypeManageInfo = JSON.parse(json);
                        vc.component.govAreaTypeManageInfo.total = _govAreaTypeManageInfo.total;
                        vc.component.govAreaTypeManageInfo.records = _govAreaTypeManageInfo.records;
                        vc.component.govAreaTypeManageInfo.govAreaTypes = _govAreaTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govAreaTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovAreaTypeModal: function () {
                vc.emit('addGovAreaType', 'openAddGovAreaTypeModal', {});
            },
            _openEditGovAreaTypeModel: function (_govAreaType) {
                vc.emit('editGovAreaType', 'openEditGovAreaTypeModal', _govAreaType);
            },
            _openDeleteGovAreaTypeModel: function (_govAreaType) {
                vc.emit('deleteGovAreaType', 'openDeleteGovAreaTypeModal', _govAreaType);
            },
            _queryGovAreaTypeMethod: function () {
                vc.component._listGovAreaTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govAreaTypeManageInfo.moreCondition) {
                    vc.component.govAreaTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govAreaTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
