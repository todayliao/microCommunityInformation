/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPartyOrgManageInfo: {
                govPartyOrgs: [],
                total: 0,
                records: 1,
                moreCondition: false,
                orgId: '',
                componentShow: 'govPartyOrgManage',
                conditions: {
                    orgName:'',
                    caId: vc.getCurrentCommunity().caId,
                    preOrgId: '',
                    personName: '',
                    orgLevel: '',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPartyOrgs(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govPartyOrgManage', 'listGovPartyOrg', function (_param) {
                $that.govPartyOrgManageInfo.componentShow ='govPartyOrgManage';
                vc.component._listGovPartyOrgs(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPartyOrgs(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPartyOrgs: function (_page, _rows) {

                vc.component.govPartyOrgManageInfo.conditions.page = _page;
                vc.component.govPartyOrgManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPartyOrgManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPartyOrg/queryGovPartyOrg',
                    param,
                    function (json, res) {
                        var _govPartyOrgManageInfo = JSON.parse(json);
                        vc.component.govPartyOrgManageInfo.total = _govPartyOrgManageInfo.total;
                        vc.component.govPartyOrgManageInfo.records = _govPartyOrgManageInfo.records;
                        vc.component.govPartyOrgManageInfo.govPartyOrgs = _govPartyOrgManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPartyOrgManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovPartyOrgModal: function () {
                $that.govPartyOrgManageInfo.componentShow = 'addGovPartyOrg';
            },
            _openEditGovPartyOrgModel: function (_govPartyOrg) {
                $that.govPartyOrgManageInfo.componentShow = 'editGovPartyOrg';
                vc.emit('editGovPartyOrg', 'openEditGovPartyOrgModal', _govPartyOrg);
            },
            _openDeleteGovPartyOrgModel: function (_govPartyOrg) {
                vc.emit('deleteGovPartyOrg', 'openDeleteGovPartyOrgModal', _govPartyOrg);
            },
            _queryGovPartyOrgMethod: function () {
                vc.component._listGovPartyOrgs(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govPartyOrgManageInfo.moreCondition) {
                    vc.component.govPartyOrgManageInfo.moreCondition = false;
                } else {
                    vc.component.govPartyOrgManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
