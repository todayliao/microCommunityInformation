/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govPoorPersonManageInfo: {
                govPersons: [],
                total: 0,
                records: 1,
                moreCondition: false,
                govPersonId: '',
                componentShow: 'govPoorPersonManage',
                politicalOutlooks:[],
                isWeb: '',
                govCommunityAreas: [],
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    idCardLike: '',
                    personNameLike: '',
                    personTelLike: '',
                    labelCd: '6009'

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovPoorPerson(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.getDict('gov_volunteer', "political_outlook", function (_data) {
                vc.component.govPoorPersonManageInfo.politicalOutlooks = _data;
            });
        },
        _initEvent: function () {

            vc.on('govPoorPersonManage', 'listGovPerson', function (_param) {
                $that.govPoorPersonManageInfo.componentShow ='govPoorPersonManage';
                vc.component._listGovPoorPerson(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovPoorPerson(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovPoorPerson: function (_page, _rows) {

                vc.component.govPoorPersonManageInfo.conditions.page = _page;
                vc.component.govPoorPersonManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govPoorPersonManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govPerson/getGovPersonHelpPolicy',
                    param,
                    function (json, res) {
                        var _govPoorPersonManageInfo = JSON.parse(json);
                        vc.component.govPoorPersonManageInfo.total = _govPoorPersonManageInfo.total;
                        vc.component.govPoorPersonManageInfo.records = _govPoorPersonManageInfo.records;
                        vc.component.govPoorPersonManageInfo.govPersons = _govPoorPersonManageInfo.data;
                        console.log(vc.component.govPoorPersonManageInfo.govPersons);
                        vc.emit('pagination', 'init', {
                            total: vc.component.govPoorPersonManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _getSpecName: function (_politicalOutlook) {
                let _retutest = '';
                $that.govPoorPersonManageInfo.politicalOutlooks.forEach(_item => {
                    if (_item.statusCd == _politicalOutlook) {
                        _retutest = _item.name + ',' + _retutest;
                    }
                });
                return _retutest.substr(0, _retutest.length - 1);
            },
            _openViewGovPersons: function (_govPerson) {

                vc.jumpToPage('/admin.html#/gov/viewGovPoolPerson?govPersonId='+_govPerson.govPersonId+'&govOwnerId='
                +_govPerson.govOwnerId+'&govHelpListId='+_govPerson.govHelpListId);
            },
            _openDeleteGovPoolPerson: function (_govPerson) {

                vc.emit('deleteGovPoolPerson', 'openDeleteGovPersonModal', _govPerson);
            },
            _queryGovPersonMethod: function () {
                vc.component._listGovPoorPerson(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _openAddGovPoorPerson: function () {
                $that.govPoorPersonManageInfo.componentShow = 'addGovPoorPerson';
                vc.emit('addGovPoorPerson', 'openAddGovPoorPersonModal', {});
            },
            _moreCondition: function () {
                if (vc.component.govPoorPersonManageInfo.moreCondition) {
                    vc.component.govPoorPersonManageInfo.moreCondition = false;
                } else {
                    vc.component.govPoorPersonManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
