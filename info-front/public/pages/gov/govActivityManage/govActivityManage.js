/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govActivityManageInfo:{
                govActivitys:[],
                total:0,
                records:1,
                moreCondition:false,
                actId:'',
                conditions:{
                    caId:'',
                    actName:'',
                    contactName:'',

                },
                govActivityTypes:[]
            }
        },
        _initMethod:function(){
            vc.component._listGovActivitys(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovActivityTypes();
            
        },
        _initEvent:function(){

            vc.on('govActivityManage','listGovActivity',function(_param){
                  vc.component._listGovActivitys(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovActivitys(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovActivityTypes:function(){
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
               };
               //发送get请求
               vc.http.apiGet('/govActivityType/queryGovActivityType',
                             param,
                             function(json,res){
                                var _govActivityTypeManageInfo=JSON.parse(json);
                                vc.component.govActivityManageInfo.govActivityTypes = _govActivityTypeManageInfo.data;
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _listGovActivitys:function(_page, _rows){
                vc.component.govActivityManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                vc.component.govActivityManageInfo.conditions.page = _page;
                vc.component.govActivityManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.govActivityManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govActivity/queryGovActivity',
                             param,
                             function(json,res){
                                var _govActivityManageInfo=JSON.parse(json);
                                vc.component.govActivityManageInfo.total = _govActivityManageInfo.total;
                                vc.component.govActivityManageInfo.records = _govActivityManageInfo.records;
                                vc.component.govActivityManageInfo.govActivitys = _govActivityManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govActivityManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovActivityModal:function(){
                vc.emit('addGovActivity','openAddGovActivityModal',{});
            },
            _openEditGovActivityModel:function(_govActivity){
                vc.emit('editGovActivity','openEditGovActivityModal',_govActivity);
            },
            _openDeleteGovActivityModel:function(_govActivity){
                vc.emit('deleteGovActivity','openDeleteGovActivityModal',_govActivity);
            },
            _queryGovActivityMethod:function(){
                vc.component._listGovActivitys(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govActivityManageInfo.moreCondition){
                    vc.component.govActivityManageInfo.moreCondition = false;
                }else{
                    vc.component.govActivityManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
