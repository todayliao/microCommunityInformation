/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            guideSubscribeManageInfo:{
                guideSubscribes:[],
                total:0,
                records:1,
                moreCondition:false,
                wgsId:'',
                conditions:{
                    caId:'',
                    wgId:'',
                    guideName:'',
                    person:'',
                    link:'',
                },
                govGuides:[]
            }
        },
        _initMethod:function(){
            vc.component._listGuideSubscribes(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovGuides();
        },
        _initEvent:function(){
            
            vc.on('guideSubscribeManage','listGuideSubscribe',function(_param){
                  vc.component._listGuideSubscribes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGuideSubscribes(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovGuides:function(){
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        subscribe:1001,
                        page: 1,
                        row: 50
                    }
               };
               //发送get请求
               vc.http.apiGet('/govWorkGuide/queryGovWorkGuide',
                             param,
                             function(json,res){
                                var _govGuideManageInfo=JSON.parse(json);
                                vc.component.guideSubscribeManageInfo.govGuides = _govGuideManageInfo.data;
                                console.log(vc.component.guideSubscribeManageInfo.govGuides);
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _listGuideSubscribes:function(_page, _rows){
                vc.component.guideSubscribeManageInfo.conditions.caId = vc.getCurrentCommunity().caId,
                vc.component.guideSubscribeManageInfo.conditions.page = _page;
                vc.component.guideSubscribeManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.guideSubscribeManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govGuideSubscribe/queryGovGuideSubscribe',
                             param,
                             function(json,res){
                                var _guideSubscribeManageInfo=JSON.parse(json);
                                vc.component.guideSubscribeManageInfo.total = _guideSubscribeManageInfo.total;
                                vc.component.guideSubscribeManageInfo.records = _guideSubscribeManageInfo.records;
                                vc.component.guideSubscribeManageInfo.guideSubscribes = _guideSubscribeManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.guideSubscribeManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGuideSubscribeModal:function(){
                vc.emit('addGuideSubscribe','openAddGuideSubscribeModal',{});
            },
            _openEditGuideSubscribeModel:function(_guideSubscribe){
                vc.emit('editGuideSubscribe','openEditGuideSubscribeModal',_guideSubscribe);
            },
            _openDeleteGuideSubscribeModel:function(_guideSubscribe){
                vc.emit('deleteGuideSubscribe','openDeleteGuideSubscribeModal',_guideSubscribe);
            },
            _queryGuideSubscribeMethod:function(){
                vc.component._listGuideSubscribes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.guideSubscribeManageInfo.moreCondition){
                    vc.component.guideSubscribeManageInfo.moreCondition = false;
                }else{
                    vc.component.guideSubscribeManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
