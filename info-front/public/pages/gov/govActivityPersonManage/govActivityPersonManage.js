/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govActivityPersonManageInfo:{
                govActivityPersons:[],
                total:0,
                records:1,
                moreCondition:false,
                actPerId:'',
                conditions:{
                    caId:'',
                    personName:'',
                    personLink:'',

                },
                govActivitys:[]
            }
        },
        _initMethod:function(){
            vc.component._listGovActivityPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.component._listGovActivitys();
        },
        _initEvent:function(){
            
            vc.on('govActivityPersonManage','listGovActivityPerson',function(_param){
                  vc.component._listGovActivityPersons(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovActivityPersons(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovActivitys:function(){
                var param = {
                    params: {
                        caId : vc.getCurrentCommunity().caId,
                        page: 1,
                        row: 50
                    }
               };
               //发送get请求
               vc.http.apiGet('/govActivity/queryGovActivity',
                             param,
                             function(json,res){
                                var _govActivityManageInfo=JSON.parse(json);
                                vc.component.govActivityPersonManageInfo.govActivitys = _govActivityManageInfo.data;
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _listGovActivityPersons:function(_page, _rows){
                vc.component.govActivityPersonManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                vc.component.govActivityPersonManageInfo.conditions.page = _page;
                vc.component.govActivityPersonManageInfo.conditions.row = _rows;
                var param = {
                    params:vc.component.govActivityPersonManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govActivityPerson/queryGovActivityPerson',
                             param,
                             function(json,res){
                                var _govActivityPersonManageInfo=JSON.parse(json);
                                vc.component.govActivityPersonManageInfo.total = _govActivityPersonManageInfo.total;
                                vc.component.govActivityPersonManageInfo.records = _govActivityPersonManageInfo.records;
                                vc.component.govActivityPersonManageInfo.govActivityPersons = _govActivityPersonManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govActivityPersonManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovActivityPersonModal:function(){
                vc.emit('addGovActivityPerson','openAddGovActivityPersonModal',{});
            },
            _openEditGovActivityPersonModel:function(_govActivityPerson){
                vc.emit('editGovActivityPerson','openEditGovActivityPersonModal',_govActivityPerson);
            },
            _openDeleteGovActivityPersonModel:function(_govActivityPerson){
                vc.emit('deleteGovActivityPerson','openDeleteGovActivityPersonModal',_govActivityPerson);
            },
            _queryGovActivityPersonMethod:function(){
                vc.component._listGovActivityPersons(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition:function(){
                if(vc.component.govActivityPersonManageInfo.moreCondition){
                    vc.component.govActivityPersonManageInfo.moreCondition = false;
                }else{
                    vc.component.govActivityPersonManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
