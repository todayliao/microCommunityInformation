/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govSymptomTypeManageInfo: {
                govSymptomTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    symptomId: '',
                    caId: '',
                    name: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovSymptomTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govSymptomTypeManage', 'listGovSymptomType', function (_param) {
                vc.component._listGovSymptomTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovSymptomTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovSymptomTypes: function (_page, _rows) {

                vc.component.govSymptomTypeManageInfo.conditions.page = _page;
                vc.component.govSymptomTypeManageInfo.conditions.row = _rows;
                vc.component.govSymptomTypeManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govSymptomTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govSymptomType/queryGovSymptomType',
                    param,
                    function (json, res) {
                        var _govSymptomTypeManageInfo = JSON.parse(json);
                        vc.component.govSymptomTypeManageInfo.total = _govSymptomTypeManageInfo.total;
                        vc.component.govSymptomTypeManageInfo.records = _govSymptomTypeManageInfo.records;
                        vc.component.govSymptomTypeManageInfo.govSymptomTypes = _govSymptomTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govSymptomTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovSymptomTypeModal: function () {
                vc.emit('addGovSymptomType', 'openAddGovSymptomTypeModal', {});
            },
            _openEditGovSymptomTypeModel: function (_govSymptomType) {
                vc.emit('editGovSymptomType', 'openEditGovSymptomTypeModal', _govSymptomType);
            },
            _openDeleteGovSymptomTypeModel: function (_govSymptomType) {
                vc.emit('deleteGovSymptomType', 'openDeleteGovSymptomTypeModal', _govSymptomType);
            },
            _queryGovSymptomTypeMethod: function () {
                vc.component._listGovSymptomTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govSymptomTypeManageInfo.moreCondition) {
                    vc.component.govSymptomTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govSymptomTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
