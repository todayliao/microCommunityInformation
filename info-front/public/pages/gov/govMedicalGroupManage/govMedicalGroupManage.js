/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMedicalGroupManageInfo: {
                govMedicalGroups: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                componentShow: 'govMedicalGroupManage',
                conditions: {
                    groupId: '',
                    caId: '',
                    hospitalId: '',
                    name: '',
                    companyName: '',
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMedicalGroups(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govMedicalGroupManage', 'listGovMedicalGroup', function (_param) {
                $that.govMedicalGroupManageInfo.componentShow ='govMedicalGroupManage';
                vc.component._listGovMedicalGroups(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMedicalGroups(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMedicalGroups: function (_page, _rows) {

                vc.component.govMedicalGroupManageInfo.conditions.page = _page;
                vc.component.govMedicalGroupManageInfo.conditions.row = _rows;
                vc.component.govMedicalGroupManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMedicalGroupManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMedicalGroup/queryGovMedicalGroup',
                    param,
                    function (json, res) {
                        var _govMedicalGroupManageInfo = JSON.parse(json);
                        vc.component.govMedicalGroupManageInfo.total = _govMedicalGroupManageInfo.total;
                        vc.component.govMedicalGroupManageInfo.records = _govMedicalGroupManageInfo.records;
                        vc.component.govMedicalGroupManageInfo.govMedicalGroups = _govMedicalGroupManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMedicalGroupManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMedicalGroupModal: function () {
                $that.govMedicalGroupManageInfo.componentShow = 'addGovMedicalGroup';
            },
            _openEditGovMedicalGroupModel: function (_govMedicalGroup) {
                $that.govMedicalGroupManageInfo.componentShow = 'editGovMedicalGroup';
                vc.emit('editGovMedicalGroup', 'openEditGovMedicalGroupModal', _govMedicalGroup);
            },
            _openDeleteGovMedicalGroupModel: function (_govMedicalGroup) {
                vc.emit('deleteGovMedicalGroup', 'openDeleteGovMedicalGroupModal', _govMedicalGroup);
            },
            _queryGovMedicalGroupMethod: function () {
                vc.component._listGovMedicalGroups(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMedicalGroupManageInfo.moreCondition) {
                    vc.component.govMedicalGroupManageInfo.moreCondition = false;
                } else {
                    vc.component.govMedicalGroupManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
