/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCommunityLocationManageInfo: {
                govCommunityLocations: [],
                govCommunitys: [],
                total: 0,
                records: 1,
                moreCondition: false,
                locationId: '',
                conditions: {
                    caId: vc.getCurrentCommunity().caId,
                    locationId: '',
                    name: '',
                    govCommunityId: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCommunityLocations(DEFAULT_PAGE, DEFAULT_ROWS);
            $that._listGovCommunitys();
        },
        _initEvent: function () {

            vc.on('govCommunityLocationManage', 'listGovCommunityLocation', function (_param) {
                vc.component._listGovCommunityLocations(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCommunityLocations(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCommunityLocations: function (_page, _rows) {

                vc.component.govCommunityLocationManageInfo.conditions.page = _page;
                vc.component.govCommunityLocationManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCommunityLocationManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCommunityLocation/queryGovCommunityLocation',
                    param,
                    function (json, res) {
                        var _govCommunityLocationManageInfo = JSON.parse(json);
                        vc.component.govCommunityLocationManageInfo.total = _govCommunityLocationManageInfo.total;
                        vc.component.govCommunityLocationManageInfo.records = _govCommunityLocationManageInfo.records;
                        vc.component.govCommunityLocationManageInfo.govCommunityLocations = _govCommunityLocationManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCommunityLocationManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovCommunityLocationModal: function () {
                vc.emit('addGovCommunityLocation', 'openAddGovCommunityLocationModal', {});
            },
            _openEditGovCommunityLocationModel: function (_govCommunityLocation) {
                vc.emit('editGovCommunityLocation', 'openEditGovCommunityLocationModal', _govCommunityLocation);
            },
            _openDeleteGovCommunityLocationModel: function (_govCommunityLocation) {
                vc.emit('deleteGovCommunityLocation', 'openDeleteGovCommunityLocationModal', _govCommunityLocation);
            },
            _queryGovCommunityLocationMethod: function () {
                vc.component._listGovCommunityLocations(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _listGovCommunitys: function () {

                let param = {
                    params: {
                        page: 1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govCommunity/queryGovCommunity',
                    param,
                    function (json, res) {
                        var _govCommunityManageInfo = JSON.parse(json);
                        vc.component.govCommunityLocationManageInfo.govCommunitys = _govCommunityManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _moreCondition: function () {
                if (vc.component.govCommunityLocationManageInfo.moreCondition) {
                    vc.component.govCommunityLocationManageInfo.moreCondition = false;
                } else {
                    vc.component.govCommunityLocationManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
