/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govCiviladminTypeManageInfo: {
                govCiviladminTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeCd: '',
                conditions: {
                    caId : vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovCiviladminTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govActivitiesTypeManage', 'listGovActivitiesType', function (_param) {
                vc.component._listGovCiviladminTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovCiviladminTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovCiviladminTypes: function (_page, _rows) {

                vc.component.govCiviladminTypeManageInfo.conditions.page = _page;
                vc.component.govCiviladminTypeManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.govCiviladminTypeManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govCiviladminType/queryGovCiviladminType',
                    param,
                    function (json, res) {
                        var _govCiviladminTypeManageInfo = JSON.parse(json);
                        vc.component.govCiviladminTypeManageInfo.total = _govCiviladminTypeManageInfo.total;
                        vc.component.govCiviladminTypeManageInfo.records = _govCiviladminTypeManageInfo.records;
                        vc.component.govCiviladminTypeManageInfo.govCiviladminTypes = _govCiviladminTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govCiviladminTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovActivitiesTypeModal: function () {
                vc.emit('addGovActivitiesType', 'openAddGovActivitiesTypeModal', {});
            },
            _openEditGovActivitiesTypeModel: function (_govActivitiesType) {
                vc.emit('editGovActivitiesType', 'openEditGovActivitiesTypeModal', _govActivitiesType);
            },
            _openDeleteGovActivitiesTypeModel: function (_govActivitiesType) {
                vc.emit('deleteGovActivitiesType', 'openDeleteGovActivitiesTypeModal', _govActivitiesType);
            },
            _queryGovActivitiesTypeMethod: function () {
                vc.component._listGovCiviladminTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govCiviladminTypeManageInfo.moreCondition) {
                    vc.component.govCiviladminTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.govCiviladminTypeManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
