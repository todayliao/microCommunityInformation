/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govServFieldManageInfo: {
                govServFields: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    name: '',
                    isShow: 'Y',

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovServFields(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govServFieldManage', 'listGovServField', function (_param) {
                vc.component._listGovServFields(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovServFields(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovServFields: function (_page, _rows) {

                vc.component.govServFieldManageInfo.conditions.page = _page;
                vc.component.govServFieldManageInfo.conditions.row = _rows;
                vc.component.govServFieldManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govServFieldManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govServField/queryGovServField',
                    param,
                    function (json, res) {
                        var _govServFieldManageInfo = JSON.parse(json);
                        vc.component.govServFieldManageInfo.total = _govServFieldManageInfo.total;
                        vc.component.govServFieldManageInfo.records = _govServFieldManageInfo.records;
                        vc.component.govServFieldManageInfo.govServFields = _govServFieldManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govServFieldManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovServFieldModal: function () {
                vc.emit('addGovServField', 'openAddGovServFieldModal', {});
            },
            _openEditGovServFieldModel: function (_govServField) {
                vc.emit('editGovServField', 'openEditGovServFieldModal', _govServField);
            },
            _openDeleteGovServFieldModel: function (_govServField) {
                vc.emit('deleteGovServField', 'openDeleteGovServFieldModal', _govServField);
            },
            _queryGovServFieldMethod: function () {
                vc.component._listGovServFields(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govServFieldManageInfo.moreCondition) {
                    vc.component.govServFieldManageInfo.moreCondition = false;
                } else {
                    vc.component.govServFieldManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
