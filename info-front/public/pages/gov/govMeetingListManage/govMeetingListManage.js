/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govMeetingListManageInfo: {
                govMeetingLists: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                componentShow: 'govMeetingListManage',
                conditions: {
                    meetingName: '',
                    meetingTime: ''
                }
            }
        },
        _initMethod: function () {
            vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);
            vc.initDateTime('meetingTime', function (_value) {
                $that.govMeetingListManageInfo.conditions.meetingTime = _value;
            });
        },
        _initEvent: function () {

            vc.on('govMeetingListManage', 'listGovMeetingList', function (_param) {
                $that.govMeetingListManageInfo.componentShow ='govMeetingListManage';
                vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovMeetingLists(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovMeetingLists: function (_page, _rows) {

                vc.component.govMeetingListManageInfo.conditions.page = _page;
                vc.component.govMeetingListManageInfo.conditions.row = _rows;
                vc.component.govMeetingListManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govMeetingListManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govMeetingList/queryGovMeetingList',
                    param,
                    function (json, res) {
                        var _govMeetingListManageInfo = JSON.parse(json);
                        vc.component.govMeetingListManageInfo.total = _govMeetingListManageInfo.total;
                        vc.component.govMeetingListManageInfo.records = _govMeetingListManageInfo.records;
                        vc.component.govMeetingListManageInfo.govMeetingLists = _govMeetingListManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govMeetingListManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovMeetingListModal: function () {
                $that.govMeetingListManageInfo.componentShow = 'addGovMeetingList';
                //vc.emit('addGovMeetingList', 'openAddGovMeetingListModal', {});
            },
            _openEditGovMeetingListModel: function (_govMeetingList) {
                $that.govMeetingListManageInfo.componentShow = 'editGovMeetingList';
                vc.emit('editGovMeetingList', 'openEditGovMeetingListModal', _govMeetingList);
            },
            _openSelectGovMeetingListModel: function (_govMeetingList) {
                $that.govMeetingListManageInfo.componentShow = 'selectGovMeetingList';
                vc.emit('selectGovMeetingList', 'openselectGovMeetingListModal', _govMeetingList);
            },
            _openDeleteGovMeetingListModel: function (_govMeetingList) {

                vc.emit('deleteGovMeetingList', 'openDeleteGovMeetingListModal', _govMeetingList);
            },
            _queryGovMeetingListMethod: function () {
                vc.component._listGovMeetingLists(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govMeetingListManageInfo.moreCondition) {
                    vc.component.govMeetingListManageInfo.moreCondition = false;
                } else {
                    vc.component.govMeetingListManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
