/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            reportPoolManageInfo: {
                reportPools: [],
                total: 0,
                records: 1,
                moreCondition: false,
                reportId: '',
                repairSettings: [],
                conditions: {
                    reportType: '',
                    reportName: '',
                    tel: '',
                    caId: vc.getCurrentCommunity().caId
                }
            }
        },
        _initMethod: function () {
            $that._listGovReportSettings(DEFAULT_PAGE, 50);
            vc.component._listReportPools(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('reportPoolManage', 'listReportPool', function (_param) {
                vc.component._listReportPools(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listReportPools(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listReportPools: function (_page, _rows) {

                vc.component.reportPoolManageInfo.conditions.page = _page;
                vc.component.reportPoolManageInfo.conditions.row = _rows;
                var param = {
                    params: vc.component.reportPoolManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/reportPool/queryReportPool',
                    param,
                    function (json, res) {
                        var _reportPoolManageInfo = JSON.parse(json);
                        vc.component.reportPoolManageInfo.total = _reportPoolManageInfo.total;
                        vc.component.reportPoolManageInfo.records = _reportPoolManageInfo.records;
                        vc.component.reportPoolManageInfo.reportPools = _reportPoolManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.reportPoolManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _listGovReportSettings: function () {


                var param = {
                    params: {
                        page:1,
                        row: 50,
                        caId: vc.getCurrentCommunity().caId
                    }
                };

                //发送get请求
                vc.http.apiGet('/govReportSetting/queryGovReportSetting',
                    param,
                    function (json, res) {
                        var _govReportSettingManageInfo = JSON.parse(json);
                        vc.component.reportPoolManageInfo.total = _govReportSettingManageInfo.total;
                        vc.component.reportPoolManageInfo.records = _govReportSettingManageInfo.records;
                        vc.component.reportPoolManageInfo.repairSettings = _govReportSettingManageInfo.data;
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddReportPoolModal: function () {
                vc.emit('addReportPool', 'openAddReportPoolModal', {});
            },
            _openEditReportPoolModel: function (_reportPool) {
                vc.emit('editReportPool', 'openEditReportPoolModal', _reportPool);
            },
            _openDeleteReportPoolModel: function (_reportPool) {
                vc.emit('deleteReportPool', 'openDeleteReportPoolModal', _reportPool);
            },
            _queryReportPoolMethod: function () {
                vc.component._listReportPools(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.reportPoolManageInfo.moreCondition) {
                    vc.component.reportPoolManageInfo.moreCondition = false;
                } else {
                    vc.component.reportPoolManageInfo.moreCondition = true;
                }
            },
            _disposeReportPool:function(_reportPool){
               _reportPool.action = "DISPATCH";
                vc.emit('dispatchReportPoolDispatch', 'opendispatchReportPoolDispatchModal', _reportPool);
            }
            , 
            _openDetail: function (_pool) {
                vc.jumpToPage('/admin.html#/pages/gov/reportPoolDetail?reportId=' + _pool.reportId+"&ruId="+_pool.ruId)
            }


        }
    });
})(window.vc);
