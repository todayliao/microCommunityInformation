/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            govHealthTermManageInfo: {
                govHealthTerms: [],
                total: 0,
                records: 1,
                moreCondition: false,
                caId: '',
                conditions: {
                    caId: ''

                }
            }
        },
        _initMethod: function () {
            vc.component._listGovHealthTerms(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {

            vc.on('govHealthTermManage', 'listGovHealthTerm', function (_param) {
                vc.component._listGovHealthTerms(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listGovHealthTerms(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listGovHealthTerms: function (_page, _rows) {

                vc.component.govHealthTermManageInfo.conditions.page = _page;
                vc.component.govHealthTermManageInfo.conditions.row = _rows;
                vc.component.govHealthTermManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params: vc.component.govHealthTermManageInfo.conditions
                };

                //发送get请求
                vc.http.apiGet('/govHealthTerm/queryGovHealthTerm',
                    param,
                    function (json, res) {
                        var _govHealthTermManageInfo = JSON.parse(json);
                        vc.component.govHealthTermManageInfo.total = _govHealthTermManageInfo.total;
                        vc.component.govHealthTermManageInfo.records = _govHealthTermManageInfo.records;
                        vc.component.govHealthTermManageInfo.govHealthTerms = _govHealthTermManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.govHealthTermManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddGovHealthTermModal: function () {
                vc.emit('addGovHealthTerm', 'openAddGovHealthTermModal', {});
            },
            _openEditGovHealthTermModel: function (_govHealthTerm) {
                vc.emit('editGovHealthTerm', 'openEditGovHealthTermModal', _govHealthTerm);
            },
            _openDeleteGovHealthTermModel: function (_govHealthTerm) {
                vc.emit('deleteGovHealthTerm', 'openDeleteGovHealthTermModal', _govHealthTerm);
            },
            _queryGovHealthTermMethod: function () {
                vc.component._listGovHealthTerms(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.govHealthTermManageInfo.moreCondition) {
                    vc.component.govHealthTermManageInfo.moreCondition = false;
                } else {
                    vc.component.govHealthTermManageInfo.moreCondition = true;
                }
            }


        }
    });
})(window.vc);
