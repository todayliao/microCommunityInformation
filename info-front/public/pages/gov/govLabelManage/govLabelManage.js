/**
    入驻小区
**/
(function(vc){
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data:{
            govLabelManageInfo:{
                govLabels:[],
                total:0,
                records:1,
                moreCondition:false,
                govLabelId:'',
                conditions:{
                    govLabelId:'',
                    caId:'',
                    labelName:'',
                    labelCd:'',
                    labelType:'',
                    statusCd:'',

                }
            }
        },
        _initMethod:function(){
            vc.component._listGovLabels(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent:function(){
            vc.on('govLabelManage','listGovLabel',function(_param){
                  vc.component._listGovLabels(DEFAULT_PAGE, DEFAULT_ROWS);
            });
             vc.on('pagination','page_event',function(_currentPage){
                vc.component._listGovLabels(_currentPage,DEFAULT_ROWS);
            });
        },
        methods:{
            _listGovLabels:function(_page, _rows){

                vc.component.govLabelManageInfo.conditions.page = _page;
                vc.component.govLabelManageInfo.conditions.row = _rows;
                vc.component.govLabelManageInfo.conditions.caId = vc.getCurrentCommunity().caId;
                var param = {
                    params:vc.component.govLabelManageInfo.conditions
               };

               //发送get请求
               vc.http.apiGet('/govLabel/queryGovLabel',
                             param,
                             function(json,res){
                                var _govLabelManageInfo=JSON.parse(json);
                                vc.component.govLabelManageInfo.total = _govLabelManageInfo.total;
                                vc.component.govLabelManageInfo.records = _govLabelManageInfo.records;
                                vc.component.govLabelManageInfo.govLabels = _govLabelManageInfo.data;
                                vc.emit('pagination','init',{
                                     total:vc.component.govLabelManageInfo.records,
                                     currentPage:_page
                                 });
                             },function(errInfo,error){
                                console.log('请求失败处理');
                             }
                           );
            },
            _openAddGovLabelModal:function(){
                vc.emit('addGovLabel','openAddGovLabelModal',{});
            },
            _openEditGovLabelModel:function(_govLabel){
                vc.emit('editGovLabel','openEditGovLabelModal',_govLabel);
            },
            _openDeleteGovLabelModel:function(_govLabel){
                vc.emit('deleteGovLabel','openDeleteGovLabelModal',_govLabel);
            },
            _queryGovLabelMethod:function(){
                vc.component._listGovLabels(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _getLabelType: function (_state) {
                if (_state == 'P') {
                    return '系统默认';
                } else if (_state == 'C') {
                    return '用户自定义';
                }
                return '状态异常';
            },
            _moreCondition:function(){
                if(vc.component.govLabelManageInfo.moreCondition){
                    vc.component.govLabelManageInfo.moreCondition = false;
                }else{
                    vc.component.govLabelManageInfo.moreCondition = true;
                }
            }

             
        }
    });
})(window.vc);
