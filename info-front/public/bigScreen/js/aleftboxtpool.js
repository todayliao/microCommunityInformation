function _loadStoreOrderCart() {
  let context = '';
  let param = {
    params: {
      page: 1,
      row: 8
    }
  }
  vc.http.apiGet(
    '/govOldPerson/queryStoreOrderCart',
    param,
    function (json, res) {
      //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
      let _json = JSON.parse(json);
      console.log(_json);
      if (_json.code == 0) {
        let _data = _json.data;
        amiddboxtbott1(_data);
        let aleftboxtpool = document.getElementById("aleftboxtpool");
        _data.storeOrderCart.data.forEach(element => {
          let cartTypeName = '商品订单';
          if(element.cartType == '3307'){
            cartTypeName = '服务订单';
          }
            context = context + '<li><p class="fl"><b>' + cartTypeName + '</b><br>'
                + element.prodName + '<b class="fr">' + element.stateName + '</b></p><p class="fr pt17">' + element.createTime + '</p></li>';
        });

        aleftboxtpool.innerHTML = context;

        return;
      }
    },
    function (errInfo, error) {
      console.log('请求失败处理');
      vc.toast(errInfo);

    });
}
function amiddboxtbott1(_data) {
  var myChart = echarts.init(document.getElementById('amiddboxtbott1'));
  option = {
    color: ['#7ecef4'],
    backgroundColor: 'rgba(1,202,217,.2)',
    grid: {
      left: 20,
      right: 50,
      top: 23,
      bottom: 30,
      containLabel: true
    },
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        console.log(params);
        var color = "#FFB84D";
        var a = "<div style='background-color:'" + color +
          ":padding: 5px 10px:text-align:center:color:white:font-size: 16px:'>" + params.name + "</div>" +
          params.value + "<br>";
        return a;
      }
    },
    xAxis: {
      type: 'value',
      axisLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.2)'
        }
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,0)'
        }
      },
      axisLabel: {
        color: "rgba(255,255,255,1)"
      },
      data: ['1000', '5000', '10000', '15000', '20000', '25000'],
      boundaryGap: [0, 0.01]
    },
    yAxis: {
      type: 'category',
      axisLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.5)'
        }
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.1)'
        }
      },
      axisLabel: {
        color: "rgba(255,255,255,.5)"
      },
      data: ['服务订单：' + (_data.servTotal), '商品订单：' + (_data.productTotal), '总订单：' + (Number(_data.servTotal)+Number(_data.productTotal))]
    },
    series: [
      {
        name: '2011年',
        type: 'bar',
        barWidth: 30,
        itemStyle: {
          normal: {
            color: new echarts.graphic.LinearGradient(
              1, 0, 0, 1,
              [
                { offset: 0, color: 'rgba(230,253,139,.7)' },
                { offset: 1, color: 'rgba(41,220,205,.7)' }
              ]
            )
          }
        },
        data: [_data.servTotal, _data.productTotal, Number(_data.servTotal)+Number(_data.productTotal)]
      }
    ]
  };
  myChart.setOption(option);
};
_loadStoreOrderCart();