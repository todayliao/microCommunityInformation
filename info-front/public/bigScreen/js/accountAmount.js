function _loadAccountAmount() {

  let param = {
    params: {
    }
  }
  vc.http.apiGet(
    '/govOldPerson/queryAccountAmount',
    param,
    function (json, res) {
      //vm.menus = vm.refreshMenuActive(JSON.parse(json),0);
      let _json = JSON.parse(json);
      console.log(_json);
      if (_json.code == 0) {
        let _data = _json.data;
        console.log("查询时间银行数据");
        initAleftbox(_data);
        return;
      }
    },
    function (errInfo, error) {
      console.log('请求失败处理');
      vc.toast(errInfo);

    });
};
function initAleftbox(_data) {
  var myChart = echarts.init(document.getElementById('aleftboxtbott'));
  option = {
    color: ['#7ecef4'],
    backgroundColor: 'rgba(1,202,217,.2)',
    grid: {
      left: 20,
      right: 50,
      top: 23,
      bottom: 30,
      containLabel: true
    },
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        console.log(params);
        var color = "#FFB84D";
        var a = "<div style='background-color:'" + color +
          ":padding: 5px 10px:text-align:center:color:white:font-size: 16px:'>" + params.name + "</div>" +
          params.value + "<br>";
        return a;
      }
    },
    xAxis: {
      type: 'value',
      axisLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.2)'
        }
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,0)'
        }
      },
      axisLabel: {
        color: "rgba(255,255,255,1)"
      },
      data: ['1000', '5000', '10000', '15000', '20000', '25000'],
      boundaryGap: [0, 0.01]
    },
    yAxis: {
      type: 'category',
      axisLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.5)'
        }
      },
      splitLine: {
        lineStyle: {
          color: 'rgba(255,255,255,.1)'
        }
      },
      axisLabel: {
        color: "rgba(255,255,255,.5)"
      },
      data: ['累计消耗：' + (_data.outaVolunAmount), '总池余额：' + (_data.outOldAmount), '总池：' + (_data.amout)]
    },
    series: [
      {
        name: '2011年',
        type: 'bar',
        barWidth: 30,
        itemStyle: {
          normal: {
            color: new echarts.graphic.LinearGradient(
              1, 0, 0, 1,
              [
                { offset: 0, color: 'rgba(230,253,139,.7)' },
                { offset: 1, color: 'rgba(41,220,205,.7)' }
              ]
            )
          }
        },
        data: [_data.outaVolunAmount, _data.outOldAmount, _data.amout]
      }
    ]
  };
  myChart.setOption(option)
};
_loadAccountAmount();