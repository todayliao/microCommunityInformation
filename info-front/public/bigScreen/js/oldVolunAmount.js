function _loadOld() {
  let loadNum = 0;
  let param = {
    params: {
      page: 1,
      row: 10
    }
  }
  vc.http.apiGet(
    '/govOldPerson/getGovOldTypePersonCount',
    param,
    function (json, res) {
      let _json = JSON.parse(json);
      console.log(_json);
      if (_json.code == 0) {
        let _data = _json.data;
        _data.forEach(item => {
          loadNum = loadNum + Number(item.num);
        });
        _loadVolunAmount(loadNum);
        initLeftboxtmiddr(_data);
      }
    },
    function (errInfo, error) {
      console.log('查询志愿者状态');
      vc.toast(errInfo);
    });
}

function _loadVolunAmount(_loadNum) {
  let volunNum = 0;
  let param = {
    params: {
      page: 1,
      row: 10
    }
  }
  vc.http.apiGet(
    '/govVolunteer/getGovStateVolunteerCount',
    param,
    function (json, res) {
      let _json = JSON.parse(json);
      console.log(_json);
      if (_json.code == 0) {
        let _data = _json.data;
        console.log("查询时间银行数据");
        _data.forEach(item => {
          volunNum = + Number(item.num);
        });
        var dt = [
          { name: '湖滨区', value: 411202, text: '志愿者总数：0<br/>老人总数：0' },
          { name: '陕州区', value: 411203, text: '志愿者总数：' + volunNum + '<br/>老人总数：' + _loadNum },
          { name: '渑池县', value: 411221, text: '志愿者总数：0<br/>老人总数：0' },
          { name: '卢氏县', value: 411224, text: '志愿者总数：0<br/>老人总数：0' },
          { name: '义马市', value: 411281, text: '志愿者总数：0<br/>老人总数：0' },
          { name: '灵宝市', value: 411282, text: '志愿者总数：0<br/>老人总数：0' }

        ];
        drawMap('三门峡', dt);
        initLeftboxtmidd(_data);
      }
    },
    function (errInfo, error) {
      console.log('请求失败处理');
      vc.toast(errInfo);
    });
}

$(function () {
  _loadOld();
});
  var onNum = 0;
function drawMap(drawMapType, mydata) {
  //使用echarts.init()方法初始化一个Echarts实例，在init方法中传入echarts map的容器 dom对象
  // mapChart的配置
  var option = {
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        var color = "#FFB84D";
        var a = "<div style='background-color:'" + color +
          ":padding: 5px 10px:text-align:center:color:white:font-size: 16px:'>" + mydata[params.dataIndex].name + "</div>" +
          mydata[params.dataIndex].text + "<br>";
        return a;
      }
    },
    toolbox: {
      show: true,
      orient: 'vertical',
      left: 'right',
      top: 'center',
      feature: {
        dataView: { readOnly: false },
        restore: {},
        saveAsImage: {}
      }
    },
    visualMap: {
      min: 10,
      max: 2000,
      text: ['高', '低'],
      realtime: false,
      calculable: true,
      inRange: {
        color: ['lightskyblue', 'yellow', 'orangered']
      }
    },
    series: [
      {
        name: drawMapType,
        type: 'map',//type必须声明为 map 说明该图标为echarts 中map类型
        map: drawMapType, //这里需要特别注意。如果是中国地图，map值为china，如果为各省市则为中文。这里用北京
        aspectScale: 0.75, //长宽比. default: 0.75
        zoom: 1.2,
        //roam: true,
        itemStyle: {
          normal: { label: { show: true } },
          emphasis: { label: { show: true } }
        },
        data: mydata
      }
    ]
  };
  var mapChart = echarts.init(document.getElementById('topmap'));
  //设置图表的配置项
  mapChart.setOption(option);
  mapChart.on("dblclick", function (param) {
      drawMap(param.name, mydata);
  })
};

function initLeftboxtmiddr(_data) {
  var datas = [];
  var datas2 = [];

  _data.forEach(item => {
    if (item.typeName) {
      datas.push('(' + item.num + ')' + item.typeName);
      let param = {};
      param['value'] = item.num;
      param['name'] = '(' + item.num + ')' + item.typeName;
      datas2.push(param);
    }

  });

  console.log('组装数据完成');
  console.log(datas);
  console.log(datas2);

  var myChart = echarts.init(document.getElementById('aleftboxtmiddr'));
  option = {
    color: ['#f1b1ff', '#aae3fb', '#e5ffc7'],
    backgroundColor: 'rgba(1,202,217,.2)',
    grid: {
      left: 60,
      right: 60,
      top: 20,
      bottom: 0,
      containLabel: true
    },
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        var color = "#FFB84D";
        var a = "<div style='background-color:'" + color +
          ":padding: 5px 10px:text-align:center:color:white:font-size: 16px:'>" + params.name + "</div>" +
          params.value + "<br>";
        return a;
      }
    },
    legend: {
      left: 10,
      top: 5,
      textStyle: {
        fontSize: 10,
        color: 'rgba(255,255,255,.6)'
      },
      data: datas
    },
    calculable: true,
    series: [

      {
        name: '面积模式',
        type: 'pie',
        radius: [20, 70],
        center: ['50%', '55%'],
        roseType: 'area',
        data: datas2
      }
    ]
  };
  myChart.setOption(option);
}
function initLeftboxtmidd(_data) {
  var datas = [];
  var datas2 = [];

  _data.forEach(item => {
    if (item.stateName) {
      datas.push('(' + item.num + ')' + item.stateName);
      let param = {};
      param['value'] = item.num;
      param['name'] = '(' + item.num + ')' + item.stateName;
      datas2.push(param);
    }

  });
  console.log('组装数据完成');
  console.log(datas);
  console.log(datas2);
  var myChart = echarts.init(document.getElementById('aleftboxtmidd'));
  option = {
    color: ['#4d72d9', '#76c4bf', '#e5ffc7'],
    backgroundColor: 'rgba(1,202,217,.2)',
    grid: {
      left: 60,
      right: 60,
      top: 20,
      bottom: 0,
      containLabel: true
    },
    tooltip: {
      trigger: 'item',
      formatter: function (params) {
        var color = "#FFB84D";
        var a = "<div style='background-color:'" + color +
          ":padding: 5px 10px:text-align:center:color:white:font-size: 16px:'>" + params.name + "</div>" +
          params.value + "<br>";
        return a;
      }
    },
    legend: {
      left: 10,
      top: 5,
      textStyle: {
        fontSize: 10,
        color: 'rgba(255,255,255,.6)'
      },
      data: datas
    },
    calculable: true,
    series: [

      {
        name: '面积模式',
        type: 'pie',
        radius: [20, 70],
        center: ['50%', '55%'],
        roseType: 'area',
        data: datas2
      }
    ]
  };
  myChart.setOption(option);
}