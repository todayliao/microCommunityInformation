import conf from '../conf/config.js'
const baseUrl = conf.baseUrl;
export default {
	NEED_NOT_LOGIN_PAGE: [
		'/pages/login/login',
		'/pages/register/register',
		'/pages/my/my',
		'/pages/index/index',
	],
	NEED_NOT_LOGIN_URL: [
		// this.listActivitiess,
		// this.listAdvertPhoto,
		// this.queryAppUserBindingOwner,
		// this.listJunkRequirements
	],
	baseUrl: baseUrl,
	
	expeditingDelivery: baseUrl + 'app/storeOrder/expeditingDelivery',
	//发送验证码
	userSendSms: baseUrl + "app/user.userSendSms",
	register: baseUrl + "app/userAuth/register",
	login: baseUrl + "app/userAuth/login",
	queryStoreAds: baseUrl + "app/storeAds/queryStoreAds",
	listAreas: baseUrl + "app/govCommunityArea/queryGovCommunityArea",//查询所有区域
	listAdvertPhoto: baseUrl + "app/govAdvert/queryGovAdvertItems",//查询社区广告
	queryActivitiesType: baseUrl + 'app/govActivitiesType/queryGovActivitiesType',
	listActivitiess: baseUrl + 'app/govActivities/queryGovActivities',
	queryReportSetting: baseUrl + 'app/govReportSetting/queryGovReportSetting',
	queryReports: baseUrl + 'app/reportPool/queryReportPool',
	saveReportInfo: baseUrl + 'app/reportPool/savePhoneReportPool',
	deleteReportInfo: baseUrl + 'app/reportPool/deleteReportPool',
	queryGovReportUser: baseUrl + 'app/govReportUser/queryGovReportUser',
	
	//活动报名
	queryGovActivityType: baseUrl + 'app/govActivityType/queryGovActivityType',
	queryGovActivity: baseUrl + 'app/govActivity/queryGovActivity',
	queryGovActivityPerson: baseUrl + 'app/govActivityPerson/queryGovActivityPerson',
	deleteGovActivityPerson: baseUrl + 'app/govActivityPerson/deleteGovActivityPerson',
	saveGovActivityPerson: baseUrl + 'app/govActivityPerson/saveGovActivityPerson',
	
	//办事指南、预约
	queryGovWorkGuide: baseUrl + 'app/govWorkGuide/queryGovWorkGuide',
	queryGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/queryGovGuideSubscribe',
	saveGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/saveGovGuideSubscribe',
	deleteGovGuideSubscribe: baseUrl + 'app/govGuideSubscribe/deleteGovGuideSubscribe',
	
	/**企业信息**/
	queryGovCompany: baseUrl + 'app/govCompany/queryGovCompany',
	saveGovCompany: baseUrl + 'app/govCompany/saveGovCompany',
	
	/**查询村、小区**/
	queryGovCommunity: baseUrl + 'app/govCommunity/queryGovCommunity',
}
