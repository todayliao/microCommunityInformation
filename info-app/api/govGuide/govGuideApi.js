import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';

/**
 * 查询指南
 * @param {Object} _data 
 */
export function queryGovWorkGuide(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovWorkGuide,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 查询指南预约
 * @param {Object} _data 
 */
export function queryGovGuideSubscribe(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovGuideSubscribe,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存指南预约
 */
export function saveGovGuideSubscribe(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovGuideSubscribe,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}


/**
 * add by wuxw
 * @param {Object} _data 删除指南预约
 */
export function deleteGovGuideSubscribe(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.deleteGovGuideSubscribe,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
