import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';
/**
 * 报事类型查询
 * @param {Object} _data 
 */
export function queryReportSetting(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryReportSetting,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function saveReportInfo(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveReportInfo,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function deleteReportInfo(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.deleteReportInfo,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 报事查询
 * @param {Object} _data 
 */
export function queryGovReportUsers(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovReportUser,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * 报事查询
 * @param {Object} _data 
 */
export function queryReports(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryReports,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}