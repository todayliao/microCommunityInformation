import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';
import {
	dateTimeStringToDateString
} from '../../lib/java110/utils/DateUtil.js';
/**
 * 查询活动类型
 */
export function getActivitiTitle(dataObj) {

	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.queryActivitiesType,
				method: "GET",
				data: dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _activites = res.data.data;
						resolve(_activites);
						return;
					}
					reject();
				},
				fail: function(e) {
					reject();
				}
			});
		})
}

/**
 * 查询活动
 */
export function loadActivites(dataObj) {
	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.listActivitiess,
				method: "GET",
				data: dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _activites = res.data.data;
						let _acts = [];
						_activites.forEach(function(_item) {
							_item.src = url.filePath + "?fileId=" + _item.headerImg +
								"&time=" + new Date();
							_item.startTime = dateTimeStringToDateString(_item.startTime);
							_acts.push(_item);
						});
						resolve(_acts);
						return;
					}
					reject();
				},
				fail: function(e) {
					reject();
				}
			});
		})
}

export function getIndexCategoryList(_dataObj) {
	return new Promise(
		(resolve, reject) => {
			let _data = [{
				name: "报事",
				src: "/static/images/baoshi.png",
				href: "/pages/reportManage/reportManage"
			}, {
				name: "活动",
				src: "/static/images/huodong.png",
				href: "/pages/activityManage/activityManage"
			}, {
				name: "办事指南",
				src: "/static/images/banshizn.png",
				href: "/pages/guideSubManage/guideSubManage"
			}, {
				name: "党建",
				src: "/static/images/dangjian.png",
				href: "/pages/repair/repair"
			},{
				name: "企业信息上报",
				src: "/static/images/qiyesb.png",
				href: "/pages/companyManage/companyManage"
			},{
				name: "投票表决",
				src: "/static/images/toupiao.png",
				href: "/pages/notice/index"
			}
			];
			resolve(_data);
		})
		
}

export function queryProductLabel(_dataObj) {
	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.queryProductLabel,
				method: "GET",
				data: _dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _data = res.data
						resolve(_data)
						return
					}
					reject()
				},
				fail: function(e) {
					reject()
				}
			})
		})
}

export function queryStoreMsg(_dataObj) {
	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.queryStoreMsg,
				method: "GET",
				data: _dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _data = res.data
						resolve(_data)
						return
					}
					reject()
				},
				fail: function(e) {
					reject()
				}
			})
		})
}

export function queryStoreAds(_dataObj) {
	return new Promise(
		(resolve, reject) => {
			requestNoAuth({
				url: url.queryStoreAds,
				method: "GET",
				data: _dataObj,
				//动态数据
				success: function(res) {
					if (res.statusCode == 200) {
						let _data = res.data
						resolve(_data)
						return
					}
					reject()
				},
				fail: function(e) {
					reject()
				}
			})
		})
}
