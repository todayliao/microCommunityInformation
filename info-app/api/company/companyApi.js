import {
	requestNoAuth,
	request
} from '../../lib/java110/java110Request.js';
import url from '../../constant/url.js';


/**
 * 查询小区
 * @param {Object} _data 
 */
export function queryGovCommunity(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovCommunity,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}


/**
 * 报事类型查询
 * @param {Object} _data 
 */
export function queryGovCompany(_data){
	return new Promise((resolve, reject) => {
		let moreRooms = [];
		requestNoAuth({
			url: url.queryGovCompany,
			method: "GET",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}

/**
 * add by wuxw
 * @param {Object} _data 保存 用户地址
 */
export function saveGovCompany(_data){
	return new Promise((resolve, reject) => {
		requestNoAuth({
			url: url.saveGovCompany,
			method: "POST",
			data: _data, //动态数据
			success: function(res) {
				let _data = res.data;
				if (_data.code == 0) {
					resolve(_data);
					return ;
				}
				reject(_data.msg);
			},
			fail: function(e) {
				reject(e);
			}
		});
	})
}
