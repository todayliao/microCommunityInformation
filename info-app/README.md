> 说明：商城目前主要打包成h5，通过webview嵌入到业主端，开发的时候使用hbuilder运行到浏览器即可

由于小区id和用户code是通过业主端传参的，开发的时候需要自行拼接 `本地开发域名`+`hcCommunityId=2021052879970020&hcCode=123456789`

如：商城首页 http://localhost:8000/#/?hcCommunityId=2021052879970020&hcCode=123456789

家政服务首页 http://localhost:8000/#/pages/homemaking/index?hcCommunityId=2021052879970020&hcCode=123456789



遇到问题：部分接口访问302

> // 非演示模式  http://localhost:8000/#/pages/homemaking/index?hcCommunityId=7020181217000001&hcCode=123456789 这个地址上的hcCode 就需要物业系统生成 只能访问一次 临时票据



整合uview，拥有丰富的组件，参考https://www.uviewui.com/

配置easycom自动导入组件，页面中无需手动导入配置的组件

推荐同业务的页面（如地址列表、地址编辑都放在address中）放在同一个目录下，方便后续小程序场景分包
