/**
 * HC社区政务配置文件
 * 
 * 本项目只有这里修改相应配置信息，如果不是二次开发 请不要修改其他文件内容
 * 
 * @website http://www.homecommunity.cn/
 * @author 吴学文
 * @QQ 928255095
 */

// 服务器域名 公众号时，配置为 / 就可以
const baseUrl = '/';

const commonBaseUrl = 'http://gov.homecommunity.cn/';

//默认的区域ID和名称，用户还没有登录时显示的区域信息

const DEFAULT_AREA_ID = "602021081929270001"; //HC测试区域id  用于没有登录时展示相关信息
const DEFAULT_AREA_NAME = "测试区域";

// APP 或者 公众号  appId
const wAppId = ""; //微信AppId

//app支付时这里需要填写支付秘钥
const appPayKey = "";

const logLevel = "DEBUG"; // 日志级别

/**
 * 应用信息，主要是用来和后端服务交互时的时候用
 */

// HC平台分配 应用ID
const appId = "992021090275720011";
const appSecurity = "";

export default {
	baseUrl: baseUrl,
	commonBaseUrl: commonBaseUrl,
	wAppId: wAppId,
	logLevel: logLevel,
	appPayKey: appPayKey,
	appId: appId,
	appSecurity: appSecurity,
	DEFAULT_AREA_ID:DEFAULT_AREA_ID,
	DEFAULT_AREA_NAME:DEFAULT_AREA_NAME
}
