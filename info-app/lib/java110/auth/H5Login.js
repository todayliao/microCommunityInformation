

import url from '../../../constant/url.js'
import {requestNoAuth} from '../java110Request.js'
import {isNull} from '../utils/StringUtil.js'
// import {getWAppId} from '../api/init/initApi.js'
import mapping from '../java110Mapping.js'


/**
 * 
 * H5 刷新鉴权登录
 * @param {Object} errorUrl 登录失败时的跳转页面
 * @param {Object} _login  
 * 
 * add by 吴学文 QQ 928255095
 */
export function wechatRefreshToken(errorUrl, _login) {
		let _errorUrl = errorUrl;
		//是否来自于HC的嵌套
		let _mallFrom = uni.getStorageSync(mapping.MALL_FROM);
		
		if(_mallFrom){
			let _param = {
				action:'refreshToken',
				url:errorUrl
			}
			window.parent.postMessage(_param,'*');
			return;
		}
		
		uni.navigateTo({
			url:'/pages/public/login'
		})
		
		// if(isNull(_login)){
		// 	_login = 0; // 不是登录页面鉴权
		// }
		// requestNoAuth({
		// 	url: url.wechatRefrashToken,
		// 	method: 'get',
		// 	data: {
		// 		redirectUrl: window.location.href, // 当前页地址
		// 		errorUrl: _errorUrl,
		// 		loginFlag: _login
		// 	},
		// 	success: function(res) {
		// 		let _param = res.data;
		// 		if (_param.code == 0) {
		// 			window.location.href = _param.data.openUrl;
		// 			return;
		// 		}
		// 	},
		// 	fail: function(error) {
		// 		// 调用服务端登录接口失败
		// 		if (error.statusCode == 401) {
		// 			return;
		// 		}
		// 	}
		// });
	}