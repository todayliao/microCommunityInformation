# HC社区政务对接协议

## 说明

首先依照政务所加的区域下的小区信息主键ID，将物业系统的小区管理里面的社区编码改为政务小区主键，完成关联。按照政务接口规范同步数据到政务系统。目前将小区密钥与楼栋类型，在物业系统映射管理中进行配置，后期需物业系统增加相关属性进行添加
## 1、楼栋上传

说明：物业系统添加楼栋时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_FLOOR | 添加楼栋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | floorNum | String | 是 | 1 | 楼栋编号 |
| body | floorName | String | 是 | 花园楼 | 楼栋名称 |
| body | floorType | String | 是 | 66525455124451 | 建筑物类型(住宅) |
| body | floorArea | String | 是 | 300 | 楼栋面积 |
| body | layerCount | String | 是 | 33 | 楼层数 |
| body | unitCount | String | 是 | 2 | 单元数 |
| body | floorUse | String | 是 | 小区住宅 | 建筑物用途 |
| body | personName | String | 是 | 凌乱 | 责任人 |
| body | personLink | String | 是 | 15236985632 | 联系电话 |


返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_FLOOR | 添加楼栋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |

| - | body | Object | 是 | - | - |
| body | extFloorId | String | 是 | 96655232651212355 | 外部楼栋ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_FLOOR",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"floorNum": "1",
		"floorName": "花园楼",
		"floorType": "66525455124451",
		"floorArea": "300",
		"layerCount": "33",
		"unitCount": "2",
		"floorUse": "小区住宅",
		"personName": "凌乱",
		"personLink": "15236985632"
		
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_FLOOR",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功",

	},
	"body": {
		"extFloorId": "96655232651212355"

	}
}
```
## 2、修改楼栋上传

说明：物业系统修改楼栋时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_FLOOR | 添加楼栋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | extFloorId | String | 是 | 1 | 外部楼栋ID |
| body | floorNum | String | 是 | 1 | 楼栋编号 |
| body | floorName | String | 是 | 花园楼 | 楼栋名称 |
| body | floorType | String | 是 | 66525455124451 | 建筑物类型(住宅) |
| body | floorArea | String | 是 | 300 | 楼栋面积 |
| body | layerCount | String | 是 | 33 | 楼层数 |
| body | unitCount | String | 是 | 2 | 单元数 |
| body | floorUse | String | 是 | 小区住宅 | 建筑物用途 |
| body | personName | String | 是 | 凌乱 | 责任人 |
| body | personLink | String | 是 | 15236985632 | 联系电话 |


返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_FLOOR | 添加楼栋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extFloorId | String | 是 | 96655232651212355 | 外部楼栋ID |


请求样例
```json
{
	"header": {
		"serviceCode": "EDIT_FLOOR",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
	  "extFloorId": "768463413135",
		"floorNum": "1",
		"floorName": "花园楼",
		"floorType": "66525455124451",
		"floorArea": "300",
		"layerCount": "33",
		"unitCount": "2",
		"floorUse": "小区住宅",
		"personName": "凌乱",
		"personLink": "15236985632"
		
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_FLOOR",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extFloorId": "96655232651212355"
	}
}
```
## 3、房屋上传

说明：物业系统添加房屋时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_ROOM | 添加房屋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | roomNum | String | 是 | 1 | 房屋编号 |
| body | builtUpArea | String | 是 | 200 | 建筑面积 |
| body | layer | String | 是 | 24 | 房屋层数 |
| body | roomArea | String | 是 | 120 | 室内面积 |
| body | layerCount | String | 否 | 33 | 楼层数 |
| body | unitCount | String | 否 | 2 | 单元数 |
| body | roomRent | String | 否 | 1200 | 租金 |
| body | userId | String | 否 | 655412154 | 房主ID| 
| body | extFloorId | String | 是 | 65456414515 | 所属楼栋 |
| body | state | String | 是 | 出售 | 房屋状态 出售/闲置/待验房 | 
| body | roomSubType | String | 是 | 住宅 | 房屋类型 住宅/办公/宿舍 |
| body | roomRight | String | 否 | 1001 | 房屋产权 1001 大产权 2002 小产权 |
| body | roomAddress | String | 否 | **** | 房屋地址 |
| body | isConservati | String | 否 | Y | 是否常住 Y 是 N不是 |
| body | isSettle | String | 否 | N | 是否落户 Y 是 N不是 |
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_ROOM | 添加房屋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extRoomId | String | 是 | 96655232651212355 | 外部房屋ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_ROOM",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"roomNum": "房屋编号",
		"builtUpArea": "建筑面积",
		"layer": "层数",
		"roomArea": "室内面积",
		"roomRent": "租金",
		"userId": "户主ID",
		"extFloorId": "所属楼栋",
		"layerCount": "楼层数",
		"unitCount": "单元数",
		"state": "房屋状态",
		"roomSubType": "房屋类型",
		"roomRight":"1001",
		"roomAddress":"****",
		"isConservati":"Y",
		"isSettle":"N"
		
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_ROOM",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extRoomId": "96655232651212355"
	}
}
```

## 4、修改房屋上传

说明：物业系统修改房屋时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_ROOM | 修改房屋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | roomNum | String | 是 | 1 | 房屋编号 |
| body | extRoomId | String | 是 | 1 | 外部房屋编码 |
| body | builtUpArea | String | 是 | 200 | 建筑面积 |
| body | layer | String | 是 | 24 | 房屋层数 |
| body | roomArea | String | 是 | 120 | 室内面积 |
| body | layerCount | String | 否 | 33 | 楼层数 |
| body | unitCount | String | 否 | 2 | 单元数 |
| body | roomRent | String | 否 | 1200 | 租金 |
| body | userId | String | 否 | 655412154 | 房主ID| 
| body | extFloorId | String | 是 | 65456414515 | 所属楼栋 |
| body | state | String | 是 | 出售/闲置/待验房 | 房屋状态| 
| body | roomSubType | String | 是 | 住宅/办公/宿舍 | 房屋类型 |
| body | roomRight | String | 否 | 大产权 | 房屋产权 1001 大产权 2002 小产权 |
| body | roomAddress | String | 否 | **** | 房屋地址 |
| body | isConservati | String | 否 | Y | 是否常住 Y 是 N不是 |
| body | isSettle | String | 否 | N | 是否落户 Y 是 N不是 |

返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_ROOM | 修改房屋 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extRoomId | String | 是 | 96655232651212355 | 外部房屋ID |

请求样例
```json
{
	"header": {
		"serviceCode": "EDIT_ROOM",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"roomNum": "房屋编号",
		"extRoomId":"34456456456",
		"builtUpArea": "建筑面积",
		"layer": "层数",
		"roomArea": "室内面积",
		"roomRent": "租金",
		"userId": "户主ID",
		"extFloorId": "所属楼栋",
		"layerCount": "楼层数",
		"unitCount": "单元数",
		"state": "房屋状态",
		"roomSubType": "房屋类型",
		"roomRight":"1001",
		"roomAddress":"****",
		"isConservati":"Y",
		"isSettle":"N"
		
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_ROOM",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extRoomId": "96655232651212355"
	}
}
```


## 5、添加位置上报

说明：物业系统添加小区位置时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_LOCATION | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | name | String | 是 | 1 | 位置名称 |

返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_LOCATION | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extLocationId | String | 是 | 96655232651212355 | 外部房屋ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_LOCATION",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"name": "小区大门"	
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_LOCATION",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extLocationId": "96655232651212355"
	}
}
```

## 6、修改位置上报

说明：物业系统修改小区位置时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_LOCATION | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | name | String | 是 | 1 | 位置名称 |
| body | extLocationId | String | 是 | 1 | 外部位置ID |

返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_LOCATION | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extLocationId | String | 是 | 96655232651212355 | 外部位置ID |



请求样例
```json
{
	"header": {
		"serviceCode": "EDIT_LOCATION",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"name": "小区大门"	
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_LOCATION",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extLocationId": "96655232651212355"
	}
}
```


## 7、人员开门记录上报

说明：物业系统添加人员开门记录时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_PERSON_INOUT_RECORD | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | extLocationId | String | 是 | 1 | 外部位置ID |
| body | name | String | 是 | 64 | 用户名称 |
| body | openTypeCd | String | 是 | 12 | 开门方式 1000 人脸开门 2000 钥匙开门 |
| body | tel | String | 是 | 11 | 手机号 |
| body | idCard | String | 是 | 20 | 身份证号 |
| body | recordTypeCd | String | 是 | 12 | 记录类型，8888 业主 6666 访客 |
| body | faceUrl | String | 是 | 512 | 人脸url 政务服务可访问 |
| body | state | String | 是 | 12 | 状态 C开门成功 F 开门失败 |



返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_PERSON_INOUT_RECORD | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extRecordId | String | 是 | 96655232651212355 | 外部记录ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_PERSON_INOUT_RECORD",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"name": "张三",
		"extLocationId":"123",
		"openTypeCd":"1000",
		"tel":"18909711234",
		"idCard":"632126199109162055",
		"recordTypeCd":"8888",
		"faceUrl":"http://xxx",
		"state":"C",
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_PERSON_INOUT_RECORD",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extRecordId": "96655232651212355"
	}
}
```
## 8、添加业主上传信息

说明：物业系统添加业主时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_OWNER | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | idType | String | 是 | 12 | 证件类型 1 身份证 |
| body | idCard | String | 是 | 64 | 证件号码 |
| body | personName | String | 是 | 64 | 姓名 |
| body | personTel | String | 是 | 11 | 电话 |
| body | personSex | String | 是 | 11 | 性别 0 女 1 男 |
| body | prePersonName | String | 否 | 4 | 曾用名 |
| body | birthday | String | 是 | 12 | 生日 |
| body | nation | String | 是 | 12 | 民族  |
| body | nativePlace | String | 是 | 12 | 籍贯 |
| body | politicalOutlook | String | 是 | 12 | 政治面貌 |
| body | maritalStatus | String | 是 | 12 | 婚姻状况 Y 已婚,N 未婚 |
| body | religiousBelief | String | 否 | 12 | 宗教信仰 |
| body | ramark | String | 否 | 500 | 备注 |
| body | extRoomId | JSONArray | 否 | 12 | 政务房屋ID 业主本人是必填，成员时可以不填 |
| body | ownerType | String | 是 | 12 | 户别 1001 农村户口 2002 城市户口 |
| body | ownerAddress | String | 否 | 512 | 户籍地址 |
| body | ownerTypeCd | String | 是 | 12 | 业主成员标识 1001 业主本人 1002 家庭成员 |
| body | extOwnerId | String | 是 | 12 | 外部人员ID,类型未业主本人时可以为空，家庭成员时必填 |
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_OWNER | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extMemberId | String | 是 | 96655232651212355 | 外部记录ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_OWNER",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"idType": "1",
		"idCard":"632126199109162055",
		"personName":"李四",
		"personTel":"18909711234",
		"personSex":"1",
		"prePersonName":"李四",
		"birthday":"1988-02-03",
		"nation":"汉",
		"nativePlace": "中国",
		"politicalOutlook":"党员",
		"maritalStatus":"Y",
		"religiousBelief":"无",
		"ramark":"系统自动推送",
		"extRoomId":["474684654687","345345345345"],
		"ownerType":"2002",
		"ownerAddress":"无",
		"ownerTypeCd":"1001",
		"extOwnerId":"6574564564567567"
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_OWNER",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extMemberId": "96655232651212355"
	}
}
```
## 9、修改业主上传信息

说明：物业系统修改业主时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_OWNER | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | idType | String | 是 | 12 | 证件类型 1 身份证 |
| body | idCard | String | 是 | 64 | 证件号码 |
| body | personName | String | 是 | 64 | 姓名 |
| body | personTel | String | 是 | 11 | 电话 |
| body | personSex | String | 是 | 11 | 性别 0 女 1 男 |
| body | prePersonName | String | 否 | 4 | 曾用名 |
| body | birthday | String | 是 | 12 | 生日 |
| body | nation | String | 是 | 12 | 民族  |
| body | nativePlace | String | 是 | 12 | 籍贯 |
| body | politicalOutlook | String | 是 | 12 | 政治面貌 |
| body | maritalStatus | String | 是 | 12 | 婚姻状况 Y 已婚,N 未婚 |
| body | religiousBelief | String | 否 | 12 | 宗教信仰 |
| body | ramark | String | 否 | 500 | 备注 |
| body | extRoomId | JSONArray | 是 | 12 | 政务房屋ID |
| body | ownerType | String | 是 | 12 | 户别 1001 农村户口 2002 城市户口 |
| body | ownerAddress | String | 否 | 512 | 户籍地址 |
| body | ownerTypeCd | String | 是 | 12 | 业主成员标识 1001 业主本人 1002 家庭成员 |
| body | extOwnerId | String | 是 | 12 | 外部业主人员ID,类型未业主本人时可以为空，家庭成员时必填 |
| body | extMemberId | String | 是 | 12 | 外部人员ID |
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_OWNER | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extMemberId | String | 是 | 96655232651212355 | 外部记录ID |


请求样例
```json
{
	"header": {
		"serviceCode": "EDIT_OWNER",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"idType": "1",
		"idCard":"632126199109162055",
		"personName":"李四",
		"personTel":"18909711234",
		"personSex":"1",
		"prePersonName":"李四",
		"birthday":"1988-02-03",
		"nation":"汉",
		"nativePlace": "中国",
		"politicalOutlook":"党员",
		"maritalStatus":"Y",
		"religiousBelief":"无",
		"ramark":"系统自动推送",
		"extRoomId":["474684654687","345345345345"],
		"ownerType":"2002",
		"ownerAddress":"无",
		"ownerTypeCd":"1001",
		"extOwnerId":"5467567567567",
		"extMemberId":"5467567567567",
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_OWNER",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extMemberId": "96655232651212355"
	}
}
```

## 10、车辆进出记录上报

说明：物业系统添加人员开门记录时 实时将数据上报HC社区政务系统

请求方式： kafka topic

请求topic：hcGov

请求参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_CAR_INOUT | 车辆进出 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| - | body | Object | 是 | - | - |
| body | extLocationId | String | 是 | 1 | 外部位置ID |
| body | carNum | String | 是 | 64 | 车牌号 |
| body | personName | String | 是 | 64 | 车主 |
| body | personLink | String | 是 | 11 | 车主电话 |
| body | idCard | String | 是 | 20 | 身份证号 |
| body | inoutTime | String | 是 | 20 | 进出场时间 YYYY-MM-DD hh:mi:ss |
| body | carImgUrl | String | 是 | 512 | 车牌url 政务服务可访问 |
| body | state | String | 是 | 12 | 状态 C开门成功 F 开门失败 |



返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_CAR_INOUT | 车辆进出 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extInoutId | String | 是 | 96655232651212355 | 外部记录ID |


请求样例
```json
{
	"header": {
		"serviceCode": "ADD_CAR_INOUT",
		"extCommunityId": "123123123",
		"tranId": "678465161420210831444",
		"reqTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846"
	},
	"body": {
		"name": "张三",
		"extLocationId":"123",
		"carNum":"青AGK916",
		"personName":"吴学文",
		"personLink":"18909711445",
		"idCard":"632126199109162055",
		"inoutTime":"2021-09-07 00:01:01",
		"carImgUrl":"http://xxx",
		"state":"C",
	}
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_CAR_INOUT",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extInoutId": "96655232651212355"
	}
}
```
 ## 11、添加员工上传商户信息                                                                               
                                                                                                      
 说明：物业系统添加员工时 实时将商户、员工数据上报HC社区政务系统，并讲组织信息及公司信息上传                    
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
 请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | ADD_COMPANY | 添加位置 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | - | body | Object | 是 | - | - | 
 | body | companyInfo | Object| 是 | - | - |                                                                   
 | companyInfo | companyName | String | 是 | **物业 | 企业名称 |                                             
 | companyInfo | companyType | String | 是 | 2002 | 组织类型物业默认为企业2002|                              
 | companyInfo | idCard | String | 是 | 64345345 | 证件号码 |                                                
 | companyInfo | artificialPerson | String | 是 | 11 | 法人 |                                                
 | companyInfo | companyAddress | String | 否 | **省城北区36号 | 企业地址 |                                  
 | companyInfo | registerTime | String | 是 | 2008-03-25 12:00 | 注册时间 |                                  
 | companyInfo | personName | String | 是 | 张三 | 负责人 |                                                  
 | companyInfo | personTel | String | 是 | 13478900987 | 负责人电话  |                                       
 | companyInfo | personIdCard | String | 是 | 2342343243 | 负责人身份证 |       
 | body | staffInfo | Object| 是 | - | - |                                        
 | staffInfo | idType | String | 否 | 12 | 证件类型 1 身份证 |                                             
 | staffInfo | IdCard | String | 否 | 64 | 证件号码 |                                                      
 | staffInfo | PersonName | String | 是 | 64 | 姓名 |                                                 
 | staffInfo | PersonTel | String | 是 | 11 | 电话 |                                                  
 | staffInfo | PersonSex | String | 是 | 11 | 性别 0 女 1 男 |                                        
 | staffInfo | prePersonName | String | 否 | 4 | 曾用名 |                                                  
 | staffInfo | birthday | String | 否 | 12 | 生日 |                                                        
 | staffInfo | nation | String | 否 | 12 | 民族  |                                                         
 | staffInfo | nativePlace | String | 否 | 12 | 籍贯 |                                 
 | staffInfo | politicalOutlook | String | 否 | 12 | 政治面貌 |                        
 | staffInfo | maritalStatus | String | 否 | 12 | 婚姻状况 Y 已婚,N 未婚 |             
 | staffInfo | religiousBelief | String | 否 | 12 | 宗教信仰 |                                                  
 | staffInfo | ramark | String | 否 | 500 | 备注 |                                                              
 | staffInfo | govOrgName | String | 是 | 12 | 员工所属组织名称 |                                                   
 | staffInfo | relCd | String | 是 | 12 | 户别 员工组织编码 |                                

返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_COMPANY | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extCompanyId | String | 是 | 96655232651212355 | 企业外部记录ID |
| body | extPersonId | String | 是 | 96655232651212355 | 从业人员外部记录ID |


请求样例
```json
{
    "body": {
        "companyInfo": {
            "artificialPerson": "吴学文",
            "companyAddress": "青海省西宁市城西区紫恒公寓",
            "companyName": "东莞市东城学文工作室",
            "companyType": "2002",
            "idCard": "6321261852326588888",
            "personIdCard": "6321261852326588888",
            "personName": "吴学文",
            "personTel": "18909711443",
            "registerTime": "2021-08-09"
        },
        "staffInfo": {
            "PersonName": "诗诗3",
            "PersonSex": "0",
            "PersonTel": "15245697854",
            "govOrgName": "HC分部",
            "prePersonName": "诗诗3",
            "relCd": "4000"
        }
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "ADD_COMPANY",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_OWNER",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extCompanyId": "96655232651212355"，
		"extPersonId": "96655232651212355"
	}
}
```
 ## 12、添加员工上传信息                                                                               
                                                                                                      
 说明：物业系统添加员工时 实时将员工数据上报HC社区政务系统，并讲组织信息及公司信息上传                    
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
 请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | ADD_PERSON | 添加位置 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | body | staffInfo | Object| 是 | - | - |                                        
 | staffInfo | idType | String | 否 | 12 | 证件类型 1 身份证 |                                             
 | staffInfo | IdCard | String | 否 | 64 | 证件号码 |                                                      
 | staffInfo | PersonName | String | 是 | 64 | 姓名 |                                                 
 | staffInfo | PersonTel | String | 是 | 11 | 电话 |                                                  
 | staffInfo | PersonSex | String | 是 | 11 | 性别 0 女 1 男 |                                        
 | staffInfo | prePersonName | String | 否 | 4 | 曾用名 |                                                  
 | staffInfo | birthday | String | 否 | 12 | 生日 |                                                        
 | staffInfo | nation | String | 否 | 12 | 民族  |                                                         
 | staffInfo | nativePlace | String | 否 | 12 | 籍贯 |                                 
 | staffInfo | politicalOutlook | String | 否 | 12 | 政治面貌 |                        
 | staffInfo | maritalStatus | String | 否 | 12 | 婚姻状况 Y 已婚,N 未婚 |             
 | staffInfo | religiousBelief | String | 否 | 12 | 宗教信仰 |                                                  
 | staffInfo | ramark | String | 否 | 500 | 备注 |                                                              
 | staffInfo | govOrgName | String | 是 | 12 | 员工所属组织名称 |                                                   
 | staffInfo | relCd | String | 是 | 12 | 户别 员工组织编码 |            
 | staffInfo | extCompanyId | String | 是 | 96655232651212355 | 企业外部记录ID |                      
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_PERSON | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extPersonId | String | 是 | 96655232651212355 | 从业人员外部记录ID |


请求样例
```json
{
    "body": {
        "staffInfo": {
            "PersonName": "诗诗3",
            "PersonSex": "0",
            "PersonTel": "15245697854",
            "govOrgName": "HC分部",
            "prePersonName": "诗诗3",
            "relCd": "4000"
        }
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "ADD_COMPANY",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_OWNER",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extPersonId": "96655232651212355"
	}
}
```

## 13、添加停车场上传信息                                                                               
                                                                                                      
 说明：物业系统添加停车场时 实时将停车场数据上报HC社区政务系统                  
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
 请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | ADD_PARKING_AREA | 添加停车场 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | - | body | Object| 是 | - | - |                                        
 | body | num | String | 是 | 12 | 停车场编号 |                                             
 | body | parkingCount | int | 是 | - | 车位数量 |                                                      
 | body | typeCd | String | 是 | 12 | 停车场类型1001 地上停车场 2001 地下停车场 | 
 
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_PARKING_AREA | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extPaId | String | 是 | 96655232651212355 | 外部停车场ID |


请求样例
```json
{
    "body": {
            "num": "1",
            "parkingCount": "0",
            "typeCd": "1001",
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "ADD_PARKING_AREA",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_PARKING_AREA",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extPaId": "96655232651212355"
	}
}
```

## 14、修改停车场上传信息                                                                               
                                                                                                      
 说明：物业系统修改停车场时 实时将停车场数据上报HC社区政务系统                  
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
 请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | EDIT_PARKING_AREA | 添加停车场 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | - | body | Object| 是 | - | - |                                        
 | body | num | String | 是 | 12 | 停车场编号 |                                             
 | body | parkingCount | int | 是 | - | 车位数量 |                                                      
 | body | typeCd | String | 是 | 12 | 停车场类型1001 地上停车场 2001 地下停车场 | 
 | body | extPaId | String | 是 | 12 | 外部停车场ID | 
 
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_PARKING_AREA | 添加位置 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extPaId | String | 是 | 96655232651212355 | 外部停车场ID |


请求样例
```json
{
    "body": {
            "num": "1",
            "parkingCount": "0",
            "typeCd": "1001",
	    "extPaId":"96655232651212355",
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "EDIT_PARKING_AREA",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "EDIT_PARKING_AREA",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extPaId": "96655232651212355"
	}
}
```


## 15、添加车辆上传信息                                                                               
                                                                                                      
 说明：物业系统添加车辆时 实时将车辆数据上报HC社区政务系统                  
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
 请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | ADD_CAR | 添加车辆 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | - | body | Object| 是 | - | - |                                        
 | body | carNum | String | 是 | 12 | 车牌号 |                                             
 | body | carBrand | String | 是 | 12 | 车辆品牌 |  
 | body | carType | String | 是 | 12 | 车辆类型 9901 家用小汽车，9902 客车，9903 货车 |  
 | body | carColor | String | 是 | 12 | 车辆颜色 |  
 | body | startTime | String | 是 | 12 | 起租时间 |                                             
 | body | endTime | String | 是 | 12 | 截租时间 |  
 | body | personName | String | 是 | 12 | 车主 |  
 | body | personTel | String | 是 | 12 | 车主电话 |  
 | body | extPaId | String | 是 | 12 | 外部停车场ID |  
 
 
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | ADD_CAR | 添加车辆 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extCarId | String | 是 | 96655232651212355 | 外部车辆ID |


请求样例
```json
{
    "body": {
            "carNum": "青A88888",
            "carBrand": "黄色",
            "carType": "9901",
	    "carColor": "黄色",
	    "startTime": "2021-09-01",
	    "endTime": "2021-10-01",
	    "personName": "HC作者",
	    "personTel": "18909711234",
	    "extPaId": "123456789"
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "ADD_CAR",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_CAR",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extCarId": "96655232651212355"
	}
}
```

## 16、修改停车场上传信息                                                                               
                                                                                                      
 说明：物业系统修改停车场时 实时将停车场数据上报HC社区政务系统                  
                                                                                                      
 请求方式： kafka topic                                                                               
                                                                                                      
 请求topic：hcGov                                                                                     
                                                                                                      
请求参数：                                                                                           
                                                                                                      
 |上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |                                                    
 | :----:| :----:| :----: | :----: | :----: | :----: |                                                
 | - | header | Object | 是 | - | - |                                                                 
 | header | serviceCode | String | 是 | EDIT_CAR | 添加车辆 |                                       
 | header | tranId | String | 是 | 12345677 | 交易流水 |                                              
 | header | reqTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |                        
 | header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |                                            
 | header | extCommunityId | String | 是 | 123123123 | 外部小区ID |                                   
 | - | body | Object| 是 | - | - |                                        
 | body | carNum | String | 是 | 12 | 车牌号 |                                             
 | body | carBrand | String | 是 | 12 | 车辆品牌 |  
 | body | carType | String | 是 | 12 | 车辆类型 9901 家用小汽车，9902 客车，9903 货车 |  
 | body | carColor | String | 是 | 12 | 车辆颜色 |  
 | body | startTime | String | 是 | 12 | 起租时间 |                                             
 | body | endTime | String | 是 | 12 | 截租时间 |  
 | body | personName | String | 是 | 12 | 车主 |  
 | body | personTel | String | 是 | 12 | 车主电话 |  
 | body | extPaId | String | 是 | 12 | 外部停车场ID |  
 | body | extCarId | String | 是 | 12 | 外部车辆ID |
 
 
返回参数：

|上一级| 名称 | 类型 | 是否必填 | 示例值 | 描述 |
| :----:| :----:| :----: | :----: | :----: | :----: |
| - | header | Object | 是 | - | - |
| header | serviceCode | String | 是 | EDIT_CAR | 添加车辆 |
| header | tranId | String | 是 | 12345677 | 交易流水 |
| header | extCommunityId | String | 是 | 123123123 | 外部小区ID |
| header | resTime | String | 是 | 20210831000000 | 请求时间 YYYYMMDDHHmmSS |
| header | sign | String | 是 | dffdsdfsdfsdfsdf | 签名 |
| header | code | int | 是 | 0 | 状态 0 成功 其他失败 |
| header | msg | String | 是 | 0 | 状态说明 |
| - | body | Object | 是 | - | - |
| body | extCarId | String | 是 | 96655232651212355 | 外部车辆ID |


请求样例
```json
{
    "body": {
            "carNum": "青A88888",
            "carBrand": "黄色",
            "carType": "9901",
	    "carColor": "黄色",
	    "startTime": "2021-09-01",
	    "endTime": "2021-10-01",
	    "personName": "HC作者",
	    "personTel": "18909711234",
	    "extPaId": "123456789",
	    "extCarId": "96655232651212355"
    },
    "header": {
        "extCommunityId": "212220",
        "reqTime": "20210911132052",
        "serviceCode": "ADD_CAR",
        "sign": "51beeefa237271969a4d09fa71202516",
        "tranId": "b60b25b4-4110-42d6-86eb-c61569112462"
    }
}
```

返回样例
```json
{
	"header": {
		"serviceCode": "ADD_CAR",
		"tranId": "678465161420210831444",
		"extCommunityId": "123123123",
		"resTime": "2020-12-31 18:06:40",
		"sign": "2020122471920846",
		"code": 0,
		"msg": "成功"
	},
	"body": {
		"extCarId": "96655232651212355"
	}
}
```